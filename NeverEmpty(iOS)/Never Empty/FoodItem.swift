//
//  FoodItem.swift
//  Never Empty
//
//  Created by cristobal rios on 10/12/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class FoodItem{
    var name: String
    var image: UIImage
    var _keywords: [String]
    var expirationDate: String
    var addedDate: String
    var ingredients: String
    var serving_size: String
    var servingQuantity: Int
    var sodium: Decimal
    var carbohydrates: Decimal
    var proteins: Decimal
    var fat: Decimal
    var calories: Int
    var barcode: String
    var _UID: String
    var AddedDate: Date
    var ExpDate: Date
    
    init(name: String, image: UIImage, _keywords: [String], expirationDate: String, addedDate: String, ingredients: String, serving_size: String, servingQuantity: Int, sodium: Decimal, carbohydrates: Decimal, proteins: Decimal, fat: Decimal, calories: Int, barcode: String, _UID: String, AddedDate: Date, ExpDate: Date) {
        self.name = name
        self.image = image
        self._keywords = _keywords
        self.expirationDate = expirationDate
        self.addedDate = addedDate
        self.ingredients = ingredients
        self.serving_size = serving_size
        self.servingQuantity = servingQuantity
        self.sodium = sodium
        self.carbohydrates = carbohydrates
        self.proteins = proteins
        self.fat = fat
        self.calories = calories
        self.barcode = barcode
        self._UID = _UID
        self.AddedDate = AddedDate
        self.ExpDate = ExpDate
    }
    
    
   
   
    
    
  
}
