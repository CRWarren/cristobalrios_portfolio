//
//  FirebaseHelper.swift
//  Never Empty
//
//  Created by cristobal rios on 10/17/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import Firebase
import UIKit

public class FirebaseHelper{
    var db = Firestore.firestore()
    let storageRef = Storage.storage().reference().child("food_images")
    let PS = PushNotificationSender()
    
    func uploadFood(food: FoodItem){
        
        db.collection("food_items").document("\(food.barcode)").setData(
            [  "name":food.name,
               "_keywords":food._keywords,
               "ingredients":food.ingredients,
               "serving_size":food.serving_size,
               "serving_quantity":food.servingQuantity,
               "sodium":food.sodium,
               "carbohydrates":food.carbohydrates,
               "proteins":food.proteins,
               "fat":food.fat,
               "calories":food.calories,
               "barcode":food.barcode,
            ], completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {
                }
        })
    }
    
    func uploadTempUser(user: User){
        db.collection("users").document("\(Auth.auth().currentUser!.uid)").collection("internal").document(user.id).setData(
            [ "email": user.email,
              "full_name": user.fname,
              "photo": user.photo,
              "first": false,
              "id": user.id])
    }
    
    func uploadFoodPending(food: FoodItem, _errorReported: String){
        db.collection("food_flaged").document("\(food.barcode)").setData(
            [  "name":food.name,
               "_keywords":food._keywords,
               "ingredients":food.ingredients,
               "serving_size":food.serving_size,
               "serving_quantity":food.servingQuantity,
               "sodium":food.sodium,
               "carbohydrates":food.carbohydrates,
               "proteins":food.proteins,
               "fat":food.fat,
               "calories":food.calories,
               "barcode":food.barcode,
               "error": _errorReported
               ], completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {
                }
        })
    }
    func uploadImage(barcode: String, image: UIImage) {
        //let imageData = UIImageJPEGRepresentation(food.image, 1)
        DispatchQueue.main.async{
            let uploadImageReference = self.storageRef.child(barcode)
            // set upload path
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            let uploadTask = uploadImageReference.putData(image.jpegData(compressionQuality: 1)!, metadata: metaData) { (metadata, error) in
                print(metadata ?? "No Metadata")
                print(error ?? "No Error")
            }
            uploadTask.resume()
        }
    }
    
    func addToPantryList(food: FoodItem){
        db.collection("users").document("\(Auth.auth().currentUser!.uid)").collection("pantry").document("\(food.barcode)").setData(
            [  "name":food.name,
               "_keywords":food._keywords,
               "ingredients":food.ingredients,
               "serving_size":food.serving_size,
               "date_added":food.addedDate,
               "expiration_date":food.expirationDate,
               "serving_quantity":food.servingQuantity,
               "sodium":food.sodium,
               "carbohydrates":food.carbohydrates,
               "proteins":food.proteins,
               "fat":food.fat,
               "calories":food.calories,
               "barcode":food.barcode,
               "addedby": food._UID
               ], completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } 
        })
        
    }

    
    func addToCartList(food: FoodItem){
        db.collection("users").document("\(Auth.auth().currentUser!.uid)").collection("cart").document("\(food.barcode)").setData(
            [  "name":food.name,
               "_keywords":food._keywords,
               "ingredients":food.ingredients,
               "serving_size":food.serving_size,
               "date_added":food.addedDate,
               "expiration_date":food.expirationDate,
               "serving_quantity":food.servingQuantity,
               "sodium":food.sodium,
               "carbohydrates":food.carbohydrates,
               "proteins":food.proteins,
               "fat":food.fat,
               "calories":food.calories,
               "barcode":food.barcode,
               "addedby": food._UID
            ], completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                }
        })
        
    }
    
    
}
