//
//  TabController.swift
//  Never Empty
//
//  Created by cristobal rios on 10/9/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // make unselected icons white
        self.tabBar.unselectedItemTintColor = UIColor.white
        self.selectedIndex = 1
    }
}
