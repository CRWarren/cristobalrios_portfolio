//
//  Toast.swift
//  Never Empty
//
//  Created by cristobal rios on 10/26/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showToast(message : String) {
        
       
        //create label
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2-75, y: self.view.frame.size.height-130, width: 150, height: 30))
        
        //add background to label
        toastLabel.backgroundColor = UIColor.init(red: 0/255, green: 193/255, blue: 200/255, alpha: 1)
        
        //set text color
        toastLabel.textColor = UIColor.white
        
        //align text
        toastLabel.textAlignment = .center;
        
        //set text font
        toastLabel.font = UIFont(name: "System", size: 12)
        
        //set message
        toastLabel.text = message
        
        //give corners to alpha
        toastLabel.layer.cornerRadius = 15;
        
        toastLabel.clipsToBounds  =  true
        
        
        //add label to current view
        self.view.addSubview(toastLabel)
        
        //animate lable to disssapear and once is completed remove
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }
