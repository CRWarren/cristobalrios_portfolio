//
//  NewInternalUser.swift
//  Never Empty
//
//  Created by cristobal rios on 10/24/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import Firebase
import UIKit

class  NewInternalUser: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var PurpleBttn: UIButton!
    @IBOutlet weak var OrangeBttn: UIButton!
    @IBOutlet weak var GreenBttn: UIButton!
    @IBOutlet weak var blueBttn: UIButton!
    @IBOutlet weak var FullNameTxt: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var SaveRounded: UIButton!
    
    let defaults = UserDefaults.standard
    let myColor = UIColor.white
    let db = Firestore.firestore()
    var user: User?
    var image: Int?
    let images = ["Blue", "Green", "Orange", "Purple"]
    let fh = FirebaseHelper.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FullNameTxt.delegate = self
        SaveRounded.layer.cornerRadius = 15
        SaveRounded.layer.borderWidth = 1
        SaveRounded.layer.borderColor = myColor.cgColor
        PurpleBttn.layer.cornerRadius = 10
        PurpleBttn.imageView!.layer.cornerRadius = 10
        GreenBttn.layer.cornerRadius = 10
         GreenBttn.imageView!.layer.cornerRadius = 10
        OrangeBttn.layer.cornerRadius = 10
         OrangeBttn.imageView!.layer.cornerRadius = 10
        blueBttn.layer.cornerRadius = 10
         blueBttn.imageView!.layer.cornerRadius = 10
        profileImage.layer.cornerRadius = 10
        profileImage.layer.masksToBounds = true
        if user == nil {
            image = 0
            blueBttn.layer.borderWidth = 2
            blueBttn.layer.borderColor = myColor.cgColor
            profileImage.image = UIImage(named: images[0])
        }else {
            profileImage.image = UIImage(named: images[user!.photo])
            FullNameTxt.text = user!.fname
            blueBttn.layer.borderWidth = 0
            switch user!.photo {
            case 0:
                image = 0
                blueBttn.layer.borderColor = myColor.cgColor
                blueBttn.layer.borderWidth = 2
            case 1:
                image = 1
                GreenBttn.layer.borderColor = myColor.cgColor
                GreenBttn.layer.borderWidth = 2
            case 2:
                image = 2
                OrangeBttn.layer.borderColor = myColor.cgColor
                OrangeBttn.layer.borderWidth = 2
            case 3:
                image = 3
                PurpleBttn.layer.borderColor = myColor.cgColor
                PurpleBttn.layer.borderWidth = 2
            default:
                print("error on image")
            }
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteUser(_:)))
            
        }
        
    }
    
    @objc func deleteUser(_ sender: Any?) {
        
        let alert = UIAlertController(title: "Delete User?", message: "Are you sure you want to delete user?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (UIAlertAction) in
        self.fh.db.collection("users").document(Auth.auth().currentUser!.uid).collection("internal").document(self.user!.id).delete()
            
            if self.defaults.string(forKey: "id") != nil {
                if self.defaults.string(forKey: "id") == self.user!.id{
                    self.defaults.removeObject(forKey: "name")
                    self.defaults.removeObject(forKey: "id")
                    self.defaults.removeObject(forKey: "image")
                }
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(yes)
        alert.addAction(no)
        
        self.present(alert, animated: true)
        
    }
    @IBAction func imageProfileSelected(_ sender: UIButton) {
        PurpleBttn.layer.borderWidth = 0
        OrangeBttn.layer.borderWidth = 0
        blueBttn.layer.borderWidth = 0
        GreenBttn.layer.borderWidth = 0
        sender.layer.borderColor = myColor.cgColor
        sender.layer.borderWidth = 2
        switch sender.tag {
        case 0:
            image = 0
        case 1:
            image = 1
        case 2:
            image = 2
        case 3:
            image = 3
        default:
            print("error on image")
        }
        profileImage.image = UIImage(named: images[(image)!] )
        
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        //let characters
        if FullNameTxt.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            let alert = UIAlertController(title: "Invalid Username", message: "username must be longer than 1 letter", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true)
        }else{
            if user == nil {
                let uid = "\(UUID.init())"
                let temp_user = User(FullName: FullNameTxt.text!, Email: "", _photo: image!, _firstTime: false, _id: uid)
                fh.uploadTempUser(user: temp_user)
                navigationController?.popViewController(animated: true)
            }else {
                let temp_user = User(FullName: FullNameTxt.text!, Email: "", _photo: image!, _firstTime: false, _id: user!.id)
                fh.uploadTempUser(user: temp_user)
                navigationController?.popViewController(animated: true)
            }
            
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    
}
