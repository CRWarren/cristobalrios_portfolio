//
//  ViewController.swift
//  Never Empty
//
//  Created by cristobal rios on 10/7/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit
import Firebase

class LogInVC: UIViewController, UITextFieldDelegate {
   
    let defaults = UserDefaults.standard
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var EmailTxt: UITextField!
    @IBOutlet weak var SignInRounded: UIButton!
    @IBOutlet weak var LogoGraphic: UIImageView!
    @IBOutlet weak var TextFieldsContainer: UIView!
    @IBOutlet weak var ForgotPassword: UIButton!
    @IBOutlet weak var SignUp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        SignInRounded.layer.cornerRadius = 15
        let myColor = UIColor.white
        SignInRounded.layer.borderColor = myColor.cgColor
        SignInRounded.layer.borderWidth = 1
        EmailTxt.delegate = self
        PasswordTxt.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {

            if Auth.auth().currentUser != nil {
                if let _ = Auth.auth().currentUser {
                    performSegue(withIdentifier: "toApp", sender: nil)
                }
            }
        animateLogoIn()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         view.endEditing(true)
    }
    
    func downloadUsers() {
         let db = Firestore.firestore()
        let UID = Auth.auth().currentUser!.uid
        let docRef = db.collection("users").document(UID)
        docRef.collection("internal").getDocuments { (QuerySnapshot, Error) in
            if Error == nil {
                for _ in QuerySnapshot!.documents {
                    self.performSegue(withIdentifier: "toUsers", sender: nil)
                    return
                }
                self.performSegue(withIdentifier: "toApp", sender: nil)
            }
        }
    }
    
    
    func validate() -> Bool{
        if(EmailTxt.text!.isEmpty||PasswordTxt.text!.isEmpty){
            alert(Title: "Field(s) Empty", _Message: "Please fill all fields")
            return false
        }else if(!isValidEmail(email: EmailTxt.text!)){
             alert(Title: "Invalid Email", _Message: "Please enter a valid email")
            return false
        }
        return true
    }
    
    func alert(Title: String, _Message: String){
        let alert = UIAlertController(title: Title, message: _Message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    func alertEmailValidation(){
        let alert = UIAlertController(title: "Verify Account", message: "Before continuing, Please verify your email address", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    @IBAction func signInClicked(_ sender: UIButton) {
        if(validate()){
            Auth.auth().signIn(withEmail: EmailTxt.text!, password: PasswordTxt.text!) { (user, error) in
                if (error == nil) {
                    if Auth.auth().currentUser != nil{
                        self.downloadUsers()
                    }
                }else{
                    //error._Code
                     self.handleError(error!)
                }
            }
        }
    }
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    @IBAction func unwindToContainerVC(segue: UIStoryboardSegue) {
        do {
            defaults.removeObject(forKey: "name")
            defaults.removeObject(forKey: "id")
            defaults.removeObject(forKey: "image")
            
             // defaults.set(Auth.auth().currentUser!.uid, forKey: "id")
            try Auth.auth().signOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}

extension AuthErrorCode {
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "The email is already in use with another account"
        case .userNotFound:
            return "Account not found for the specified user. Please check and try again"
        case .userDisabled:
            return "Your account has been disabled. Please contact support."
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return "Please enter a valid email"
        case .networkError:
            return "Network error. Please try again."
        case .weakPassword:
            return "Your password is too weak. The password must be 6 characters long or more."
        case .wrongPassword:
            return "Your password is incorrect. Please try again or use 'Forgot password' to reset your password"
        case .tooManyRequests:
            return  "You've entered and incorrect password too many times. Please try again later or use 'Forgot password' to reset your password"
        default:
            return "Unknown error occurred"
        }
    }
}

extension UIViewController{
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            let alert = UIAlertController(title: "Error", message: errorCode.errorMessage, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
}
