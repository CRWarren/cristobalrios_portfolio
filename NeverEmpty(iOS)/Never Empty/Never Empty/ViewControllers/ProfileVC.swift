//
//  ProfileVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/15/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseFirestore

class ProfileVC: UIViewController{
    let db = Firestore.firestore()
    let images = ["Blue", "Green", "Orange", "Purple"]
    var user: User!
    let defaults = UserDefaults.standard
    var users = [User]()
    var sendingUser: User!
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileImageV: UIImageView!
    @IBOutlet weak var EmailTxt: UITextField!
    @IBOutlet weak var internalNameTxt: UILabel!
    @IBOutlet weak var NameTxt: UITextField!
    @IBOutlet weak var user1: UIButton!
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var user2: UIButton!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var user3: UIButton!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var user4: UIButton!
    @IBOutlet weak var name4: UILabel!
    @IBOutlet weak var EditRounded: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let myColor = UIColor.white
        
        let transform = CGAffineTransform(scaleX: 2, y: 2);
        ActivityIndicator.transform = transform
        EditRounded.layer.cornerRadius = 15
        EditRounded.layer.borderColor = myColor.cgColor
        EditRounded.layer.borderWidth = 1
        user1.layer.cornerRadius = 10
        user1.imageView!.layer.cornerRadius = 10
        user2.layer.cornerRadius = 10
        user2.imageView!.layer.cornerRadius = 10
        user3.layer.cornerRadius = 10
        user3.imageView!.layer.cornerRadius = 10
        user4.layer.cornerRadius = 10
        user4.imageView!.layer.cornerRadius = 10
        profileImageV.layer.cornerRadius = 10
        profileImageV.layer.masksToBounds = true
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(longTap2(_:)))
        let longGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(longTap3(_:)))
        let longGesture4 = UILongPressGestureRecognizer(target: self, action: #selector(longTap4(_:)))
        
        user2.addGestureRecognizer(longGesture2)
        user3.addGestureRecognizer(longGesture3)
        user4.addGestureRecognizer(longGesture4)
    }
    
    @objc func longTap4(_ sender: UIGestureRecognizer){
        
         if sender.state == .began {
            if user4.titleLabel!.text != "Add" && user4.titleLabel!.text != "add" {
                showToast(message: "LongGesture")
                sendingUser = users[3]
                performSegue(withIdentifier: "toEditUser", sender: nil)
            }
        }
    }
    
    @objc func longTap3(_ sender: UIGestureRecognizer){
        
        if sender.state == .began {
            if user3.titleLabel!.text != "Add" && user3.titleLabel!.text != "add" {
                showToast(message: "LongGesture")
                sendingUser = users[2]
                performSegue(withIdentifier: "toEditUser", sender: nil)
            }
        }
    }
    
    @objc func longTap2(_ sender: UIGestureRecognizer){
       
        if sender.state == .began {
            if user2.titleLabel!.text != "Add" && user2.titleLabel!.text != "add" {
                showToast(message: "LongGesture")
                sendingUser = users[1]
                performSegue(withIdentifier: "toEditUser", sender: nil)
            }
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
         downloadUser()
    }

    override func viewDidAppear(_ animated: Bool) {
      
        
        sendingUser = nil
    }
    
    func downloadUser(){
        ActivityIndicator.isHidden = false
        ActivityIndicator.startAnimating()
        let UID = Auth.auth().currentUser!.uid
        let docRef = db.collection("users").document(UID)
        docRef.getDocument { (document, error) in
            if let userDocument = document?.data(), (document?.exists)! {
                guard let userName = userDocument["full_name"] as? String else {print("FAIL: name"); return }
                guard let email = userDocument["email"] as? String else {print("FAIL: email"); return }
                guard let photo = userDocument["photo"] as? Int else {print("FAIL: photo"); return }
                guard let first = userDocument["first"] as? Bool else {print("FAIL: first"); return }
                guard let id = Auth.auth().currentUser?.uid else{ return }
                self.user = User(FullName: userName, Email: email, _photo: photo, _firstTime: first, _id: id)
                self.users.removeAll()
                self.users.append(self.user)
                self.EditRounded.isHidden = false
                self.EmailTxt.text = self.user.email
                self.NameTxt.text = self.user.fname
                let internalName = self.user.fname.split(separator: " ")
                print(internalName.count)
                self.internalNameTxt.text = "" + internalName[0]
                self.name1.text = "" + internalName[0]
                self.profileImageV.image = UIImage(named: self.images[photo])
                self.user1.setImage(UIImage(named: self.images[photo]), for: .normal)
                if(self.defaults.string(forKey: "id") != Auth.auth().currentUser!.uid) && self.defaults.string(forKey: "id") != nil {
                    self.internalNameTxt.text = self.defaults.string(forKey: "name")
                    self.profileImageV.image = UIImage(named: self.images[self.defaults.integer(forKey: "image")])
                }
                docRef.collection("internal").getDocuments { (QuerySnapshot, Error) in
                    if Error == nil {
                        for document in QuerySnapshot!.documents {
                            let tempDoc = document.data()
                            
                            guard let name = tempDoc["full_name"] as? String
                                else{print("failed: Name"); return}
                            guard let photo = tempDoc["photo"] as? Int
                                else{print("failed: photo"); return}
                            guard let id = tempDoc["id"] as? String
                                else{print("failed: id"); return}
                            let temp_user = User.init(FullName: name, Email: "", _photo: photo, _firstTime: false, _id: id)
                            self.users.append(temp_user)
                        }
                        
                        
                        
                        if self.users.count > 0{
                            self.user1.setImage(UIImage(named: self.images[self.users[0].photo]), for: .normal)
                            let internalname = self.users[0].fname.split(separator: " ")
                            self.name1.text = internalname[0] + ""
                            self.user1.setTitle(self.users[0].fname, for: .normal)
                            self.user1.isHidden = false
                            self.name1.isHidden = false
                            self.user2.isHidden = false
                            self.name2.isHidden = false
                            if self.users.count > 1{
                                self.user2.setImage(UIImage(named: self.images[self.users[1].photo]), for: .normal)
                                self.user2.setTitle(self.users[1].fname, for: .normal)
                                self.name2.text = self.users[1].fname
                                self.user3.isHidden = false
                                self.name3.isHidden = false
                                if self.users.count > 2{
                                    self.user3.setImage(UIImage(named: self.images[self.users[2].photo]), for: .normal)
                                    self.user3.setTitle(self.users[2].fname, for: .normal)
                                    self.name3.text = self.users[2].fname
                                    self.user4.isHidden = false
                                    self.name4.isHidden = false
                                    if self.users.count > 3{
                                        self.user4.setImage(UIImage(named: self.images[self.users[3].photo]), for: .normal)
                                        self.user4.setTitle(self.users[3].fname, for: .normal)
                                        self.name4.text = self.users[3].fname
                                    }else {
                                        self.user4.isHidden = false
                                        self.name4.isHidden = false
                                        self.user4.setImage(UIImage(named: "add"), for: .normal)
                                        self.user4.setTitle("Add", for: .selected)
                                        self.user4.setTitle("Add", for: .normal)
                                        self.name4.text = "Add"
                                    }
                                }else {
                                    self.user3.setImage(UIImage(named: "add"), for: .normal)
                                    self.user3.setTitle("Add", for: .normal)
                                    self.user3.setTitle("Add", for: .selected)
                                    self.name3.text = "Add"
                                    self.user3.isHidden = false
                                    self.name3.isHidden = false
                                    
                                    self.user4.isHidden = true
                                    self.name4.isHidden = true
                                    self.user4.setImage(UIImage(named: "add"), for: .normal)
                                    self.user4.setTitle("Add", for: .normal)
                                    self.user4.setTitle("Add", for: .selected)
                                    self.name4.text = "Add"
                                }
                            }else {
                                self.user2.setImage(UIImage(named: "add"), for: .normal)
                                self.user2.setTitle("Add", for: .normal)
                                self.user2.setTitle("Add", for: .selected)
                                self.name2.text = "Add"
                                self.user2.isHidden = false
                                self.name2.isHidden = false
                                
                                self.user3.setImage(UIImage(named: "add"), for: .normal)
                                self.user3.setTitle("Add", for: .normal)
                                self.user3.setTitle("Add", for: .selected)
                                self.name3.text = "Add"
                                self.user3.isHidden = true
                                self.name3.isHidden = true
                                
                                self.user4.isHidden = true
                                self.name4.isHidden = true
                                self.user4.setImage(UIImage(named: "add"), for: .normal)
                                self.user4.setTitle("Add", for: .normal)
                                self.user4.setTitle("Add", for: .selected)
                                self.name4.text = "Add"
                            }
                        }else {
                            
                            self.user1.isHidden = true
                            self.name1.isHidden = true
                            self.user2.setImage(UIImage(named: "add"), for: .normal)
                            self.user2.setTitle("Add", for: .normal)
                            self.user2.setTitle("Add", for: .selected)
                            self.name2.text = "Add"
                            self.user2.isHidden = true
                            self.name2.isHidden = true
                            
                            self.user3.setImage(UIImage(named: "add"), for: .normal)
                            self.user3.setTitle("Add", for: .normal)
                            self.user3.setTitle("Add", for: .selected)
                            self.name3.text = "Add"
                            self.user3.isHidden = true
                            self.name3.isHidden = true
                            
                            self.user4.isHidden = true
                            self.name4.isHidden = true
                            self.user4.setImage(UIImage(named: "add"), for: .normal)
                            self.user4.setTitle("Add", for: .normal)
                            self.user4.setTitle("Add", for: .selected)
                            self.name4.text = "Add"
                        }
                    }
                }
                self.ActivityIndicator.isHidden = true
                self.ActivityIndicator.stopAnimating()
               }else {
                print("Document does not exist")
            }
           
        }
        
        
        
    }
    
    @IBAction func userSelected(sender: UIButton){
        if sender.titleLabel!.text == "add" || sender.titleLabel!.text == "Add"{
            performSegue(withIdentifier: "toEditUser", sender: nil)
        }else {
            if users[sender.tag].fname.contains(" "){
                let internalName = users[sender.tag].fname.split(separator: " ")
                defaults.set("" + internalName[0], forKey: "name")
            }else {
              defaults.set(users[sender.tag].fname, forKey: "name")
            }
            defaults.set(users[sender.tag].id, forKey: "id")
            defaults.set(users[sender.tag].photo, forKey: "image")
            self.internalNameTxt.text = self.defaults.string(forKey: "name")
            self.profileImageV.image = UIImage(named: self.images[self.defaults.integer(forKey: "image")])
            self.showToast(message: "Logged In - \(users[sender.tag].fname)")
        }
    
    }
    
    @IBAction func editTapped(_ sender: Any) {
        if defaults.string(forKey: "id") == nil || ( defaults.string(forKey: "id") != nil && defaults.string(forKey: "id") == Auth.auth().currentUser!.uid){
             performSegue(withIdentifier: "toEditProfile", sender: nil)
        }else {
            let alert = UIAlertController(title: "Permission Denied", message: "Only Main User can edit profile", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toEditProfile" {
        let destinationVC = segue.destination as! EditProfileVC
            destinationVC.user = user
        }else if segue.identifier == "toEditUser" {
            if sendingUser != nil {
                let destinationVC = segue.destination as! NewInternalUser
                destinationVC.user = sendingUser
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.EditRounded.isHidden = true
    }
}
