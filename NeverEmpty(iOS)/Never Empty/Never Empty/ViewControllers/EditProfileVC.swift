//
//  EditProfileVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/16/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class EditProfileVC: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var PurpleBttn: UIButton!
    @IBOutlet weak var OrangeBttn: UIButton!
    @IBOutlet weak var GreenBttn: UIButton!
    @IBOutlet weak var blueBttn: UIButton!
    @IBOutlet weak var FullNameTxt: UITextField!
    @IBOutlet weak var EmailTxt: UITextField!
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var SaveRounded: UIButton!
    
    let myColor = UIColor.white
    let db = Firestore.firestore()
    var user: User?
    var image: Int?
    let images = ["Blue", "Green", "Orange", "Purple"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if user != nil {
            EmailTxt.text = user?.email
            FullNameTxt.text = user?.fname
            profileImage.image = UIImage(named: images[(user?.photo)!] )
            switch user!.photo {
            case 0:
                blueBttn.sendActions(for: .touchUpInside)
            case 1:
               GreenBttn.sendActions(for: .touchUpInside)
            case 2:
               OrangeBttn.sendActions(for: .touchUpInside)
            case 3:
                PurpleBttn.sendActions(for: .touchUpInside)
            default:
                print("error on image")
            }
        }
        SaveRounded.layer.cornerRadius = 15
        SaveRounded.layer.borderColor = myColor.cgColor
        SaveRounded.layer.borderWidth = 1
        
        PurpleBttn.layer.cornerRadius = 10
        PurpleBttn.imageView!.layer.cornerRadius = 10
        GreenBttn.layer.cornerRadius = 10
        GreenBttn.imageView!.layer.cornerRadius = 10
        OrangeBttn.layer.cornerRadius = 10
        OrangeBttn.imageView!.layer.cornerRadius = 10
        blueBttn.layer.cornerRadius = 10
        blueBttn.imageView!.layer.cornerRadius = 10
        profileImage.layer.cornerRadius = 10
        profileImage.layer.masksToBounds = true
        
        FullNameTxt.delegate = self
        EmailTxt.delegate = self
        PasswordTxt.delegate = self
    }
    
    
    @IBAction func infoPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Edit Profile", message: "Changing Email or setting new password will require re-authentication", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
         self.present(alert, animated: true)
    }
    
    @IBAction func imageProfileSelected(_ sender: UIButton) {
        PurpleBttn.layer.borderWidth = 0
        OrangeBttn.layer.borderWidth = 0
        blueBttn.layer.borderWidth = 0
        GreenBttn.layer.borderWidth = 0
        sender.layer.borderColor = myColor.cgColor
        sender.layer.borderWidth = 2
        switch sender.tag {
        case 0:
            image = 0
        case 1:
            image = 1
        case 2:
            image = 2
        case 3:
            image = 3
        default:
            print("error on image")
        }
         profileImage.image = UIImage(named: images[(image)!] )
        
    }
    
    func validate() -> Bool {
        let fullname = FullNameTxt.text
        let email = EmailTxt.text
        let password = PasswordTxt.text
        if((fullname?.isEmpty)! || (email?.isEmpty)!){
            alert(errorString: "Please fill at least email and password")
            return false;
        }else if(!isValidEmail(testStr: email!)){
            alert(errorString: "invalid Email")
            return false;
        }else if(!(password!.isEmpty)&&((password?.count)!)<6){
            alert(errorString: "Password too short!")
            return false;
        }else if(!(fullname!.contains(" "))){
            alert(errorString: "Please enter firstname and lastname")
            return false;
        }
        return true;
    }
    
    func alert(errorString: String){
        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func save(){
        
        let docRef = db.collection("users").document(Auth.auth().currentUser!.uid)
        if(EmailTxt.text != user!.email) || !(PasswordTxt.text!.isEmpty){
            alertReAuth(docRef: docRef)
            
        }
        docRef.updateData(["full_name":self.FullNameTxt.text!,
                           "photo":image!])
        navigationController?.popViewController(animated: true)
    }
    
    func alertReAuth(docRef: DocumentReference){
        var password = ""
         let userAuth = Auth.auth().currentUser
        let alert = UIAlertController(title: "Security Measure", message: "due to your protection before changing this data, you need to enter your password", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your password"
            textField.isSecureTextEntry = true
        }
        alert.addAction(UIAlertAction(title: "Continue", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            password = textField.text!
             var credential: AuthCredential
            credential = EmailAuthProvider.credential(withEmail: self.user!.email, password: password)
            
            userAuth?.reauthenticateAndRetrieveData(with: credential, completion: { (AuthDataResult, Error) in
                    //change to new password
                    if Error != nil {
                        // An error happened.
                    } else {
                        // User re-authenticated.
                        if self.EmailTxt.text != self.user!.email {
                            Auth.auth().currentUser?.updateEmail(to: self.EmailTxt.text!)
                            docRef.updateData(["email":self.EmailTxt.text!])
                        }
                        
                        if !self.PasswordTxt.text!.isEmpty{
                            Auth.auth().currentUser?.updatePassword(to: self.PasswordTxt.text!)
                        }
                    }
            })

        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    @IBAction func savedPressed(_ sender: UIButton) {
        if validate(){
            save()
        }
    }
    
}
