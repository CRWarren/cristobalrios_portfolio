//
//  NewRegisterVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/22/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class NewRegisterVC: UIViewController, UITextFieldDelegate {
    
    var image: Int = 0
    let myColor = UIColor.white
    @IBOutlet weak var PurpleBttn: UIButton!
    @IBOutlet weak var OrangeBttn: UIButton!
    @IBOutlet weak var GreenBttn: UIButton!
    @IBOutlet weak var blueBttn: UIButton!
    @IBOutlet weak var signUpRounded: UIButton!
    @IBOutlet weak var LogoGraphic: UIImageView!
    @IBOutlet weak var TextFieldsContainer: UIView!
    @IBOutlet weak var ImagesContainer: UIView!
    
    @IBOutlet weak var FullNameLb: UILabel!
    @IBOutlet weak var FullNameTxt: UITextField!
    @IBOutlet weak var EmailLbl: UILabel!
    @IBOutlet weak var EmailTxt: UITextField!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var RePasswordLbl: UILabel!
    @IBOutlet weak var RePasswordTxt: UITextField!
    @IBOutlet weak var TopConstraintLogo: NSLayoutConstraint!
    
    
    let defaults = UserDefaults.standard
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        signUpRounded.layer.cornerRadius = 15
        let myColor = UIColor.white
        signUpRounded.layer.borderColor = myColor.cgColor
        signUpRounded.layer.borderWidth = 1
        blueBttn.layer.borderWidth = 2
        blueBttn.layer.borderColor = myColor.cgColor
        
        PurpleBttn.layer.cornerRadius = 10
        PurpleBttn.imageView!.layer.cornerRadius = 10
        GreenBttn.layer.cornerRadius = 10
        GreenBttn.imageView!.layer.cornerRadius = 10
        OrangeBttn.layer.cornerRadius = 10
        OrangeBttn.imageView!.layer.cornerRadius = 10
        blueBttn.layer.cornerRadius = 10
        blueBttn.imageView!.layer.cornerRadius = 10
        FullNameTxt.delegate = self
        EmailTxt.delegate = self
        PasswordTxt.delegate = self
        RePasswordTxt.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateLogoIn()
    }
    @IBAction func cancelButton(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func imageProfileSelected(_ sender: UIButton) {
        PurpleBttn.layer.borderWidth = 0
        OrangeBttn.layer.borderWidth = 0
        blueBttn.layer.borderWidth = 0
        GreenBttn.layer.borderWidth = 0
        sender.layer.borderColor = myColor.cgColor
        sender.layer.borderWidth = 2
        switch sender.tag {
        case 0:
            image = 0
        case 1:
            image = 1
        case 2:
            image = 2
        case 3:
            image = 3
        default:
            print("error on image")
        }
        
    }
    @IBAction func RegisterClicked(_ sender: UIButton) {
        // validate
        if(self.validate()){
            Auth.auth().createUser(withEmail: EmailTxt.text!, password: PasswordTxt.text!) { (authResult, error) in
                guard let user = authResult?.user else {
                    self.alert(errorString: (error?.localizedDescription)!)
                    return }
                self.addDocument(UID: user.uid)
                let internalName = self.FullNameTxt.text!.split(separator: " ")
                print(internalName.count)
                
                self.defaults.set("" + internalName[0], forKey: "name")
                self.defaults.set(user.uid, forKey: "id")
                self.defaults.set(self.image, forKey: "image")
                self.performSegue(withIdentifier: "toApp", sender: nil)
            }
        }else {
            print("error on validation")
        }
    }
    
    func validate() -> Bool {
        let fullname = FullNameTxt.text
        let email = EmailTxt.text
        let password = PasswordTxt.text
        let repassword = RePasswordTxt.text
        if((fullname?.isEmpty)! || (email?.isEmpty)! || (password?.isEmpty)! || (repassword?.isEmpty)!){
            alert(errorString: "Please fill all Fields")
            return false;
        }else if(password! != repassword!){
            alert(errorString: "Passwords don't match")
            return false;
        }else if(!isValidEmail(testStr: email!)){
            alert(errorString: "invalid Email")
            return false;
        }else if((password?.count)!<6){
            alert(errorString: "Password too short! \nneeds to be longer than 6 charcters")
            return false;
        }else if(!(fullname!.contains(" "))){
            alert(errorString: "Please enter firstname and lastname")
            return false;
        }
        return true;
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func alert(errorString: String){
        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func addDocument(UID: String){
        let db = Firestore.firestore()
        db.collection("users").document("\(UID)").setData(
            [ "email": EmailTxt.text!,
              "full_name": FullNameTxt.text!,
              "photo": image,
              "first": true,
              "id": UID] , completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {
                }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
}
