//
//  SelectUserVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/30/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SelectUserVC: UIViewController{
    @IBOutlet weak var user1: UIButton!
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var user2: UIButton!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var user3: UIButton!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var user4: UIButton!
    @IBOutlet weak var name4: UILabel!
    @IBOutlet weak var actIn: UIActivityIndicatorView!
    @IBOutlet weak var LogoGraphic: UIImageView!
    @IBOutlet weak var WhoLbl: UILabel!
    
    let db = Firestore.firestore()
    let images = ["Blue", "Green", "Orange", "Purple"]
    let defaults = UserDefaults.standard
    var users = [User]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user1.layer.cornerRadius = 10
        user1.imageView!.layer.cornerRadius = 10
        user2.layer.cornerRadius = 10
        user2.imageView!.layer.cornerRadius = 10
        user3.layer.cornerRadius = 10
        user3.imageView!.layer.cornerRadius = 10
        user4.layer.cornerRadius = 10
        user4.imageView!.layer.cornerRadius = 10
        let transform = CGAffineTransform(scaleX: 2, y: 2);
        actIn.transform = transform
    }
    override func viewDidAppear(_ animated: Bool) {
        actIn.startAnimating()
        actIn.isHidden = false
        downloadUsers()
        
    }
    
    func downloadUsers(){
        self.users.removeAll()
        if(Auth.auth().currentUser?.uid == nil){
            return
        }
        
        let UID = Auth.auth().currentUser!.uid
        let docRef = db.collection("users").document(UID)
        
        docRef.getDocument { (document, error) in
            if let userDocument = document?.data(), (document?.exists)! {
                guard let userName = userDocument["full_name"] as? String else {print("FAIL: name"); return }
                guard let email = userDocument["email"] as? String else {print("FAIL: email"); return }
                guard let photo = userDocument["photo"] as? Int else {print("FAIL: photo"); return }
                guard let first = userDocument["first"] as? Bool else {print("FAIL: first"); return }
                guard let id = Auth.auth().currentUser?.uid else{ return }
                let user = User(FullName: userName, Email: email, _photo: photo, _firstTime: first, _id: id)
                self.users.append(user)
                
                docRef.collection("internal").getDocuments { (QuerySnapshot, Error) in
                    if Error == nil {
                        for document in QuerySnapshot!.documents {
                            let tempDoc = document.data()
                            
                            guard let name = tempDoc["full_name"] as? String
                                else{print("failed: Name"); return}
                            guard let photo = tempDoc["photo"] as? Int
                                else{print("failed: photo"); return}
                            guard let id = tempDoc["id"] as? String
                                else{print("failed: id"); return}
                            let temp_user = User.init(FullName: name, Email: "", _photo: photo, _firstTime: false, _id: id)
                            self.users.append(temp_user)
                        }
                        
                        if self.users.count > 0{
                            self.user1.setImage(UIImage(named: self.images[self.users[0].photo]), for: .normal)
                            let internalname = self.users[0].fname.split(separator: " ")
                            self.name1.text = internalname[0] + ""
                            self.user1.setTitle(self.users[0].fname, for: .normal)
                            self.user1.isHidden = false
                            self.name1.isHidden = false
                            if self.users.count > 1{
                                self.user2.isHidden = false
                                self.name2.isHidden = false
                                self.user2.setImage(UIImage(named: self.images[self.users[1].photo]), for: .normal)
                                self.user2.setTitle(self.users[1].fname, for: .normal)
                                self.name2.text = self.users[1].fname
                                
                                if self.users.count > 2{
                                    self.user3.isHidden = false
                                    self.name3.isHidden = false
                                    self.user3.setImage(UIImage(named: self.images[self.users[2].photo]), for: .normal)
                                    self.user3.setTitle(self.users[2].fname, for: .normal)
                                    self.name3.text = self.users[2].fname
                                    
                                    if self.users.count > 3{
                                        self.user4.isHidden = false
                                        self.name4.isHidden = false
                                        self.user4.setImage(UIImage(named: self.images[self.users[3].photo]), for: .normal)
                                        self.user4.setTitle(self.users[3].fname, for: .normal)
                                        self.name4.text = self.users[3].fname
                                    }
                                }
                            }
                        }
                        self.actIn.stopAnimating()
                        self.actIn.isHidden = true
                        if AnimationManager.screenBounds.height <= 568 {
                            self.LogoGraphic.frame.origin.y -= 40
                            self.WhoLbl.frame.origin.y -= 40
                            self.view.layoutIfNeeded()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func userSelected(_ sender: UIButton) {
        if users[sender.tag].fname.contains(" "){
            let internalName = users[sender.tag].fname.split(separator: " ")
            defaults.set("" + internalName[0], forKey: "name")
        }else {
            defaults.set(users[sender.tag].fname, forKey: "name")
        }
        defaults.set(users[sender.tag].id, forKey: "id")
        defaults.set(users[sender.tag].photo, forKey: "image")
        performSegue(withIdentifier: "toApp", sender: nil)
    }
}
