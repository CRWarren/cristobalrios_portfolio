//
//  PantryVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/15/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class PantryVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
    var food: FoodItem?
    let db = Firestore.firestore()
    var pantryList = [FoodItem]()
    var filteredArray = [FoodItem]()
    let fh = FirebaseHelper.init()
    let defaults = UserDefaults.standard
    var order = "name"
    var onEdit = false
    let PS = PushNotificationSender()
    
    @IBOutlet weak var newDatePicker: UIDatePicker!
    @IBOutlet var popOverNew: UIView!
    @IBOutlet weak var collView: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var addBttn: UIButton!
    @IBOutlet weak var EmptyLbl: UILabel!
    @IBOutlet weak var emptyInstructions: UILabel!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var addBarBttn: UIBarButtonItem!
    @IBOutlet weak var filterBttn: UIBarButtonItem!
    @IBOutlet weak var userBttn: UIBarButtonItem!
    
    @IBOutlet var popOverDate: UIView!
    
    override func viewDidLoad() {
        collView.dataSource = self
        collView.delegate = self
        SearchBar.delegate = self
        super.viewDidLoad()
        let myColor = UIColor.white
        addBttn.layer.cornerRadius = 15
        addBttn.layer.borderColor = myColor.cgColor
        addBttn.layer.borderWidth = 1
        popOverNew.layer.cornerRadius = 10
        popOverDate.layer.cornerRadius = 10
        datePicker.minimumDate = Date()
        SearchBar.backgroundColor = self.navigationController?.navigationBar.barTintColor
        SearchBar.returnKeyType = .done
        downloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        filteredArray = pantryList
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pantryList.count != 0 {
            EmptyLbl.isHidden = true
            emptyInstructions.isHidden = true
        }else{
            EmptyLbl.isHidden = false
            emptyInstructions.isHidden = false
            return 0
        }
        return filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            print("Delete Action Tapped")
        self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("pantry").document(self.pantryList[indexPath.row].barcode).delete()
        self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("cart").document(self.pantryList[indexPath.row].barcode).delete()
            self.showToast(message: "Deleted!")
            self.pantryList.remove(at: indexPath.row)
            self.filteredArray = self.pantryList
            self.collView.reloadData()
            self.collView.isUserInteractionEnabled = false
            
        }
        deleteAction.backgroundColor = UIColor.init(red: 194/255, green: 61/255, blue: 51/255, alpha: 1)
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        food = filteredArray[indexPath.row]
        performSegue(withIdentifier: "toView2", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toView2"{
            let destination = segue.destination as! ViewFood3
            destination.food = self.food
            destination.addedDate = self.food!.addedDate
        }
    }
    
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cartAction = UIContextualAction(style: .normal, title: "Shopping Cart") { (action, view, handler) in
            if self.filteredArray[indexPath.row].addedDate == "" {
                self.showToast(message: "Already on cart!")
                self.collView.isEditing = false
            }else{
                self.filteredArray[indexPath.row].addedDate = ""
                if self.defaults.string(forKey: "id") != nil{
                    if self.defaults.string(forKey: "id")! != Auth.auth().currentUser!.uid{
                        self.filteredArray[indexPath.row]._UID = self.defaults.string(forKey: "name")!
                    }else {
                        self.filteredArray[indexPath.row]._UID = Auth.auth().currentUser!.uid
                    }
                }
                self.fh.addToCartList(food: self.filteredArray[indexPath.row])
                self.fh.addToPantryList(food: self.filteredArray[indexPath.row])
                self.showToast(message: "Added to Cart!")
                self.SearchBar.endEditing(true)
                self.SearchBar.text = nil
                self.PS.sendNotification(food: self.filteredArray[indexPath.row], title: "New Item Added to the cart!")
            }
            
        }
        cartAction.backgroundColor = UIColor.init(red: 0/255, green: 193/255, blue: 200/255, alpha: 1)
        let configuration = UISwipeActionsConfiguration(actions: [cartAction])
        return configuration
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //create a cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_1", for: indexPath) as! ListsCVcell
        //configure a cell
        cell.foodImageView.image = filteredArray[indexPath.row].image
        cell.foodImageView.layer.cornerRadius = 10
        cell.foodImageView.layer.masksToBounds = true
        cell.foodNameLbl.text = filteredArray[indexPath.row].name
        cell.extrasLbl.text = "Exp: " + filteredArray[indexPath.row].expirationDate
        if filteredArray[indexPath.row].addedDate == "" {
            cell.shoppingIndicatorView.isHidden = false
        }else{
             cell.shoppingIndicatorView.isHidden = true
        }
        return cell
    }
    
    
    func downloadData(){
        pantryList.removeAll()
        self.DownloadPantry()
    }
    
    @IBAction func addToList(_ sender: UIBarButtonItem) {
        let alert =  UIAlertController(title: "Add to pantry?", message: "Scan product or custom item?", preferredStyle: .actionSheet)
        
        let custom = UIAlertAction(title: "Custom", style: .default) { (UIAlertAction) in
        //popCustom
            self.alertnew()
        }
        let _Barcode = UIAlertAction(title: "Scan", style: .default) { (UIAlertAction) in
           self.tabBarController!.selectedIndex = 1
        }
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel) { (UIAlertAction) in
            
        }
        
        alert.addAction(custom)
        alert.addAction(_Barcode)
        alert.addAction(Cancel)
        
        self.present(alert, animated: true)
    }
    
    func DownloadPantry(){
        db.collection("users").document("\(Auth.auth().currentUser!.uid)").collection("pantry").addSnapshotListener() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.pantryList.removeAll()
                if (querySnapshot?.isEmpty)!{
                    print("empty")
                    self.collView.reloadData()
                }
                for document in querySnapshot!.documents {
                     let foodDoc = document.data()
                        guard let name = foodDoc["name"] as? String,
                            let _keywords = foodDoc["_keywords"] as? [String],
                            let ingredients = foodDoc["ingredients"] as? String,
                            let serving_size = foodDoc["serving_size"] as? String,
                            let date_added = foodDoc["date_added"] as? String,
                            let expiration_date = foodDoc["expiration_date"] as? String,
                            let serving_quantity = foodDoc["serving_quantity"] as? Int,
                            let sodium = foodDoc["sodium"] as? Double,
                            let carbs = foodDoc["carbohydrates"] as? Double,
                            let proteins = foodDoc["proteins"] as? Double,
                            let fat = foodDoc["fat"] as? Double,
                            let calories = foodDoc["calories"] as? Int,
                            let barcode = foodDoc["barcode"] as? String,
                            let addedBy = foodDoc["addedby"] as? String
                            else {print("FAIL: food item"); return }
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy"
                        dateFormatter.dateStyle = .short
                        dateFormatter.timeStyle = .none
                        let empty = dateFormatter.date(from: "01/01/0001")
                        let currentdate = dateFormatter.date(from: date_added)
                        let exp = dateFormatter.date(from: expiration_date)
                        
                        let storageRef = Storage.storage().reference().child("food_images")
                        storageRef.child(barcode).getData(maxSize: 1 * 1024 * 1024) { data, error in
                            if let data = data {
                                let image = UIImage(data: data)!
                                let food = FoodItem(name: name, image:  image, _keywords: _keywords, expirationDate: expiration_date, addedDate: date_added, ingredients: ingredients, serving_size: serving_size, servingQuantity: serving_quantity, sodium: Decimal(sodium), carbohydrates: Decimal(carbs), proteins: Decimal(proteins), fat: Decimal(fat), calories: calories, barcode: barcode, _UID: addedBy, AddedDate: currentdate ?? empty!, ExpDate: exp ?? empty!)
                                if self.pantryList.contains(where: { $0.barcode == food.barcode}){
                                    self.pantryList.removeAll(where: { $0.barcode == food.barcode})
                                }
                                self.pantryList.append(food)
                                self.filteredArray = self.pantryList
                                self.collView.reloadData()
                                self.collView.isUserInteractionEnabled = true
                            }else {
                                let food = FoodItem(name: name, image:  UIImage(named:"image_holder")!, _keywords: _keywords, expirationDate: expiration_date, addedDate: date_added, ingredients: ingredients, serving_size: serving_size, servingQuantity: serving_quantity, sodium: Decimal(sodium), carbohydrates: Decimal(carbs), proteins: Decimal(proteins), fat: Decimal(fat), calories: calories, barcode: barcode, _UID: addedBy, AddedDate: currentdate ?? empty!, ExpDate: exp ?? empty!)
                                self.pantryList.append(food)
                                self.filteredArray = self.pantryList
                                self.collView.reloadData()
                                self.collView.isUserInteractionEnabled = true
                            }
                        }
                }
            }
        }
    }
    
    var newName: String?
    func alertnew(){
        
        let alert = UIAlertController(title: "New Item?", message: "Please enter your items name", preferredStyle: .alert)
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Item Name"
        }
        let save = UIAlertAction(title: "Add", style: .default) { (UIAlertAction) in
            let itemName = alert.textFields![0] as UITextField
            self.newName = itemName.text!
            if self.newName != nil && !self.newName!.isEmpty{
                 self.collView.isUserInteractionEnabled = false
                self.addBarBttn.isEnabled = false
                self.userBttn.isEnabled = false
                self.filterBttn.isEnabled = false
                self.SearchBar.isUserInteractionEnabled = false
                self.view.addSubview(self.popOverNew)
                self.popOverNew.center = self.view.center
            }else{
                self.alertnew()
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(save)
        alert.addAction(cancel)
        self.present(alert, animated: true)
    }
    
    @IBAction func addNew(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let currentdate = dateFormatter.string(from: Date())
         let fuid = UUID().uuidString
        let Newfood = FoodItem(name: self.newName!, image:  UIImage(named:"image_holder")!, _keywords:[], expirationDate: dateFormatter.string(from: newDatePicker.date), addedDate: currentdate, ingredients: "", serving_size: "0", servingQuantity: 0, sodium: 0.0, carbohydrates: 0.0, proteins: 0.0, fat: 0.0, calories: 0, barcode: fuid, _UID: "", AddedDate: Date(), ExpDate: newDatePicker.date)
        self.fh.addToPantryList(food: Newfood)
         self.collView.isUserInteractionEnabled = true
        self.addBarBttn.isEnabled = true
        self.userBttn.isEnabled = true
        self.filterBttn.isEnabled = true
        self.SearchBar.isUserInteractionEnabled = true
        self.popOverNew.removeFromSuperview()
        showToast(message: "Updated Pantry")
    }
    
    @IBAction func addPressed(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let currentdate = dateFormatter.string(from: Date())
        food!.expirationDate = dateFormatter.string(from: datePicker.date)
        food!.addedDate = currentdate
        self.fh.addToPantryList(food: food!)
        self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("cart").document(food!.barcode).delete()
         self.collView.isUserInteractionEnabled = true
        self.popOverDate.removeFromSuperview()
         showToast(message: "Updated Pantry")
        
    }
    
    @IBAction func orderBy(_ sender: Any) {
        let alert = UIAlertController(title: "Filter By:", message: "", preferredStyle: .actionSheet)
        let alphabetically = UIAlertAction(title: "A - Z", style: .default) { (UIAlertAction) in
            self.order = "name"
            self.orderData()
        }
        let alphabetically2 = UIAlertAction(title: "Z - A", style: .default) { (UIAlertAction) in
            self.order = "reverse"
            self.orderData()
        }
        let dateFilter = UIAlertAction(title: "Expiration Date", style: .default) { (UIAlertAction) in
            self.order = "exp"
            self.orderData()
        }
        
        
        let none = UIAlertAction(title: "Cancel", style: .cancel) { (UIAlertAction) in
        }
        alert.addAction(alphabetically)
        alert.addAction(alphabetically2)
        alert.addAction(dateFilter)
        alert.addAction(none)
        self.present(alert, animated: true)
    }
    
    func orderData(){
        switch order {
        case "name":
            pantryList.sort(by: {$0.name < $1.name})
        case "exp":
            pantryList.sort(by: {$0.ExpDate < $1.ExpDate})
        case "reverse":
            pantryList.sort(by: {$1.name < $0.name})
        default:
            print("error sorting")
        }
        self.filteredArray = pantryList
        collView.reloadData()
    }
    
    @IBAction func CanceledAdd(_ sender: Any) {
        popOverDate.removeFromSuperview()
        popOverNew.removeFromSuperview()
        self.collView.isUserInteractionEnabled = true
        self.addBarBttn.isEnabled = true
        self.userBttn.isEnabled = true
        self.filterBttn.isEnabled = true
        self.SearchBar.isUserInteractionEnabled = true
    }
    


    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SearchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         filteredArray = pantryList
        
        if searchText != "" {
        filteredArray = filteredArray.filter{ $0.name.lowercased().starts(with: searchText.lowercased())}
        }else{
             filteredArray = pantryList
        }
         self.collView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.SearchBar.endEditing(true)
    }
    
    
    
    
}
