//
//  ForgotPasswordVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/9/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ForgotPasswordVC: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var LogoGraphic: UIImageView!
    @IBOutlet weak var ForgotPsswrdLbl: UILabel!
    @IBOutlet weak var ResetmessageLbl: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var sendBttn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        sendBttn.layer.cornerRadius = 15
        let myColor = UIColor.white
        sendBttn.layer.borderColor = myColor.cgColor
        sendBttn.layer.borderWidth = 1
        emailTXT.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateTextField1()
    }
    
    @IBAction func cancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendClicked(_ sender: Any) {
        if(isValidEmail(testStr: emailTXT.text!)){
            let alert = UIAlertController(title: "Email Sent", message: "Check your email for instructions to reset your password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                Auth.auth().sendPasswordReset(withEmail: self.emailTXT.text!, completion: nil)
                self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true)

        }else{
            let alert = UIAlertController(title: "Invalid Email", message: "Please enter a valid Email Address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
}
