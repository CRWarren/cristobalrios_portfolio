//
//  CartVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/10/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class CartVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var emptyLabel: UILabel!
    var food: FoodItem?
    var index: Int?
    let db = Firestore.firestore()
    @IBOutlet weak var collView: UITableView!
    var cartList = [FoodItem]()
    let fh = FirebaseHelper.init()
    let PS = PushNotificationSender()
    
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var addBttn: UIButton!
    @IBOutlet weak var emptyInstruction: UILabel!
    @IBOutlet var PopOverDate: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    override func viewDidLoad() {
        collView.dataSource = self
        collView.delegate = self
        super.viewDidLoad()
        addBttn.layer.cornerRadius = 15
        let myColor = UIColor.white
        addBttn.layer.borderColor = myColor.cgColor
        addBttn.layer.borderWidth = 1
        PopOverDate.layer.cornerRadius = 10
        datePicker.minimumDate = Date()
        downloadData()
    }

    
    public func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cartList.count != 0 {
            emptyLbl.isHidden = true
            emptyInstruction.isHidden = true
        }else{
            emptyLbl.isHidden = false
            emptyInstruction.isHidden = false
        }
        return cartList.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            print("Delete Action Tapped")
            
            self.cartList[indexPath.row].addedDate = "01/01/0001"
            self.fh.addToPantryList(food: self.cartList[indexPath.row])
            self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("cart").document(self.cartList[indexPath.row].barcode).delete()
            self.cartList.remove(at: indexPath.row)
            self.collView.reloadData()
            self.showToast(message: "Deleted from cart!")
        }
        
        deleteAction.backgroundColor = UIColor.init(red: 194/255, green: 61/255, blue: 51/255, alpha: 1)
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cartAction = UIContextualAction(style: .normal, title: "Bought It") { (action, view, handler) in
            self.index = indexPath.row
            self.food = self.cartList[indexPath.row]
            self.view.addSubview(self.PopOverDate)
            self.PopOverDate.center = self.view.center
            self.collView.isUserInteractionEnabled = false
        }
        cartAction.backgroundColor = UIColor.init(red: 0/255, green: 193/255, blue: 200/255, alpha: 1)
        let configuration = UISwipeActionsConfiguration(actions: [cartAction])
        return configuration
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //create a cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_1", for: indexPath) as! ListsCVcell
        //configure a cell
        cell.foodImageView.image = cartList[indexPath.row].image
        cell.foodImageView.layer.cornerRadius = 10
        cell.foodImageView.layer.masksToBounds = true
        cell.foodNameLbl.text = cartList[indexPath.row].name
        if cartList[indexPath.row]._UID != Auth.auth().currentUser?.uid{
             cell.extrasLbl.text = cartList[indexPath.row]._UID
        }else {
            cell.extrasLbl.text = ""
        }
        return cell
    }
    
    func downloadData(){
       
        cartList.removeAll()
        DispatchQueue.main.async {
            self.DownloadPantry()
        }
    }
    
    func DownloadPantry(){
        db.collection("users").document("\(Auth.auth().currentUser!.uid)").collection("cart").addSnapshotListener() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.cartList.removeAll()
                if (querySnapshot?.isEmpty)!{
                    print("empty")
                    self.collView.reloadData()
                }
                for document in querySnapshot!.documents {
                        let foodDoc = document.data()
                        guard let name = foodDoc["name"] as? String,
                            let _keywords = foodDoc["_keywords"] as? [String],
                            let ingredients = foodDoc["ingredients"] as? String,
                            let serving_size = foodDoc["serving_size"] as? String,
                            let date_added = foodDoc["date_added"] as? String,
                            let expiration_date = foodDoc["expiration_date"] as? String,
                            let serving_quantity = foodDoc["serving_quantity"] as? Int,
                            let sodium = foodDoc["sodium"] as? Double,
                            let carbs = foodDoc["carbohydrates"] as? Double,
                            let proteins = foodDoc["proteins"] as? Double,
                            let fat = foodDoc["fat"] as? Double,
                            let calories = foodDoc["calories"] as? Int,
                            let barcode = foodDoc["barcode"] as? String,
                            let addedBy = foodDoc["addedby"] as? String
                            else {print("FAIL: food item"); return }
                    
                        let storageRef = Storage.storage().reference().child("food_images")
                        storageRef.child(barcode).getData(maxSize: 1 * 1024 * 1024) { data, error in
                            if let data = data {
                                let image = UIImage(data: data)!
                                let food = FoodItem(name: name, image:  image, _keywords: _keywords, expirationDate: expiration_date, addedDate: date_added, ingredients: ingredients, serving_size: serving_size, servingQuantity: serving_quantity, sodium: Decimal(sodium), carbohydrates: Decimal(carbs), proteins: Decimal(proteins), fat: Decimal(fat), calories: calories, barcode: barcode, _UID: addedBy, AddedDate: Date(), ExpDate: Date())
                                if self.cartList.contains(where: { $0.barcode == food.barcode}){
                                    self.cartList.removeAll(where: { $0.barcode == food.barcode})
                                }
                                 self.collView.isUserInteractionEnabled = true
                                self.cartList.append(food)
                                self.collView.reloadData()
                            }else {
                                let food = FoodItem(name: name, image:  UIImage(named:"image_holder")!, _keywords: _keywords, expirationDate: expiration_date, addedDate: date_added, ingredients: ingredients, serving_size: serving_size, servingQuantity: serving_quantity, sodium: Decimal(sodium), carbohydrates: Decimal(carbs), proteins: Decimal(proteins), fat: Decimal(fat), calories: calories, barcode: barcode, _UID: addedBy, AddedDate: Date(), ExpDate: Date())
                                if self.cartList.contains(where: { $0.barcode == food.barcode}){
                                    self.cartList.removeAll(where: { $0.barcode == food.barcode})
                                }
                                self.collView.isUserInteractionEnabled = true
                                self.cartList.append(food)
                                self.collView.reloadData()
                            }
                            
                        }
                }
            
            }
            
        }
        
    }
    
    func downloadImage(barcode: String) -> UIImage{
            print(barcode)
        var image =  UIImage(named: "image_holder")!
            let storageRef = Storage.storage().reference().child("food_images")
            storageRef.child(barcode).getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let data = data {
                    image = UIImage(data: data)!
                    self.collView.reloadData()
                }
                
            }
        return image
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        food = cartList[indexPath.row]
        performSegue(withIdentifier: "toView3", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toView3"{
            let destination = segue.destination as! ViewFood3
            destination.food = self.food
        }
    }
    
    @IBAction func addPressed(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let selectedDate = dateFormatter.string(from: datePicker.date)
        let currentdate = dateFormatter.string(from: Date())
        food!.addedDate = currentdate
        food!.expirationDate = selectedDate
        self.fh.addToPantryList(food:  food!)
    self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("cart").document(food!.barcode).delete()
        self.cartList.remove(at: self.index!)
        self.PopOverDate.removeFromSuperview()
        self.collView.reloadData()
        self.showToast(message: "Bought It!")
        self.PS.sendNotification(food: food!, title: "New Item Added to the Pantry!")
    }
    
    @IBAction func CancelTapped(_ sender: UIButton) {
        PopOverDate.removeFromSuperview()
        collView.isEditing = false
        collView.isUserInteractionEnabled = true
    }
    
}
