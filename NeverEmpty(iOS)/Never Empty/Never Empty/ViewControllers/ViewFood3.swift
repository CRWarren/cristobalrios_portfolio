//
//  ViewFood3.swift
//  Never Empty
//
//  Created by cristobal rios on 10/18/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ViewFood3: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate{

    var food: FoodItem?
    var addedDate: String?
    let fh = FirebaseHelper.init()
    let db = Firestore.firestore()
    let PS = PushNotificationSender()

    @IBOutlet weak var reportBttn: UIButton!
    @IBOutlet weak var addBttn: UIButton!
    @IBOutlet weak var addPantryBttn: UIButton!
    @IBOutlet var popOverDate: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var foodIV: UIImageView!
    @IBOutlet weak var servingSize: UILabel!
    @IBOutlet weak var caloriesTxt: UITextField!
    @IBOutlet weak var fatTxt: UITextField!
    @IBOutlet weak var carbsTxt: UITextField!
    @IBOutlet weak var SodiumTxt: UITextField!
    @IBOutlet weak var proteinTxt: UITextField!
    @IBOutlet weak var ingredientsTxt: UITextView!
    @IBOutlet weak var servingSizeTxtField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!


    @IBOutlet weak var doneBttn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var backgroundView: UIView!
    var barcode: String?
    var moving = false
    var KeyboardHeight: CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        addPantryBttn.layer.cornerRadius = 15
        let myColor = UIColor.white
        addPantryBttn.layer.borderColor = myColor.cgColor
        addPantryBttn.layer.borderWidth = 1
        
        
        if addPantryBttn.tag != 2 {
            addBttn.layer.cornerRadius = 15
            addBttn.layer.borderColor = myColor.cgColor
            addBttn.layer.borderWidth = 1
            popOverDate.layer.cornerRadius = 10
            datePicker.minimumDate = Date()
        }
        SodiumTxt.delegate = self
        proteinTxt.delegate = self
        caloriesTxt.delegate = self
        fatTxt.delegate = self
        carbsTxt.delegate = self
        backgroundView.layer.cornerRadius = 15
        display()
        foodIV.layer.cornerRadius = 10
        foodIV.layer.masksToBounds = true
        ingredientsTxt.delegate = self
    }
    
    
    func downloadImage(barcode: String){
        let storageRef = Storage.storage().reference().child("food_images")
        //var image = UIImage(named: "image_holder")!
        storageRef.child(barcode).getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let data = data {
                self.foodIV.image = UIImage(data: data)!
            }else {
                self.foodIV.image = UIImage(named: "image_holder")!
                print(error ?? "No Error")}
        }
        
    }
    
    
    
    func display(){
        if food != nil{
            barcode = food!.barcode
            nameLbl.text = food!.name
            servingSize.text = food!.serving_size
            fatTxt.text = "\(food!.fat) g"
            caloriesTxt.text = "\(food!.calories) g"
            SodiumTxt.text = "\(food!.sodium) mg"
            carbsTxt.text = "\(food!.carbohydrates) g"
            proteinTxt.text = "\(food!.proteins) g"
            ingredientsTxt.text = food!.ingredients
            if(food!.barcode.count > 20){
            reportBttn.isHidden = true
            }else{
                reportBttn.isHidden = false
            }
             self.foodIV.image = food!.image
            if addPantryBttn.tag == 0 {
                downloadImage(barcode: food!.barcode)
            }
            addedDate = food!.addedDate
        }
    }
  
    
    @IBAction func finish(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func add(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let dateString = dateFormatter.string(from: datePicker!.date)
        food!.expirationDate = dateString
        let currentdate = dateFormatter.string(from: Date())
        food!.addedDate = currentdate
        food!._UID = (Auth.auth().currentUser?.uid)!
        fh.addToPantryList(food: food!)
        self.db.collection("users").document(Auth.auth().currentUser!.uid).collection("cart").document(barcode!).delete()
        self.popOverDate.removeFromSuperview()
        self.PS.sendNotification(food: food!, title: "New Item Added to the Pantry!")
        self.dismiss(animated: true, completion: nil)
        

    }
    
    @IBAction func addPantry(_ sender: UIButton) {
                self.view.addSubview(self.popOverDate)
                self.popOverDate.center = self.view.center
        
    }
    
    
    func alert(message: String, title: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addToCart(_ sender: UIButton) {

            if food?.addedDate == "" {
                self.showToast(message: "Aleready on Cart")
                self.dismiss(animated: true, completion: nil)
            }else{
                self.food!.addedDate = ""
                self.fh.addToPantryList(food:  self.food!)
                self.fh.addToCartList(food:  self.food!)
                showToast(message: "Added To Cart")
                self.PS.sendNotification(food: food!, title: "New Item Added to the cart!")
                self.dismiss(animated: true, completion: nil)
            }
    }
    @IBAction func CancelTapped(_ sender: UIButton) {
        popOverDate.removeFromSuperview()
    }
    
    @IBAction func Report(_ sender: Any) {
        alertError()
    }
    
    @IBAction func openOFF(_ sender: Any) {
        guard let url = URL(string: "https://openfoodfacts.org") else { return }
        UIApplication.shared.open(url)
    }
    
    func alertError(){
        let alert = UIAlertController(title: "Report Error", message: "Please tell us what's wrong with this item.", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "let us know..."
        }
        var errorText: String?
        let save = UIAlertAction(title: "Report", style: .default) { (UIAlertAction) in
            let itemName = alert.textFields![0] as UITextField
            errorText = itemName.text!
            if errorText != nil && !errorText!.isEmpty{
                self.fh.uploadFoodPending(food: self.food!, _errorReported: errorText!)
                self.alertThanks()
            }else{
                self.alertError()
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(save)
        alert.addAction(cancel)
        self.present(alert, animated: true)
    }
    func alertThanks(){
        let alert = UIAlertController(title: "Thank You", message: "The Item has been reported and we'll try to fix it ASAP! You might have to wait a while, delete and then re-add the item to your pantry", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}
