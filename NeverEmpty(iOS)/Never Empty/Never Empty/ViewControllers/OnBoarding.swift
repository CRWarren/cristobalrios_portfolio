//
//  OnBoarding.swift
//  Never Empty
//
//  Created by cristobal rios on 11/2/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class OnBoarding: UIViewController {
    @IBOutlet weak var NextBttn: UIButton!
    @IBOutlet weak var showingImage: UIImageView!
    
    var image = 0
    let db = Firestore.firestore()
    let images = ["welcome","scan_ob","view","pantry_ob","shopping_ob","profile_ob"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NextBttn.layer.cornerRadius = 15
        NextBttn.layer.borderWidth = 1
        NextBttn.layer.borderColor = UIColor.white.cgColor
        showingImage.image = UIImage(named: images[image])
        
    }
    @IBAction func SkipBttn(_ sender: UIButton) {
        updateTrue()
    }
    
    func updateTrue(){
        let docRef = db.collection("users").document(Auth.auth().currentUser!.uid)
        docRef.updateData(["first" : false]){ err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        image += 1
        if image < images.count{
            showingImage.image = UIImage(named: images[image])
            if image == (images.count-1) {
                NextBttn.setTitle("Done", for: .normal)
                NextBttn.setTitle("Done", for: .selected)
            }
        }else {
            updateTrue()
           
        }
    }
    
}
