//
//  NewFoodVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/19/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class NewFoodVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet var licenseView: UIView!
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var SelectedImage: UIImageView!
    @IBOutlet weak var servingSize: UITextField!
    @IBOutlet weak var Calories: UITextField!
    @IBOutlet weak var Fat: UITextField!
    @IBOutlet weak var Carbs: UITextField!
    @IBOutlet weak var Sodium: UITextField!
    @IBOutlet weak var Protein: UITextField!
    @IBOutlet weak var Ingredients: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelBttn: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var BackGround: UIView!
    @IBOutlet weak var SaveBttn: UIButton!
    @IBOutlet weak var agreeBttn: UIButton!
    
    var KeyboardHeight: CGFloat?
    var barcode: String?
    var moving = false
    
    var fh = FirebaseHelper.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Ingredients.delegate = self
        BackGround.layer.cornerRadius = 15
        SaveBttn.layer.cornerRadius = 15
        SaveBttn.layer.borderColor = UIColor.white.cgColor
        SaveBttn.layer.borderWidth = 1
        SelectedImage.layer.cornerRadius = 10
        SelectedImage.layer.masksToBounds = true
        Sodium.delegate = self
        Protein.delegate = self
        Calories.delegate = self
        Fat.delegate = self
        Carbs.delegate = self
        licenseView.layer.cornerRadius = 10
        licenseView.layer.borderWidth = 1
        agreeBttn.layer.cornerRadius = 15
        agreeBttn.layer.borderColor = UIColor.white.cgColor
        agreeBttn.layer.borderWidth = 1
        Name.delegate = self
        servingSize.delegate = self
        Calories.delegate = self
        Fat.delegate = self
        Carbs.delegate = self
        Sodium.delegate = self
        Protein.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let blue = UIColor.init(red: 0/255, green: 193/255, blue: 200/255, alpha: 1)
        licenseView.layer.borderColor = blue.cgColor
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        moving = true
        bottomConstraint.constant = 260
        topConstraint.constant = -198
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            SelectedImage.image = UIImage(data: image.jpegData(compressionQuality: 0.5)!)
        }else{
            print("Something went wrong")
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func OpenCamera(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
   
    @IBAction func Save(_ sender: Any) {
        if validate(){
            self.view.addSubview(self.licenseView)
            self.licenseView.center = self.view.center
          SaveBttn.isEnabled = false
            cancelBttn.isEnabled = false
        }
    }
    
    @IBAction func agree(_ sender: Any) {
        let keywords = Name.text!.split(separator: " ")
        var array = [String]()
        for s in keywords{
            array.append(String(s))
        }
        let currentDateTime = Date()
        let food = FoodItem(name: Name.text!, image: SelectedImage.image!, _keywords: array, expirationDate: " ", addedDate: " ", ingredients: Ingredients.text!, serving_size: servingSize.text!, servingQuantity: 0, sodium: Decimal(Double(Sodium.text!)!), carbohydrates: Decimal(Double(Carbs.text!)!), proteins: Decimal(Double(Protein.text!)!), fat: Decimal(Double(Fat.text!)!), calories: Int(Calories.text!)!, barcode: barcode!, _UID: Auth.auth().currentUser!.uid, AddedDate: currentDateTime, ExpDate: currentDateTime)
        fh.uploadFood(food: food)
        fh.uploadImage(barcode: barcode!, image: SelectedImage.image!)
        SaveBttn.isEnabled = true
        cancelBttn.isEnabled = true
        self.dismiss(animated: true, completion: nil)
        self.licenseView.removeFromSuperview()
        
    }
    
    @IBAction func cancelLicense(_ sender: Any) {
        self.licenseView.removeFromSuperview()
        SaveBttn.isEnabled = true
        cancelBttn.isEnabled = true
    }
    
    @IBAction func finishClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validate() -> Bool{
        if barcode == nil{
            alert(message: "Error", title: "coudn't find barcode please scan again")
            self.dismiss(animated: true, completion: nil)
            return false
        }else if SelectedImage.image == nil{
            alert(message: "No Image", title: "Please take an image")
            return false
        }else if Calories.text!.isEmpty || Fat.text!.isEmpty || servingSize.text!.isEmpty || Carbs.text!.isEmpty || Sodium.text!.isEmpty || Protein.text!.isEmpty || Ingredients.text!.isEmpty || Name.text!.isEmpty {
            alert(message: "Empty spaces", title: "Plese fill all spaces available")
            return false
        }
        
        return true
    }
    
    func alert(message: String, title: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        if moving{
            bottomConstraint.constant = 33.5
            topConstraint.constant = 13.5
        }
        moving = false
       
    }
    
    private let decimalSeparator = "."
    
    func isFractionalPartValid(in string: String) -> Bool {
        let maxFractionalPartLength = 1
        guard let separator = decimalSeparator.first,
            let comaIndex = string.firstIndex(of: separator),
            string.distance(from: comaIndex, to: string.endIndex) > maxFractionalPartLength + 2 else {
                return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let set = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: decimalSeparator)).inverted
        let maxLength = 8
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)
        
        if newString.count > maxLength || newString.rangeOfCharacter(from: set) != nil {
            return false
        }
        
        if newString.count == maxLength && newString.last == decimalSeparator.first {
            return false
        }
        
        return isFractionalPartValid(in: newString)
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }

    @IBAction func openDCL(_ sender: Any) {
        guard let url = URL(string: "https://opendatacommons.org/licenses/dbcl/1.0/") else { return }
        UIApplication.shared.open(url)
        
    }
    
    @IBAction func openCCLicense(_ sender: Any) {
        guard let url = URL(string: "https://creativecommons.org/licenses/by-sa/4.0/legalcode") else { return }
        UIApplication.shared.open(url)
    }
    
   
    
    
}


