//
//  ScannerVC.swift
//  Never Empty
//
//  Created by cristobal rios on 10/9/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Firebase



class ScannerVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate{
    
    let db = Firestore.firestore()
    let PS = PushNotificationSender()
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var popOverName: UILabel!
    @IBOutlet var PopOverDate: UIView!
    @IBOutlet weak var square: UIImageView!
    var video:AVCaptureVideoPreviewLayer?
    var captureDevice:AVCaptureDevice?
    let session = AVCaptureSession()
    let fh = FirebaseHelper.init()
    var name = ""
    var ingredients = ""
    var image = ""
    var _keywords = [String]()
    var servingSize = ""
    var servingQuantity = 0
    var fat: Decimal = 0.0
    var proteins: Decimal = 0.0
    var sodium: Decimal = 0.0
    var energy = 0
    var carbs: Decimal = 0.0
    var imageUI: UIImage?
    var barcode = ""
    //define output
    let output = AVCaptureMetadataOutput()
    @IBOutlet weak var addBttn: UIButton!
    @IBOutlet weak var allowAccess: UIButton!
    

    var food: FoodItem!
    //create our capture session
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.minimumDate = Date()
        // Do any additional setup after loading the view.
        self.PopOverDate.layer.cornerRadius = 10
    
        allowAccess.layer.borderWidth = 1
        allowAccess.layer.borderColor = UIColor.blue.cgColor
        allowAccess.layer.cornerRadius = 15
        addBttn.layer.cornerRadius = 15
        let myColor = UIColor.white.cgColor
        addBttn.layer.borderColor = myColor
        addBttn.layer.borderWidth = 1
      
        let pushManager = PushNotificationManager(userID: Auth.auth().currentUser!.uid)
        pushManager.registerForPushNotifications()
        //define capture device
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        //add capture device to our sessicion
        
        do {
            //this will try get the input of what the camera is recording
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            //this will add our data scanned to the session
            session.addInput(input)
        }
        catch{
            print("Error")
        }
        
        checkFirst()
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
         startSession()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !session.isRunning{
            startSession()
        }
    }
    
    @IBAction func CancelTapped(_ sender: UIButton) {
        PopOverDate.removeFromSuperview()
        startSession()
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if  metadataObjects.count > 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
                if object.type == AVMetadataObject.ObjectType.ean13 || object.type == AVMetadataObject.ObjectType.upce{
                    downloadFBandValidate(Barcode: object.stringValue!)
                    barcode = object.stringValue!
                    session.stopRunning()
                }
            }
        }
    }
    
    
    func downloadFBandValidate(Barcode: String){
        
        let docRef = db.collection("food_items").document(Barcode)
        docRef.getDocument { (document, error) in
            if let _ = document?.data(), (document?.exists)!{
                self.DownloadItem(barcode: Barcode, _where: "food_items")
            }else {
                self.downloadAndParse(Barcode: Barcode)
                }
            }
        }
    

    func downloadAndParse(Barcode: String) {
        self.name = ""
        self.ingredients = ""
        self.image = ""
        self._keywords = [String]()
        self.servingSize = ""
        self.servingQuantity = 0
        self.fat = 0.0
        self.proteins = 0.0
        self.sodium = 0.0
        self.energy = 0
        self.carbs = 0.0
        self.imageUI = UIImage(named: "image_holder")
        //create a default configuration
        print(Barcode)
        let config = URLSessionConfiguration.default
        
        //create a session
        let session = URLSession(configuration: config)
        
        if let validURL = URL(string: "https://world.openfoodfacts.org/api/v0/product/\(Barcode).json"){
            
            //create task
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                //check for error
                if let error = error {
                    print("data task failed \nerror: \(error.localizedDescription)")
                    return
                }
                
                //check the response status
                guard  let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let validData = data
                    else{return}
                do{
                    if let jsonObj = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [String: Any]{
                        guard let status = jsonObj["status_verbose"] as? String
                            else{return}
                        if(status == "product found"){
                            guard let foodItem = jsonObj["product"] as? [String: Any]
                                else{return}
                            if let _ = foodItem["product_name"] as? String{
                                self.name = foodItem["product_name"] as! String
                            }
                            if let _ = foodItem["ingredients_text_en"] as? String{
                                self.ingredients = foodItem["ingredients_text_en"] as! String
                            }
                            if let _ = foodItem["image_url"] as? String{
                                self.image = foodItem["image_url"] as! String
                                 self.downloadImage(from: URL(string: self.image)!)
                            }
                            if let _ = foodItem["_keywords"] as? [String]{
                                self._keywords = foodItem["_keywords"] as! [String]
                            }
                            if let _ = foodItem["serving_size"] as? String{
                                self.servingSize = foodItem["serving_size"] as! String
                            }
                            if let _ = foodItem["serving_quantity"] as? Int{
                                self.servingQuantity = foodItem["serving_quantity"] as! Int
                            }
                            
                            if let nutriments = foodItem["nutriments"] as? [String: Any]{
                                
                                if let _ = nutriments["fat_serving"] as? Double{
                                    self.fat = Decimal(nutriments["fat_serving"] as! Double)
                                }else if let fat = nutriments["fat_serving"] as? String{
                                    self.fat = Decimal(Double(fat) ?? 0.0)
                                }
                                if let _ = nutriments["proteins_serving"] as? Double{
                                    self.proteins = Decimal(nutriments["proteins_serving"] as! Double)
                                }else if let proteins = nutriments["proteins_serving"] as? String{
                                    self.proteins = Decimal(Double(proteins) ?? 0.0)
                                }
                                
                                if let _ = nutriments["sodium_serving"] as? Double{
                                    self.sodium = Decimal((nutriments["sodium_serving"] as! Double)/4.184)
                                }else if let salt = nutriments["sodium_serving"] as? String{
                                    self.sodium = Decimal(Double(salt) ?? 0.0)
                                }

                                if let energyD = nutriments["energy_serving"] as? Double{
                                    self.energy = Int(round(energyD))
                                }else if let energyD = nutriments["energy_serving"] as? String{
                                    self.energy = Int(round(Double(energyD) ?? 0.0))
                                }
                                
                                if let decimalCarbs = nutriments["carbohydrates_serving"] as? Double{
                                    self.carbs = Decimal(decimalCarbs)
                                    
                                    //divide/ 4.184
                                }else if let decimalCarbs = nutriments["carbohydrates_serving"] as? String{
                                    self.carbs = Decimal(Double(decimalCarbs) ?? 0.0)
                                }
                            }
                            //guard let brands = foodItem["brands"] as? String
                            if(self.name.count <= 0 && self.image.count <= 0 && self.servingSize.count <= 0){
                                self.alertNotFound()
                            }else{
                                 let currentDateTime = Date()
                                self.food = FoodItem(name: self.name, image: self.imageUI!, _keywords: self._keywords, expirationDate: "", addedDate: "", ingredients: self.ingredients, serving_size: self.servingSize, servingQuantity: self.servingQuantity, sodium: self.sodium, carbohydrates: self.carbs, proteins: self.proteins, fat: self.fat, calories: self.energy, barcode: Barcode, _UID: "", AddedDate: currentDateTime, ExpDate: currentDateTime)
                                self.fh.uploadFood(food: self.food)
                                self.alertFound(name: self.name)
                            }
                        }else{
                             self.alertNotFound()
                        }
                        
                        }
                   
                }
                catch{print(error.localizedDescription)}
                
            })
            
            //always start the task
            task.resume()
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func downloadImage(from url: URL) {
        print("download url " + url.description)
        print("Downloaing")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Downloaded")
            self.imageUI = UIImage(data: data)!
            self.fh.uploadImage(barcode: self.barcode, image: self.imageUI!)
        }
    }
    
    @IBAction func addClicked(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        let dateString = dateFormatter.string(from: datePicker!.date)
        food.expirationDate = dateString
        let currentdate = dateFormatter.string(from: Date())
        food.addedDate = currentdate
        food._UID = (Auth.auth().currentUser?.uid)!
        fh.addToPantryList(food: food)
        self.PS.sendNotification(food: food, title: "New Item Added to the Pantry!")
        self.session.startRunning()
        self.PopOverDate.removeFromSuperview()
        showToast(message: "Added To Pantry!")
        
    }
    
    func showView(){
        performSegue(withIdentifier: "ViewItemSegue", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewItemSegue"{
            let destination = segue.destination as! ViewFood3
            destination.food = self.food
        }else if segue.identifier == "toNew" {
            let destination = segue.destination as! NewFoodVC
            destination.barcode = barcode
        }
    }
    
    func DownloadItem(barcode: String, _where: String){
       db.collection(_where).document(barcode).addSnapshotListener{ (document, error) in
            if let foodDoc = document?.data(), (document?.exists)! {
                guard let name = foodDoc["name"] as? String,
                    let _keywords = foodDoc["_keywords"] as? [String],
                    let ingredients = foodDoc["ingredients"] as? String,
                    let serving_size = foodDoc["serving_size"] as? String,
                    let serving_quantity = foodDoc["serving_quantity"] as? Int,
                    let sodium = foodDoc["sodium"] as? Double,
                    let carbs = foodDoc["carbohydrates"] as? Double,
                    let proteins = foodDoc["proteins"] as? Double,
                    let fat = foodDoc["fat"] as? Double,
                    let calories = foodDoc["calories"] as? Int,
                    let barcode = foodDoc["barcode"] as? String
                    
                    else {print("FAIL: food item"); return }
                let storageRef = Storage.storage().reference().child("food_images")
                storageRef.child(barcode).getData(maxSize: 1 * 1024 * 1024) { data, error in
                     var image = UIImage(named:"image_holder")!
                    if let data = data {
                     image = UIImage(data: data)!
                    }
                    let currentDateTime = Date()
                    self.food = FoodItem(name: name, image:  image, _keywords: _keywords, expirationDate: "", addedDate: "", ingredients: ingredients, serving_size: serving_size, servingQuantity: serving_quantity, sodium: Decimal(sodium), carbohydrates: Decimal(carbs), proteins: Decimal(proteins), fat: Decimal(fat), calories: calories, barcode: barcode, _UID: "", AddedDate: currentDateTime, ExpDate: currentDateTime)
                    self.popOverName.text = self.name
                self.alertFound(name: name)
                }
            }else {
                print(barcode)
                print("Document does not exist")
            }
        }
    }
    func alertNotFound(){
        let alert = UIAlertController(title: "Food Not found", message: "Do you want to help us and add it to our data? \n thanks!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (UIAlertAction) in
            self.session.startRunning()
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            self.performSegue(withIdentifier: "toNew", sender: nil)
        }))
        self.present(alert, animated: true)
        
    }
    func alertFound(name: String){
        let alert = UIAlertController(title: name, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "View", style: .default, handler: { (UIAlertAction) in
            self.showView()
        }))
        
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (UIAlertAction) in
            
            self.view.addSubview(self.PopOverDate)
            self.popOverName.text = self.name
            self.PopOverDate.center = self.view.center
        }))
        self.present(alert, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        session.stopRunning()
    }
    
    func checkFirst(){
        db.collection("users").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
            if error == nil {
                if let user = document?.data(), (document?.exists)! {
                    guard let firstTime = user["first"] as? Bool
                        else{print("failed First"); return}
                    if firstTime {
                        self.performSegue(withIdentifier: "ToOnBoarding", sender: nil)
                    }
                }
            }
        }
    }
    
    func startSession(){
            session.removeOutput(output)
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
        
            session.addOutput(output)
            //output proccessed on the main queue
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            //we want only qr codes
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.ean13,AVMetadataObject.ObjectType.upce]
            
            //show video -- add the video preview layer to a new sub layer in order to show video.
            video = AVCaptureVideoPreviewLayer(session: session)
            video?.frame = view.layer.bounds
            view.layer.addSublayer(video!)
            
            //this will bring the square image used to help the user aim the code
            self.view.bringSubviewToFront(square)
            //start the session
            session.startRunning()
           
        }else{
            allowAccess.isHidden = false
        }
    }
    

    @IBAction func grantAccess(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
    
}


