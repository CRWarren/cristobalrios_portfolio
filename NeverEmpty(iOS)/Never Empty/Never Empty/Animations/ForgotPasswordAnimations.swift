//
//  ForgotPasswordAnimations.swift
//  Never Empty
//
//  Created by cristobal rios on 5/3/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

extension ForgotPasswordVC{
    
    override func viewWillAppear(_ animated: Bool) {
        LogoGraphic.alpha = 0
        ForgotPsswrdLbl.alpha = 0
        ResetmessageLbl.alpha = 0
        emailTXT.alpha = 0
        sendBttn.alpha = 0
    }
    func animateTextField1(){
        UIView.animateKeyframes(withDuration: 2, delay: 0, options: [.calculationModeLinear], animations: {
            
            //right
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.2, animations: {
                self.LogoGraphic.alpha = 1
                self.LogoGraphic.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)

            })
            //top
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.2, animations: {
                self.ForgotPsswrdLbl.alpha = 1
            })
            //left
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.2, animations: {
                self.ResetmessageLbl.alpha = 1
            })
            //bottom
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.2, animations: {
                self.emailTXT.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 0.8, relativeDuration: 0.2, animations: {
                self.sendBttn.alpha = 1
            })
        })
    
    }
}
