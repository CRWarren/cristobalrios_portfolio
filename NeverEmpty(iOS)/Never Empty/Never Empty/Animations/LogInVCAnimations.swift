//
//  LogInVCAnimations.swift
//  Never Empty
//
//  Created by cristobal rios on 5/3/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

extension LogInVC{
    
    override func viewWillAppear(_ animated: Bool) {
        LogoGraphic.alpha = 0
        TextFieldsContainer.alpha = 0
        SignInRounded.alpha = 0
        ForgotPassword.alpha = 0
        SignUp.alpha = 0
    }
    
    func animateLogoIn(){
        UIView.animate(withDuration: 1.5, delay: 0, options: [.transitionCrossDissolve], animations: {
            self.LogoGraphic.alpha = 1
        }) { (complete) in
            self.animateTextFieldsIn()
        }
    }
    
    func animateTextFieldsIn(){
        UIView.animate(withDuration: 1, animations: {
            self.TextFieldsContainer.alpha = 1
            self.SignInRounded.alpha = 1
            self.SignUp.alpha = 1
            self.ForgotPassword.alpha = 1
        self.TextFieldsContainer.frame.origin.y -= 1
        })
    }

}
