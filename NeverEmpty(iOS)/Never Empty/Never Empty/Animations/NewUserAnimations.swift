//
//  NewUserAnimations.swift
//  Never Empty
//
//  Created by cristobal rios on 5/3/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

extension NewRegisterVC{
    
    override func viewWillAppear(_ animated: Bool) {
        LogoGraphic.alpha = 0
        EmailTxt.alpha = 0
        FullNameTxt.alpha = 0
        PasswordTxt.alpha = 0
        RePasswordTxt.alpha = 0
        EmailLbl.alpha = 0
        FullNameLb.alpha = 0
        passwordLbl.alpha = 0
        RePasswordLbl.alpha = 0
        ImagesContainer.alpha = 0
        self.signUpRounded.alpha = 0
    }
    
    func animateLogoIn(){
        UIView.animate(withDuration: 1.25, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.curveLinear], animations: {
            self.LogoGraphic.alpha = 1
            if AnimationManager.screenBounds.height <= 568 {
                self.TopConstraintLogo.constant = 50
            }else{
                self.TopConstraintLogo.constant = 75
            }
           // self.titleLabel.frame.origin.y += 150
            self.view.layoutIfNeeded()
        }){ complete in
            self.animateTextField1()
        }
    }
    
    func animateTextField1(){
        UIView.animateKeyframes(withDuration: 2, delay: 0, options: [.calculationModeLinear], animations: {
            
            //right
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                self.FullNameLb.alpha = 1
                self.FullNameTxt.alpha = 1
            })
            //top
            UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: {
                self.EmailLbl.alpha = 1
                self.EmailTxt.alpha = 1
            })
            //left
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.25, animations: {
                self.PasswordTxt.alpha = 1
                self.passwordLbl.alpha = 1
            })
            //bottom
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.25, animations: {
                self.RePasswordLbl.alpha = 1
                self.RePasswordTxt.alpha = 1
            })
        }){complete in
            self.animateUserImages()
        }
       
    }
    
    func animateUserImages(){
        UIView.animate(withDuration: 1.5, delay: 0, options: [.transitionFlipFromLeft], animations: {
            self.ImagesContainer.alpha = 1
        }) { complete in
            self.animateSignUp()
        }
    }
    
    func animateSignUp(){
        UIView.animate(withDuration: 1.5, delay: 0, options: [.transitionCrossDissolve], animations: {
            self.signUpRounded.alpha = 1
        })
    }


}
