//
//  PushNotificationSender.swift
//  Never Empty
//
//  Created by cristobal rios on 5/8/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit
import Foundation
import FirebaseAuth
import Firebase
class PushNotificationSender {
    
    func sendPushNotification(to token: String, title: String, body: String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body],
                                           "data" : ["user" : Auth.auth().currentUser?.uid]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAdnUVBEo:APA91bGMQ_v2ElFWM-lQleoRdFNcfVcOS2abmCJxGgvYkcRsvYkftAxwBhGE9rbWGtwYE7CVZSRQB-md1j9FWs71W72TFx38SSiLM37_UxUB-NavH66dRSTrXjpKjEGYBrHixuuRz_bg", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
    func sendNotification(food: FoodItem, title: String){
        let db = Firestore.firestore()
        db.collection("users").document(Auth.auth().currentUser!.uid).collection("tokens").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err.localizedDescription)")
            } else {
                for document in querySnapshot!.documents {
                    let token = document.data()
                    
                    guard let tokenString = token["fcmToken"] as? String
                        else{print("failed: Name"); return}
                    let PS = PushNotificationSender()
                    PS.sendPushNotification(to: tokenString, title: title, body: food.name)
                }
            }
        }
        
    }
}
