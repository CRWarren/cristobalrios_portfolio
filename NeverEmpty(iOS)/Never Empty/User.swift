//
//  User.swift
//  Never Empty
//
//  Created by cristobal rios on 10/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation

class User {
    var fname: String
    var email: String
    var photo: Int
    var firstTime: Bool
    var id: String
    
    init(FullName: String, Email: String, _photo: Int, _firstTime: Bool, _id: String) {
        fname = FullName
        email = Email
        photo = _photo
        firstTime = _firstTime
        id = _id
    }
    

}
