# Premium Club App #

Welcome to the **PCA** development repository, this our main **repository** with our project folders. Make sure to keep up to date the repository and in a **healthy** manner. **Communication** is key and essential for **team development**.

* * *

## Team Members ##

Jonathan **" Dwayne "** Becerra

* Mobile / Web Engineer
* SQL, C#, Swift, and Bash.

Cristobal **" Warren "** Rios

* Mobile / Web Engineer
* SQL, C#, Swift, and Bash.

Chad **" FSChad "** Gibson

* Product Owner
* Scrum Master
* Client
* Instructor
* Mentor

* * *

Trello Boards:
https://trello.com/b/DRNNuckb/premiumclubapp1803

* * *

## Development Pipeline ##

Development Resources:

* mapbox.com
* rescuetime.com
* wakatime.com
* trello.com

Databases:

* Firebase

* * *

## Give Back ##

Make sure to be a team player, if you find resources, share them. It is good to share knowledge for self and team growth. Feel free to add headers on to the readme file, if it benefits the team, for example a link to algorithms pdf file. Anything can help.
