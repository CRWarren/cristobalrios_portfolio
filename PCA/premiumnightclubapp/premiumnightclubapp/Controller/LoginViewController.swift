//
//  LoginViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/16/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var clubToggleSwitch: UISwitch!
    @IBOutlet weak var logInButtonOutlet: UIButton!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    
    
    var userDefaults: UserDefaults!
    
    

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDefaults = UserDefaults.standard
        
        if let email = userDefaults.string(forKey: "email"), let password = userDefaults.string(forKey: "password") {
            let accountType = userDefaults.bool(forKey: "accountType")
            clubToggleSwitch.isOn = accountType
            emailTextField.text = email
            passwordTextField.text = password
            logInButtonOutlet.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            logInButtonOutlet.isUserInteractionEnabled = true
            signUpButtonOutlet.setTitleColor(#colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1), for: .normal)
            signUpButtonOutlet.isUserInteractionEnabled = true

        } else {
            // This will set the texview delegates to each to jump through
            // fields using the return key from the keyboard.
            textFieldDelegation()
            // The intial method call that will setup the textfield and
            // button handlers for validation and state changes.
            textFiedlHandlerSetUp()
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            if clubToggleSwitch.isOn == true {
                print(Auth.auth().currentUser?.email)
               self.performSegue(withIdentifier: "ClubAutoSignIn", sender: nil)
            } else {
               self.performSegue(withIdentifier: "UserLogInSegue", sender: nil)
            }
        }
    }
    
    // Delegate assigment to the texfields.
    func textFieldDelegation() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    // This method will add targets to the texfields to see live updates of the textfield
    // while editing, once it complyes it will turn the button on to perform the
    // sign up, however, it needs to validate the conditional chain.
    func textFiedlHandlerSetUp() {
        // Target Addition To TextFields With Selector Functions.
        emailTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        // Button State Changes That Will Be Handled By The Target Functions.
        logInButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
        logInButtonOutlet.isUserInteractionEnabled = false
        signUpButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
        signUpButtonOutlet.isUserInteractionEnabled = false
    }
    
    // This is the target function that will be validating if the texfields are nil or empty, once the
    // condiotanl are true it will set the get startetd button state back to its normal and on state.
    @objc func textFieldEditing() {
      guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty
            else { logInButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
                logInButtonOutlet.isUserInteractionEnabled = false
                signUpButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
                signUpButtonOutlet.isUserInteractionEnabled = false; return}
        logInButtonOutlet.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        logInButtonOutlet.isUserInteractionEnabled = true
        signUpButtonOutlet.setTitleColor(#colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1), for: .normal)
        signUpButtonOutlet.isUserInteractionEnabled = true
    }
    
    @IBAction func logInButtonTapped(_ sender: UIButton) {
        
        if clubToggleSwitch.isOn == true {
            if textFieldErrorHandelingContidionals() {
                FIRAuthentication.authObj.authSignIn(userEmail: self.emailTextField.text, userPassword: self.passwordTextField.text, switchON: self.clubToggleSwitch.isOn, VC: self)
            }
        } else {
            if textFieldErrorHandelingContidionals() {
                FIRAuthentication.authObj.authSignIn(userEmail: self.emailTextField.text, userPassword: self.passwordTextField.text,switchON: self.clubToggleSwitch.isOn, VC: self)
            }
        }
    }
    
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        if clubToggleSwitch.isOn == true {
            if textFieldErrorHandelingContidionals() {
                
                self.performSegue(withIdentifier: "ClubSignUpSegue", sender: nil)
            }
        } else {
            if textFieldErrorHandelingContidionals() {
                
                self.performSegue(withIdentifier: "UserSignUpSegue", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ClubSignUpSegue" {
            let segueDestination = segue.destination as! ClubSignUpViewController
            segueDestination.clubEmailString = emailTextField.text
            segueDestination.clubPasswordString = passwordTextField.text
        } else if segue.identifier == "UserSignUpSegue" {
            let segueDestination = segue.destination as! UserSignUpViewController
            segueDestination.userEmailString = emailTextField.text
            segueDestination.userPasswordString = passwordTextField.text
        }
    }
        
    // This is a multplie block conditonal that will validate for all textfields
    // in order to compy with signup policy.
    func textFieldErrorHandelingContidionals() -> Bool {
                // The third conditional will validate the email format using Regex that
                // will return true if the format complies with email structure.
                if emailTextField.text!.count > 1 && emailFormatValidation(email: emailTextField.text) {
                    // This fourth conditional will check for password structure using regex
                    // aswell, to build up a more secure password to avoid weak login credentials.
                    if passwordTextField.text!.count > 6 && passwordFormatValidation(password: passwordTextField.text) {
                        // This will validate the title of the button and see if there is a date in
                        // the title if not it would not complete the chain of condiotnal and return false.
                        return true
                    } else { errorHandelingAlertView(errorMessage: "Password Not Strong Enough") }
                    // Password Error
                } else { errorHandelingAlertView(errorMessage: "Email Format Error") }
        // If any conditional fails this will fire and exit the conditional chain.
        return false
    }
    
    
    func passwordFormatValidation(password: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let password = password {
            // This regex validates the password structure, it needs Uppercase,
            // lowercase and numbers. Must be more than 7 characters in order to validate as well.
            let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{7,}$"
            // This will do the comparison/evaluation of the regex vs the password
            // to see if both comply and match paramters.
            return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
        }
        return false
    }
    
    func emailFormatValidation(email: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let email = email {
            // This regex validates the email structure, strins,
            // @ sign and . plus string.
            let emailregex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            // This will do the comparison/evaluation of the regex vs the email
            // to see if both comply and match paramters.
            return NSPredicate(format:"SELF MATCHES %@", emailregex).evaluate(with: email)
        }
        return false
    }
    
    // This is the custom alert view which is being recycled to handle the
    // error alerts that will fire if the condiontal chain for signup validation fails and returns false.
    func errorHandelingAlertView(errorMessage: String) {
        let alertView = UIAlertController(title: "Error!", message: "\(errorMessage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        FIRAuthentication.authObj.authForgotPassword(userEmail: emailTextField.text, controller: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTextFieldTag = textField.tag + 1
        // This will try to search for the next texfield that contains the delegate
        // and switch until it runs out of options.
        let nextResponder = textField.superview?.viewWithTag(nextTextFieldTag) as UIResponder?
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return false
    }
    
    // This will handle the touching upon anywhere else to hide the keyboard when not needed.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
}


