//
//  homeViewController.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/14/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class homeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionViewAll: UICollectionView!
    @IBOutlet weak var allClubsBttn: UIButton!
    @IBOutlet weak var favClubsBttn: UIButton!
    @IBOutlet weak var favsLbl: UILabel!
    @IBOutlet weak var allLbl: UILabel!
    
    var allClubs = [(name: String,image: UIImage,id: String)]()
    var favClubs = [(name: String,image: UIImage,id: String)]()
    var arrayToShow = [(name: String,image: UIImage,id: String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        allClubs.removeAll()
        favClubs.removeAll()
        self.pullCommentsDB()
        self.pullFavs()
        favsLbl.isHidden = true
        allLbl.isHidden = false
        DispatchQueue.main.async {
            self.arrayToShow = self.allClubs
            self.collectionViewAll.reloadData()
        }
    }
    
    let userId: String? = FIRAuthentication.authObj.currentUserIdString
    
    
    @IBAction func IDCardPressed(_ sender: UIBarButtonItem) {
        var filter:CIFilter!
        
        
        if let text = userId {
            print(text)
            let data = text.data(using: .ascii, allowLossyConversion: false)
            filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            let image = UIImage(ciImage: filter.outputImage!.transformed(by: transform))
            
            let alert = UIAlertController(title: "ID CARD", message: "\(text)\n\n\n\n\n\n\n", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            let imgViewTitle = UIImageView(frame: CGRect(x: 80, y: 70, width: 110, height: 110))
            imgViewTitle.image = image
            
            alert.view.addSubview(imgViewTitle)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        
        return arrayToShow.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        //create a cell
        let cell: cardsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "card_1", for: indexPath) as! cardsCollectionViewCell
        //configure a cell
        cell.clubName.text = arrayToShow[indexPath.row].name
        cell.clubImage.image = arrayToShow[indexPath.row].image
        cell.clubImage.contentMode = .scaleAspectFit
        //return a cell
        return cell
        
        
    }
    
    
    
    @IBAction func AllClubsPressed(_ sender: Any) {
        arrayToShow = allClubs
        
        allLbl.isHidden = false
        favsLbl.isHidden = true
        DispatchQueue.main.async {
            self.collectionViewAll.reloadData()
        }
    }
    
    @IBAction func favClubsPressed(_ sender: UIButton) {
        
        arrayToShow = favClubs
        
        allLbl.isHidden = true
        favsLbl.isHidden = false
        DispatchQueue.main.async {
            self.collectionViewAll.reloadData()
        }
    }
    
    func pullCommentsDB() {
        let db = Firestore.firestore()
        db.collection("clubs").getDocuments { (snapshot, error) in
            if error != nil{
                print(error ?? "error")
                
            } else {
                for document in (snapshot?.documents)!{
                    var image = UIImage()
                    guard let name = document.data()["club_name"] as? String else{print("error: club_name"); return }
                    image = self.downloadPPImage(ID: document.documentID)
                    
                    self.allClubs.append((name ,image , document.documentID))
                    print("added")
                    
                }
                DispatchQueue.main.async {
                    self.collectionViewAll.reloadData()
                }
            }
        }
        
    }
    
    func pullFavs() {
        let db = Firestore.firestore()
        db.collection("favorites").document(FIRAuthentication.authObj.currentUserIdString).collection("addedClubs").getDocuments { (snapshot, error) in
            if error != nil{
                print(error ?? "error")
                
            } else {
                for document in (snapshot?.documents)!{
                    guard let theclub = document.data()["club_id"] as? String else{print("error: clubId"); return }
                    self.clubSettingsPullFromDatabase(idString: theclub)
                    print(theclub)
                }
                DispatchQueue.main.async {
                    self.collectionViewAll.reloadData()
                }
            }
            
        }
    }
    
    func clubSettingsPullFromDatabase(idString: String) {
        
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(idString)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                guard let clubName = clubDocument["club_name"] as? String else {print("FAIL: clubName"); return }
                let image = self.downloadPPImage(ID: idString)
                self.favClubs.append((name: clubName , image: image, id: idString))
                print("\(self.favClubs.count)"  + "   " + self.favClubs.description)
                
            }
        }
        
    }
    
    func downloadPPImage(ID: String) -> UIImage{
        var theimage = UIImage()
        let storageRef = Storage.storage().reference().child("userProfilePicture")
        let downloadImageRef = storageRef.child(ID)
        _ = downloadImageRef.getData(maxSize: 1024 * 10 * 12) { (data, error) in
            if let data = data {
                if let image = UIImage(data: data){
                    theimage = image
                }else{
                    theimage = #imageLiteral(resourceName: "pca_icon")
                }
                
            }
        }
        return theimage
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = collectionViewAll.indexPathsForSelectedItems?.first{
            let idToSend = allClubs[indexPath.row].id
            
            if let destination = segue.destination as? ClubProfileUserClientSideViewController {
                destination.clubId = idToSend
            }
        }
        
        
    }
    
    
    
}


