//
//  UserSignUpViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/16/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class UserSignUpViewController: UIViewController, UITextFieldDelegate {

    // UserSignUpViewController Outlets
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    @IBOutlet weak var dobButtonOutlet: UIButton!
    @IBOutlet weak var getStartedButtonOutlet: UIButton!
    
    var userEmailString: String?
    var userPasswordString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let userEmail = userEmailString, let userPassword = userPasswordString {
            emailTextField.text = userEmail.description
            passwordtextField.text = userPassword.description
        }
        // This will set the texview delegates to each to jump through
        // fields using the return key from the keyboard.
        textFieldDelegation()
        // The intial method call that will setup the textfield and
        // button handlers for validation and state changes.
        textFiedlHandlerSetUp()
    }
    
    // Delegate assigment to the texfields.
    func textFieldDelegation() {
        fullNameTextField.delegate = self
        usernameTextField.delegate = self
        emailTextField.delegate = self
        passwordtextField.delegate = self
    }
    
    // This method will add targets to the texfields to see live updates of the textfield
    // while editing, once it complyes it will turn the button on to perform the
    // sign up, however, it needs to validate the conditional chain.
    func textFiedlHandlerSetUp() {
        // Target Addition To TextFields With Selector Functions.
        fullNameTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        passwordtextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        // Button State Changes That Will Be Handled By The Target Functions.
        getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
        getStartedButtonOutlet.isUserInteractionEnabled = false
    }
    
    // This is the target function that will be validating if the texfields are nil or empty, once the
    // condiotanl are true it will set the get startetd button state back to its normal and on state.
    @objc func textFieldEditing() {
        guard let fullName = fullNameTextField.text, !fullName.isEmpty,
              let username = usernameTextField.text, !username.isEmpty,
              let email = emailTextField.text, !email.isEmpty,
              let password = passwordtextField.text, !password.isEmpty,
              let dob = dobButtonOutlet.currentTitle, !dob.isEmpty
              else { getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
                     getStartedButtonOutlet.isUserInteractionEnabled = false; return}
        getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        getStartedButtonOutlet.isUserInteractionEnabled = true
    }
    
    // This is a multplie block conditonal that will validate for all textfields
    // in order to compy with signup policy.
    func textFieldErrorHandelingContidionals() -> Bool {
        // The first conditional will validate the fullname, it can't be blank, it
        //must have spaces between strings since its taking fullname.
        if fullNameTextField.text!.count > 0 && fullNameTextField.text!.contains(" ") && fullNameTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
            // The username field is going to be validated for not having whitespaces
            // nor less than the amount of characters requiered which is 4.
            if usernameTextField.text!.count > 3 && usernameTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
                // The third conditional will validate the email format using Regex that
                // will return true if the format complies with email structure.
                if emailTextField.text!.count > 1 && emailFormatValidation(email: emailTextField.text) {
                    // This fourth conditional will check for password structure using regex
                    // aswell, to build up a more secure password to avoid weak login credentials.
                    if passwordtextField.text!.count > 6 && passwordFormatValidation(password: passwordtextField.text) {
                        // This will validate the title of the button and see if there is a date in
                        // the title if not it would not complete the chain of condiotnal and return false.
                        if dobButtonOutlet.currentTitle != " date of birth" {
                            return true
                        } else { errorHandelingAlertView(errorMessage: "Date Of Birth Not Selected") }
                        // Dob Error
                    } else { errorHandelingAlertView(errorMessage: "Password Not Strong Enough") }
                    // Password Error
                } else { errorHandelingAlertView(errorMessage: "Email Format Error") }
                // Email Error
            } else { errorHandelingAlertView(errorMessage: "Username Must Have More Than 4 Characters") }
            // Full Name Error
        } else { errorHandelingAlertView(errorMessage: "Full Name Must Contain First & Last Name") }
        // If any conditional fails this will fire and exit the conditional chain.
        return false
    }
    
    
    func passwordFormatValidation(password: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let password = password {
            // This regex validates the password structure, it needs Uppercase,
            // lowercase and numbers. Must be more than 7 characters in order to validate as well.
            let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{7,}$"
            // This will do the comparison/evaluation of the regex vs the password
            // to see if both comply and match paramters.
            return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
        }
        return false
    }
    
    func emailFormatValidation(email: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let email = email {
            // This regex validates the email structure, strins,
            // @ sign and . plus string.
            let emailregex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            // This will do the comparison/evaluation of the regex vs the email
            // to see if both comply and match paramters.
            return NSPredicate(format:"SELF MATCHES %@", emailregex).evaluate(with: email)
        }
        return false
    }
    
    // This is the custom alert view which is being recycled to handle the
    // error alerts that will fire if the condiontal chain for signup validation fails and returns false.
    func errorHandelingAlertView(errorMessage: String) {
        let alertView = UIAlertController(title: "Error!", message: "\(errorMessage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    
    // This is the signUp Button that will call firebase and sign up a user to the
    // backend, it is being already optional bidnded to see if the values being
    // passed are not nil and it will only run if the condtional chain is succesful.
    @IBAction func getStartedButtonTapped(_ sender: UIButton) {
        if textFieldErrorHandelingContidionals() {
            // If true this will use the singleton instance of the Firebase
            // Authentication Helper Class call its methods.
            FIRAuthentication.authObj.authSignUpUser(userFullName: fullNameTextField.text, userEmail: emailTextField.text, userPassword: passwordtextField.text, username: usernameTextField.text, userDOB: dobButtonOutlet.currentTitle)
        }
    }
    
    // This will act as a UITextField Animation Since The TF can take the values but needs multiple targets,
    // to avoid we use a button in the shape as a field to capture the date.
    @IBAction func dateSelector() {
        // Here we create the date picker for the age with simple sizes, the origin will be zero and the
        // width will be the device screen, however, it will have a fixed height to avoid display complications.
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 260))
        datePicker.datePickerMode = UIDatePickerMode.date
        // Here we create a dateSelected OBJC function to target the values of the date picker and assign it to the button title.
        // This will actually give us the live value since its event is value changed.
        datePicker.addTarget(self, action: #selector(dateSelected(datePicker:)), for: UIControlEvents.valueChanged)
        // We Will use the action sheet style since it looks cleaner and it is in the bottom, therfore
        // the view is not completly interrupted by the modal view.
        let alertController = UIAlertController(title: "Select Date Of Birth", message: nil , preferredStyle: .actionSheet)
        // This is where we add the datepicker Object to the alert view's subview.
        alertController.view.addSubview(datePicker)
        // This action will be displayed as a cancel since the cancel style will not be attahced to the view, it contains a gap.
        let selected = UIAlertAction(title: "Select Date", style: .cancel) { (alert) in
            self.dateSelected(datePicker: datePicker)
        }
        //add button to action sheet
        alertController.addAction(selected)
        // Here we are settings some constraints that will comply to the action sheet view, its heghit is equal to the sheet height.
        let height: NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 300)
        // Adding the constraint to the view.
        alertController.view.addConstraint(height)
        // Present the Sheet Controller.
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func dateSelected(datePicker: UIDatePicker) {
        // This will create a date format object that will manipulate the format of the date being returned.
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "  MMM dd, YYYY"
        let currentDateSelected = dateFormatter.string(from: datePicker.date)
        // Since our button will act as textfield we will have to animate the changes of the "Field." equaling
        // the textfield properties.
        dobButtonOutlet.setTitle(currentDateSelected.description, for: .normal)
        dobButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        dobButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
    }
    
    // This will dismiss the view and unwind back to the login screen.
    @IBAction func accountExistButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTextFieldTag = textField.tag + 1
        // This will try to search for the next texfield that contains the delegate
        // and switch until it runs out of options.
        let nextResponder = textField.superview?.viewWithTag(nextTextFieldTag) as UIResponder?
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return false
    }
    
    // This will handle the touching upon anywhere else to hide the keyboard when not needed.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
