//
//  SettingsVC.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/19/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase
import UIKit

class UserSettingsVC: UIViewController {
    
  

    @IBAction func SignOutPressed(_ sender: Any) {
        
        
            do {
                let userDefaults = UserDefaults.standard
                userDefaults.removeObject(forKey: "email")
                userDefaults.removeObject(forKey: "password")
                userDefaults.removeObject(forKey: "accountType")
                try Auth.auth().signOut()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initalViewController = storyboard.instantiateViewController(withIdentifier: "MainLoginStoryboard")
                self.present(initalViewController, animated: true, completion: nil)
            } catch {
                print(error)
        }
    }
    
}
