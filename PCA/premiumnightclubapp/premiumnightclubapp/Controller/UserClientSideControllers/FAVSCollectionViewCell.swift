//
//  FAVSCollectionViewCell.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/22/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class FAVSCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
}
