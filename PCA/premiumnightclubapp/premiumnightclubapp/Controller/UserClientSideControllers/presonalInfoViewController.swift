//
//  presonalInfoViewController.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/15/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseAuth
import UIKit

class personalInfoController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var userId: UITextField!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var editbutton: UIBarButtonItem!
    
    let storageRef = Storage.storage().reference().child("userProfilePicture")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userSettingsPullFromDatabase()
        downloadPPImage()
        
        
    }
    
    
    @IBAction func Edittapped(_ sender: UIBarButtonItem) {
        if editbutton.title == "Edit"{
            nameTxt.isEnabled = true
            usernameTxt.isEnabled = true
            emailTxt.isEnabled = true
            editbutton.title = "Save"
        }else{
            userSettingsPushToDatabase()
        }
        
    }
    
    func userSettingsPushToDatabase() {
        if textFieldErrorHandelingContidionals(){
            editbutton.title = "Edit"
            FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
                [ "fullName": nameTxt.text!,
                  "username": usernameTxt.text!,
                  "email": emailTxt.text!
                ] , completion: { (error) in
                    if let err = error {
                        print("Error writing document: \(err)")
                    } else {
                        
                    }
            })
            updated()
        }
    }
    
    func userSettingsPullFromDatabase() {
        let userDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        userDocumentReference.getDocument { (document, error) in
            if let userDocument = document?.data() {
                
                guard let fullName = userDocument["fullName"] as? String else {print("FAIL: fullName"); return }
                self.nameTxt.text = fullName
                guard let userName = userDocument["username"] as? String else {print("FAIL: username"); return }
                self.usernameTxt.text = userName
                guard let email = userDocument["email"] as? String else {print("FAIL: email"); return }
                self.emailTxt.text = email
                
            }
        }
        userId.text = FIRAuthentication.authObj.currentUserIdString
        
    }
    
    func textFieldErrorHandelingContidionals() -> Bool {
        // The first conditional will validate the fullname, it can't be blank, it
        //must have spaces between strings since its taking fullname.
        if nameTxt.text!.count > 0 && nameTxt.text!.contains(" ") && nameTxt.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
            // The username field is going to be validated for not having whitespaces
            // nor less than the amount of characters requiered which is 4.
            if usernameTxt.text!.count > 3 && usernameTxt.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
                // The third conditional will validate the email format using Regex that
                // will return true if the format complies with email structure.
                if emailTxt.text!.count > 1 && emailFormatValidation(email: emailTxt.text) {
                    nameTxt.isEnabled = false
                    usernameTxt.isEnabled = false
                    emailTxt.isEnabled = false
                    return true
                } else { errorHandelingAlertView(errorMessage: "Email Format Error") }
                // Email Error
            } else { errorHandelingAlertView(errorMessage: "Username Must Have More Than 4 Characters") }
            // Full Name Error
        } else { errorHandelingAlertView(errorMessage: "Full Name Must Contain First & Last Name") }
        // If any conditional fails this will fire and exit the conditional chain.
        return false
    }
    
    func emailFormatValidation(email: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let email = email {
            // This regex validates the email structure, strins,
            // @ sign and . plus string.
            let emailregex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            // This will do the comparison/evaluation of the regex vs the email
            // to see if both comply and match paramters.
            return NSPredicate(format:"SELF MATCHES %@", emailregex).evaluate(with: email)
        }
        return false
    }
    
    // This is the custom alert view which is being recycled to handle the
    // error alerts that will fire if the condiontal chain for signup validation fails and returns false.
    func errorHandelingAlertView(errorMessage: String) {
        let alertView = UIAlertController(title: "Error!", message: "\(errorMessage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func DonePressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updated(){
        let alert = UIAlertController(title: "Data Updated", message: "Data was updated Successfully", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeImage(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        // The info dictionary contains multiple representations of the image, and this uses the original.
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set photoImageView to display the selected image.
        let image = selectedImage
        let imageData = UIImageJPEGRepresentation(image, 1)
        profileImage.image = selectedImage
        let uploadImageReference = storageRef.child(FIRAuthentication.authObj.currentUserIdString)
        let uploadTask = uploadImageReference.putData(imageData!, metadata: nil) { (metadata, error) in
            print(metadata ?? "No Metadata")
            print(error ?? "No Error")
        }
        uploadTask.resume()
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    func downloadPPImage() {
        
        let downloadImageRef = storageRef.child(FIRAuthentication.authObj.currentUserIdString)
        let dowloadTask = downloadImageRef.getData(maxSize: 1024 * 10 * 12) { (data, error) in
            if let data = data {
                let image = UIImage(data: data)
                self.profileImage.image = image
            }
        }
        dowloadTask.resume()
    }
    
    
}

