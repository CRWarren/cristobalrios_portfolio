//
//  reviewsVC.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/21/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage

class reviewsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var clubId: String?
    var commentsList = [(String, String, Int)]()
    let db = Firestore.firestore()

    @IBOutlet weak var tableView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pullCommentsDB()
        
    }
    
    @IBAction func exit(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addReview(_ sender: UIBarButtonItem) {
        
        let alertVC = UIAlertController(title: "Add Review", message: "Feel free to leave an honest review!", preferredStyle: .alert)
        alertVC.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter review"
        }
        alertVC.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "grade your experience 1-5"
        }
        
        let save = UIAlertAction(title: "Save", style: .default, handler: {
            (alert) -> Void in
            
            let textField = alertVC.textFields![0]
            let textField2 = alertVC.textFields![1]
            if textField.text!.count > 0{
                if textField2.text!.count > 0{
                    if let number = Int(textField2.text!){
                        self.pushCommentToDB(userComment: textField.text!, rating: number)
                        self.commentsList.removeAll()
                        self.pullCommentsDB()
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    
                }
            }
            
        })
        alertVC.addAction(save)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
  
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        //create a cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Comments", for: indexPath) as! CollectionViewCell
        //configure a cell
        cell.username.text = commentsList[indexPath.row].0
        cell.commentLbl.text = commentsList[indexPath.row].1
        switch commentsList[indexPath.row].2 {
        case 1:
            cell.stars[0].isHidden = false
        case 2:
            cell.stars[0].isHidden = false
            cell.stars[1].isHidden = false
        case 3:
            cell.stars[0].isHidden = false
            cell.stars[1].isHidden = false
            cell.stars[2].isHidden = false
        case 4:
            cell.stars[0].isHidden = false
            cell.stars[1].isHidden = false
            cell.stars[2].isHidden = false
            cell.stars[3].isHidden = false
        case 5:
            cell.stars[0].isHidden = false
            cell.stars[1].isHidden = false
            cell.stars[2].isHidden = false
            cell.stars[3].isHidden = false
            cell.stars[4].isHidden = false
        default:
            cell.stars[0].isHidden = false
        }
        //return a cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return commentsList.count
    }
    
    
    
    
    func pullCommentsDB() {
        let db = Firestore.firestore()
         guard let newClubId = self.clubId else {print("failed: CLub Id"); return }
    
        db.collection("comments").document(newClubId).collection("postedComments").getDocuments { (snapshot, error) in
            if error != nil{
                print(error ?? "Error")
                
            } else {
                for document in (snapshot?.documents)!{
                    guard let comment = document.data()["comment"] as? String else{print("error: comment"); return }
                    guard let username = document.data()["userName"] as? String else{print("error: name"); return }
                    guard let rating = document.data()["rating"] as? Int
                        else{print("error: Rating"); return }
                    self.commentsList.append((username, comment, rating))
                    print("added")
                    
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }

    }
    
    func pushCommentToDB(userComment: String, rating: Int) {
       
        if userComment.count>0{
        
            let userDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)")
            userDocumentReference.getDocument { (document, error) in
                if let userDocument = document?.data() {
                    guard let userName = userDocument["username"] as? String else {print("FAIL: username"); return }
                    guard let newClubId = self.clubId else {print("failed: CLub Id"); return }
                    
                    FIRDatabase.dbObj.firestoreDatabase.collection("comments").document("\(newClubId)").collection("postedComments").addDocument(data: ["userName": userName, "comment": "\(userComment)", "rating": rating], completion: { (error) in
                        if let err = error {
                            print("Error writing document: \(err)")
                        } else {}
                    }
                        
                    )
                }
            }
        }
    }
    
}
