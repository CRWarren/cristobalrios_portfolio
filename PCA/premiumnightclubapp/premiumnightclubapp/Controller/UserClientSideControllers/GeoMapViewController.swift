//
//  GeoMapViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/15/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import Mapbox

class GeoMapViewController: UIViewController, MGLMapViewDelegate {

    var pointsArray = [MGLPointAnnotation()]
    
    @IBOutlet weak var mapBoxVIew: MGLMapView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let styleURL = URL(string: "mapbox://styles/premiumclubapp/cjet6yec5asaf2sp3cwjyv1ba")
        let mapView = MGLMapView(frame: view.bounds, styleURL: styleURL)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.setCenter(CLLocationCoordinate2D(latitude: 28.596222, longitude: -81.306389), zoomLevel: 9, animated: false)
        mapBoxVIew.addSubview(mapView)
        
        let annotationOne = MGLPointAnnotation()
        annotationOne.coordinate = CLLocationCoordinate2D(latitude: -28.596222, longitude: 81.306389)
        annotationOne.title = "Full Sail"
        annotationOne.subtitle = "Mobile P&P4"
        
        let annotation = MGLPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 28.596222, longitude: -81.306389)
        annotation.title = "Full Sail Univesity"
        annotation.subtitle = "Mobile Development P&P4"
        
        pointsArray.append(annotationOne)
        pointsArray.append(annotation)
        
        for (_, value) in pointsArray.enumerated() {
            mapView.addAnnotation(value)
        }

        mapView.delegate = self
        mapView.showsUserLocation = true
        // Do any additional setup after loading the view.
    }

    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        // Always allow callouts to popup when annotations are tapped.
        return true
    }
    
    // Zoom to the annotation when it is selected
//    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
//        let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 3000, pitch: 0, heading: 0)
//        mapView.setCamera(camera, animated: true)
//    }
    


}
