//
//  ClubProfileUserSideVC.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/21/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import CoreLocation
import MapKit

var clubId: String?

class ClubProfileUserClientSideViewController : UIViewController //UICollectionViewDelegate, UICollectionViewDataSource
{
    
    
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var clubDescriptionTextView: UITextView!
    @IBOutlet weak var clubContactLabel: UILabel!
    @IBOutlet weak var clubScheduleLabel: UILabel!
    @IBOutlet weak var clubDressCodeLabel: UILabel!
    @IBOutlet weak var clubCoverLabel: UILabel!
    @IBOutlet weak var clubImagesFeed: UICollectionView!
    @IBOutlet weak var clubAdressLabel: UILabel!
    @IBOutlet weak var clubImageView: UIImageView!
    
    var arrayOfImagePost = [UIImage]()
    var arrayOfImagePostURLs = [String]()
    
    var clubId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clubImageView.layer.cornerRadius = (clubImageView?.frame.height)! / 2
        clubImageView.layer.borderWidth = 4
        clubImageView.layer.borderColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        clubImageView.clipsToBounds = true
        
        DispatchQueue.main.async {
            self.clubSettingsPullFromDatabase()
            self.pullImages()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clubSettingsPullFromDatabase()
         downloadPPImage()
    }
    
    
    @IBAction func ReviewsPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "toReviews", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? reviewsVC {
            dest.clubId = clubId
        }
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func addToFavs(_ sender: UIBarButtonItem) {
        pushFavToDB()
        
        let alert = UIAlertController(title: "favorite Added", message: "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func pushFavToDB() {
        
        guard let newClubId = self.clubId else {print("failed: CLub Id"); return }
        FIRDatabase.dbObj.firestoreDatabase.collection("favorites").document("\(FIRAuthentication.authObj.currentUserIdString)").collection("addedClubs").addDocument(data: ["club_id" : newClubId])
    }
    
    func clubSettingsPullFromDatabase() {
        
        guard let clubStringId = clubId else {
            print("Failed to fetch club data"); return
        }
        
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(clubStringId)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                guard let clubName = clubDocument["club_name"] as? String else {print("FAIL: clubName"); return }
                self.navItem.title = clubName
                guard let clubAddress = clubDocument["address"] as? String else {print("FAIL: clubAddress"); return}
                self.clubAdressLabel.text = clubAddress
                
                guard let clubDescription = clubDocument["description"] as? String else {print("FAIL: clubDescription"); return}
                self.clubDescriptionTextView.text = clubDescription
                guard let clubContact = clubDocument["contact"] as? String else {print("FAIL: clubContact"); return}
                self.clubContactLabel.text = clubContact
                guard let clubSchedule = clubDocument["schedule"] as? String else {print("FAIL: clubSchedule"); return}
                self.clubScheduleLabel.text = clubSchedule
                
                guard let clubCoverLimit = clubDocument["cover_limit"] as? String else {print("FAIL: clubCoverLimit"); return}
                self.clubCoverLabel.text = clubCoverLimit
                guard let clubDressCode = clubDocument["dress_code"] as? String else {print("FAIL: clubDressCode"); return}
                self.clubDressCodeLabel.text = clubDressCode
                
                if let imageProfileURL = clubDocument["club_profile_image"] as? String {
                    if let url = URL(string: imageProfileURL) {
                        URLSession.shared.dataTask(with: url) { data, response, error in
                            guard let data = data else { return }
                            let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .userInitiated, attributes: .concurrent)
                            concurrentQueue.async {
                                DispatchQueue.main.async {
                                    self.clubImageView.image = UIImage(data: data)
                                }
                            }
                            }.resume()
                    }
                }
            }
            
            
        }
        
    }
    
    @IBAction func mapsLaunch(_ sender: UIBarButtonItem) {
        openMapForPlace()
    }
    
    func downloadPPImage() {
        print(FIRAuthentication.authObj.currentUserIdString)
        let storageRef = Storage.storage().reference().child("userProfilePicture")
        let downloadImageRef = storageRef.child(clubId!)
        let downloadTask = downloadImageRef.getData(maxSize: 1024 * 10 * 12) { (data, error) in
            if let data = data {
                let image = UIImage(data: data)
                self.clubImageView.image = image
            }
        }
        downloadTask.resume()
        
    }

    
    func openMapForPlace() {
        
        guard let clubStringId = clubId else {
            print("Failed to fetch club data"); return
        }
        
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(clubStringId)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                guard let clubName = clubDocument["club_name"] as? String else {print("FAIL: clubName"); return }
                guard let clubAddress = clubDocument["address"] as? String else {print("FAIL: clubAddress"); return}
                
                let geocoder = CLGeocoder()
                let address = clubAddress
                geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                    if((error) != nil){
                        print("Error", error ?? "")
                    }
                    if let placemark = placemarks?.first {
                        let coordinates: CLLocationCoordinate2D = placemark.location!.coordinate
                        let regionDistance:CLLocationDistance = 10000
                        
                        let coordinatesMake = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
                        
                        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinatesMake, regionDistance, regionDistance)
                        
                        let options = [
                            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                        ]
                        
                        let placemark = MKPlacemark(coordinate: coordinatesMake, addressDictionary: nil)
                        let mapItem = MKMapItem(placemark: placemark)
                        mapItem.name = clubName
                        mapItem.openInMaps(launchOptions: options)
                        
                    }
                })
                
            }
            
            
            
        }
   
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfImagePost.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clubImagesFeed.dequeueReusableCell(withReuseIdentifier: "clubImageCellIdentifier", for: indexPath) as! ClubCustomCollectionViewCell
        
        DispatchQueue.main.async {
            cell.clubPostImageView.image = self.arrayOfImagePost[indexPath.row]
            
        }
        
        
        return cell
        
    }
    
    func pullImages() {
        
        guard let clubStringId = clubId else {
            print("Failed to fetch club data"); return
        }
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(clubStringId)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                if let clubImages = clubDocument["club_collection_images"] as? [String] {
                    self.arrayOfImagePost = []
                    for each in clubImages {
                        if let url = URL(string: each) {
                            URLSession.shared.dataTask(with: url) { data, response, error in
                                guard let data = data else { return }
                                
                                let images = UIImage(data: data)
                                if let image = images {
                                    self.arrayOfImagePost.append(image)
                                }
                                
                                
                                }.resume()
                        }
                    }
                } else {
                    print("Fail Images")
                }
            }
        }
    }
}
