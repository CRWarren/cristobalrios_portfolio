//
//  PaymentMethodController.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/15/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import UIKit

class PaymentMethodVC: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var cardTxt: UITextField!
    @IBOutlet weak var monthTxt: UITextField!
    @IBOutlet weak var yearTxt: UITextField!
    @IBOutlet weak var secCodeTxt: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTxt.delegate = self
        cardTxt.delegate = self
        monthTxt.delegate = self
        yearTxt.delegate = self
        secCodeTxt.delegate = self
        
    }
    
    
    @IBAction func savepressed(_ sender: UIButton) {
        if isValidName() && containsNumbersOnly(text: cardTxt.text!) && containsNumbersOnly(text: monthTxt.text!) && containsNumbersOnly(text: yearTxt.text!) && containsNumbersOnly(text: secCodeTxt.text!){
            savedAlert()
        }else{
            alert()
        }
        
    }
    
    
    func isValidName() -> Bool {
        //this regex validates that the textfield only contains letters and spaces
        let nameRegex = "([A-Za-z ]+)"
        //makes the comparison between the string and the regex pattern and returns a bool
        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: nameTxt.text!)
    }
    //^[0-9]+$
    func containsNumbersOnly(text: String) -> Bool{
        //this regex validates that the textfield only numberss
        let nameRegex = "^[0-9]+$"
        //makes the comparison between the string and the regex pattern and returns a bool
        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: text)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text ?? ""
        //this counts the characters in the string
        let creditLength = text.count + string.count - range.length
        
        //this will change the max value depending on the tag number of the texfield
        var maxRange = 0
        switch textField.tag {
        case 0:
            maxRange = 100
        case 1:
            maxRange = 16
        case 2:
            maxRange = 2
        case 3:
            maxRange = 2
        case 4:
            maxRange = 4
        default:
            maxRange = 50
        }
        
        return creditLength <= maxRange // this sets the value of the max characters you want
    }
    
    func alert(){
        let alert = UIAlertController(title: "Something went wrong", message: "Please validate Credit Card Informartion", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func savedAlert() {
        let alert = UIAlertController(title: "Success!", message: "Payment method was updated successfully", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler:  {
            (alert) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


