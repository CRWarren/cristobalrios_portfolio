//
//  cardsCollectionViewCell.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/21/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class cardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var clubImage: UIImageView!
    @IBOutlet weak var clubName: UILabel!
    
}
