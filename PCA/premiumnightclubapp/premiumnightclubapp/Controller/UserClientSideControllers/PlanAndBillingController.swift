//
//  PlanAndBillingController.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/15/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import UIKit

class PlanAndBillingVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userSettingsPullFromDatabase()
    }
    var activeUser = false
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var subscriptionLabel: UILabel!
    
    
    @IBAction func changeAddresspressed(_ sender: UIButton) {
        
        let alertVC = UIAlertController(title: "Change Billing Address", message: "", preferredStyle: .alert)
        alertVC.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Street Address with zip code"
        }
        alertVC.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter City, state"
        }
        let save = UIAlertAction(title: "Update", style: .default, handler: {
            (alert) -> Void in
            
            let textField = alertVC.textFields![0] as UITextField!
            let textField2 = alertVC.textFields![1] as UITextField!
            
            self.streetLabel.text = textField!.text! + " " + textField2!.text!
            
            self.userSettingsPushToDatabaseAddress()
            
        })
        alertVC.addAction(save)
        
        self.present(alertVC, animated: true, completion: nil)
        
        
    }
    
    func userSettingsPushToDatabaseSubscription() {
        FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
            [ "ActiveUser": activeUser
            ] , completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {
                    
                }
        })
    }
    
    func userSettingsPushToDatabaseAddress() {
        FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
            [ "address": streetLabel.text!
            ] , completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {
                    
                }
        })
    }
    
    func userSettingsPullFromDatabase() {
        let userDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("users").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        userDocumentReference.getDocument { (document, error) in
            if let userDocument = document?.data() {
                
                guard let active = userDocument["ActiveUser"] as? Bool else {print("FAIL: ActiveUser"); return }
                if active{
                    self.activeUser = active
                    self.subscriptionLabel.text = "Basic Plan - $50.00"
                }else {
                    self.subscriptionLabel.text = "Basic Plan - Not Active"
                    self.activeUser = active
                }
                guard let address = userDocument["address"] as? String else {print("Fail: Address"); return}
                self.streetLabel.text = address
                
            }
        }
        
    }
    
    @IBAction func changeSubscriptionpressed(_ sender: UIButton) {
        if activeUser{
            let alertVC = UIAlertController(title: "Change subscription", message: "Basic Plan - $50.00\nType Stop to cancel subscription", preferredStyle: .alert)
            
            alertVC.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Type Stop"
            }
            let save = UIAlertAction(title: "Update", style: .default, handler: {
                (alert) -> Void in
                
                let textField = alertVC.textFields![0] as UITextField!
                
                if textField!.text!.uppercased() == "STOP"{
                    self.subscriptionLabel.text = "Basic Plan - Canceled"
                    self.activeUser = false
                    self.userSettingsPushToDatabaseSubscription()
                }
                
                
            })
            let Back = UIAlertAction(title: "Back", style: .default, handler: nil)
            alertVC.addAction(Back)
            alertVC.addAction(save)
            
            self.present(alertVC, animated: true, completion: nil)
        }else {
            let alertVC = UIAlertController(title: "Start subscription", message: "Basic Plan - $50.00\nType Start to Begin subscription", preferredStyle: .alert)
            
            alertVC.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Type Start"
            }
            let save = UIAlertAction(title: "Update", style: .default, handler: {
                (alert) -> Void in
                
                let textField = alertVC.textFields![0] as UITextField!
                if textField!.text!.uppercased() == "START"{
                    self.subscriptionLabel.text = "Basic Plan - $50.00"
                    self.activeUser = true
                    self.userSettingsPushToDatabaseSubscription()
                }
                
                
            })
            let Back = UIAlertAction(title: "Back", style: .default, handler: nil)
            alertVC.addAction(Back)
            alertVC.addAction(save)
            
            self.present(alertVC, animated: true, completion: nil)
        }
        
        
        
    }
    @IBAction func DonePressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

