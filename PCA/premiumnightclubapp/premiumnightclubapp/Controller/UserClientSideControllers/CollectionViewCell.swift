//
//  CollectionViewCell.swift
//  premiumnightclubapp
//
//  Created by cristobal rios on 3/21/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var commentLbl: UITextView!
    
    @IBOutlet var stars: [UIImageView]!
    
    
    
}
