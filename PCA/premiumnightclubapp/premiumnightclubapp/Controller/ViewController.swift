//
//  ViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/8/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var clubSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func signUpPressed(_ sender: UIButton) {
        if clubSwitch.isOn{
            self.performSegue(withIdentifier: "ClubSignUp", sender: nil)
        }else{
            self.performSegue(withIdentifier: "UserSignUp", sender: nil)
        }
        
    }
    
    @IBAction func signInButton(_ sender: UIButton) {
        
//        guard let email = emailTxt.text,
//            let pass = passwordTxt.text
//            else {return}
        
        if clubSwitch.isOn{
            self.performSegue(withIdentifier: "ClubLogIn", sender: nil)
        }else{
            self.performSegue(withIdentifier: "UserLogIn", sender: nil)
        }
        
        //Authentication.authSignIn(userEmail: email, userPassword: pass, switchOn: clubSwitch.isOn)
        
    }
    
    
}

