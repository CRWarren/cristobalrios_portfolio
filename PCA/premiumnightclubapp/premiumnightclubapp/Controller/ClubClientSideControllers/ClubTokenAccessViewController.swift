//
//  ClubTokenAccessViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/17/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import AVFoundation

class ClubTokenAccessViewController: UIViewController {

    
    @IBOutlet weak var clubTokenAccessTextField: UITextField!
    
    @IBOutlet weak var accessClubButtonOutlet: UIButton!
    
    
    var qrFilter: CIFilter!
    
    var signedUpTokenString: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let token = self.signedUpTokenString {
                self.clubQRTokenGenerator(token: token)
            }
        }
  
    }
    
    func clubQRTokenGenerator(token: String) {
        
        let data = token.data(using: .ascii, allowLossyConversion: false)
        qrFilter = CIFilter(name: "CIQRCodeGenerator")
        
        qrFilter.setValue(data, forKey: "inputMessage")
        let transformedDataObject = CGAffineTransform(scaleX: 10, y: 10)
        
        let qrTokenImage = UIImage(ciImage: qrFilter.outputImage!.transformed(by: transformedDataObject))
        
        let qrTokenAlertView = UIAlertController(title: "Access Token", message: "\(token)\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        
        let copyQrAction = UIAlertAction(title: "Save", style: .default) { (alert) in
            UIPasteboard.general.string = token.description
        }
        let qrTokenDimensions = UIImageView(frame: CGRect(x: 80, y: 70, width: 110, height: 110))
        qrTokenDimensions.image = qrTokenImage
        
        qrTokenAlertView.view.addSubview(qrTokenDimensions)
        qrTokenAlertView.addAction(copyQrAction)
        
        self.present(qrTokenAlertView, animated: true, completion: nil)
        
    }


    // This will handle the touching upon anywhere else to hide the keyboard when not needed.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func accessClubButtontapped(_ sender: UIButton) {
        validateTokenSignIn()
    }
    
    
    @IBAction func returnKey(_ sender: UITextField) {
        validateTokenSignIn()
        sender.resignFirstResponder()
    }
    
    func validateTokenSignIn(){
        
        let clubTokenDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        clubTokenDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                guard let token = clubDocument["token"] as? String else {return}
                
                if token == self.clubTokenAccessTextField.text {
                    print("Token Validated: \(token)")
                    self.performSegue(withIdentifier: "ClubProfileView", sender: nil)
                } else if self.clubTokenAccessTextField.text?.isEmpty == true {
                    self.errorHandelingAlertView(errorMessage: "Empty Token Field")
                } else {
                    self.errorHandelingAlertView(errorMessage: "Invalid Token")
                }
            } else {
                self.errorHandelingAlertView(errorMessage: error!.localizedDescription)
            }
        }
    }
    

    // This is the custom alert view which is being recycled to handle the
    // error alerts that will fire if the condiontal chain for signup validation fails and returns false.
    func errorHandelingAlertView(errorMessage: String) {
        let alertView = UIAlertController(title: "Error!", message: "\(errorMessage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    
    // This will dismiss the view and unwind back to the login screen.
    @IBAction func noAccountExistButtonTapped(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initalViewController = storyboard.instantiateViewController(withIdentifier: "MainLoginStoryboard")
            self.present(initalViewController, animated: true, completion: nil)
        } catch {
            print(error)
        }
    }
}
