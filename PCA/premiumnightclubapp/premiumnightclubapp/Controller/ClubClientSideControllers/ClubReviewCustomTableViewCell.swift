//
//  ClubReviewCustomTableViewCell.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/23/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class ClubReviewCustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var clubOrProfileImageView: UIImageView!
    
    @IBOutlet weak var clubOrUserTitleName: UILabel!
    
    @IBOutlet weak var clubOrUserReviewLabel: UILabel!
    
    @IBOutlet var clubRatingValueCollection: [UIImageView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clubOrProfileImageView.layer.cornerRadius = (clubOrProfileImageView?.frame.height)! / 2
        clubOrProfileImageView.layer.borderWidth = 2.5
        clubOrProfileImageView.layer.borderColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        clubOrProfileImageView.clipsToBounds = true
        
        
    }
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
