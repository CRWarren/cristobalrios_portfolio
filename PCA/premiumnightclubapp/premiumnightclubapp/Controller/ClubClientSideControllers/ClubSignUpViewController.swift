//
//  ClubSignUpViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/16/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import AVFoundation

class ClubSignUpViewController: UIViewController, UITextFieldDelegate {

    // UserSignUpViewController Outlets
    @IBOutlet weak var clubNameTextField: UITextField!
    @IBOutlet weak var clubAdressTextField: UITextField!
    @IBOutlet weak var clubEmailTextField: UITextField!
    @IBOutlet weak var clubPasswordtextField: UITextField!
    @IBOutlet weak var getStartedButtonOutlet: UIButton!
    
    // Qr Values For Generator
    var tokenString: String = ""
    
    var clubEmailString: String?
    var clubPasswordString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let clubEmail = clubEmailString, let clubPassword = clubPasswordString {
            clubEmailTextField.text = clubEmail.description
            clubPasswordtextField.text = clubPassword.description
        }
        // This will set the texview delegates to each to jump through
        // fields using the return key from the keyboard.
        textFieldDelegation()
        // The intial method call that will setup the textfield and
        // button handlers for validation and state changes.
        textFiedlHandlerSetUp()
    }
    
    // Delegate assigment to the texfields.
    func textFieldDelegation() {
        clubNameTextField.delegate = self
        clubAdressTextField.delegate = self
        clubEmailTextField.delegate = self
        clubPasswordtextField.delegate = self
    }
    
    // This method will add targets to the texfields to see live updates of the textfield
    // while editing, once it complyes it will turn the button on to perform the
    // sign up, however, it needs to validate the conditional chain.
    func textFiedlHandlerSetUp() {
        // Target Addition To TextFields With Selector Functions.
        clubNameTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        clubAdressTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        clubEmailTextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        clubPasswordtextField.addTarget(self, action: #selector(textFieldEditing), for: .editingChanged)
        // Button State Changes That Will Be Handled By The Target Functions.
        getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
        getStartedButtonOutlet.isUserInteractionEnabled = false
    }
    
    // This is the target function that will be validating if the texfields are nil or empty, once the
    // condiotanl are true it will set the get startetd button state back to its normal and on state.
    @objc func textFieldEditing() {
        guard let fullName = clubNameTextField.text, !fullName.isEmpty,
            let username = clubAdressTextField.text, !username.isEmpty,
            let email = clubEmailTextField.text, !email.isEmpty,
            let password = clubPasswordtextField.text, !password.isEmpty
            else { getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .normal)
                getStartedButtonOutlet.isUserInteractionEnabled = false; return}
        getStartedButtonOutlet.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        getStartedButtonOutlet.isUserInteractionEnabled = true
    }
    
    // This is a multplie block conditonal that will validate for all textfields
    // in order to compy with signup policy.
    func textFieldErrorHandelingContidionals() -> Bool {
        // The first conditional will validate the fullname, it can't be blank, it
        //must have spaces between strings since its taking fullname.
        if clubNameTextField.text!.count > 0 || clubNameTextField.text!.contains(" ") && clubNameTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
            // The username field is going to be validated for not having whitespaces
            // nor less than the amount of characters requiered which is 4.
            if clubAdressTextField.text!.contains(" ") && clubAdressTextField.text!.trimmingCharacters(in: .whitespaces).isEmpty == false {
                // The third conditional will validate the email format using Regex that
                // will return true if the format complies with email structure.
                if clubEmailTextField.text!.count > 1 && emailFormatValidation(email: clubEmailTextField.text) {
                    // This fourth conditional will check for password structure using regex
                    // aswell, to build up a more secure password to avoid weak login credentials.
                    if clubPasswordtextField.text!.count > 6 && passwordFormatValidation(password: clubPasswordtextField.text) {
                         return true
                        // Dob Error
                    } else { errorHandelingAlertView(errorMessage: "Password Not Strong Enough") }
                    // Password Error
                } else { errorHandelingAlertView(errorMessage: "Email Format Error") }
                // Email Error
            } else { errorHandelingAlertView(errorMessage: "Adress Format Error") }
            // Full Name Error
        } else { errorHandelingAlertView(errorMessage: "Club Name Format Error") }
        // If any conditional fails this will fire and exit the conditional chain.
        return false
    }
    
    
    func passwordFormatValidation(password: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let password = password {
            // This regex validates the password structure, it needs Uppercase,
            // lowercase and numbers. Must be more than 7 characters in order to validate as well.
            let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{7,}$"
            // This will do the comparison/evaluation of the regex vs the password
            // to see if both comply and match paramters.
            return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
        }
        return false
    }
    
    func emailFormatValidation(email: String?) -> Bool {
        // This will validate id the textfield beign passed in is not nil to enter
        // the regex validation block to return thr predicate bool
        if let email = email {
            // This regex validates the email structure, strins,
            // @ sign and . plus string.
            let emailregex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            // This will do the comparison/evaluation of the regex vs the email
            // to see if both comply and match paramters.
            return NSPredicate(format:"SELF MATCHES %@", emailregex).evaluate(with: email)
        }
        return false
    }
    
    // This is the custom alert view which is being recycled to handle the
    // error alerts that will fire if the condiontal chain for signup validation fails and returns false.
    func errorHandelingAlertView(errorMessage: String) {
        let alertView = UIAlertController(title: "Error!", message: "\(errorMessage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    
    // This is the signUp Button that will call firebase and sign up a user to the
    // backend, it is being already optional bidnded to see if the values being
    // passed are not nil and it will only run if the condtional chain is succesful.
    @IBAction func getStartedButtonTapped(_ sender: UIButton) {
        if textFieldErrorHandelingContidionals() {
            // If true this will use the singleton instance of the Firebase
            // Authentication Helper Class call its methods.
            tokenString = clubTokenGenerator(tokenLength: 20)
            FIRAuthentication.authObj.authSignUpClub(clubName: clubNameTextField.text, clubAddress: clubAdressTextField.text, clubEmail: clubEmailTextField.text, clubPassword: clubPasswordtextField.text, clubToken: tokenString)
            self.performSegue(withIdentifier: "ClubTokenAccessSegue", sender: nil)
        }
    }
    
    // This will dismiss the view and unwind back to the login screen.
    @IBAction func accountExistButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ClubTokenAccessSegue" {
            let segueDestination = segue.destination as! ClubTokenAccessViewController
            segueDestination.signedUpTokenString = tokenString
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTextFieldTag = textField.tag + 1
        // This will try to search for the next texfield that contains the delegate
        // and switch until it runs out of options.
        let nextResponder = textField.superview?.viewWithTag(nextTextFieldTag) as UIResponder?
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    func clubTokenGenerator(tokenLength: Int) -> String {
        let alphaNumericalValues = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var tokenHoldString: String = ""
        
        for _ in 0..<tokenLength {
            let recycler = arc4random_uniform(UInt32(alphaNumericalValues.count))
            tokenHoldString += "\(alphaNumericalValues[alphaNumericalValues.index(alphaNumericalValues.startIndex, offsetBy: Int(recycler))])"
        }
        
        return tokenHoldString
    }

    // This will handle the touching upon anywhere else to hide the keyboard when not needed.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

}
