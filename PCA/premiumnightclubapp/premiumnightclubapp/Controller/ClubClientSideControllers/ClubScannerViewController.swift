//
//  QrCodeScannerViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/13/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import AVFoundation

class ClubScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var square: UIImageView!
    
    var video: AVCaptureVideoPreviewLayer?
    var captureDevice: AVCaptureDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //create our capture session
        let session = AVCaptureSession()
        
        // define capture device
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        
        //add capture device to our sessicion
        
        do {
            //this will try get the input of what the camera is recording
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            //this will add our data scanned to the session
            session.addInput(input)
        }
        catch{
            print("Error")
        }
        
        //define output
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        //output proccessed on the main queue
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        //we want only qr codes
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        //show video -- add the video preview layer to a new sub layer in order to show video.
        video = AVCaptureVideoPreviewLayer(session: session)
        video?.frame = view.layer.bounds
        view.layer.addSublayer(video!)
        
        //this will bring the square image used to help the user aim the code
        self.view.bringSubview(toFront: square)
        
        //this will start the session, making the video appear on our view.
        session.startRunning()
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if  metadataObjects.count > 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                if object.type == AVMetadataObject.ObjectType.qr{
                    alert(value: object)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alert(value: AnyObject) {
        let alert = UIAlertController(title: "Guest Digital Id", message: value.stringValue, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Scan", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Grant Access", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }

}

