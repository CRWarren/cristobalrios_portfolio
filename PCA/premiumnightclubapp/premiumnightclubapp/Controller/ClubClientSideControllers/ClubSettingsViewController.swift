//
//  ClubSettingsViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/18/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class ClubSettingsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var clubDescriptionTextView: UITextView!
    @IBOutlet weak var clubNameTextField: UITextField!
    @IBOutlet weak var clubEmailTextField: UITextField!
    @IBOutlet weak var clubContactTextField: UITextField!
    @IBOutlet weak var clubAddressTextField: UITextField!
    @IBOutlet weak var coverLimitButtonOutlet: UIButton!
    @IBOutlet weak var scheduledHoursButtonOutlet: UIButton!
    @IBOutlet weak var dressCodeButtonOutlet: UIButton!
    @IBOutlet weak var editBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var clubImageView: UIImageView!
    
    var hoursLeft: [String] = []
    var hoursRight: [String] = []
    var dressCode: [String] = []
    var clubLimit: [String] = []
    
    var buttonTag: Int = 0
    var selectedImage: UIImage?
    
    var editBarButton: UIBarButtonItem!
    var saveBarButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerSetup()
        clubSettingsPullFromDatabase()
        pickerModelDataSetUp()
        viewControlsState(enabled: false)
    }
    
    func imagePickerSetup() {
        let imageSelector = UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView))
        clubImageView.addGestureRecognizer(imageSelector)
        clubImageView.layer.cornerRadius = (clubImageView?.frame.height)! / 2
        clubImageView.layer.borderWidth = 4
        clubImageView.layer.borderColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        clubImageView.clipsToBounds = true
  
    }
    
    @objc func handleSelectProfileImageView() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clubSettingsPullFromDatabase()
        pickerModelDataSetUp()
        viewControlsState(enabled: false)
        editBarButton = UIBarButtonItem(image:UIImage(named: "create_new"), style:.plain, target:self, action:#selector(editBarButtonTapped))
        editBarButton.tintColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        
        saveBarButton = UIBarButtonItem(image:UIImage(named: "save"), style:.plain, target:self, action:#selector(saveBarButtonTapped))
        saveBarButton.tintColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        
        self.navigationItem.rightBarButtonItem = editBarButton
        
    }
    
    func viewControlsState(enabled: Bool) {
        clubDescriptionTextView.isUserInteractionEnabled = enabled
        clubNameTextField.isUserInteractionEnabled = enabled
        clubContactTextField.isUserInteractionEnabled = enabled
        clubAddressTextField.isUserInteractionEnabled = enabled
        coverLimitButtonOutlet.isUserInteractionEnabled = enabled
        scheduledHoursButtonOutlet.isUserInteractionEnabled = enabled
        dressCodeButtonOutlet.isUserInteractionEnabled = enabled
        clubImageView.isUserInteractionEnabled = enabled
    }
    
    @objc func editBarButtonTapped(){
        self.navigationItem.rightBarButtonItem = saveBarButton
        viewControlsState(enabled: true)
    }
    
    @objc func saveBarButtonTapped(){
        self.navigationItem.rightBarButtonItem = editBarButton
        viewControlsState(enabled: false)
        clubSettingsPushToDatabase()
        clubProfileImagePushToDatabase()
        errorHandelingAlertView(title: "Saved", messsage: "Your Settings Have Been Saved!")
    }
    
    func clubProfileImagePushToDatabase() {
//        let storageRef = Storage.storage().reference(forURL: "gs://premiumnightclubapp-32cd1.appspot.com/").child("profile_images").child(FIRAuthentication.authObj.currentUserIdString)
//
//        if let profile = selectedImage, let imageData = UIImageJPEGRepresentation(profile, 0.1) {
//            storageRef.putData(imageData, metadata: nil, completion: { (metadata, err) in
//                if err != nil {
//                    return
//                } else {
//                    if let profileImageURLString = metadata?.downloadURL()?.absoluteString {
//                        FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
//                            [ "club_profile_image": profileImageURLString.description ]
//                            , completion: { (error) in
//                                if let err = error {
//                                    print("Error writing document: \(err)")
//                                } else {
//
//                                }
//                        })
//                    }
//                }
//            })
//        }
        
        let storageRef = Storage.storage().reference().child("userProfilePicture")
        if let imageSelected = selectedImage {
            self.selectedImage = imageSelected
            let image = imageSelected
            let imageData = UIImageJPEGRepresentation(image, 1)
            self.clubImageView.image = image
            let uploadImageReference = storageRef.child(FIRAuthentication.authObj.currentUserIdString)
            let uploadTask = uploadImageReference.putData(imageData!, metadata: nil) { (metadata, error) in
                print(metadata ?? "No Metadata")
                print(error ?? "No Error")
            }
            uploadTask.resume()
        }
    }
    
    func clubSettingsPushToDatabase() {
        
        FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
            [ "club_name": clubNameTextField.text!.description,
              "address": clubAddressTextField.text!.description,
              "description": clubDescriptionTextView.text.description,
              "contact": clubContactTextField.text!.description,
              "schedule" : scheduledHoursButtonOutlet.currentTitle!.description,
              "cover_limit": coverLimitButtonOutlet.currentTitle!.description,
              "dress_code": dressCodeButtonOutlet.currentTitle!.description
            ] , completion: { (error) in
                if let err = error {
                    print("Error writing document: \(err)")
                } else {

                }
        })
    }
    
    func clubSettingsPullFromDatabase() {
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                guard let clubName = clubDocument["club_name"] as? String else {print("FAIL: clubName"); return }
                self.clubNameTextField.text = clubName
                guard let clubEmail = clubDocument["email"] as? String else {print("FAIL: clubEmail"); return}
                self.clubEmailTextField.text = clubEmail
                guard let clubAddress = clubDocument["address"] as? String else {print("FAIL: clubAddress"); return}
                self.clubAddressTextField.text = clubAddress
                
                guard let clubDescription = clubDocument["description"] as? String else {print("FAIL: clubDescription"); return}
                self.clubDescriptionTextView.text = clubDescription
                guard let clubContact = clubDocument["contact"] as? String else {print("FAIL: clubContact"); return}
                self.clubContactTextField.text = clubContact
                guard let clubSchedule = clubDocument["schedule"] as? String else {print("FAIL: clubSchedule"); return}
                self.scheduledHoursButtonOutlet.setTitle(clubSchedule, for: .normal)
                self.scheduledHoursButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
                self.scheduledHoursButtonOutlet.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                guard let clubCoverLimit = clubDocument["cover_limit"] as? String else {print("FAIL: clubCoverLimit"); return}
                self.coverLimitButtonOutlet.setTitle(clubCoverLimit, for: .normal)
                self.coverLimitButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                self.coverLimitButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
                guard let clubDressCode = clubDocument["dress_code"] as? String else {print("FAIL: clubDressCode"); return}
                self.dressCodeButtonOutlet.setTitle(clubDressCode, for: .normal)
                self.dressCodeButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: . normal)
                self.dressCodeButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
                
                if let imageProfileURL = clubDocument["club_profile_image"] as? String {
                    if let url = URL(string: imageProfileURL) {
                        URLSession.shared.dataTask(with: url) { data, response, error in
                            guard let data = data else { return }
                            let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .userInitiated, attributes: .concurrent)
                            concurrentQueue.async {
                                DispatchQueue.main.async {
                                    self.clubImageView.image = UIImage(data: data)
                                }
                            }
                            }.resume()
                    }
                }

            }
        }

    }
    
    
    func pickerModelDataSetUp() {
        hoursLeft = ["12:00AM","1:00AM","2:00AM","3:00AM","4:00AM","5:00AM","6:00AM","7:00AM","8:00AM","9:00AM","10:00AM","11:00AM"]
        hoursRight = ["12:00PM","1:00PM","2:00PM","3:00PM","4:00PM","5:00PM","6:00PM","7:00PM","8:00PM","9:00PM","10:00PM","11:00PM"]
        dressCode = ["Casual", "Festive", "Semi-Formal", "Cocktail Dress", "Black Tie", "White Tie"]
        
        if clubLimit.count != 1000 {
            for index in 0...1000 {
                clubLimit.append(index.description)
            }
        }
    }

    @IBAction func mutablePickerButtonTapped(_ sender: UIButton) {
        buttonTag = sender.tag
        var title = ""
        switch self.buttonTag {
        case 0:
            title = "Schedule Hours"
        case 1:
           title = "Cover Limit"
        case 2:
            title = "Dress Code"
        default:
            break
        }
        
        let alertView = UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 50, width: 260, height: 162))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        
        alertView.view.addSubview(pickerView)
        
        let action = UIAlertAction(title: "Save", style: .cancel) { (alert) in
            switch self.buttonTag {
            case 0:
                self.scheduleHoursSelected(scheduleLimitValue: pickerView)
            case 1:
                self.coverLimitSelected(coverLimitValue: pickerView)
            case 2:
                self.dressCodeSelected(dressCodeValue: pickerView)
            default:
                break
            }
        }
        
        
        alertView.addAction(action)
        self.present(alertView, animated: true) {
            pickerView.frame.size.width = alertView.view.frame.size.width
        }
    }
    
    func scheduleHoursSelected(scheduleLimitValue: UIPickerView) {
        scheduledHoursButtonOutlet.setTitle("  \(hoursLeft[(scheduleLimitValue.selectedRow(inComponent: 0))]) - \(hoursRight[(scheduleLimitValue.selectedRow(inComponent: 1))])", for: .normal)
        scheduledHoursButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        scheduledHoursButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
    }
    
    func coverLimitSelected(coverLimitValue: UIPickerView) {
        coverLimitButtonOutlet.setTitle("  \(clubLimit[(coverLimitValue.selectedRow(inComponent: 0))])", for: .normal)
        coverLimitButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        coverLimitButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
    }
    
    func dressCodeSelected(dressCodeValue: UIPickerView) {
        dressCodeButtonOutlet.setTitle("  \(dressCode[(dressCodeValue.selectedRow(inComponent: 0))])", for: .normal)
        dressCodeButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        dressCodeButtonOutlet.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
    }
    
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch buttonTag {
        case 0:
            return 2
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 1
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch buttonTag {
        case 0:
            if component == 0 {
                return hoursLeft.count
            } else {
                return hoursRight.count
            }
        case 1:
            return clubLimit.count
         case 2:
            return dressCode.count
        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch buttonTag {
        case 0:
            if component == 0 {
                return hoursLeft[row]
            } else {
                return hoursRight[row]
            }
        case 1:
            return clubLimit[row]
        case 2:
            return dressCode[row]
        default:
            return ""
        }
    }
    

    func errorHandelingAlertView(title: String, messsage: String) {
        let alertView = UIAlertController(title: title, message: "\(messsage)", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Got It!", style: .cancel, handler: nil)
        alertView.addAction(dismiss)
        self.present(alertView, animated: true, completion: nil)
    }
    

    @IBAction func signOutBarButtonTapped(_ sender: UIBarButtonItem) {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initalViewController = storyboard.instantiateViewController(withIdentifier: "MainLoginStoryboard")
            self.present(initalViewController, animated: true, completion: nil)
        } catch {
            print(error)
        }
    }
    
    // This will handle the touching upon anywhere else to hide the keyboard when not needed.
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DispatchQueue.main.async {
            if let imageSelected = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                self.clubImageView.image = imageSelected
                self.selectedImage = imageSelected
            }
            self.saveBarButtonTapped()
        }
        
        clubImageView.image = nil
        selectedImage = nil
        dismiss(animated: true, completion: nil)
       
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        clubImageView.image = nil
        selectedImage = nil
        dismiss(animated: true, completion: nil)
    }
    


}
