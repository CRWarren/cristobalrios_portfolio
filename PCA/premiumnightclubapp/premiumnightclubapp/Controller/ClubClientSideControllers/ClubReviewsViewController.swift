//
//  ClubReviewsViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/23/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class ClubReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var clubComposeReviewBarButton: UIBarButtonItem!
    @IBOutlet weak var clubReviewsTableView: UITableView!
    
    var reviewsArray = [ClubReviews]()
    var composedReview: ClubReviews!

    override func viewDidLoad() {
        super.viewDidLoad()
        clubReviewsPull()
//        clubReviewsUpdateListner()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //clubReviewsUpdateListner()
        clubReviewsPull()
    }
    
    func clubReviewsPull() {
        FIRDatabase.dbObj.firestoreDatabase.collection("club_reviews").document("\(FIRAuthentication.authObj.currentUserIdString)").collection("reviews").getDocuments { (snapshot, err) in
            if let error = err {
                print("\(error.localizedDescription)")
            } else {
                self.reviewsArray = snapshot!.documents.flatMap({ClubReviews(document: $0.data())})
                DispatchQueue.main.async {
                    self.clubReviewsTableView.reloadData()
                }
            }
        }
    }
    
    func clubReviewsUpdateListner() {
          FIRDatabase.dbObj.firestoreDatabase.collection("club_reviews").document("\(FIRAuthentication.authObj.currentUserIdString)").collection("reviews").whereField("timestamp", isGreaterThan: Date())
            .addSnapshotListener { (snapshot, err) in
                guard let snap = snapshot else { return }
                snap.documentChanges.forEach {
                    diff in
                    if diff.type == .added {
                        self.reviewsArray.append(ClubReviews(document: diff.document.data())!)
                        DispatchQueue.main.async {
                            self.clubReviewsTableView.reloadData()
                        }
                    }
                }
        }
    }

    
    @IBAction func clubComposeReviewBarButtonTapped(_ sender: UIBarButtonItem) {

        let clubComposerAlert = UIAlertController(title: "Compose Review", message: "Enter Your Review And Rating", preferredStyle: .alert)
        
        clubComposerAlert.addTextField { (textField: UITextField) in
            textField.placeholder = "Rate: 1 - 5"
        }
        
        clubComposerAlert.addTextField { (textField: UITextField) in
            textField.placeholder = "Review"
        }
        
        clubComposerAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        clubComposerAlert.addAction(UIAlertAction(title: "Post", style: .default, handler: { (postAction: UIAlertAction) in

            guard let rating = clubComposerAlert.textFields?.first?.text,
                  let review = clubComposerAlert.textFields?.last?.text
                else {return}
            
            let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)")
            
            clubDocumentReference.getDocument { (document, error) in
                if let clubDocument = document?.data() {
                    
                    guard let clubName = clubDocument["club_name"] as? String,
                        let imageProfileURL = clubDocument["club_profile_image"] as? String
                        else {return}
 
                    self.composedReview = ClubReviews(profileImageURL: imageProfileURL, profileImage: nil, accountHolder: clubName, rating: rating, review: review)
                    
                    if let review = self.composedReview {
                        // The values are being stored in JSON/Dictionary format since the db works out of NoSQL,
                        // the values being stored are user information which will be used to authenticate in the future.
                        
                        FIRDatabase.dbObj.firestoreDatabase.collection("club_reviews").document("\(FIRAuthentication.authObj.currentUserIdString)").collection("reviews").addDocument(data: review.reviewDocument, completion: { (error) in
                            if let err = error {
                                print("Error writing document: \(err)")
                            } else {
                                print(review.reviewDocument)
                                DispatchQueue.main.async {
                                    self.clubReviewsPull()
                                    self.clubReviewsTableView.reloadData()
                                }
                            }
                        })
                        
                        
//                        FIRDatabase.dbObj.firestoreDatabase.collection("club_reviews").document().setData(
//                            review.reviewDocument , completion: { (error) in
//                                if let err = error {
//                                    print("Error writing document: \(err)")
//                                } else {
//                                    print(review.reviewDocument)
//                                    DispatchQueue.main.async {
//                                        self.clubReviewsTableView.reloadData()
//                                    }
//                                }
//                        })
                    }
                }
            }
        }))

        self.present(clubComposerAlert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = clubReviewsTableView.dequeueReusableCell(withIdentifier: "reviewCell") as! ClubReviewCustomTableViewCell
        
        if let url = URL(string: reviewsArray[indexPath.row].profileImageURL) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data else { return }
                let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .userInitiated, attributes: .concurrent)
                concurrentQueue.async {
                    DispatchQueue.main.async {
                        cell.clubOrProfileImageView.image = UIImage(data: data)
                    }
                }
                }.resume()
        }
        
        cell.clubOrUserTitleName.text = reviewsArray[indexPath.row].accountHolder
        cell.clubOrUserReviewLabel.text = reviewsArray[indexPath.row].review
        
        
        return cell
        
    }


}
