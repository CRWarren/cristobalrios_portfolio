//
//  ClubCustomCollectionViewCell.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/22/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

class ClubCustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var clubPostImageView: UIImageView!
    
}
