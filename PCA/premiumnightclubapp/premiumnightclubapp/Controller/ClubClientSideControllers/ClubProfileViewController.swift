//
//  ClubProfileViewController.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/18/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage

class ClubProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var arrayOfImagePost = [UIImage]()
    var arrayOfImagePostURLs = [String]()
    var selectedImage: UIImage!
    
    
    @IBOutlet weak var clubDescriptionTextView: UITextView!
    @IBOutlet weak var clubContactLabel: UILabel!
    @IBOutlet weak var clubScheduleLabel: UILabel!
    @IBOutlet weak var clubDressCodeLabel: UILabel!
    @IBOutlet weak var clubCoverLabel: UILabel!
    @IBOutlet weak var clubImagesFeed: UICollectionView!
    @IBOutlet weak var clubAdressLabel: UILabel!
    @IBOutlet weak var clubImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clubImagesFeed.delegate = self
        clubImagesFeed.dataSource = self
        
        clubImageView.layer.cornerRadius = (clubImageView?.frame.height)! / 2
        clubImageView.layer.borderWidth = 4
        clubImageView.layer.borderColor = #colorLiteral(red: 0.2666666667, green: 0.2196078431, blue: 0.5254901961, alpha: 1)
        clubImageView.clipsToBounds = true
        
        DispatchQueue.main.async {
            self.clubSettingsPullFromDatabase()
            self.pullImages()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        DispatchQueue.main.async {
            self.clubSettingsPullFromDatabase()
            self.downloadPPImage()
        }
    }

    
    func pullImages() {
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
        
        if let clubImages = clubDocument["club_collection_images"] as? [String] {
            self.arrayOfImagePost = []
            for each in clubImages {
                if let url = URL(string: each) {
                    URLSession.shared.dataTask(with: url) { data, response, error in
                        guard let data = data else { return }
                        
                        let images = UIImage(data: data)
                        if let image = images {
                            self.arrayOfImagePost.append(image)
                        }
                        
                        
                        }.resume()
                }
            }
        } else {
            print("Fail Images")
        }
    }
        }
    }
        
    func clubSettingsPullFromDatabase() {
        let clubDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)")
        
        clubDocumentReference.getDocument { (document, error) in
            if let clubDocument = document?.data() {
                
                guard let clubName = clubDocument["club_name"] as? String else { print("FAIL: clubName"); return }
                self.navigationItem.title = clubName
                guard let clubAddress = clubDocument["address"] as? String else { print("FAIL: clubAddress"); return}
                self.clubAdressLabel.text = clubAddress
                
                guard let clubDescription = clubDocument["description"] as? String else { print("FAIL: clubDescription"); return}
                self.clubDescriptionTextView.text = clubDescription
                guard let clubContact = clubDocument["contact"] as? String else { print("FAIL: clubContact"); return}
                self.clubContactLabel.text = clubContact
                guard let clubSchedule = clubDocument["schedule"] as? String else { print("FAIL: clubSchedule"); return}
                self.clubScheduleLabel.text = clubSchedule
                
                guard let clubCoverLimit = clubDocument["cover_limit"] as? String else { print("FAIL: clubCoverLimit"); return}
                self.clubCoverLabel.text = clubCoverLimit
                guard let clubDressCode = clubDocument["dress_code"] as? String else { print("FAIL: clubDressCode"); return }
                self.clubDressCodeLabel.text = clubDressCode
                
//                if let imageProfileURL = clubDocument["club_profile_image"] as? String {
//                    if let url = URL(string: imageProfileURL) {
//                        URLSession.shared.dataTask(with: url) { data, response, error in
//                            guard let data = data else { return }
//                            let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .userInitiated, attributes: .concurrent)
//                            concurrentQueue.async {
//                                DispatchQueue.main.async {
//                                    self.clubImageView.image = UIImage(data: data)
//                                }
//                            }
//                            }.resume()
//                    }
//                }
            }
        }
        
        DispatchQueue.main.async {
            self.clubImagesFeed.reloadData()
        }

    }
    
    
    @IBAction func clubImageUploadBarButtonTapped(_ sender: UIBarButtonItem) {
        handleSelectProfileImageView()
    }
    
    func uploadImage() {
        let key = NSUUID().uuidString
        let storageRef = Storage.storage().reference(forURL: "gs://premiumnightclubapp-32cd1.appspot.com/").child("post_images").child(FIRAuthentication.authObj.currentUserIdString).child(key)
        
        if let profile = selectedImage, let imageData = UIImageJPEGRepresentation(profile, 0.1) {
            storageRef.putData(imageData, metadata: nil, completion: { (metadata, err) in
                if err != nil {
                    return
                } else {
                    if let profileImageURLString = metadata?.downloadURL()?.absoluteString {
                        self.arrayOfImagePostURLs.append(profileImageURLString)
                        FIRDatabase.dbObj.firestoreDatabase.collection("clubs").document("\(FIRAuthentication.authObj.currentUserIdString)").updateData(
                            [ "club_collection_images": self.arrayOfImagePostURLs ]
                            , completion: { (error) in
                                if let err = error {
                                    print("Error writing document: \(err)")
                                } else {
                                }
                        })
                    }
                }
            })
        }
        DispatchQueue.main.async {
            self.clubImagesFeed.reloadData()
        }
    }
    
    
    func handleSelectProfileImageView() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        DispatchQueue.main.async {
            if let imageSelected = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                self.selectedImage = imageSelected
            }
            self.uploadImage()
            
            DispatchQueue.main.async {
                self.pullImages()
                self.clubImagesFeed.reloadData()
            }
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
   

        func downloadPPImage() {
            print(FIRAuthentication.authObj.currentUserIdString)
            let storageRef = Storage.storage().reference().child("userProfilePicture")
            let downloadImageRef = storageRef.child(FIRAuthentication.authObj.currentUserIdString)
            let downloadTask = downloadImageRef.getData(maxSize: 1024 * 10 * 12) { (data, error) in
                if let data = data {
                    let image = UIImage(data: data)
                    self.clubImageView.image = image
                }
            }
            downloadTask.resume()
            
        }
    

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfImagePost.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = clubImagesFeed.dequeueReusableCell(withReuseIdentifier: "clubImageCellIdentifier", for: indexPath) as! ClubCustomCollectionViewCell
        
        DispatchQueue.main.async {
            cell.clubPostImageView.image = self.arrayOfImagePost[indexPath.row]
            
        }
        
        
        return cell
        
    }
    
    
    
}
