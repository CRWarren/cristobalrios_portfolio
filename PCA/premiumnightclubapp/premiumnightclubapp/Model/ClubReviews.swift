//
//  ClubReviews.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/23/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import UIKit

protocol ReviewDocumentSerializable {
    init?(document:[String: Any])
}

struct ClubReviews {
    
    var profileImageURL: String
    var profileImage: UIImage! = nil
    var accountHolder: String
    var rating: String
    var review: String
   
    
    var reviewDocument: [String : Any] {
        return [
            "profile_image": profileImageURL,
            "account_holder": accountHolder,
            "rating": rating,
            "review": review,
            "timestamp": Date().description
        ]
    }
  
}

extension ClubReviews : ReviewDocumentSerializable {
    
     init?(document: [String: Any]) {
        guard let account = document["account_holder"] as? String,
            let profileImage = document["profile_image"] as? String,
            let ratingVal = document["rating"] as? String,
            let reviewStr = document["review"] as? String
            else {return nil}
        
        
        
        self.init(profileImageURL: profileImage, profileImage: nil, accountHolder: account, rating: ratingVal, review: reviewStr)

    }
}
