//
//  Authentication.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/8/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseStorage


class FIRAuthentication {
    
    let storageRef = Storage.storage().reference().child("userProfilePicture")
    
    static let authObj = FIRAuthentication()
    private init(){}
    
    var currentUserIdString: String {
        if let user = Auth.auth().currentUser {
            return user.uid.description
        }
        return "invalid id"
    }
    
    var currentUserEmailString: String {
        if let user = Auth.auth().currentUser {
            if let email = user.email {
                return email
            }
        }
        return "invalid email"
    }
    
    var currentUsernameString: String {
        if let user = Auth.auth().currentUser {
            if let username = user.displayName {
                return username
            }
        }
        return "invalid username"
    }

    // This static function will take as parameters the main textfields on the signup view,
    // the values are strings based on the email, password, and username.
    func authSignUpUser(userFullName:String?, userEmail: String?, userPassword: String?, username: String?, userDOB: String?) {
        // This is the database reference to access or create the different endpoints for storing or reading data.
        var db: Firestore!
        // This will handle the optional value and return the non-optional to then be handled
        // on the static function of the Auth Library from Firebase
        guard let email = userEmail,
              let password = userPassword,
              let username = username,
              let fullName = userFullName,
              let dob = userDOB
        else { print("Missing Items"); return }
        // This is the static function that Firebase has for authenticating and creating new
        // users will error closuere handeling.
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            // The database being initalized to gain access to the main endpoint we are
            // going to store the user data.
            db = Firestore.firestore()
            // Validating by optional handeling the error.
            guard error == nil else{ print(error!.localizedDescription); return }
            // Once we validate the user being created by optional handeling to see if the user is not nil,
            // the function does not take care of value storage, this is where we create
            // a reference and stor the user values for database input.
            if let newUser = user {
                
                // The values are being stored in JSON/Dictionary format since the db works out of NoSQL,
                // the values being stored are user information which will be used to authenticate in the future.
                db.collection("users").document("\(newUser.uid)").setData(
                    [ "fullName": fullName.description,
                      "username": username.description,
                      "email": email.description,
                      "dob": dob.description,
                      "ActiveUser": false,
                      "address": "",
                      "account_type": "user"
                    ] , completion: { (error) in
                        if let err = error {
                            print("Error writing document: \(err)")
                        } else {
                            // If the error comes out nil it will falthrough this block and will print out
                            // basic user information that was stored using the User class Firebase has already handled for us.
                            // From now forward we will be using the User class and Db reference to handle the application.
                            print("uid: \(String(describing: newUser.uid)) was created\nemail: \(String(describing: newUser.email!))")
                            
                            let image = #imageLiteral(resourceName: "pca_icon")
                            let imageData = UIImageJPEGRepresentation(image, 1)
                            
                            let uploadImageReference = self.storageRef.child(FIRAuthentication.authObj.currentUserIdString)
                            let uploadTask = uploadImageReference.putData(imageData!, metadata: nil) { (metadata, error) in
                                print(metadata ?? "No Metadata")
                                print(error ?? "No Error")
                            }
                            uploadTask.resume()
                            
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(email, forKey: "email")
                            userDefaults.set(password, forKey: "password")
                            userDefaults.set(false, forKey: "accountType")
                        }
                })
            }
        }
    }
    
    
    // This static function will take as parameters the main textfields on the signup view,
    // the values are strings based on the email, password, and username.
    func authSignUpClub(clubName: String?, clubAddress: String?, clubEmail: String?, clubPassword: String?, clubToken: String?) {
        // This is the database reference to access or create the different endpoints for storing or reading data.
        var db: Firestore!
        // This will handle the optional value and return the non-optional to then be handled
        // on the static function of the Auth Library from Firebase
        guard let email = clubEmail,
            let password = clubPassword,
            let clubName = clubName,
            let address = clubAddress,
            let token = clubToken
            else { print("Missing Items"); return }
        // This is the static function that Firebase has for authenticating and creating new
        // users will error closuere handeling.
        Auth.auth().createUser(withEmail: email, password: password) { (club, error) in
            // The database being initalized to gain access to the main endpoint we are
            // going to store the user data.
            db = Firestore.firestore()
            // Validating by optional handeling the error.
            guard error == nil else{ print(error!.localizedDescription); return }
            // Once we validate the user being created by optional handeling to see if the user is not nil,
            // the function does not take care of value storage, this is where we create
            // a reference and stor the user values for database input.
            if let newClub = club {
                // The values are being stored in JSON/Dictionary format since the db works out of NoSQL,
                // the values being stored are user information which will be used to authenticate in the future.
                db.collection("clubs").document("\(newClub.uid)").setData(
                    [ "club_name": clubName.description,
                      "address": address.description,
                      "email": email.description,
                      "token": token.description,
                      "account_type": "club"
                    ] , completion: { (error) in
                        if let err = error {
                            print("Error writing document: \(err)")
                        } else {
                            // If the error comes out nil it will falthrough this block and will print out
                            // basic user information that was stored using the User class Firebase has already handled for us.
                            // From now forward we will be using the User class and Db reference to handle the application.
                            print("uid: \(String(describing: newClub.uid)) was created\nemail: \(String(describing: newClub.email))")
                            
                            let image = #imageLiteral(resourceName: "pca_icon")
                            let imageData = UIImageJPEGRepresentation(image, 1)
                            
                            let uploadImageReference = self.storageRef.child(FIRAuthentication.authObj.currentUserIdString)
                            let uploadTask = uploadImageReference.putData(imageData!, metadata: nil) { (metadata, error) in
                                print(metadata ?? "No Metadata")
                                print(error ?? "No Error")
                            }
                            uploadTask.resume()
                            
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(email, forKey: "email")
                            userDefaults.set(password, forKey: "password")
                            userDefaults.set(true, forKey: "accountType")
                        }
                })
            }
        }
    }
    
    
    func authSignIn(userEmail: String?, userPassword: String?, switchON: Bool, VC: LoginViewController)  {
        
        guard let email = userEmail,
              let password = userPassword
              else { print("Missing Items"); return}
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            
            guard error == nil else {  self.alert(VC: VC); print(error!.localizedDescription) ;return }
            
            if let newUser = user {
                let userDefaults = UserDefaults.standard
                userDefaults.set(userEmail, forKey: "email")
                userDefaults.set(userPassword, forKey: "password")

                
                var signingIn = "users"
                if switchON{
                    userDefaults.set(true, forKey: "accountType")
                    signingIn = "clubs"
                }else{
                    userDefaults.set(false, forKey: "accountType")
                }
                let userDocumentReference = FIRDatabase.dbObj.firestoreDatabase.collection(signingIn).document("\(FIRAuthentication.authObj.currentUserIdString)")
                
                userDocumentReference.getDocument { (document, error) in
                    if let userDocument = document?.data() {
                        
                        guard let type = userDocument["account_type"] as? String else {print("FAIL: Account type"); return }
                        if type == "club"{
                             VC.performSegue(withIdentifier: "ClubLoginSegue", sender: nil)
                        }else if type == "user"{
                            VC.performSegue(withIdentifier: "UserLogInSegue", sender: nil)
                        }

             
                        
                    }
                }
//                if switchON{
//                    VC.performSegue(withIdentifier: "ClubLoginSegue", sender: nil)
//                }else{
//                    VC.performSegue(withIdentifier: "UserLogInSegue", sender: nil)
//                }
                print(newUser.email ?? "MISSING EMAIL")
                print(newUser.displayName ?? "MISSING DISPLAY NAME")
                print(newUser.uid)
                
            }
        })
        
    }
    
    
    func authForgotPassword(userEmail: String?, controller: UIViewController) {
        guard let email = userEmail else {return}
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let err = error {
                let alert = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
                let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
                alert.addAction(dismiss)
                controller.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Success", message: "Email Has Been Sent To \(email)", preferredStyle: .alert)
                let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
                alert.addAction(dismiss)
                controller.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func alert(VC: LoginViewController) {
        let alert = UIAlertController(title: "Wrong Email/Password", message: "Please verify your information", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(ok)
        VC.present(alert, animated: true, completion: nil)
    }
    
    
}

    
    
    
    

