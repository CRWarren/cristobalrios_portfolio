//
//  FIRDatabase.swift
//  premiumnightclubapp
//
//  Created by codenroot on 3/17/18.
//  Copyright © 2018 pca1803. All rights reserved.
//

import Foundation
import Firebase

enum DatabaseCollections: String {
    case clubs = "clubs"
    case users = "users"
}

enum DatabaseDocuments: String {
    case token = "token"
}

class FIRDatabase  {
    
    private init() {}
    static let dbObj = FIRDatabase()
    
    var firestoreDatabase: Firestore {
        return Firestore.firestore()
    }
    
    func collectionReference(collection: DatabaseCollections) -> CollectionReference {
        return firestoreDatabase.collection(collection.rawValue)
    }
    
    func collectionReferenceAndDocumentReference(collection: DatabaseCollections, reference: DatabaseDocuments) -> DocumentReference {
        return firestoreDatabase.collection(collection.rawValue).document(reference.rawValue)
    }
    


}


