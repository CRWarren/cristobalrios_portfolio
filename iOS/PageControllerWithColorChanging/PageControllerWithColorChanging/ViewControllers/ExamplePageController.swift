//
//  ExamplePageController.swift
//  PaceControllerWithColorChange
//
//  Created by cristobal rios on 1/23/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit

enum VisibleScreen: Int {
    case one = 0
    case two = 1
    case three = 2
}

class ExamplePageController: UIPageViewController {
    
    // MARK: variables
    var visibleScreen: VisibleScreen = .one
    weak var containerVC: ContainerViewController?
    private var colorPercentage: CGFloat = 0
    private var scrollPercentage: CGFloat = 0
    weak var scrollView: UIScrollView!
    private let fgGradientConfig = GradientConfig()

    var changedScreens: VisibleScreen = .one {
        didSet {
            if oldValue != changedScreens {
                visibleScreen = changedScreens
            }
        }
    }
    
    // MARK: controllers
    let oneVC: OneViewController = {
        let controller = OneViewController()
        return controller
    }()
    
    let twoVC: TwoViewController = {
        let controller = TwoViewController()
        return controller
    }()
    
    let threeVC: ThreeViewController =  {
        let controller = ThreeViewController()
        return controller
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
        //set initial controller
        setViewControllers([oneVC], direction: .forward, animated: false, completion: nil)
        self.visibleScreen = .one
        for view in self.view.subviews {
            if let scrollView = view as? UIScrollView {
                scrollView.delegate = self
                self.scrollView = scrollView
            }
        }
    }

//this will move you to desired screen
    func move(fromScreen: VisibleScreen, toScreen: VisibleScreen) {
        switch toScreen {
        case .one:
            if visibleScreen == .one {return}
            self.visibleScreen = .one
            self.setViewControllers([self.oneVC], direction: .reverse, animated: true)
        case .two:
            if visibleScreen == .two {return}
            let direction = fromScreen == .one ? UIPageViewController.NavigationDirection.forward : .reverse
            self.setViewControllers([self.twoVC], direction: direction, animated: true) { [weak self] succes in
                self?.visibleScreen = .two
            }
        case .three:
            if visibleScreen == .three {return}
            self.visibleScreen = .three
            self.setViewControllers([self.threeVC], direction: .forward, animated: true)
            }
    }

}
// MARK: PageController Delegate and Data Source

extension ExamplePageController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
          switch viewController {
          case is OneViewController:
            visibleScreen = .one

            return nil
          case is TwoViewController:
            visibleScreen = .two

            return oneVC
          case is ThreeViewController:
            visibleScreen = .three

                return twoVC

          default:
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
         switch viewController {
        case is OneViewController:
             visibleScreen = .one

             return twoVC
           case is TwoViewController:
             visibleScreen = .two

             return threeVC
           case is ThreeViewController:
             visibleScreen = .three

                 return nil
           
           default:
             return nil
         }
    }
    
}



extension ExamplePageController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
          let width = scrollView.bounds.width
          var offset: CGFloat = scrollView.contentOffset.x - width
          let direction: UIPageViewController.NavigationDirection = offset > 0 ? .forward : .reverse
          if offset < 0 { offset = scrollView.bounds.width + offset }
        
          // Scroll Percentage
          let scrollPercentage = offset / width
          if scrollPercentage > 1 {
              self.scrollPercentage = 1
          } else if scrollPercentage < 0 {
              self.scrollPercentage = 0
          } else {
              self.scrollPercentage = scrollPercentage
          }
          // Color Percentage
          let adjustment = direction == .forward ? (scrollView.bounds.width / 4) : (scrollView.bounds.width / 4) * -1
          let adjustedOffset = offset + adjustment
          let colorPercentage = adjustedOffset / width
          if colorPercentage > 1 {
              self.colorPercentage = 1
          } else if colorPercentage < 0 {
              self.colorPercentage = 0
          } else {
              self.colorPercentage = colorPercentage
          }
        //if continue scrolling change color
          if offset != 0 {
              changeValuesForScroll(direction: direction)
          } else {
            //set the final color for the controller
              adjustValuesUponFinishingScroll()
          }
      }
      
      
      func adjustValuesUponFinishingScroll() {
          guard let vc = viewControllers?.first else { return }
          switch vc {
          case is OneViewController:
              visibleScreen = .one
              containerVC?.backgroundGradient?.colors = [fgGradientConfig.gameGradients.0.cgColor,
                                                         fgGradientConfig.gameGradients.1.cgColor]
          case is TwoViewController:
              visibleScreen = .two
              containerVC?.backgroundGradient?.colors = [fgGradientConfig.leaderboardGradients.0.cgColor,
                                                         fgGradientConfig.leaderboardGradients.1.cgColor]
          case is ThreeViewController:
              visibleScreen = .three
              containerVC?.backgroundGradient?.colors = [fgGradientConfig.winnersGradients.0.cgColor,
                                                         fgGradientConfig.winnersGradients.1.cgColor]
          default: break
          }
          
      }
      
      
      private func changeValuesForScroll(direction: UIPageViewController.NavigationDirection) {
          var startingReds: (CGFloat, CGFloat)
          var startingGreens: (CGFloat, CGFloat)
          var startingBlues: (CGFloat, CGFloat)
          var endingReds: (CGFloat, CGFloat)
          var endingGreens: (CGFloat, CGFloat)
          var endingBlues: (CGFloat, CGFloat)
          if visibleScreen == .one && direction == .forward {

                startingReds = fgGradientConfig.oneToTwoStartingReds
                startingGreens = fgGradientConfig.oneToTwoStartingGreens
                startingBlues = fgGradientConfig.oneToTwoStartingBlues
                endingReds = fgGradientConfig.oneToTwoEndingReds
                endingGreens = fgGradientConfig.oneToTwoEndingGreens
                endingBlues = fgGradientConfig.oneToTwoEndingBlues
          } else if visibleScreen == .two && direction == .forward {
              startingReds = fgGradientConfig.twoToThreeStartingReds
              startingGreens = fgGradientConfig.twoToThreeStartingGreens
              startingBlues = fgGradientConfig.twoToThreeStartingBlues
              endingReds = fgGradientConfig.twoToThreeEndingReds
              endingGreens = fgGradientConfig.twoToThreeEndingGreens
              endingBlues = fgGradientConfig.twoToThreeEndingBlues
          } else if visibleScreen == .two && direction == .reverse {
              startingReds = fgGradientConfig.oneToTwoStartingReds
              startingGreens = fgGradientConfig.oneToTwoStartingGreens
              startingBlues = fgGradientConfig.oneToTwoStartingBlues
              endingReds = fgGradientConfig.oneToTwoEndingReds
              endingGreens = fgGradientConfig.oneToTwoEndingGreens
              endingBlues = fgGradientConfig.oneToTwoEndingBlues
          } else if visibleScreen == .three && direction == .reverse {
               startingReds = fgGradientConfig.twoToThreeStartingReds
               startingGreens = fgGradientConfig.twoToThreeStartingGreens
               startingBlues = fgGradientConfig.twoToThreeStartingBlues
               endingReds = fgGradientConfig.twoToThreeEndingReds
               endingGreens = fgGradientConfig.twoToThreeEndingGreens
               endingBlues = fgGradientConfig.twoToThreeEndingBlues
              
          } else {
              return
          }
          //Red
          let sr = getNewColorValue(l: startingReds.0, r: startingReds.1, p: colorPercentage)
          let er = getNewColorValue(l: endingReds.0, r: endingReds.1, p: colorPercentage)
          // Green
          let sg = getNewColorValue(l: startingGreens.0, r: startingGreens.1, p: colorPercentage)
          let eg = getNewColorValue(l: endingGreens.0, r: endingGreens.1, p: colorPercentage)
          // Blue
          let sb = getNewColorValue(l: startingBlues.0, r: startingBlues.1, p: colorPercentage)
          let eb = getNewColorValue(l: endingBlues.0, r: endingBlues.1, p: colorPercentage)
          let startingColor = UIColor(red: sr, green: sg, blue: sb, alpha: 1).cgColor
          let endingColor = UIColor(red: er, green: eg, blue: eb, alpha: 1).cgColor
          containerVC?.backgroundGradient?.colors = [startingColor, endingColor]
      }
      
      private func getNewColorValue(l: CGFloat, r: CGFloat, p: CGFloat) -> CGFloat {
          var value: CGFloat = 0
          let amountToAdjust = abs(l - r) * p
          value = r < l ? l - amountToAdjust : l + amountToAdjust
          return value
      }
    
    
   
    
}


