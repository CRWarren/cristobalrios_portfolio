//
//  ContainerViewController.swift
//  PaceControllerWithColorChange
//
//  Created by cristobal rios on 1/23/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    // MARK: Variables & Outlets

    @IBOutlet var containerView: UIView!
    private var isFirstLoad = true
    var backgroundGradient: CAGradientLayer?

    var exPageViewController: ExamplePageController = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pageVC = storyboard.instantiateViewController(withIdentifier: "PageController_ID") as! ExamplePageController
        pageVC.view.backgroundColor = .clear
        return pageVC
    }()
    
    var oneVC: OneViewController {
        return exPageViewController.oneVC
    }
    
    var twoVC: TwoViewController {
        return exPageViewController.twoVC
    }
    
    var threeVC: ThreeViewController {
        return exPageViewController.threeVC
    }
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setPageVC()
    }

    override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
            if isFirstLoad {
                isFirstLoad = false
          backgroundGradient = GradientManager.applyGradient(to: view, startColor: .fgBlue, endColor: .fgDarkBlue, verticle: true)
        }
      }
    
    // MARK: Functions

    private func setPageVC() {
           exPageViewController.containerVC = self
           exPageViewController.oneVC.containerVC = self
           exPageViewController.twoVC.containerVC = self
           exPageViewController.threeVC.containerVC = self
           addChild(exPageViewController)
           containerView.addSubview(exPageViewController.view)
           exPageViewController.view.translatesAutoresizingMaskIntoConstraints = false
           exPageViewController.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
           exPageViewController.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
           exPageViewController.view.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
           exPageViewController.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
       }
}

