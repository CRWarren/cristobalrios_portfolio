//
//  ViewController.swift
//  Notes
//
//  Created by cristobal rios on 4/29/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSCore
import AWSDynamoDB
import AWSS3

class ViewController: UIViewController {
    //client/secret
//767756317947-c5mnigt6mmv8qg1at4of5c5kgqivgjb0.apps.googleusercontent.com
//Q_XTzZH1f3vCCbqpeVWaWVyW
    //ios Client ID
//767756317947-g4q70oahpclthhtjmihms23m7v2acgkm.apps.googleusercontent.com
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkForLoggedIn()
    }
    
    func checkForLoggedIn(){
        if !AWSSignInManager.sharedInstance().isLoggedIn{
            AWSAuthUIViewController.presentViewController(with: self.navigationController!, configuration: nil) { (provider, error) in
                if error==nil{
                    print("success")
                }else{
                    print(error?.localizedDescription ?? "No Value")
                }
            }
        }else{
//            createNote(NoteId: "100")
//            createNote(NoteId: "101")
//            createNote(NoteId: "102")
//            createNote(NoteId: "103")
            //loadNote(noteId: "123")
            //(noteId: "123", Content: "Updated Note")
            //DeleteNote(noteId: "123")
            //queryNotes()
            //uploadFile()
            //downloadData()
            DeleteImage()
        }
    }
    
    func uploadFile(){
        var completionHandler : AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = {(task, error) in
            print(task.response?.statusCode ?? "0")
            print(error?.localizedDescription ?? "no Error")
        }
        
        let expt = AWSS3TransferUtilityUploadExpression()
        expt.progressBlock = {(task,Progress) in
            DispatchQueue.main.async {
                //update UI Here
                print(Progress.fractionCompleted)
            }
        }
        
        let data =  UIImage.init(named: "ramon")!.jpegData(compressionQuality:0.5)
        
        let tUtil = AWSS3TransferUtility.default()
        tUtil.uploadData(data!, key: "public/pic.jpg", contentType: "image/jpg", expression: expt, completionHandler: completionHandler)
        
    }
    
    func downloadData(){
        var completionHandler : AWSS3TransferUtilityDownloadCompletionHandlerBlock?
        completionHandler = {(task, URL, data, error) in
            DispatchQueue.main.async {
                let iv = UIImageView.init(frame: self.view.bounds)
                iv.contentMode = .scaleAspectFit
                iv.image = UIImage(data: data!)
                self.view.addSubview(iv)
            }
        }
        
        let tUtil = AWSS3TransferUtility.default()
        tUtil.downloadData(forKey: "public/pic.jpg", expression: nil, completionHandler: completionHandler)
    }
    
    func DeleteImage(){
        let s3 = AWSS3.default()
        let dor = AWSS3DeleteObjectRequest()
        dor?.bucket = "notes-userfiles-mobilehub-1944687229"
        dor?.key = "public/pic.jpg"
        s3.deleteObject(dor!) { (output, error) in
            print(output)
            print(error?.localizedDescription)
        }
    }
    
    @IBAction func LogOut(_ sender: UIButton) {
        AWSSignInManager.sharedInstance().logout { (value, error) in
            self.checkForLoggedIn()
        }
    }
    
    
    func createNote(NoteId: String){
        guard let note = Note() else{return}
        note._userId = AWSIdentityManager.default().identityId
        note._noteId = NoteId
        note._content = "Note Id = \(NoteId)"
        note._creationDate = Date().timeIntervalSince1970 as NSNumber
        let df = DateFormatter()
        df.dateStyle = .short
        df.timeStyle = .short
        note._title = "test note on\(df.string(from: Date()))"
        saveNote(note: note)
    }
    
    func saveNote(note: Note){
        let dbObjmapper = AWSDynamoDBObjectMapper.default()
        dbObjmapper.save(note) { (error) in
            print(error?.localizedDescription ?? "No Error")
        }
    }
    
    func loadNote(noteId: String){
        let dbObjmapper = AWSDynamoDBObjectMapper.default()
        if let hashKey = AWSIdentityManager.default().identityId{
            dbObjmapper.load(Note.self, hashKey: hashKey, rangeKey: noteId){(model, error)
                in
                if let note = model as? Note{
                    print(note._content ?? "No Content")
                }
            }
        }
    }
    func UpdateNote(noteId: String, Content: String){
        let dbObjmapper = AWSDynamoDBObjectMapper.default()
        if let hashKey = AWSIdentityManager.default().identityId{
            dbObjmapper.load(Note.self, hashKey: hashKey, rangeKey: noteId){(model, error)
                in
                if let note = model as? Note{
                    note._content = Content
                    self.saveNote(note: note)
                }
            }
        }
    }
    
    func DeleteNote(noteId: String){
        if let note = Note(){
            note._userId = AWSIdentityManager.default().identityId
            note._noteId = noteId
            let dbObjmapper = AWSDynamoDBObjectMapper.default()
            dbObjmapper.remove(note) {error in
                print(error?.localizedDescription ?? "No Error")
            }
        }
        
    }
    
    func queryNotes(){
        //create the expression instance
        let qExp = AWSDynamoDBQueryExpression()
        //add the condition we want to query in this case wher ethe id = userid
            //qExp.keyConditionExpression = "#uId = :userId"
        
        //now we can limit our (Expand)query so we dont get all the values back or some other condition.
        qExp.keyConditionExpression = "#uId = :userId and #noteId > :someId"
        
        //add the attributes to our expression ---// now we need to add our limited query
        qExp.expressionAttributeNames = ["#uId": "userId", "#noteId":"noteId"]
        //add the value we are searching
        qExp.expressionAttributeValues = [":userId":AWSIdentityManager.default().identityId!, ":someId":"100"]
        
        let objMapper = AWSDynamoDBObjectMapper.default()
        objMapper.query(Note.self, expression: qExp) {(outout, error) in
            if let notes = outout?.items as? [Note]{
                for note in notes{
                        print(note._content ?? "No Content Available")
                        print(note._noteId ?? "no id Available")
                }
            }
        }
    }
}

