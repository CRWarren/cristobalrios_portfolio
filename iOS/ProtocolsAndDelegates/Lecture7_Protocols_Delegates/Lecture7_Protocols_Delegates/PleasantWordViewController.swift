//
//  PleasantWordViewController.swift
//  Lecture7_Protocols_Delegates
//
//  Copyright © 2015 Full Sail University. All rights reserved.
//

import UIKit

class PleasantWordViewController: UIViewController, UITextFieldDelegate {
  
  // MARK: - IBOutlets & Variables
  @IBOutlet weak var textField: UITextField!
  var superImportant = "I shouldn't be changed"
  var missileCodes = 110985739

    var delegate: PleasantWordViewControllerDelegate?
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
    @IBAction func saveWord() {
        if let delegate = delegate, let pleasantWord = textField.text{
            if pleasantWord.isEmpty {
                alertIfEmpty()
            }else {
            delegate.takePleasantWord(PleasantWord: pleasantWord)
                dismiss(animated: true, completion: nil)
            }
        }
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        alertIfEmpty()
        return true
    }
    
    func alertIfEmpty() {
        if textField.text!.isEmpty {
            let alert = UIAlertController(title: "Oops!", message: "Please enter a pleasant word before you can continue", preferredStyle: .alert)
            let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true, completion: nil)
        }
    }
  
}
