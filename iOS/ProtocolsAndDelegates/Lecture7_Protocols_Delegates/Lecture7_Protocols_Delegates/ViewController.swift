//
//  ViewController.swift
//  Lecture7_Protocols_Delegates
//
//  Copyright © 2017 Full Sail University. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PleasantWordViewControllerDelegate{

  // MARK: - IBOutlets & Variables
  @IBOutlet weak var textView: UITextView!
  
  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    textView.text = ""
  }
    
    func takePleasantWord(PleasantWord: String) {
        textView.text = textView.text + "\n" + PleasantWord
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PleasantWordViewController{
            destination.delegate = self
        }
    }
    
}

