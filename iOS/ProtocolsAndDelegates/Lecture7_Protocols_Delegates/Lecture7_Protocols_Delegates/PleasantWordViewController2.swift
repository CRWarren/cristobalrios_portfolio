//
//  PleasantWordViewController2.swift
//  Lecture7_Protocols_Delegates
//
//  Created by cristobal rios on 10/9/17.
//  Copyright © 2017 Full Sail University. All rights reserved.
//

import Foundation

protocol PleasantWordViewControllerDelegate {
    
    //protocols may not include inplementation, but they do define what types of parameters a protocol-based function will implement
    func takePleasantWord(PleasantWord: String)
}
