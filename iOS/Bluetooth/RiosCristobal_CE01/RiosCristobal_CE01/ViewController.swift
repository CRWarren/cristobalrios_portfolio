//
//  ViewController.swift
//  RiosCristobal_CE01
//
//  Created by cristobal rios on 5/5/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBPeripheralManagerDelegate {

    //outlets
    @IBOutlet weak var statusLbl: UILabel!
    
    //UUIDS
    let TRANSFER_SERVICE_UUID =  CBUUID(string:"06B280C1-419D-4D87-810E-00D88B506717")
    let TRANSFER_CHARACTERISTIC_UUID = CBUUID(string:"CD570797-087C-4008-B692-7835A1246377")
    //variables for  our manager, services, characteristics
    var mutableCharcteristic: CBMutableCharacteristic?
    var mutableService: CBMutableService?
    var peripheralManager: CBPeripheralManager?
    var connected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //initialize our variables
        //since our central told us we can be read only, our value needs to be nil
        mutableCharcteristic = CBMutableCharacteristic(type:TRANSFER_CHARACTERISTIC_UUID, properties:.notify, value: nil, permissions: CBAttributePermissions.readable)
        mutableService = CBMutableService(type: TRANSFER_SERVICE_UUID, primary: true)
        //our manager will have the default qeue since we passed nil
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
    }
    
    //update method
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        //check for on state
        //if is on add services, and start advertising
        if peripheral.state == .poweredOn
        {
            print("state: on")
            mutableService!.characteristics = [mutableCharcteristic!]
            peripheral.add(mutableService!)
            peripheral.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[TRANSFER_SERVICE_UUID],CBAdvertisementDataLocalNameKey:"RiosCristobal_CE01"])
        }else{
            statusLbl.text = "no Connection"
        }
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        //this will trigger if the device is adverticing
        print("Advertising")
    }
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        //this will trigger if central subscribed to our peripheral.
        print("Connection Successful")
        connected = true
        statusLbl.text = "Connected!"
    }
    @IBAction func sendClick(sender: UIButton) {
        //this will"send" data to our central(update value)
        if(connected){
            print("hey you clikced\(sender.tag)")
            let data = String(sender.tag).data(using: String.Encoding.utf8)
            peripheralManager!.updateValue(data!, for: mutableCharcteristic!, onSubscribedCentrals: nil)
        }
    }

}

