//
//  ViewExtensions.swift
//  HuliPizzaNotification
//
//  Created by cristobal rios on 5/8/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

extension UIView {
    
    func round(cornerRadius: CGFloat, borderWidth: CGFloat, borderColor: UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.clipsToBounds = true
    }
    
}
