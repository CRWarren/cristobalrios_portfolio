//
//  CustomButton.swift
//  HuliPizzaNotification
//
//  Created by cristobal rios on 5/8/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        self.round(cornerRadius: 15, borderWidth: 2, borderColor: UIColor.init(red: 255/255, green: 123/255, blue: 38/255, alpha: 1))
        self.addTarget(self, action: #selector(selectionAnimation), for: .touchUpInside)
    }
    
    @objc func selectionAnimation(){
        self.center.y -= 20
        self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        self.alpha = 0.5
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.5, options: [], animations: {
            self.center.y += 20
            self.transform = CGAffineTransform.identity
            self.alpha = 1
        }, completion: nil)
    }
    
}
