//
//  ViewController.swift
//  HuliPizzaNotification
//
//  Created by cristobal rios on 5/8/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit
import UserNotifications


class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    var isGrantedNotificationAccess = false
    var pizzaNumber = 0
    let pizzaSteps = ["Make Pizza", "Roll Dough", "Add Sauce", "Add Cheese", "Add Ingredients", "Bake", "Done"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //request notifications permision
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted, error) in
            self.isGrantedNotificationAccess = granted
            if !granted{
                //Add alert to complain ot the user
                
            }
        }
        
    }
    
    func makePizzaContent() -> UNMutableNotificationContent{
        let content = UNMutableNotificationContent()
        content.title = "A Timed Pizza Step"
        content.body = "Making Pizza"
        content.userInfo = ["step": 0]
        content.categoryIdentifier = "pizza.steps.category"
        //content.attachments = pizzaStepImage(step: 0)
        content.attachments = pizzaGIF()
        return content
    }
    
    func updatePizzaStep(request: UNNotificationRequest){
        if request.identifier.hasPrefix("message.pizza"){
            var stepNumber = request.content.userInfo["step"] as! Int
            stepNumber = (stepNumber + 1) % pizzaSteps.count
            let updatedContent = makePizzaContent()
            updatedContent.body = pizzaSteps[stepNumber]
            updatedContent.userInfo["step"] = stepNumber
            updatedContent.subtitle = request.content.subtitle
            updatedContent.attachments = pizzaStepImage(step: stepNumber)
            addNotification(trigger: request.trigger, content: updatedContent, identifier: request.identifier)
        }
    }
    
    
    func addNotification(trigger: UNNotificationTrigger?,content: UNMutableNotificationContent, identifier: String){
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil{
                print(error?.localizedDescription ?? "Unkown")
            }
        }
    }

    @IBAction func schedulePizza(_ sender: CustomButton) {
        
        if isGrantedNotificationAccess{
            let content = UNMutableNotificationContent()
            content.title = "A Scheduled Pizza"
            content.body = "Time to make a Pizza"
            content.categoryIdentifier = "snooze.category"
            let attatchment = notifiationAttatchment(for: "pizza.video", resource: "PizzaMovie", type: "mp4")
            //let attatchment = notifiationAttatchment(for: "EHuliUke.music", resource: "EHuliUke", type: "mp3")
            content.attachments = attatchment
            let unitFLags:Set<Calendar.Component>  = [.minute,.hour,.second]
            var date = Calendar.current.dateComponents(unitFLags, from: Date())
            date.second = date.second! + 15
            let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: false)
            addNotification(trigger: trigger, content: content, identifier: "message.scheduled")
        }
        
        
    }
    
    @IBAction func MakePizza(_ sender: CustomButton) {
        
        if isGrantedNotificationAccess{
            let content = makePizzaContent()
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 7.0, repeats: false)
            pizzaNumber += 1
            //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60.0, repeats: true)
            content.subtitle = "Pizza number \(pizzaNumber)"
            addNotification(trigger: trigger, content: content, identifier: "message.pizza.\(pizzaNumber)")
        }
        
        
        
    }
    
    
    @IBAction func NextPizza(_ sender: CustomButton) {
        
        
        if isGrantedNotificationAccess{
            UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                if let request = requests.first{
                    if request.identifier.hasPrefix("message.pizza"){
                        self.updatePizzaStep(request: request)
                    }else{
                        let content = request.content.mutableCopy() as! UNMutableNotificationContent
                        self.addNotification(trigger: request.trigger!, content: content, identifier: request.identifier)
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func ViewPending(_ sender: CustomButton) {
        
        
        if isGrantedNotificationAccess{
            
            UNUserNotificationCenter.current().getPendingNotificationRequests { (requestList) in
                print("\(Date())----> \(requestList.count) requests pending")
                for request in requestList{
                    print(request.identifier + " " + request.content.body)
                }
            }
            
        }
        
        
    }
    
    @IBAction func viewDelivered(_ sender: CustomButton) {
        
        
        if isGrantedNotificationAccess{
            UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
               print("\(Date())----> \(notifications.count) delivered")
                for notification in notifications{
                    print(notification.request.identifier + " " + notification.request.content.body)
                }
                
            }
        }
        
        
    }
    
    @IBAction func RemoveNotification(_ sender: CustomButton) {
        
        if isGrantedNotificationAccess{
            UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
                if let request = requests.first{
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [request.identifier])
                }
            }
        }
        
        
    }
    
    
    
    //Mark: Delegates
    //the delegate handles in-App notifications it allows notifications to be recieved when the app is running
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let action = response.actionIdentifier
        let request = response.notification.request
        switch action {
        case "next.step.action":
            updatePizzaStep(request: request)
        case "stop.action":
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [request.identifier])
        case "snooze.action":
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
            let newRequest = UNNotificationRequest(identifier: request.identifier, content: request.content, trigger: trigger)
            UNUserNotificationCenter.current().add(newRequest) { (error) in
                if error != nil{
                    print(error?.localizedDescription)
                }
            }
        case "text.input":
            let textResponse = response as! UNTextInputNotificationResponse
            let newContent = request.content.mutableCopy() as! UNMutableNotificationContent
            newContent.subtitle = textResponse.userText
            addNotification(trigger: request.trigger, content: newContent, identifier: request.identifier)
        default:
            print("oops")
        }
        completionHandler()
    }
    
    func notifiationAttatchment(for identifier: String, resource: String, type: String)->[UNNotificationAttachment]{
        let extededIdentifier = identifier + "." + type
        guard let path = Bundle.main.path(forResource: resource, ofType: type)
            else{
                print("The File \(resource).\(type) was not found")
                return []
        }
        
        let videoUrl = URL(fileURLWithPath: path)
        do{
            let attatchment = try UNNotificationAttachment(identifier: identifier, url: videoUrl, options: nil)
            return [attatchment]
        }catch{
            print("The Attatchemnt was Not Loaded")
            return []
        }
    }
    
    func pizzaGIF()->[UNNotificationAttachment]{
        let extededIdentifier = "pizza.gif"
        guard let path = Bundle.main.path(forResource: "MakePizza_0", ofType: "gif")
            else{
                print("The gif was not found")
                return []
        }
        
        let videoUrl = URL(fileURLWithPath: path)
        do{
            let attatchment = try UNNotificationAttachment(identifier: extededIdentifier, url: videoUrl, options: [UNNotificationAttachmentOptionsThumbnailTimeKey:13])
            return [attatchment]
        }catch{
            print("The Attatchemnt was Not Loaded")
            return []
        }
    }
    
    func pizzaStepImage(step: Int) -> [UNNotificationAttachment]{
        let stepString = String(format: "%i",step)
        let identifier = "pizza.step."+stepString
        let resource = "MakePizza_\(stepString)"
        var type = "jpg"
        return notifiationAttatchment(for: identifier, resource: resource, type: type)
    }
    
    
}

