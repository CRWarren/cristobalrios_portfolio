//
//  KeyframeAnimationsViewController.swift
//  Animator_Starter
//
//  Created by Harrison Ferrone on 18.02.18.
//  Copyright © 2018 Paradigm Shift Development, LLC. All rights reserved.
//

import UIKit

class KeyframeAnimationsViewController: UIViewController {

    // MARK: Storyboard outlets
    @IBOutlet weak var animationTarget: UIButton!
    var targetOffSet: CGFloat{
        return animationTarget.frame.width/2
    }
    
    // MARK: Appearance
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TODO: UI Setup
        
        animationTarget.round(cornerRadius: animationTarget.frame.size.width/2, borderWidth: 3.0, borderColor: .black)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TODO: Fire keyframe animation
        bunnceImageWithKeyframe()
        segueToNextViewController(segueID: Constants.Segues.toConstraintsVC, delay: 8)
    }

    // MARK: Keyframe animation
    
    
    func bunnceImageWithKeyframe(){
        
         var animationTargetOrinig = animationTarget.center
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: [.repeat], animations: {
            
            //right
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                self.animationTarget.center = AnimationManager.screenRigtht
                self.animationTarget.center.x -= self.targetOffSet
            })
            //top
            UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: {
                self.animationTarget.center = AnimationManager.screenTop
                self.animationTarget.center.y += self.targetOffSet
            })
            //left
            UIView.addKeyframe(withRelativeStartTime: 0.50, relativeDuration: 0.25, animations: {
                self.animationTarget.center = AnimationManager.screenLeft
                self.animationTarget.center.x += self.targetOffSet
            })
            //bottom
            UIView.addKeyframe(withRelativeStartTime: 0.75, relativeDuration: 0.25, animations: {
                self.animationTarget.center = animationTargetOrinig
            })
        }, completion: nil)
    }
}
