//
//  ConstraintAnimationsViewController.swift
//  Animator_Starter
//
//  Created by Harrison Ferrone on 18.02.18.
//  Copyright © 2018 Paradigm Shift Development, LLC. All rights reserved.
//

import UIKit

class ConstraintAnimationsViewController: UIViewController {
    
    // MARK: Storyboard outlets
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var newsletterView: UIView!
    @IBOutlet weak var welcomeCenterX: NSLayoutConstraint!
    @IBOutlet weak var newsletterCenterX: NSLayoutConstraint!
    
    // MARK: Additional variables
    var newsletterInfoLabel = UILabel()
    var animManager: AnimationManager!
    
    // MARK: Appearance
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Programmatic views
        newsletterInfoLabel.backgroundColor = .clear
        newsletterInfoLabel.text = "Help us make your animation code that much better by subscribing to our weekly newsletter! \n\n It's free and you can unsubscribe any time without hurting our feelings...much."
        newsletterInfoLabel.font = UIFont(name: "Bodoni 72 Oldstyle", size: 15)
        newsletterInfoLabel.textColor = .darkGray
        newsletterInfoLabel.textAlignment = .left
        newsletterInfoLabel.alpha = 0
        newsletterInfoLabel.backgroundColor = .clear
        newsletterInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        newsletterInfoLabel.numberOfLines = 0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO: Offscreen positioning
        
       animManager = AnimationManager(activeConstrains: [welcomeCenterX,newsletterCenterX])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TODO: Fire initial animations
        animateViewsOnScreen()
    }
    
    // MARK: Actions
    
    @IBAction func infoONBttn(_ sender: UIButton) {
        
        animateNewsLetterHeight()
        animateWelcomeLbl()
    }
    
    // MARK: Animations
    
    func animateViewsOnScreen(){
        UIView.animate(withDuration: 1.5, delay: 0.25, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.welcomeCenterX.constant = self.animManager.constraintOrigins[0]
            self.newsletterCenterX.constant  = self.animManager.constraintOrigins[1]
            
            //since we are changing the constraints manually xcode will try to accomodate those changes and there will be no animation, we need to activate the animation manually
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
    }
    
    func animateNewsLetterHeight(){
        if let heightConstraint = newsletterView.returnConstraint(withId: "NewsLetterHeight"){
            heightConstraint.constant = 350
        }
        
        UIView.animate(withDuration: 1.75, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            self.view.layoutIfNeeded()
        }) { completed in
            self.addDynamicInfoLabel()
        }
    }

    func animateWelcomeLbl(){
        //create constraint to replace the other one
        let modiefiedWelcomeTop = NSLayoutConstraint(item: welcomeLabel, attribute: .top, relatedBy: .equal, toItem: welcomeLabel.superview, attribute: .top, multiplier: 1, constant: 100)
        
        //deactivate the constraint
        if let welcomeTop = view.returnConstraint(withId: "welcomeLblTop"){
            welcomeTop.isActive = false
            modiefiedWelcomeTop.isActive = true
        }
        
        UIView.animate(withDuration: 0.75) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    func addDynamicInfoLabel(){
        newsletterView.addSubview(newsletterInfoLabel)
        
        let xAnchor = newsletterInfoLabel.centerXAnchor.constraint(equalTo: newsletterView.leftAnchor, constant: -75)
        xAnchor.identifier = "newX"
         let yAnchor = newsletterInfoLabel.centerYAnchor.constraint(equalTo: newsletterView.centerYAnchor)
        let widthAnchor = newsletterInfoLabel.widthAnchor.constraint(equalTo: newsletterView.widthAnchor, multiplier: 0.8)
        
        let heightAnchor = newsletterInfoLabel.heightAnchor.constraint(equalTo: newsletterView.heightAnchor,multiplier: 0.7)
        
        NSLayoutConstraint.activate([xAnchor,yAnchor,widthAnchor,heightAnchor])
        self.view.layoutIfNeeded()
        animatelabelFadeIn(deactivatingConstraint: xAnchor)
    }
    
    func animatelabelFadeIn(deactivatingConstraint: NSLayoutConstraint){
        let xAnchor = newsletterInfoLabel.centerXAnchor.constraint(equalTo: newsletterView.centerXAnchor, constant: 0)
            deactivatingConstraint.isActive = false
            xAnchor.isActive = true
        UIView.animate(withDuration: 0.75) {
            self.newsletterInfoLabel.alpha = 1
            self.view.layoutIfNeeded()
        }

    }
}







extension UIView{
    func returnConstraint(withId: String) -> NSLayoutConstraint?{
        var constraintSearch: NSLayoutConstraint!
        
        for constraint in self.constraints{
            if constraint.identifier == withId{
                constraintSearch = constraint
            }
        }
        return constraintSearch
    }
}
