//
//  AnimationManager.swift
//  Animator_Starter
//
//  Created by cristobal rios on 5/2/19.
//  Copyright © 2019 Paradigm Shift Development, LLC. All rights reserved.
//

import UIKit

class AnimationManager{
    
    //calculated Screen Bounds
    class var screenBounds: CGRect{
        return UIScreen.main.bounds
    }
    
    //Screen positions
    
    class var screenRigtht: CGPoint{
        return CGPoint(x: screenBounds.maxX, y: screenBounds.midY)
    }
    
    class var screenTop: CGPoint{
        return CGPoint(x: screenBounds.midX, y: screenBounds.minY)
    }
    
    class var screenLeft: CGPoint{
        return CGPoint(x: screenBounds.minX, y: screenBounds.midY)
    }
    
    class var screenBttm: CGPoint{
        return CGPoint(x: screenBounds.midX, y: screenBounds.maxY)
    }
    
    
    
    
    //Tracking variables
    var constraintOrigins = [CGFloat]()
    var currentConstraints: [NSLayoutConstraint]
    
    init(activeConstrains: [NSLayoutConstraint]){
        for constraint in activeConstrains{
            constraintOrigins.append(constraint.constant)
            constraint.constant -= AnimationManager.screenBounds.width
        }
        
        currentConstraints = activeConstrains
    }
    
}


