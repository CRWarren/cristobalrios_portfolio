//
//  DetailInterfaceController.swift
//  RiosCristobal_CE02 WatchKit Extension
//
//  Created by cristobal rios on 5/9/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import WatchKit
class DetailIC: WKInterfaceController {
    
    @IBOutlet var titleLbl: WKInterfaceLabel!
    @IBOutlet var descriptionLbl: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let custObj = context as? CustomObject {
            titleLbl.setText(custObj.getName)
            descriptionLbl.setText(custObj.getDescription)
        }
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
