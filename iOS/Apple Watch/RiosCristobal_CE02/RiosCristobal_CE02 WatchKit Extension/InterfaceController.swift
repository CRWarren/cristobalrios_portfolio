//
//  InterfaceController.swift
//  RiosCristobal_CE02 WatchKit Extension
//
//  Created by cristobal rios on 5/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    let sf = StringsFile();
     var list:[CustomObject] = [CustomObject]()
    @IBOutlet var tableView: WKInterfaceTable!
    
    @available(watchOS 2.2, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
     print("hey session watch started")
        getData()
    }
    
    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default:nil
    
    override init(){
        super.init()
        print("2")
        //Same as on the iOS side, we set our delegate then call activate to check that a session is available and initialize it.
        session?.delegate = self
        session?.activate()
        
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func displayData(){
        print("@table View")
        if tableView != nil {
            print("table View!= nil \(list.count)")
            tableView.setNumberOfRows(list.count, withRowType: "Cell_1")
            
            for (i,_) in list.enumerated() {
                if let rc = tableView.rowController(at: i) as? RowController {
                    rc.imageView.setImageNamed(list[i].imageName!)
                }
            }
            
        }
        
    }
    func getData(){
         let myMessage:[String: Any] = [sf.getData:true]
        
        if let session = session, session.isReachable{
            print("reached session")
            //Send message with a reply handler where we'll get back the data we're looking for
            session.sendMessage(myMessage, replyHandler: {
                replyData in
                print("sent message")
                //Print out the data just to check that we did get something back
                print(replyData)
                //As with our iOS side, make sure we are not blocking any other threads
                DispatchQueue.main.async {
                    //Extract our data object so we can decode it
                    if let data = replyData[self.sf.getND] as? Data{
                        print(data)
                        //Just as with our iOS side we need an NSKeyed class but this time the decoder so we can change the data back to a Balance object.
                        NSKeyedUnarchiver.setClass(CustomObject.self, forClassName: self.sf.className)
                        if let dataObject = NSKeyedUnarchiver.unarchiveObject(with: data) as? [CustomObject]{
                            print(dataObject)
                            //Use our Balance object to set our data
                            self.list = dataObject
                            print("list count \(self.list.count)")
                            self.displayData()
                        }
                    }//end if newBalance check
                }//end Dispatch queue
            }, errorHandler: nil)
            
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        pushController(withName: "DetailIC", context: list[rowIndex])
    }

}
