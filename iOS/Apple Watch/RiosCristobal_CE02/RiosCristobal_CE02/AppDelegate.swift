//
//  AppDelegate.swift
//  RiosCristobal_CE02
//
//  Created by cristobal rios on 5/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    let fs = StringsFile()
     var list:[CustomObject] = [CustomObject]()
    var window: UIWindow?
    var session: WCSession? {
        didSet{
            if let session = session{
                session.delegate = self
                session.activate()
            }
        }
    }
    
    //Set our session var if WCSession is supported. This check really just establishes that there is a paired Apple Watch and that it is available to establish a connection (Bluetooth is turned on, etc).
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if WCSession.isSupported() {
            session = WCSession.default
        }
        list.append(CustomObject(name: "Broken Heart",_desc: "emoji with broken Heart eyes", _img: "1"))
        list.append(CustomObject(name: "Confused",_desc: "emoji with broken Heart eyes", _img: "emoticons confused"))
        list.append(CustomObject(name: "crush",_desc: "emoji with broken Heart eyes", _img: "emoticons crush"))
        list.append(CustomObject(name: "crying out loud",_desc: "emoji with broken Heart eyes", _img: "emoticons crying out loud"))
        list.append(CustomObject(name: "crying",_desc: "emoji with broken Heart eyes", _img: "emoticons crying"))
        list.append(CustomObject(name: "glasses",_desc: "emoji with broken Heart eyes", _img: "emoticons glasses"))
        list.append(CustomObject(name: "good",_desc: "emoji with broken Heart eyes", _img: "emoticons good"))
        list.append(CustomObject(name: "happy",_desc: "emoji with broken Heart eyes", _img: "emoticons happy"))
        list.append(CustomObject(name: "laughing out loud",_desc: "emoji with broken Heart eyes", _img: "emoticons laughing out loud"))
        list.append(CustomObject(name: "little kiss",_desc: "emoji with broken Heart eyes", _img: "emoticons little kiss"))
        
        return true
    }
    func returnList() -> [CustomObject] {
        return list
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
extension AppDelegate:WCSessionDelegate{
    
    
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    //If the session is essentially idle
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    //Post call for the activate method. Check the error object for any potential connection issues.
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("hey session started")
    }
    
    //This is the main piece of concern in this class. Here we have recieved a message from the watch and will respond to it using the supplied replyHandler.
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        DispatchQueue.main.async {
            //Checking the message for a key/value pair. It's important to note that send message must send a dictionary or key/value pairs. Here we're checking for the one and only message we know we send from the watch, getBalance. The value for that key is a boolean object so we cast appropriately but again, this is an Any object so anything could be the value here. It's important to make sure you're checking what you're sending so that you know what to expect when recieving.
            if (message[self.fs.getData] as? Bool) != nil{
                NSKeyedArchiver.setClassName(self.fs.getClassName, for: CustomObject.self)
                //Create a dummy balance object to send over
                //let balanceObject = Balance(date: "2/7/2017", balance: "$1000")
                //Convert our Balance object into a Data object using the archiver's archivedData method.
                let data = NSKeyedArchiver.archivedData(withRootObject: self.list)
                //Respond to the watch with a dictionary containing this new data object which we'll unarchive on the other side.
                replyHandler([self.fs.getND: data])
                
            }//end if getBalance check
        }//end queue
    }
}

