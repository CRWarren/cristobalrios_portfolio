//
//  ViewController.swift
//  RiosCristobal_CE02
//
//  Created by cristobal rios on 5/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit
import WatchConnectivity
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let fs = StringsFile()
    var list:[CustomObject] = [CustomObject]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        let ap = AppDelegate()
        list = ap.returnList()
        list.append(CustomObject(name: "Broken Heart",_desc: "emoji with broken Heart eyes", _img: "emoticons broken heart"))
        list.append(CustomObject(name: "Confused",_desc: "emoji with broken Heart eyes", _img: "emoticons confused"))
        list.append(CustomObject(name: "crush",_desc: "emoji with broken Heart eyes", _img: "emoticons crush"))
        list.append(CustomObject(name: "crying out loud",_desc: "emoji with broken Heart eyes", _img: "emoticons crying out loud"))
        list.append(CustomObject(name: "crying",_desc: "emoji with broken Heart eyes", _img: "emoticons crying"))
        list.append(CustomObject(name: "glasses",_desc: "emoji with broken Heart eyes", _img: "emoticons glasses"))
        list.append(CustomObject(name: "good",_desc: "emoji with broken Heart eyes", _img: "emoticons good"))
        list.append(CustomObject(name: "happy",_desc: "emoji with broken Heart eyes", _img: "emoticons happy"))
        list.append(CustomObject(name: "laughing out loud",_desc: "emoji with broken Heart eyes", _img: "emoticons laughing out loud"))
        list.append(CustomObject(name: "little kiss",_desc: "emoji with broken Heart eyes", _img: "emoticons little kiss"))
        print("list COunt: \(list.count)")
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt
        indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_1", for: indexPath)
        cell.textLabel?.text = list[indexPath.row].getName
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}

