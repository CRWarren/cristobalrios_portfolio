//
//  CustomObject.swift
//  RiosCristobal_CE02
//
//  Created by cristobal rios on 5/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
class CustomObject: NSObject, NSCoding {
 
    var name: String?
    var imgDescription: String?
    var imageName: String?
    
    var getName: String {
        get {
            return name!
        }
    }
    var getDescription: String {
        get {
            return imgDescription!
        }
    }
    var getImg: String {
        get {
            return imageName!
        }
    }
    
    init(name: String, _desc: String, _img:String) {
        self.name = name
        self.imgDescription = _desc
        self.imageName = _img
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        //Using the basic init to ensure that we at least have dummy data available in the object.
        self.init(name: "default", _desc: "no description", _img:"")
        
        //Here we actually decode our individual member variables. This works very similarly to using a dictionary with key/value pairs. Your keys can be anything you determine, just be careful to avoid typos that could cause difficult to pin down crashes.
        
        let sf = StringsFile()
        
       name = aDecoder.decodeObject(forKey: sf.getName) as? String
       imgDescription = aDecoder.decodeObject(forKey: sf.getDescription) as? String
       imageName = aDecoder.decodeObject(forKey: sf.getImg) as? String
    }
    
    func encode(with aCoder: NSCoder) {
         let sf = StringsFile()
        aCoder.encode(name, forKey: sf.getName)
        aCoder.encode(imageName, forKey: sf.getDescription)
        aCoder.encode(imageName, forKey: sf.getImg)
    }
    
}
