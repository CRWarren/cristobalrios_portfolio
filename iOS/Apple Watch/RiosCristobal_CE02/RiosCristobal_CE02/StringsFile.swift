//
//  StringsFile.swift
//  RiosCristobal_CE02
//
//  Created by cristobal rios on 5/8/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation

struct StringsFile {
    let nameKey = "nameString"
    let descKey = "descriptionString"
    let imgKey = "imgString"
    let data = "getData"
    let className = "CustomObject"
    let newData = "newData"
    
    var getName: String {
        get {
            return nameKey
        }
    }
    var getND: String {
        get {
            return newData
        }
    }
    var getDescription: String {
        get {
            return descKey
        }
    }
    var getImg: String {
        get {
            return imgKey
        }
    }
    var getData: String {
        get {
            return data
        }
    }
    var getClassName: String {
        get {
            return className
        }
    }
    
    
}
