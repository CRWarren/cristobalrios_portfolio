**CalCount App**

CalCount is a project for my portfolio 5 course, the main idea and concept of the app, 
is to simplify calorie counting to its most simple concept
just counting calories, later on there can be features added, 
but always keeping the mission in mind.

## Features

1. **HealthKit implementation** to get the active calories(if the user gave permission)
2. simple calorie tracking on **both sides iOS and Watch OS**
3. **Data Sharing** between Both devices

## Tested Devices
1. iPhone 8, iPhone 8+, iPhone X
2. apple Watch 38mm, apple Watch 42mm

## Current Functionality
1. Application Implements Successfully **HealthKit implementation**, including permision requirements.
2. Application allows the user to modify calories on **both sides iOS and Watch OS**
3. Application saves data on 2 userDefaults since its not much data, just 2 numbers,
   one is the goal, and the other is the calorie intake. Calories burned is directly passed from health kit
4. **Data Sharing** between Both devices using WCSession.

## Repo Link:
https://bitbucket.org/CRWarren/dvp_5/src/master/