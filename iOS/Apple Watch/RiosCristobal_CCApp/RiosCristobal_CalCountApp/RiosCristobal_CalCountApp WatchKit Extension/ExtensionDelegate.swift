//
//  ExtensionDelegate.swift
//  RiosCristobal_CalCountApp WatchKit Extension
//
//  Created by cristobal rios on 5/10/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {
    let fs = StringsFile()
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if activationState == .activated{
            print("hey we are active")
        }else{
            print("we are inactive something went wrong: \(error!)")
        }
    }
    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
        if WCSession.isSupported(){
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
        
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        DispatchQueue.main.async {
            //Checking the message for a key/value pair. It's important to note that send message must send a dictionary or key/value pairs. Here we're checking for the one and only message we know we send from the watch, getBalance. The value for that key is a boolean object so we cast appropriately but again, this is an Any object so anything could be the value here. It's important to make sure you're checking what you're sending so that you know what to expect when recieving.
            if (message["getCalsI"] as? Bool) != nil{
                print("getting cals for iphone")
                let defaults = UserDefaults(suiteName: self.fs.watchDefaults)
                let cals = defaults?.integer(forKey: self.fs.cals)
                print("cals: \(cals!)")
                // NSKeyedArchiver.setClassName(self.fs.getClassName, for: CustomObject.self)
                //Create a dummy balance object to send over
                //let balanceObject = Balance(date: "2/7/2017", balance: "$1000")
                //Convert our Balance object into a Data object using the archiver's archivedData method.
                let data = NSKeyedArchiver.archivedData(withRootObject: cals!)
                //Respond to the watch with a dictionary containing this new data object which we'll unarchive on the other side.
                replyHandler(["getNewCalsI": data])
                
            }//end if getBalance check
        }//end queue
    }

}
