//
//  InterfaceController.swift
//  RiosCristobal_CalCountApp WatchKit Extension
//
//  Created by cristobal rios on 5/10/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController {

    let fs = StringsFile()
    var modCals = 0
    var cals = 0
    @IBOutlet var caloriesOutputLbl: WKInterfaceLabel!
    @IBOutlet var caloriesInputLbl: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    @IBAction func plusCals() {
        modCals+=10
        caloriesInputLbl.setText("\(modCals)")
       
    }
    
    @IBAction func minusCals() {
        modCals-=10
        caloriesInputLbl.setText("\(modCals)")
    }
    
    @IBAction func confirm() {
        let total = cals+modCals
        if total<0{
            cals = 0
        }else{
            cals = total
        }
        caloriesOutputLbl.setText("\(cals)")
        let defaults = UserDefaults(suiteName: self.fs.watchDefaults)
        defaults?.set(cals, forKey: fs.cals)
        defaults!.synchronize()
        caloriesInputLbl.setText(" ")
        modCals = 0
        sendData()
    }
    
    func sendData(){
        if WCSession.isSupported(){
            let session = WCSession.default
            let myMessage:[String: Int] = ["updateCals":cals]
            
            if session.isReachable{
                print("sending new Cals session")
                //Send message with a reply handler where we'll get back the data we're looking for
                session.sendMessage(myMessage, replyHandler: {
                    replyData in
                    print("sent message")
                }, errorHandler: nil)
            }
        }
    }
    
    
    func getData() {
        if WCSession.isSupported(){
            let session = WCSession.default
            let myMessage:[String: Any] = ["getCals":true]
            
            if session.isReachable{
                print("reached session")
                //Send message with a reply handler where we'll get back the data we're looking for
                session.sendMessage(myMessage, replyHandler: {
                    replyData in
                    print("sent message")
               
                                    DispatchQueue.main.async {
                                        //Extract our data object so we can decode it
                                        if let data = replyData["getNewCals"] as? Data{
                                            print(data)
                                            if let dataObject = NSKeyedUnarchiver.unarchiveObject(with: data) as? Int{
                                                print(dataObject)
                                                //Use our Balance object to set our data
                                                self.caloriesOutputLbl.setText("\(dataObject)")
                                                self.cals = dataObject
                               
                                                    let defaults = UserDefaults(suiteName: self.fs.watchDefaults)
                                                    defaults!.set(dataObject, forKey: self.fs.cals)
                                                
                                               
                                            }
                                        }
                                    }//end Dispatch queue
                }, errorHandler: nil)
            }
        }
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        getData()
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
