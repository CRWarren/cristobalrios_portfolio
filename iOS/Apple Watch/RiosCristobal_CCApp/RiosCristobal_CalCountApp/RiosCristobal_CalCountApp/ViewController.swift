//
//  ViewController.swift
//  RiosCristobal_CalCountApp
//
//  Created by cristobal rios on 5/10/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit
import HealthKit
import WatchConnectivity
class ViewController: UIViewController {
    var calories = 0
    var goal = 0
    var burned: Int?
    var modCals = 0
    let fs = StringsFile()

    @IBOutlet weak var burnedCalOutput: UILabel!
    @IBOutlet weak var caloriesTF: UITextField!
    @IBOutlet weak var CaoriesLbl: UILabel!
    @IBOutlet weak var goalLbl: UILabel!
    
let healthStore = HKHealthStore()
    override func viewDidLoad() {
        accessHealthstore()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive(notification:)),
            name: NSNotification.Name.UIApplicationWillEnterForeground,
            object: nil)
        
    
        super.viewDidLoad()
   
        // Do any additional setup after loading the view, typically from a nib.
        print("reading defaults")
        let defaults = UserDefaults(suiteName: fs.iphoneDefaults)
        calories = (defaults?.integer(forKey: fs.cals))!
        goal = (defaults?.integer(forKey: fs.goal))!
        CaoriesLbl.text = "\(calories)"
        goalLbl.text = "\(goal)"
         self.updateColor()
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func restart(_ sender: UIButton) {
        let defaults = UserDefaults(suiteName: fs.iphoneDefaults)
        defaults!.set(0, forKey: fs.cals)
        defaults!.synchronize()
        calories = (defaults?.integer(forKey: fs.cals))!
        CaoriesLbl.text = "\(calories)"
        self.updateColor()
    }
    
    
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        // do something
        accessHealthstore()
        //getData()
        let defaults = UserDefaults(suiteName: fs.iphoneDefaults)
        calories = (defaults?.integer(forKey: fs.cals))!
        goal = (defaults?.integer(forKey: fs.goal))!
        CaoriesLbl.text = "\(calories)"
        goalLbl.text = "\(goal)"
        self.updateColor()

    }
    
    func getData() {
        if WCSession.isSupported(){
            let session = WCSession.default
            let myMessage:[String: Any] = ["getCalsI":true]

            if session.isReachable{
                print("reached session on Iphone")
                //Send message with a reply handler where we'll get back the data we're looking for
                session.sendMessage(myMessage, replyHandler: {
                    replyData in
                    print("sent message on Iphone")

                    DispatchQueue.main.async {

                        if let data = replyData["getNewCalsI"] as? Data{
                            print(data)
                            if let dataObject = NSKeyedUnarchiver.unarchiveObject(with: data) as? Int{
                                print(dataObject)

                                let defaults = UserDefaults(suiteName: self.fs.iphoneDefaults)
                                defaults!.set(dataObject, forKey: self.fs.cals)
                                self.CaoriesLbl.text = "\(dataObject)"
                                 self.updateColor()
                            }
                        }//end if newBalance check
                    }//end Dispatch queue
                }, errorHandler: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func valuesClick(sender: UIButton){
        switch sender.tag {
        case 0:
            
            modCals-=1
           caloriesTF.text="\(modCals)"
        default:
            modCals+=1
            caloriesTF.text="\(modCals)"
            
        }
        
        caloriesTF.text = "\(modCals)"
        
    }
    

    @IBAction func manualCals(_ sender: UITextField) {
        modCals = 0
        if sender.text!.first != nil{
            let sign = String((sender.text!.first)!)
            if sign=="-"{
                let temp = sender.text?.trimmingCharacters(in: .punctuationCharacters)
                if (temp?.count)!>0{
                    let negnumber = (Int(temp!))! * -1
                    modCals = negnumber
                }
            }
            if containsNumbersOnly(text: sender.text!){
                let number = Int(sender.text!)
                modCals = number!
            }
        }
    }
    
    
    @IBAction func modifyGoal(_ sender: UIButton) {
        let alert = UIAlertController(title: "Set up your calorie Goal", message: "This is the Max amount of calories you want to eat!\nOnly type in numbers!", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Your calorie goal goes here..."
        })
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { action in
            if self.containsNumbersOnly(text: alert.textFields![0].text!){
                let temp = Int(alert.textFields![0].text!)
                self.goal = temp!
                self.goalLbl.text = "\(self.goal)"
                 self.updateColor()
                let defaults = UserDefaults(suiteName: self.fs.iphoneDefaults)
                defaults!.set(self.goal, forKey: self.fs.goal)
                defaults!.synchronize()
            }else{
                self.displayAlert()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func displayAlert(){
        let alert = UIAlertController(title: "Goals not Saved", message: "Only Numbers\nTry Again?", preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Your calorie goal goes here..."
        })
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { action in
            if self.containsNumbersOnly(text: alert.textFields![0].text!){
                let temp = Int(alert.textFields![0].text!)
                self.goal = temp!
                self.goalLbl.text = "\(self.goal) Cal"
                self.updateColor()
                let defaults = UserDefaults(suiteName: self.fs.iphoneDefaults)
                defaults!.set(self.goal, forKey: self.fs.goal)
                defaults!.synchronize()
            }else{
                self.displayAlert()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func SaveBttnPressed(_ sender: UIButton) {
        calories += modCals
        print("savind defaults")
        let defaults = UserDefaults(suiteName: fs.iphoneDefaults)
        defaults!.set(calories, forKey: fs.cals)
        defaults!.synchronize()
        if calories<0{
            calories = 0
        }
       updateColor()
        CaoriesLbl.text = "\(calories)"
        modCals = 0
        caloriesTF.text = ""
        
    }
    func updateColor(){
        if calories>goal{
            CaoriesLbl.textColor = UIColor(red: 204, green: 0, blue: 0, alpha: 1)
        }else{
            CaoriesLbl.textColor = UIColor(red: 0, green: 204, blue: 0, alpha: 1)
        }
    }
    
    func containsNumbersOnly(text: String) -> Bool{
        //this regex validates that the textfield only numberss
        let nameRegex = "^[0-9]+$"
        //makes the comparison between the string and the regex pattern and returns a bool
        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: text)
    }
    
    func accessHealthstore(){
        let objectTypes: Set<HKObjectType> = [
            HKObjectType.activitySummaryType()
        ]
        
        healthStore.requestAuthorization(toShare: nil, read: objectTypes) { (success, error) in
            
            // Authorization request finished, hopefully the user allowed access!
            if success {
                self.getActivity()
            }
        }
    }
    
    func getActivity(){
        
        var energy: Double?
        //1.- get the todays date
        let calendar = Calendar.autoupdatingCurrent
            //split it into day, month , year
            var dateComponents = calendar.dateComponents([ .year, .month, .day ],from: Date())
            //set todays date to the split components
            dateComponents.calendar = calendar
        //2.- create predicate for our query
         let predicate = HKQuery.predicateForActivitySummary(with: dateComponents)
        //3.add our predicate to our query
        let query = HKActivitySummaryQuery(predicate: predicate) { (query, summaries, error) in
            //check if we recieve the summaries
            guard let summaries = summaries, summaries.count > 0
            else {
                    // No data returned. check for error
                DispatchQueue.main.async {
                     self.burnedCalOutput.text = "..."
                }
               
                    return
            }
            let summary = summaries[0]
            // get our data
                //1.- decide unit for our data
                let energyUnit = HKUnit.kilocalorie()
                //2.- retireve the values
            energy  = summary.activeEnergyBurned.doubleValue(for: energyUnit)
            DispatchQueue.main.async {
                self.burned = Int(energy!)
                 self.burnedCalOutput.text = "\(self.burned!) Cal"
            }
        }
        
        healthStore.execute(query)

    }
    
}

