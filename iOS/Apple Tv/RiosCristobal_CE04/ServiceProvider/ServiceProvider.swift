//
//  ServiceProvider.swift
//  ServiceProvider
//
//  Created by cristobal rios on 5/20/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import TVServices

class ServiceProvider: NSObject, TVTopShelfProvider {

    override init() {
        super.init()
    }

    // MARK: - TVTopShelfProvider protocol

    var topShelfStyle: TVTopShelfContentStyle {
        // Return desired Top Shelf style.
        return .sectioned
    }

    var topShelfItems: [TVContentItem] {
        // Create an array of TVContentItems.
        return insetTopShelfItems
    }
    
    
    //The array for our inset top shelf.
    fileprivate var insetTopShelfItems: [TVContentItem]{
        //Grab the predefined data set we created in the extension for our data struct
        let itemToDisplay = TVTopShelfData.sampleItemsForInsetTopShelf[0]
        //Create the array of TVContentItems. First up to do that we need a TVContentIdentifier. Don't forget, this needs to be unique across all past, present, and future objects for your top shelf. We're also being careful here to use guard and drop errors to let us know if anything goes wrong here.
        print(itemToDisplay.count)
        //identifier
        let SealifeIdentifier = TVContentIdentifier(identifier:
            "SeaLife", container: nil)!
        //sections
        let turtlesSection = TVContentItem(contentIdentifier:
            SealifeIdentifier)!
        turtlesSection.title = "\(itemToDisplay[1].group)"
        
        let itemToDisplay2 = TVTopShelfData.sampleItemsForInsetTopShelf[1]
        let fishSection = TVContentItem(contentIdentifier:
            SealifeIdentifier)!
        fishSection.title = "\(itemToDisplay2[1].group)"
        
        let itemToDisplay3 = TVTopShelfData.sampleItemsForInsetTopShelf[2]
        let octopusSection = TVContentItem(contentIdentifier:
            SealifeIdentifier)!
        octopusSection.title = "\(itemToDisplay3[1].group)"

        turtlesSection.topShelfItems = returnItems(items: itemToDisplay)
        fishSection.topShelfItems = returnItems(items: itemToDisplay2)
        octopusSection.topShelfItems = returnItems(items: itemToDisplay3)
        return [turtlesSection,fishSection, octopusSection]
    }// end fileprivate

    fileprivate func returnItems(items: [TVTopShelfData]) -> [TVContentItem]{
        // var turtleItems: [TVContentItem] = [TVContentItem]()
        guard let contentIdentifier = TVContentIdentifier(identifier: items[0].identifier, container: nil) else{
            fatalError("Error creating content identifier")
        }
        //1---------
        guard let contentItem = TVContentItem(contentIdentifier: contentIdentifier) else {
            fatalError("Error creating content item")
        }
        contentItem.title = items[0].title
        contentItem.displayURL = items[0].displayURL
        contentItem.setImageURL(items[0].imageUrl, forTraits: .screenScale1x)
        contentItem.imageShape = .wide
        //2---------
        guard let contentIdentifier2 = TVContentIdentifier(identifier: items[1].identifier, container: nil) else{
            fatalError("Error creating content identifier")
        }
        guard let contentItem2 = TVContentItem(contentIdentifier: contentIdentifier2) else {
            fatalError("Error creating content item")
        }
        contentItem2.title = items[1].title
        contentItem2.displayURL = items[1].displayURL
        contentItem2.setImageURL(items[1].imageUrl, forTraits: .screenScale1x)
        contentItem2.imageShape = .wide
        
        return [contentItem,contentItem2]
    }
}

