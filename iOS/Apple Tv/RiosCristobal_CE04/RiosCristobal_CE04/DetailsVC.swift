//
//  DetailsVC.swift
//  RiosCristobal_CE04
//
//  Created by cristobal rios on 5/20/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class DetailsVC: UIViewController {
    var row: Int?
    var section: Int?
    

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var DescLbl: UILabel!
    @IBOutlet weak var FileLbl: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         print("\(row!)")
        titleLbl.text = TVTopShelfData.itemsCount[section!][row!].title
        DescLbl.text = TVTopShelfData.itemsCount[section!][row!].description
        FileLbl.text = TVTopShelfData.itemsCount[section!][row!].imageName
        let imageData = NSData(contentsOf: TVTopShelfData.itemsCount[section!][row!].imageUrl) // nil
        let picture = UIImage(data: imageData! as Data)
        image.image = picture
    }
   
    @IBAction func dismiss(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
}
