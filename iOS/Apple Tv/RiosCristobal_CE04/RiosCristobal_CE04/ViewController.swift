//
//  ViewController.swift
//  RiosCristobal_CE04
//
//  Created by cristobal rios on 5/17/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    var indexToSend: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return TVTopShelfData.sampleItemsForInsetTopShelf.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell_1")
        cell.textLabel?.text = "\(TVTopShelfData.itemsCount[indexPath.section][indexPath.row].title)"
        cell.detailTextLabel?.text = "\(TVTopShelfData.itemsCount[indexPath.section][indexPath.row].imageName)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexToSend = indexPath
        performSegue(withIdentifier: "s_1", sender: nil)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "s_1" {
            let vc = segue.destination as! DetailsVC
            vc.row = indexToSend?.row
            vc.section = indexToSend?.section
            print("button pressed")
        }
    }
    

    
    
}

