//
//  TVTopShelfData.swift
//  RiosCristobal_CE04
//
//  Created by cristobal rios on 5/17/18.
//  Copyright © 2018 cristobal rios. All rights reserved.
//

import Foundation

struct TVTopShelfData {
    //BASED OF CODE EXAMPLE FROM CLASS
    //struncts and NSObject are similar to each other
    enum Group:String {
        case Turtles
        case Fish
        case Octopus
        static let allGroups: [Group] = [.Turtles, .Fish, .Octopus]
    }
    
    let group: Group
    let number: Int
    let description: String
    let title: String

    var imageName: String {
        return "\(group.rawValue)\(number).jpg"
    }
    var identifier: String {
        return "\(group.rawValue)\(number)"
    }
    
    var displayURL: URL {
        var components = URLComponents()
        components.scheme = "uikitcatalog"
        components.path = "tvTopShelfItem"
        components.queryItems = [URLQueryItem(name: "identifier", value: identifier)]
        
        return components.url!
    }
    
    var imageUrl: URL {
        let mainBundle = Bundle.main
        guard let imageUrl = mainBundle.url(forResource: imageName, withExtension: nil) else {
            fatalError("Error getting local image URL")
        }
        return imageUrl
    }

    static var sampleItems: [TVTopShelfData] = {
        return[TVTopShelfData(group: .Turtles, number: 1,description: "Turtle Image Number 1", title: "Turtles1"),
               TVTopShelfData(group: .Turtles, number: 2,description: "Turtle Image Number 2", title: "Turtles2"),
               TVTopShelfData(group: .Turtles, number: 3,description: "Turtle Image Number 3", title: "Turtles3"),
               TVTopShelfData(group: .Turtles, number: 4,description: "Turtle Image Number 4", title: "Turtles4"),
               TVTopShelfData(group: .Turtles, number: 5,description: "Turtle Image Number 5", title: "Turtles5"),
               
               TVTopShelfData(group: .Fish, number: 1,description: "Fish Image Number 1", title: "Fish1"),
               TVTopShelfData(group: .Fish, number: 2,description: "Fish Image Number 2", title: "Fish2"),
               TVTopShelfData(group: .Fish, number: 3,description: "Fish Image Number 3", title: "Fish3"),
               TVTopShelfData(group: .Fish, number: 4,description: "Fish Image Number 4", title: "Fish4"),
               TVTopShelfData(group: .Fish, number: 5,description: "Fish Image Number 5", title: "Fish5"),
               
               TVTopShelfData(group: .Octopus, number: 1,description: "Octopus Image Number 1", title: "Octopus1"),
               TVTopShelfData(group: .Octopus, number: 2,description: "Octopus Image Number 2", title: "Octopus2"),
               TVTopShelfData(group: .Octopus, number: 3,description: "Octopus Image Number 3", title: "Octopus3"),
               TVTopShelfData(group: .Octopus, number: 4,description: "Octopus Image Number 4", title: "Octopus4"),
               TVTopShelfData(group: .Octopus, number: 5,description: "Octopus Image Number 5", title: "Octopus5"),
               ]
    }()
    
    static var itemsCount: [[TVTopShelfData]] = {
        let turtleItems = TVTopShelfData.sampleItems.filter {$0.group == .Turtles}
        let fishItems = TVTopShelfData.sampleItems.filter {$0.group == .Fish}
        let octopusItems = TVTopShelfData.sampleItems.filter {$0.group == .Octopus}
        return [Array(turtleItems),Array(fishItems), Array(octopusItems)]
    }()
    
    static var sampleItemsForInsetTopShelf: [[TVTopShelfData]] = {
        let turtleItems = TVTopShelfData.sampleItems.filter {$0.group == .Turtles}
        let fishItems = TVTopShelfData.sampleItems.filter {$0.group == .Fish}
        let octopusItems = TVTopShelfData.sampleItems.filter {$0.group == .Octopus}

        return [Array(turtleItems.prefix(2)),Array(fishItems.prefix(2)), Array(octopusItems.prefix(2))]
    }()
}
