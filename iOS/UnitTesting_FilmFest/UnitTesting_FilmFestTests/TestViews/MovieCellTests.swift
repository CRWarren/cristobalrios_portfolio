//
//  MovieCellTests.swift
//  UnitTesting_FilmFestTests
//
//  Created by cristobal rios on 5/1/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import XCTest
@testable import UnitTesting_FilmFest

class MovieCellTests: XCTestCase {

    var tableView: UITableView!
    var mockDataSource: MockCellDataSource!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let libraryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController
        _ = libraryVC.view
        tableView = libraryVC.LibraryTableView
        mockDataSource = MockCellDataSource()
        tableView.dataSource = mockDataSource
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCell_Config_ShouldSetLablesToMovieData(){
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCellId", for: IndexPath(row: 0, section: 0)) as! MovieCell
        cell.configmovieCell(movie: Movie(title: "Movie 1",releaseDate: 2018))
        XCTAssertEqual(cell.textLabel?.text, "Movie 1")
        XCTAssertEqual(cell.detailTextLabel?.text, "2018")
    }

    

}
