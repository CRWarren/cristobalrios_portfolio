//
//  MovielibraryDataServiceTest.swift
//  UnitTesting_FilmFestTests
//
//  Created by cristobal rios on 5/1/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import XCTest
@testable import UnitTesting_FilmFest

class MovielibraryDataServiceTest: XCTestCase {

    var sut: MovieLibraryDataService!
    var libraryTableView: UITableView!
    var libraryVC: LibraryViewController!
    var tableViewMock: TableViewMock!
    
    let movie1 = Movie(title: "Movie 1")
    let movie2 = Movie(title: "Movie 2")
    let movie3 = Movie(title: "Movie 3")
    let movie4 = Movie(title: "Movie 4")

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = MovieLibraryDataService()
        sut.movieManager = MovieManager()
        tableViewMock = TableViewMock.initMock(dataSource: sut)
        libraryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController
        _ = libraryVC.view
        libraryTableView = libraryVC.LibraryTableView
        libraryTableView.dataSource = sut
        libraryTableView.delegate = sut
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //Mark: Check Sections for table view
    
    func test_TableViewSectionsCount_Returns2(){
        
        let sections = libraryTableView.numberOfSections
        XCTAssertEqual(sections, 2)
    }
    
    func test_TableViewSections_Section1_ReturnsMoviesToSeeCount(){
        
//        sut.movieManager?.addMovie(movie: movie1)
//        sut.movieManager?.addMovie(movie: movie2)
//        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 2)
//
//        sut.movieManager?.addMovie(movie: movie3)
//        libraryTableView.reloadData()
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 7)
        
    }
    
    func test_TableViewSections_Section2_ReturnsMoviesSeenCount(){
        
//        sut.movieManager?.addMovie(movie: movie1)
//        sut.movieManager?.addMovie(movie: movie2)
//        sut.movieManager?.checkOffMovieAtIndex(index: 0)
//        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
//
//        sut.movieManager?.checkOffMovieAtIndex(index: 0)
//        libraryTableView.reloadData()
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 0)
    }
    
    //Mark: Cells
    
    func testCell_RowAtIndex_ReturnsMovieCell(){
        sut.movieManager?.addMovie(movie: movie1)
        libraryTableView.reloadData()
        
        let cellQueried = libraryTableView.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cellQueried is MovieCell)
    }
    
    func testCell_ShouldDequeCell(){
        
        sut.movieManager?.addMovie(movie: movie1)
        tableViewMock.reloadData()
        _ = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(tableViewMock.cellDequedProperly)
    }
    
    func testCEll_Section1Confic_ShouldSetCellData(){
    
        sut.movieManager?.addMovie(movie: movie1)
        tableViewMock.reloadData()
        let cell = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 0)) as! MovieCellMock
        XCTAssertEqual(cell.movieData, movie1)
    }
    
    func testCEll_Section2Confic_ShouldSetCellData(){
        
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        
        sut.movieManager?.checkOffMovieAtIndex(index: 0)
        tableViewMock.reloadData()
        let cell = tableViewMock.cellForRow(at: IndexPath(row: 0, section: 1)) as! MovieCellMock
        XCTAssertEqual(cell.movieData, movie1)
    }
    
    func test_selectionShouldCheckOffSelectedMovie(){
        sut.movieManager?.addMovie(movie: movie1)
        sut.movieManager?.addMovie(movie: movie2)
        libraryTableView.delegate?.tableView?(libraryTableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(sut.movieManager?.moviesToSee, 1)
        XCTAssertEqual(sut.movieManager?.moviesSeen, 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 0), 1)
        XCTAssertEqual(libraryTableView.numberOfRows(inSection: 1), 1)
    }
    
    func testTableViewSectionTitles_ShouldHaveCorrectStringValues(){
        let section1Title = libraryTableView.dataSource?.tableView?(libraryTableView, titleForHeaderInSection: 0)
         let section2Title = libraryTableView.dataSource?.tableView?(libraryTableView, titleForHeaderInSection: 1)
        XCTAssertEqual(section1Title, "Movies To See")
        XCTAssertEqual(section2Title, "Movies Seen")
        
    }
}
