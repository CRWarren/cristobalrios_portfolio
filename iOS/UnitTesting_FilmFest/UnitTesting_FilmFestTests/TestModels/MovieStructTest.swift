//
//  MovieStructTest.swift
//  UnitTesting_FilmFestTests
//
//  Created by cristobal rios on 4/30/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import XCTest
@testable import UnitTesting_FilmFest

class MovieStructTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //Mark: Initialization
    func testInit_MovieWithTitle(){
        let testMovie = Movie(title: "Generic BlockBuster")
        XCTAssertNotNil(testMovie)
        XCTAssertEqual(testMovie.title, "Generic BlockBuster")
    }
    
    func testInit_SetMovieTItleAndReleaseDate(){
        let testMovie = Movie(title: "Romantic Comedy", releaseDate: 1987)
        XCTAssertNotNil(testMovie)
        XCTAssertEqual(testMovie.releaseDate, 1987)
    }
    
    
    //Mark: Equatable
    func test_equateable_ReturnsTrue(){
        let actionMovie = Movie(title:"Action")
        let actionMovie2 = Movie(title:"Action")
        XCTAssertEqual(actionMovie, actionMovie2)
    }
    
    func testEquatwable_ReturnsNotequalForDifferentTitles(){
        let actionMovie = Movie(title:"Action")
        let actionMovie2 = Movie(title:"Adventure")
        XCTAssertNotEqual(actionMovie, actionMovie2)
    }
    
    func testEquatable_ReturnsNotEqualForDifferentReleaseDates(){
        let actionMovie = Movie(title:"Action",releaseDate: 1999)
        let actionMovie2 = Movie(title:"Action",releaseDate: nil)
        XCTAssertNotEqual(actionMovie, actionMovie2)
    }
    
    
}
