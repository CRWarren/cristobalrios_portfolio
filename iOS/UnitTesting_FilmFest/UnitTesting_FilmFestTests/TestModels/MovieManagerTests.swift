//
//  MovieManagerTests.swift
//  UnitTesting_FilmFestTests
//
//  Created by cristobal rios on 4/30/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import XCTest
@testable import UnitTesting_FilmFest

class MovieManagerTests: XCTestCase {
    
    var sut: MovieManager!
    let movie1 = Movie(title: "movie 1")
    let movie2 = Movie(title: "movie 2")
    let movie3 = Movie(title: "movie 3")
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = MovieManager()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //Mark: initial Values
    func testInit_MoviesToSee_ReturnsZero(){
        //sut system under test
        XCTAssertEqual(sut.moviesToSee, 0)
    }
    
    func test_moviesSeen_ReturnsZero(){
        XCTAssertEqual(sut.moviesSeen, 0)
    }
    
    //Mark: add & Query
    
    func test_Add_moviesToSee_Returns1(){
        sut.addMovie(movie: movie1)
        XCTAssertEqual(sut.moviesToSee, 1)
    }
    
    func test_Query_ReturnsMovieAtIndex(){
        sut.addMovie(movie: movie2)
        let movieQueried = sut.movieAtIndex(index: 0)
        XCTAssertEqual(movie2.title, movieQueried.title)
    }
    
    //Checking Off
    
    func test_CheckOffMovie_updatesMovieManagerCounts(){
        sut.addMovie(movie: movie3)
        sut.checkOffMovieAtIndex(index: 0)
        
        XCTAssertEqual(sut.moviesToSee, 0)
        XCTAssertEqual(sut.moviesSeen, 1)
    }
    
    func test_checkOffMovie_RemoveMovieFromArray(){
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        sut.checkOffMovieAtIndex(index: 0)
        
        XCTAssertEqual(sut.moviesToSeeArray.count, 1)
        XCTAssertEqual(sut.moviesToSeeArray[0].title, movie2.title)
    }
    
    func test_CheckedOfMovie_ReturnsMovieAtIndex(){
        sut.addMovie(movie: movie1)
        sut.checkOffMovieAtIndex(index: 0)
        
        let movieQueried = sut.checkedOffMovieAtIndex(index: 0)
        XCTAssertEqual(movie1.title, movieQueried.title)
    }
    
    //Clearing and resetting
    func test_ClearArrays_ReturnsArrayCountZero(){
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie2)
        sut.checkOffMovieAtIndex(index: 0)
        XCTAssertEqual(sut.moviesToSee, 1)
        XCTAssertEqual(sut.moviesSeen, 1)
        
        sut.clearArrays()
        XCTAssertEqual(sut.moviesToSee, 0)
        XCTAssertEqual(sut.moviesSeen, 0)
    }
    
    //Mark: Duplicates
    func test_DuplicateMovies_ShouldNotBeAdded(){
        sut.addMovie(movie: movie1)
        sut.addMovie(movie: movie1)
        
        XCTAssertEqual(sut.moviesToSee, 1)
    }

}
