//
//  LibraryViewControllerTests.swift
//  UnitTesting_FilmFestTests
//
//  Created by cristobal rios on 5/1/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import XCTest
@testable import UnitTesting_FilmFest

class LibraryViewControllerTests: XCTestCase {

    var sut: LibraryViewController!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        //to test UI Story boards we need to reference the actual storyboard and not the class
        sut = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryViewControllerID") as! LibraryViewController
        
        //we need to manually trigger the actual view did load for all views we are trying to test
        _ = sut.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //Mark: Nil Checks
    
    func test_libraryVC_TableVIewShouldNOtBeNil(){
        XCTAssertNotNil(sut.LibraryTableView)
    }
    
    
    
    //Doing This V has no benefit over doing it the normal way of draging from the StoryBoard
    
    //Mark: Data Source
    func test_DataSource_ViewDidLoad_SetsTableViewDataSource(){
        
        XCTAssertNotNil(sut.LibraryTableView.dataSource)
        XCTAssertTrue(sut.LibraryTableView.dataSource is MovieLibraryDataService)
    }
    
    //Mark: Delegate
    
    func testDelegate_VIewDidLoad_SetsTableVIewDelegate(){
        XCTAssertNotNil(sut.LibraryTableView.delegate)
         XCTAssertTrue(sut.LibraryTableView.delegate is MovieLibraryDataService)
    }
    
    //Mark: Data Service Assumptions
    func testDataService_ViewDidLoad_SingleDataServiceOBJ(){
        
        XCTAssertEqual(sut.LibraryTableView.dataSource as! MovieLibraryDataService, sut.LibraryTableView.delegate as! MovieLibraryDataService)
    }
    
}
