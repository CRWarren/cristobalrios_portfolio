//
//  MovieCell.swift
//  UnitTesting_FilmFest
//
//  Created by cristobal rios on 5/1/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configmovieCell(movie: Movie){
        self.textLabel?.text = movie.title
        if let rd = movie.releaseDate{
            self.detailTextLabel?.text = "\(rd)"
        }else{
            self.detailTextLabel?.text = "Release Date Unavailable"
            
        }
    }

}
