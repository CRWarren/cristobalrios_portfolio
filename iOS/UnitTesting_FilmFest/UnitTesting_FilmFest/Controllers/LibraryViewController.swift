//
//  ViewController.swift
//  UnitTesting_FilmFest
//
//  Created by cristobal rios on 4/30/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController {

    @IBOutlet weak var LibraryTableView: UITableView!
    @IBOutlet var dataService: MovieLibraryDataService!
    
    var movieManager = MovieManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.LibraryTableView.dataSource = dataService
        self.LibraryTableView.delegate = dataService
        dataService.movieManager = movieManager
        
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 1", releaseDate: 1800))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 2"))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 3", releaseDate: 1800))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 4"))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 5", releaseDate: 1800))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 6", releaseDate: 1800))
        dataService.movieManager?.addMovie(movie: Movie(title: "Movie 7"))
        LibraryTableView.reloadData()
        
    }


}

