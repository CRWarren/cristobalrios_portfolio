//
//  Movie.swift
//  UnitTesting_FilmFest
//
//  Created by cristobal rios on 4/30/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

struct Movie: Equatable {
    let title: String
    let releaseDate: Int?
    
    init(title: String, releaseDate: Int? = nil) {
        self.title = title
        self.releaseDate = releaseDate
    }
}
