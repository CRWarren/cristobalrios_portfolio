//
//  MovieManager.swift
//  UnitTesting_FilmFest
//
//  Created by cristobal rios on 4/30/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class MovieManager{
    var  moviesToSee: Int{return moviesToSeeArray.count}
    var moviesSeen: Int{return moviesSeenArray.count}
    var moviesToSeeArray = [Movie]()
    var moviesSeenArray = [Movie]()
    
    func addMovie(movie: Movie){
        if !moviesToSeeArray.contains(movie){
             moviesToSeeArray.append(movie)
        }
    }
    func movieAtIndex(index: Int) -> Movie{
        return moviesToSeeArray[index]
    }
    
    func checkOffMovieAtIndex(index: Int){
        
        guard index < moviesToSee else{return}
        moviesSeenArray.append(moviesToSeeArray[index])
        moviesToSeeArray.remove(at: index)
    }
    
    func checkedOffMovieAtIndex(index: Int) -> Movie{
        return moviesSeenArray[index]
    }
    
    func clearArrays(){
        moviesSeenArray.removeAll()
        moviesToSeeArray.removeAll()
    }
    
}
