package com.modecr.cristobal.neverempty.Classes;

import java.util.List;

public class Food {

    private String name;
    private String barcode;
    private int calories;
    private Double carbohydrates;
    private Double fat;
    private String ingredients;
    private Double proteins;
    private Double sodium;
    private List<String> _keywords;
    private String serving_size;
    private int serving_quantity;


    public Food(){}

    public Food(String name, String barcode, int calories, Double carbohydrates, Double fat,
                String ingredients, Double proteins, Double sodium, List<String> keywords,
                String serving_size, int serving_quantity) {
        this.name = name;
        this.barcode = barcode;
        this.calories = calories;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.ingredients = ingredients;
        this.proteins = proteins;
        this.sodium = sodium;
        _keywords = keywords;
        this.serving_size = serving_size;
        this.serving_quantity = serving_quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(Double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Double getFat() {
        return fat;
    }

    public void setFat(Double fat) {
        this.fat = fat;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public Double getProteins() {
        return proteins;
    }

    public void setProteins(Double proteins) {
        this.proteins = proteins;
    }

    public Double getSodium() {
        return sodium;
    }

    public void setSodium(Double sodium) {
        this.sodium = sodium;
    }

    public List<String> get_keywords() {
        return _keywords;
    }

    public void set_keywords(List<String> _keywords) {
        this._keywords = _keywords;
    }

    public String getServing_size() {
        return serving_size;
    }

    public void setServing_size(String serving_size) {
        this.serving_size = serving_size;
    }

    public int getServing_quantity() {
        return serving_quantity;
    }

    public void setServing_quantity(int serving_quantity) {
        this.serving_quantity = serving_quantity;
    }
}
