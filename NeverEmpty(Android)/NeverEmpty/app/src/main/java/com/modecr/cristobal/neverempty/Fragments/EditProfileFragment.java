package com.modecr.cristobal.neverempty.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.modecr.cristobal.neverempty.R;
import com.modecr.cristobal.neverempty.Classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditProfileFragment extends Fragment {

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void cancel();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    private User user;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private TextInputLayout email_input;
    private TextInputLayout name_input;
    private TextInputLayout password_input;
    private ImageButton blueBttn;
    private ImageButton greenBttn;
    private ImageButton orangeBttn;
    private ImageButton purpleBttn;
    private ImageView iv;
    private ProgressBar pb;
    private int photo;
    private Button Save;



    public static EditProfileFragment newInstance() {

        Bundle args = new Bundle();

        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Save = getActivity().findViewById(R.id.SaveBttn);
        Save.setOnClickListener(saveListener);
        name_input = getActivity().findViewById(R.id.input_name);
        email_input = getActivity().findViewById(R.id.input_Name);
        password_input = getActivity().findViewById(R.id.input_PasswordEdit);
        iv = getActivity().findViewById(R.id.UserImageView);
        mAuth= FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        pb= getActivity().findViewById(R.id.progressBar3);
        blueBttn = getActivity().findViewById(R.id.blueBttn2);
        greenBttn = getActivity().findViewById(R.id.greenBttn2);
        orangeBttn = getActivity().findViewById(R.id.orangeBttn2);
        purpleBttn = getActivity().findViewById(R.id.purpleBttn2);
        blueBttn.setOnClickListener(blue);
        greenBttn.setOnClickListener(green);
        orangeBttn.setOnClickListener(orange);
        purpleBttn.setOnClickListener(purple);
        DownloadUser();

        Button cancel = getActivity().findViewById(R.id.CancelBttn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.cancel();
            }
        });
    }

    private void DownloadUser(){
        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        user = document.toObject(User.class);
                        updateUI();
                    } else {
                        Log.d("1234567890. E", "get failed with found ", task.getException());
                    }
                } else {
                    Log.d("1234567890. E", "get failed with ", task.getException());
                }
            }
        });
    }

    private void updateUI(){
        name_input.getEditText().setText(user.getFull_name());
        email_input.getEditText().setText(user.getEmail());
        photo = user.getPhoto();
        switch (user.getPhoto()){
            case 0:
                blueBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.blue);
                break;
            case 1:
                greenBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.green);
                break;
            case 2:
                orangeBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.orange);
                break;
            case 3:
                purpleBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.purple);
                break;
        }
        pb.setVisibility(View.GONE);
    }

    private final View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validte()){

                if(password_input.getEditText().getText().toString().trim().isEmpty()){
                    if(email_input.getEditText().getText().toString().equals(user.getEmail())){
                        updateUser();
                    }else{
                        alertUpdate();
                    }
                }else{

                    alertUpdate();
                }
            }
        }
    };
    private final View.OnClickListener blue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 0;
           updateImage();
            Log.e("Photo","_________ "+photo+" ________");        }
    };

    private final View.OnClickListener green = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 1;
            updateImage();
            Log.e("Photo","_________ "+photo+" ________");
        }
    };

    private final View.OnClickListener orange = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 2;
            updateImage();
            Log.e("Photo","_________ "+photo+" ________");
        }
    };

    private final View.OnClickListener purple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 3;
            updateImage();
            Log.e("Photo","_________ "+photo+" ________");

        }
    };

    private void updateImage(){
        orangeBttn.setBackground(null);
        greenBttn.setBackground(null);
        blueBttn.setBackground(null);
        purpleBttn.setBackground(null);
        switch (photo){
            case 0:
                blueBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.blue);
                break;
            case 1:
                greenBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.green);
                break;
            case 2:
                orangeBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.orange);
                break;
            case 3:
                purpleBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                iv.setImageResource(R.drawable.purple);
                break;
        }
    }
    private void alertUpdate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Security Measure");
        builder.setMessage("for your security please enter password before updating your email or password");
        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for modecr, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = input.getText().toString();
                updateUser(password);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }


    private void updateUser(){
        String UID = mAuth.getCurrentUser().getUid();
        user.setPhoto(photo);
        user.setFull_name(name_input.getEditText().getText().toString());
        db.collection("users").document(UID).set(user);
        Toast.makeText(getActivity(),"Updated",Toast.LENGTH_SHORT).show();
        mFinishedInterface.cancel();
    }

    private void updateUser(final String _password){

        final String UID = mAuth.getCurrentUser().getUid();
        AuthCredential credential = EmailAuthProvider
                .getCredential(user.getEmail(), _password);
        user.setFull_name(name_input.getEditText().getText().toString());
        user.setEmail(email_input.getEditText().getText().toString());
        user.setPhoto(photo);
        user.setFirst(user.getFirst());
        db.collection("users").document(UID).set(user);
        mAuth.getCurrentUser().reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mAuth.getCurrentUser().updateEmail( email_input.getEditText().getText().toString())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(getActivity(),"Updated",Toast.LENGTH_SHORT).show();
                                            mFinishedInterface.cancel();
                                        }else{
                                            Log.e("UPDATED FAILED", "User email address FAILED.");
                                        }
                                    }
                                });
                        if(!password_input.getEditText().getText().toString().trim().isEmpty()) {
                            mAuth.getCurrentUser().updatePassword(_password).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getActivity(), "Updated", Toast.LENGTH_SHORT).show();
                                        mFinishedInterface.cancel();
                                    } else {
                                        Log.e("UPDATED FAILED", "User email address FAILED.");
                                    }
                                }
                            });
                        }
                    }
                });

    }

    private Boolean validte(){
     if(!isEmailValid(email_input.getEditText().getText().toString().trim())){
        Toast.makeText(getActivity(),"Invalid Email address", Toast.LENGTH_SHORT).show();
        return false;
    }else if(name_input.getEditText().getText().toString().split(" ").length<2){
        Toast.makeText(getActivity(),"Full Name Incomplete", Toast.LENGTH_SHORT).show();
        return false;
    }else if(password_input.getEditText().getText().toString().trim().length()<6&&password_input.getEditText().getText().toString().trim().length()!=0){
        Toast.makeText(getActivity(),"Passwords do not match", Toast.LENGTH_SHORT).show();
        return false;
    }
        return true;
}



    private static boolean isEmailValid(String email) {
        String expression = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
