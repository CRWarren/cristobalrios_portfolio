package com.modecr.cristobal.neverempty.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.modecr.cristobal.neverempty.Classes.FireBaseHelper;
import com.modecr.cristobal.neverempty.Classes.Food;
import com.modecr.cristobal.neverempty.R;

import java.util.ArrayList;
import java.util.List;


public class NewFoodFragment extends Fragment {

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;


    public static NewFoodFragment newInstance() {

        Bundle args = new Bundle();

        NewFoodFragment fragment = new NewFoodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FireBaseHelper fh;
    private ImageButton openCamera;
    private Button cancelBttn;
    private TextInputLayout name;
    private TextInputLayout serving;
    private TextInputLayout calories;
    private TextInputLayout fat;
    private TextInputLayout carbs;
    private TextInputLayout sodium;
    private TextInputLayout protein;
    private TextView ingredients;
    private ImageView foodIV;
    private Button Save;
    private Bitmap image;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_food_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fh = new FireBaseHelper();
        openCamera = getActivity().findViewById(R.id.openCamera);
        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraFUnc();
            }
        });

        cancelBttn = getActivity().findViewById(R.id.CancelBttn);
        cancelBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        name = getActivity().findViewById(R.id.input_Name);
        serving = getActivity().findViewById(R.id.input_ServingSize);
        calories = getActivity().findViewById(R.id.input_Calories);
        fat = getActivity().findViewById(R.id.input_fat);
        carbs = getActivity().findViewById(R.id.input_carbs);
        sodium = getActivity().findViewById(R.id.input_Sodium);
        protein = getActivity().findViewById(R.id.input_Protein);
        ingredients = getActivity().findViewById(R.id.ingredients);
        foodIV = getActivity().findViewById(R.id.foodImageView);
        Save = getActivity().findViewById(R.id.SaveBttn);
        Save.setOnClickListener(saveClicked);

    }

    private void openCameraFUnc(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            return;
        }else {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new
                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                image = (Bitmap) data.getExtras().get("data");

                foodIV.setImageBitmap(image);

        }
    }

    View.OnClickListener saveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validate()){

                Double fat_Double = Double.parseDouble(fat.getEditText().getText().toString());
                Double protein_Double = Double.parseDouble(protein.getEditText().getText().toString());
                Double carbs_Double = Double.parseDouble(carbs.getEditText().getText().toString());
                Double Sodium_Double = Double.parseDouble(sodium.getEditText().getText().toString());
                Double calories_Double = Double.parseDouble(calories.getEditText().getText().toString());
                int cals = (int) Math.round(calories_Double);
                Log.i("OBJ", "Creating food");
                List<String> list = new ArrayList<>();
                for (String s: name.getEditText().getText().toString().split(" ")) {
                    list.add(s);
                }
                Food food = new Food(name.getEditText().getText().toString(),getActivity().getIntent().getStringExtra("notbarcode"),
                        cals,carbs_Double,fat_Double,ingredients.getText().toString(),
                        protein_Double,Sodium_Double,list,serving.getEditText().getText().toString(),0);
                fh.uploadFood(food);
                fh.uploadImage(image,getActivity().getIntent().getStringExtra("notbarcode"));
                Toast.makeText(getActivity(),"We Appreciate your help!",Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    };

    private Boolean validate(){
        if(image!=null){
            if(!name.getEditText().getText().toString().trim().isEmpty()){
                if(!serving.getEditText().getText().toString().trim().isEmpty()){
                    if(!calories.getEditText().getText().toString().trim().isEmpty()){
                        if(!fat.getEditText().getText().toString().trim().isEmpty()){
                            if(!carbs.getEditText().getText().toString().trim().isEmpty()){
                                if(!sodium.getEditText().getText().toString().trim().isEmpty()){
                                    if(!protein.getEditText().getText().toString().trim().isEmpty()){
                                        if(!ingredients.getText().toString().trim().isEmpty()){
                                            return true;
                                        }else{
                                            alertError("ingredients");
                                        }
                                    }else{
                                        alertError("Protein");
                                    }
                                }else{
                                    alertError("Sodium");
                                }
                            }else{
                                alertError("Carbs");
                            }
                        }else{
                            alertError("Total Fat");
                        }
                    }else{
                        alertError("Calories");
                    }
                }else{
                    alertError("Serving Size");
                }
            }else{
                alertError("name");
            }
        }else{
            alertError("image");
        }
        return false;
    }

    private void alertError(String error){
        Toast.makeText(getActivity(),error+" is missing",Toast.LENGTH_SHORT).show();
    }

}
