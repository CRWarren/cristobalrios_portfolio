package com.modecr.cristobal.neverempty.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.modecr.cristobal.neverempty.Classes.FireBaseHelper;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Classes.PantryAdapter;
import com.modecr.cristobal.neverempty.Classes.UserFood;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

public class PantryFragment extends Fragment {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GridView _gridView;
    private final ArrayList<UserFood> _Food = new ArrayList<>();
    private TextView empty;
    private FireBaseHelper fh;

    public static PantryFragment newInstance() {

        Bundle args = new Bundle();

        PantryFragment fragment = new PantryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void scann();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pantry_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        fh = new FireBaseHelper();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.top_menu_pantry,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.filter){
            showDialogNotificationAction();

        }else if(item.getItemId()== R.id.add_item){
            showDialogNotificationActionAdd();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       _gridView = getActivity().findViewById(R.id.pantryGrid);
       empty = getActivity().findViewById(R.id.EmtyText);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(_Food!=null){
            _Food.clear();
            DownloadPantry();
        }
    }
    private void setupCustomBaseAdapterView() {
        PantryAdapter aa = new PantryAdapter(getActivity(), _Food);
        if(aa.getCount()==0) {
           empty.setVisibility(View.VISIBLE);
        }else{
            empty.setVisibility(View.GONE);
        }
            _gridView.setAdapter(aa);

    }
    private void DownloadPantry(){
        _Food.clear();
        if(mAuth.getCurrentUser()!=null){
            db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("pantry")
                    .orderBy("name", Query.Direction.ASCENDING).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value,
                                    @Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        Log.w("NeverEmpty", "Listen failed.", e);
                        return;
                    }



                    if(value.size()==0){
                        _Food.clear();
                        setupCustomBaseAdapterView();
                    }
                    _Food.clear();
                    for (QueryDocumentSnapshot doc : value) {
                        UserFood temp = doc.toObject(UserFood.class);
                        _Food.add(temp);
                        setupCustomBaseAdapterView();
                    }

                }
            });

        }
    }

    BottomSheetDialog mBottomDialogNotificationAction;

    private void showDialogNotificationAction() {
        try {
            View sheetView = getActivity().getLayoutInflater().inflate(R.layout.action_sheet_filter, null);
            mBottomDialogNotificationAction = new BottomSheetDialog(getActivity());
            mBottomDialogNotificationAction.setContentView(sheetView);
            Button A_Z = mBottomDialogNotificationAction.findViewById(R.id.buttonA_Z);
            Button Z_A = mBottomDialogNotificationAction.findViewById(R.id.buttonZ_A);
            Button CancelSheet = mBottomDialogNotificationAction.findViewById(R.id.cancelButtonSheet);
            A_Z.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collections.sort(_Food, new Comparator<UserFood>() {
                        public int compare(UserFood o1, UserFood o2) {
                            return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
                        }
                    });
                    setupCustomBaseAdapterView();
                    mBottomDialogNotificationAction.cancel();
                }
            });
            Z_A.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collections.sort(_Food, new Comparator<UserFood>() {
                        public int compare(UserFood o1, UserFood o2) {
                            return o2.getName().toLowerCase().compareTo(o1.getName().toLowerCase());
                        }
                    });
                    setupCustomBaseAdapterView();
                    mBottomDialogNotificationAction.cancel();
                }
            });
            CancelSheet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomDialogNotificationAction.cancel();
                }
            });
            // Remove default white color background
            FrameLayout bottomSheet = mBottomDialogNotificationAction.findViewById(android.support.design.R.id.design_bottom_sheet);
            bottomSheet.setBackground(null);
            if(!mBottomDialogNotificationAction.isShowing()){
                mBottomDialogNotificationAction.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogNotificationActionAdd() {
        try {
            View sheetView = getActivity().getLayoutInflater().inflate(R.layout.action_sheet_add, null);
            mBottomDialogNotificationAction = new BottomSheetDialog(getActivity());
            mBottomDialogNotificationAction.setContentView(sheetView);
            Button Custom = mBottomDialogNotificationAction.findViewById(R.id.Custom);
            Button Scanner = mBottomDialogNotificationAction.findViewById(R.id.ButtonScann);
            Button CancelSheet = mBottomDialogNotificationAction.findViewById(R.id.cancelButtonSheet);
            Custom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertCustom();
                    mBottomDialogNotificationAction.cancel();
                }
            });
            Scanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFinishedInterface.scann();
                    mBottomDialogNotificationAction.cancel();
                }
            });
            CancelSheet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomDialogNotificationAction.cancel();
                }
            });
            // Remove default white color background
            FrameLayout bottomSheet = mBottomDialogNotificationAction.findViewById(android.support.design.R.id.design_bottom_sheet);
            bottomSheet.setBackground(null);
            if(!mBottomDialogNotificationAction.isShowing()){
                mBottomDialogNotificationAction.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alertCustom(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Custom Item");
        builder.setMessage("This Item will only contain the NAME you give to it!");
        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for modecr, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint("Item name");
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               if(input.getText().toString().trim().isEmpty()){
                   alertCustom();
               }else{
                  alertDate(input.getText().toString());
               }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();


    }
    private void alertDate(final String name){

        Calendar cal = Calendar.getInstance();
        int _year = cal.get(Calendar.YEAR);
        int _month = cal.get(Calendar.MONTH);
        final int _day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                String todayString = formatter.format(todayDate);
                UUID uuid = UUID.randomUUID();
                String ID = uuid.toString();

                UserFood temp = new UserFood(mAuth.getCurrentUser().getUid(),""+(month+1)+"/"+""+dayOfMonth+"/"+year,todayString,
                        name,ID,0,0.0,
                        0.0," ",0.0,0.0,
                        new ArrayList<String>()," ",0);
                fh.addPantry(temp);
            }
        }, _year, _month, _day);
        datePicker.show();
    }

}
