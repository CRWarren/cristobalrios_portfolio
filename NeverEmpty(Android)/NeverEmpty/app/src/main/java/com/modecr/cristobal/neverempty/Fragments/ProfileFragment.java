package com.modecr.cristobal.neverempty.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Activities.LogIn;
import com.modecr.cristobal.neverempty.Classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ProfileFragment extends Fragment {


    private ProgressBar pb;
    private TextView emailLbl;
    private TextView name;
    private TextView internal;
    private User user;
    private ImageView iv;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private ImageButton iB1;
    private ImageButton iB2;
    private ImageButton iB3;
    private ImageButton iB;
    private TextView n1;
    private TextView n2;
    private TextView n3;
    private TextView n4;
    private final ArrayList<User> internalUsers = new ArrayList<>();

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void edit();
        void open(User temp);
        void newuser();
        void login(User temp);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }


    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        emailLbl = getActivity().findViewById(R.id.EmailLbl);
        name = getActivity().findViewById(R.id.nameLbl);
        internal = getActivity().findViewById(R.id.internalNameLbl);
        pb = getActivity().findViewById(R.id.progressBar2);
        iv = getActivity().findViewById(R.id.userIV);
        Button signOut = getActivity().findViewById(R.id.SignOut);
        Button edit = getActivity().findViewById(R.id.editBttn);
        iB1 = getActivity().findViewById(R.id.imageButton2);
        iB2 = getActivity().findViewById(R.id.imageButton3);
        iB3 = getActivity().findViewById(R.id.imageButton4);
        iB =  getActivity().findViewById(R.id.imageButton);
        n1 = getActivity().findViewById(R.id.name1);
        n2 = getActivity().findViewById(R.id.name2);
        n3 = getActivity().findViewById(R.id.name3);
        n4 = getActivity().findViewById(R.id.name4);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.edit();
            }
        });
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent i = new Intent(getActivity(),LogIn.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });
        DownloadUser();
    }


    private void DownloadUser(){
        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        user = document.toObject(User.class);
                        iB.setOnClickListener(LoginUser);
                        updateUI(user);
                        DownloadInternal();

                    } else {
                        Log.d("1234567890. E", "get failed with found ", task.getException());
                    }
                } else {
                    Log.d("1234567890. E", "get failed with ", task.getException());
                }
            }
        });
    }

    private  void DownloadInternal(){
        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.collection("internal").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    internalUsers.clear();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        User usertemp = document.toObject(User.class);
                        internalUsers.add(usertemp);
                        updateUsersUI();
                        if(task.isComplete()){
                            pb.setVisibility(View.GONE);
                        }
                    }

                }
            }
        });}

    private void updateUsersUI(){

        iB1.setVisibility(View.INVISIBLE);
        iB2.setVisibility(View.INVISIBLE);
        iB3.setVisibility(View.INVISIBLE);
        n2.setText("Add");
        n3.setText("Add");
        n4.setText("Add");
        n3.setVisibility(View.INVISIBLE);
        n4.setVisibility(View.INVISIBLE);
        if(internalUsers.size()>=1){
            iB1.setVisibility(View.VISIBLE);
            iB2.setVisibility(View.VISIBLE);
            iB1.setOnLongClickListener(editProfile);
            iB1.setOnClickListener(LoginUser);
            iB2.setOnClickListener(newUser);
            iB1.setImageDrawable(returnDrawable(internalUsers.get(0).getPhoto()));
            n2.setText(internalUsers.get(0).getFull_name());
            n3.setVisibility(View.VISIBLE);
            if(internalUsers.size()>=2){
                iB2.setOnLongClickListener(editProfile);
                iB3.setVisibility(View.VISIBLE);
                iB2.setOnClickListener(LoginUser);
                iB3.setOnClickListener(newUser);
                iB2.setImageDrawable(returnDrawable(internalUsers.get(1).getPhoto()));
                n3.setText(internalUsers.get(1).getFull_name());
                n3.setVisibility(View.VISIBLE);
                n4.setVisibility(View.VISIBLE);
                if(internalUsers.size()==3){
                    iB3.setImageDrawable(returnDrawable(internalUsers.get(2).getPhoto()));
                    n4.setText(internalUsers.get(2).getFull_name());
                    iB3.setOnClickListener(LoginUser);
                    iB3.setOnLongClickListener(editProfile);
                }
            }
        }
    }

    private Drawable returnDrawable(int photo){
        Drawable returning = ContextCompat.getDrawable(getActivity(), R.drawable.add_user);
        switch (photo){

            case 0:
                returning = ContextCompat.getDrawable(getActivity(), R.drawable.blue);
                break;
            case 1:
                returning = ContextCompat.getDrawable(getActivity(), R.drawable.green);
                break;
            case 2:
                returning = ContextCompat.getDrawable(getActivity(), R.drawable.orange);
                break;
            case 3:
                returning = ContextCompat.getDrawable(getActivity(), R.drawable.purple);
                break;
        }
        return returning;
    }

    private void updateUI(User Temp){
        emailLbl.setText(user.getEmail());
        name.setText(user.getFull_name());
        iB.setImageDrawable(returnDrawable(user.getPhoto()));
        String[] internalName = Temp.getFull_name().split(" ");
        n1.setText(internalName[0]);
        iv.setImageDrawable(returnDrawable(Temp.getPhoto()));
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String id = preferences.getString("id", "");
        internal.setText(internalName[0]);
        if(!id.equals("") && !id.equals(mAuth.getCurrentUser().getUid())) {
           iv.setImageDrawable(returnDrawable(preferences.getInt("photo",user.getPhoto())));
           internal.setText(preferences.getString("name",""));

        }

    }

    private final View.OnClickListener LoginUser = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imageButton:
                    mFinishedInterface.login(user);
                    updateUI(user);
                    break;
                case R.id.imageButton2:
                    mFinishedInterface.login(internalUsers.get(0));
                    updateUI(internalUsers.get(0));
                    break;
                case R.id.imageButton3:
                    mFinishedInterface.login(internalUsers.get(1));
                    updateUI(internalUsers.get(1));
                    break;
                case R.id.imageButton4:
                    mFinishedInterface.login(internalUsers.get(2));
                    updateUI(internalUsers.get(2));
                    break;
            }
        }
    };

    private final View.OnClickListener newUser = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           mFinishedInterface.newuser();
        }
    };

    private final View.OnLongClickListener editProfile = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
                case R.id.imageButton2:
                    mFinishedInterface.open(internalUsers.get(0));
                    break;
                case R.id.imageButton3:
                    mFinishedInterface.open(internalUsers.get(1));
                    break;
                case R.id.imageButton4:
                    mFinishedInterface.open(internalUsers.get(2));
                    break;
            }
            return false;
        }
    };




}
