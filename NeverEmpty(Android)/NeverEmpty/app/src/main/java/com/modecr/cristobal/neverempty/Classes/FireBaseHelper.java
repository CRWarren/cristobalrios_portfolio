package com.modecr.cristobal.neverempty.Classes;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class FireBaseHelper {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public void uploadFood(Food food){
        if(food.getBarcode().length()!=13&&food.getBarcode().length()!=8){
            String barcode = "0"+food.getBarcode();
            food.setBarcode(barcode);
        }
        db.collection("food_items").document(food.getBarcode()).set(food);

    }

    public void addPantry(UserFood food){
        if(food.getBarcode().length()!=13&&food.getBarcode().length()!=8&&food.getBarcode().length()<19){
            String barcode = "0"+food.getBarcode();
            food.setBarcode(barcode);
        }

        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .collection("pantry").document(food.getBarcode()).set(food);
    }
    public void addCart(UserFood food){

        if(food.getBarcode().length()!=13&&food.getBarcode().length()!=8&&food.getBarcode().length()<19){
            String barcode = "0"+food.getBarcode();
            food.setBarcode(barcode);
        }

        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .collection("cart").document(food.getBarcode()).set(food);
    }

    public void deleteFromCart(UserFood food){
        if(food.getBarcode().length()!=13&&food.getBarcode().length()!=8&&food.getBarcode().length()<19){
            String barcode = "0"+food.getBarcode();
            food.setBarcode(barcode);
        }
        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .collection("cart").document(food.getBarcode()).delete();
    }
    public void deleteFromPantry(UserFood food){
        if(food.getBarcode().length()!=13&&food.getBarcode().length()!=8&&food.getBarcode().length()<19){
            String barcode = "0"+food.getBarcode();
            food.setBarcode(barcode);
        }
        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .collection("pantry").document(food.getBarcode()).delete();
    }

    public void Report(Food _food, String report){
        Map<String, Object> Food_report = new HashMap<>();
        Food_report.put("name",_food.getName());
        Food_report.put("_keywords",_food.get_keywords());
        Food_report.put("ingredients",_food.getIngredients());
        Food_report.put("serving_size",_food.getServing_size());
        Food_report.put("serving_quantity",_food.getServing_quantity());
        Food_report.put("sodium",_food.getSodium());
        Food_report.put("carbohydrates",_food.getCarbohydrates());
        Food_report.put("proteins",_food.getProteins());
        Food_report.put("fat",_food.getFat());
        Food_report.put("calories",_food.getCalories());
        Food_report.put("barcode",_food.getBarcode());
        Food_report.put("error",report);
        db.collection("food_flaged").document(_food.getBarcode()).set(Food_report);
    }

    public void uploadNewInternalUser(User user){
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("internal").document(user.getId()).set(user);
    }
    public void DeleteInternalUser(User user){
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("internal").document(user.getId()).delete();
    }

    public void uploadImage(Bitmap bitmap, String Barcode){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
        byte[] data = outputStream.toByteArray();

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference().child("food_images").child(Barcode);
        UploadTask uploadTask = storageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        });

    }
}
