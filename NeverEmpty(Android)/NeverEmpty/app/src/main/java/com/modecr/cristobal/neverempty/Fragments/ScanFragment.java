package com.modecr.cristobal.neverempty.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.modecr.cristobal.neverempty.Classes.DownloadAsyncTask;
import com.modecr.cristobal.neverempty.Classes.DownloadAsyncTask.CompletionObj;
import com.modecr.cristobal.neverempty.Classes.FireBaseHelper;
import com.modecr.cristobal.neverempty.Classes.Food;
import com.modecr.cristobal.neverempty.Classes.UserFood;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.modecr.cristobal.neverempty.R;

public class ScanFragment extends Fragment implements CompletionObj{
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    private Food food;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private Boolean Found = false;
    private SurfaceView cameraView;
    private CameraSource camSource;
    private BarcodeDetector barcodeDetector;
    private Handler mainHandler;
    private ProgressBar Scanning;
    private String ScannedBarcode;
    private final FireBaseHelper fh = new FireBaseHelper();

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void NotFound(String barcode);
        void Found(Food foundFood);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    public static ScanFragment newInstance() {

        Bundle args = new Bundle();

        ScanFragment fragment = new ScanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.scan_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        Scanning = getActivity().findViewById(R.id.progressBarScann);
        cameraView = getActivity().findViewById(R.id.cameraView);
        mainHandler = new Handler(Looper.getMainLooper());
        barcodeDetector = new BarcodeDetector.Builder(getActivity()).setBarcodeFormats(Barcode.UPC_A|Barcode.UPC_E).build();
        camSource = new CameraSource.Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .build();

        cameraView.getHolder().addCallback(sfC);

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if(!Found){
                    if(barcodes.size()!=0){
                        Found = true;
                        ScannedBarcode = barcodes.valueAt(0).rawValue;
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                Scanning.setVisibility(View.VISIBLE);
                                String dBarcode = barcodes.valueAt(0).rawValue;
                                if(dBarcode.length()!=13&&dBarcode.length()!=8){
                                    ScannedBarcode = "0"+barcodes.valueAt(0).rawValue;

                                    downloadFood("0"+barcodes.valueAt(0).rawValue);
                                }else{
                                    downloadFood(barcodes.valueAt(0).rawValue);
                                }

                            } // This is your code
                        };
                        mainHandler.post(runnable);

                    }
                }
            }
        });
    }

    private final SurfaceHolder.Callback sfC = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
                return;
            }
            try{
                camSource.start(holder);
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            camSource.stop();
        }
    };

    private void downloadFood(final String barcode){
        db.collection("food_items").document(barcode)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    if(task.isComplete()){
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            food = document.toObject(Food.class);
                            displayFound(food);
                            Scanning.setVisibility(View.GONE);
                        } else {
                            webConnection(barcode);
                        }
                    }
                }
            }
        });
    }


    private void webConnection(String barcode) {

        if (!validConnection()){
            Toast nointernet = Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT);
            nointernet.show();
            return;
        }
        final String webAddress = "https://world.openfoodfacts.org/api/v0/product/"+barcode+".json";

        DownloadAsyncTask downloadTask = new DownloadAsyncTask( this);

        downloadTask.execute(webAddress);


    }
    private boolean validConnection(){
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            if(networkInfo.isAvailable()){
                return true;
            }else {
                Toast noInternet = Toast.makeText(getActivity(),"No internet Access", Toast.LENGTH_SHORT);
                noInternet.show();
                return false;
            }
        }
        return false;
    }

    @Override
    public void updateInfo(String downloaded) {
        parseData(downloaded);
    }

    private void displayFound(final Food _food){

        new AlertDialog.Builder(getActivity()).setTitle(_food.getName())
                .setNegativeButton("View",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mFinishedInterface.Found(_food);
                    }
                }).setPositiveButton("Add",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Found = false;
                alertDate();
                dialog.cancel();
            }
        }).show();
    }

    private void DisplayNotFound(){

        new AlertDialog.Builder(getActivity()).setTitle("Item Not Found")
                .setNegativeButton("Add",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mFinishedInterface.NotFound(ScannedBarcode);
                    }
                }).setPositiveButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
        Scanning.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        Found = false;
    }

    private void alertDate(){

        Calendar cal = Calendar.getInstance();
        int _year = cal.get(Calendar.YEAR);
        int _month = cal.get(Calendar.MONTH);
        final int _day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                String todayString = formatter.format(todayDate);


                UserFood temp = new UserFood(mAuth.getCurrentUser().getUid(),""+(month+1)+"/"+""+dayOfMonth+"/"+year,todayString,
                        food.getName(),food.getBarcode(),food.getCalories(),food.getCarbohydrates(),
                        food.getFat(),food.getIngredients(),food.getProteins(),food.getSodium(),
                        food.get_keywords(),food.getServing_size(),food.getServing_quantity());
                fh.addPantry(temp);
            }
        }, _year, _month, _day);
        datePicker.show();

    }

    private void parseData(String data){

        try {
            JSONObject outerObj = new JSONObject(data);
            String status = outerObj.getString("status_verbose");
            Log.i("firstObj---------", "outer");
            if(status.equals("product found")){
                JSONObject obj = outerObj.getJSONObject("product");

                    Log.i("OBJ", "Name");
                    String name = obj.getString("product_name");
                    Log.i("OBJ", "INgredients");
                    String ingredients = obj.getString("ingredients_text_en");
                Log.i("OBJ", "Keywords");
                JSONArray _Keywords = obj.getJSONArray("_keywords");
                List<String> list = new ArrayList<>();
                for(int i = 0; i < _Keywords.length(); i++){
                    list.add(_Keywords.getString(i));
                }
                Log.i("OBJ", "ServingSize");
                String servingSize =obj.getString("serving_size");
                Log.i("OBJ", "ServinQuantity");
                int servingQuantity = obj.getInt("serving_quantity");
                Log.i("OBJ", "Image");
                URL imageUrl = new URL(obj.getString("image_url"));
                Log.i("OBJ", "Nutriments");
                JSONObject nutriments = obj.getJSONObject("nutriments");
                if(nutriments!=null){
                    Log.i("OBJ", "Fat");
                    Double fat = nutriments.getDouble("fat_serving");
                    Log.i("OBJ", "Protein");
                    Double protien = nutriments.getDouble("proteins_serving");
                    Log.i("OBJ", "Carbs");
                    Double carbs = nutriments.getDouble("carbohydrates_serving");
                    Log.i("OBJ", "Sodium");
                    Double sodium = nutriments.getDouble("sodium_serving");
                    Log.i("OBJ", "Cals");
                    Double calories = nutriments.getDouble("energy_serving");
                    Log.i("OBJ", "parsing cals");
                    int cals = (int) Math.round(calories);
                    Log.i("OBJ", "Creating food");

                    food = new Food(name,ScannedBarcode,cals,carbs,fat,ingredients,protien,sodium,list,servingSize,servingQuantity);
                    Log.i("OBJ", "parsing cals");
                    displayFound(food);
                    fh.uploadFood(food);
                    Log.i("OBJ", "Display");
                }

            }else{
                DisplayNotFound();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
