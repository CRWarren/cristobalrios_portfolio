package com.modecr.cristobal.neverempty.Classes;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadAsyncTask extends AsyncTask<String,String, String> {


    final private CompletionObj mFinishedInterface;

    public interface CompletionObj {
        void updateInfo(String downloaded);
        //void startProgressBar();
    }

    public DownloadAsyncTask(CompletionObj _finished) {

        mFinishedInterface = _finished;
    }

    @Override
    protected void onPreExecute()
    {
       // mFinishedInterface.startProgressBar();
    }

    @Override
    protected String doInBackground(String... strings) {
        if(strings[0]==null){
            return "";
        }
        String result = "";


        URL url;
        HttpURLConnection connection = null;
        InputStream inStream = null;

        try {
            Log.i("debug-------", strings[0]);
            // Create new URL
            url = new URL(strings[0]);
            // Open connection
            connection = (HttpURLConnection)url.openConnection();
            Log.i("debug---------", "opened connection");
            // Perform connection operation
            connection.setRequestMethod("GET");
            Log.i("debug---------", "RequestMethod");

            connection.connect();
            Log.i("debug---------", "connect connection");
        } catch(Exception e) {
            Log.i("debug---------", "connection catch");

            e.printStackTrace();
        }

        try {
            inStream = connection.getInputStream();
            Log.i("debug---------", "get stream");
            result = IOUtils.toString(inStream, "UTF-8");
            Log.i("debug---------", "converted results to stream");
        } catch(Exception e) {
            e.printStackTrace();
        }

        finally {

            if (connection != null) {
                if (inStream != null) {
                    try {
                        // close stream if open
                        inStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                // always disconnect if there is a connection
                connection.disconnect();
            }
        }


        // Display result
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        if(mFinishedInterface != null){
            mFinishedInterface.updateInfo(s);
        }
    }
}

