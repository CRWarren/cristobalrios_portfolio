package com.modecr.cristobal.neverempty.Classes;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Activities.ViewFoodActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CartAdapter extends BaseAdapter {

    private static final long Base_ID = 0x01001;
    private final FireBaseHelper fh = new FireBaseHelper();
    private final Context _Context;
    private final ArrayList<UserFood > _Foods;

    private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    public CartAdapter(Context _context, ArrayList<UserFood> _foods){
        _Context = _context;
        _Foods = _foods;
    }

    @Override
    public int getCount() {
        if(_Foods!=null){
            return _Foods.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(_Foods != null && (position >= 0||position<_Foods.size())) {
            return _Foods.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID+position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final CartAdapter.ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.cart_food_cell_layout, parent, false);
            vh =  new CartAdapter.ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(CartAdapter.ViewHolder)convertView.getTag();
        }

        final UserFood food = (UserFood) getItem(position);
        if(food!=null){
            vh.Name.setText(food.getName());
            vh.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _Foods.get(position).setDate_added("01/01/0001");
                    fh.addPantry(_Foods.get(position));
                    fh.deleteFromCart(_Foods.get(position));
                    _Foods.remove(_Foods.get(position));

                }
            });
            if(!food.getAddedby().equals(mAuth.getCurrentUser().getUid())){
                vh.Extra.setText(food.getAddedby());
            }

            vh.Name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(_Context,ViewFoodActivity.class);
                    i.putExtra("barcode",_Foods.get(position).getBarcode());
                    _Context.startActivity(i);
                }
            });
            vh.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar cal = Calendar.getInstance();
                    int _year = cal.get(Calendar.YEAR);
                    int _month = cal.get(Calendar.MONTH);
                    final int _day = cal.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog datePicker = new DatePickerDialog(_Context,
                            android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Date todayDate = Calendar.getInstance().getTime();
                            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                            String todayString = formatter.format(todayDate);


                            UserFood temp = new UserFood(mAuth.getCurrentUser().getUid(),""+(month+1)+"/"+""+dayOfMonth+"/"+year,todayString,
                                    _Foods.get(position).getName(),_Foods.get(position).getBarcode(),_Foods.get(position).getCalories(),_Foods.get(position).getCarbohydrates(),
                                    _Foods.get(position).getFat(),_Foods.get(position).getIngredients(),_Foods.get(position).getProteins(),_Foods.get(position).getSodium(),
                                    _Foods.get(position).get_keywords(),_Foods.get(position).getServing_size(),_Foods.get(position).getServing_quantity());
                            fh.addPantry(temp);
                            fh.deleteFromCart(_Foods.get(position));
                            _Foods.remove(_Foods.get(position));
                        }
                    }, _year, _month, _day);
                    datePicker.show();

                }
            });

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("food_images/"+food.getBarcode());
            final long ONE_MEGABYTE = 1024 * 1024;
            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    // Data for "images/island.jpg" is returns, use this as needed
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    vh.iv.setImageBitmap(bmp);
                }
            });

        }
        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{


        final Button Name;
        final TextView Extra;
        final Button add;
        final Button delete;
        final ImageView iv;

        ViewHolder(View _layout){
            Name = _layout.findViewById(R.id.NameBttn);
            Extra = _layout.findViewById(R.id.extraName);
            add = _layout.findViewById(R.id.AddPantryBttn);
            delete = _layout.findViewById(R.id.DeleteBttn);
            iv = _layout.findViewById(R.id.foodImageView);


        }
    }
}
