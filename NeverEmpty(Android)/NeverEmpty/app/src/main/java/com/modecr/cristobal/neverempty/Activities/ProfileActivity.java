package com.modecr.cristobal.neverempty.Activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Classes.User;
import com.modecr.cristobal.neverempty.Fragments.EditProfileFragment;
import com.modecr.cristobal.neverempty.Fragments.NewInternalUserFragment;
import com.modecr.cristobal.neverempty.Fragments.ProfileFragment;

public class ProfileActivity extends AppCompatActivity implements ProfileFragment.CompletionObj, EditProfileFragment.CompletionObj, NewInternalUserFragment.CompletionObj{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_conatiner,ProfileFragment.newInstance()).commit();
    }

    @Override
    public void edit() {
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_conatiner,EditProfileFragment.newInstance()).commit();
    }

    @Override
    public void open(User temp) {
        Bundle bundle = new Bundle();
        bundle.putString("id", temp.getId());
        // set Fragmentclass Arguments
        NewInternalUserFragment fragobj = NewInternalUserFragment.newInstance();
        fragobj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_conatiner,fragobj).commit();
    }

    @Override
    public void newuser() {
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_conatiner,NewInternalUserFragment.newInstance()).commit();
    }

    @Override
    public void login(User temp) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("id",temp.getId());
        editor.putString("name",temp.getFull_name());
        Toast.makeText(this,temp.getFull_name() + " Logged In", Toast.LENGTH_SHORT).show();
        editor.putInt("photo", temp.getPhoto());
        editor.apply();
    }

    @Override
    public void cancel() {
        getSupportFragmentManager().beginTransaction().replace(R.id.profile_conatiner,ProfileFragment.newInstance()).commit();
    }
}
