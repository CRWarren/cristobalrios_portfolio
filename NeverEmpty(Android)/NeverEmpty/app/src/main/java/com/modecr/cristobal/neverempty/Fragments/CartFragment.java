package com.modecr.cristobal.neverempty.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Classes.CartAdapter;
import com.modecr.cristobal.neverempty.Classes.UserFood;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class CartFragment extends Fragment {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GridView _gridView;
    private final ArrayList<UserFood> _Food = new ArrayList<>();
    private TextView empty;

    public static CartFragment newInstance() {

        Bundle args = new Bundle();

        CartFragment fragment = new CartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cart_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _gridView = getActivity().findViewById(R.id.cartGrid);
        empty = getActivity().findViewById(R.id.EmtyText);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(_Food!=null){
            _Food.clear();
            DownloadPantry();
        }
    }
    private void setupCustomBaseAdapterView() {
        CartAdapter aa = new CartAdapter(getActivity(), _Food);
        if(aa.getCount()==0) {
            empty.setVisibility(View.VISIBLE);
        }else{
            empty.setVisibility(View.GONE);
        }
        _gridView.setAdapter(aa);

    }
    private void DownloadPantry(){
        _Food.clear();
        if(mAuth.getCurrentUser()!=null){
            db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("cart")
                    .orderBy("name", Query.Direction.ASCENDING).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value,
                                    @Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        Log.w("NeverEmpty", "Listen failed.", e);
                        return;
                    }
                    if(value.size()==0){
                        _Food.clear();
                        setupCustomBaseAdapterView();
                    }

                    _Food.clear();
                    for (QueryDocumentSnapshot doc : value) {
                        UserFood temp = doc.toObject(UserFood.class);

                        for(UserFood o : _Food) {
                            if(o != null && o.getBarcode().equals(temp.getBarcode())) {
                                _Food.remove(o);
                            }
                        }
                        _Food.add(temp);
                        setupCustomBaseAdapterView();
                    }


                }
            });

        }
    }
}
