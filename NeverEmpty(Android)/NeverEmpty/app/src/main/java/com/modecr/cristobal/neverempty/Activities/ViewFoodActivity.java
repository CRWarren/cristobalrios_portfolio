package com.modecr.cristobal.neverempty.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.modecr.cristobal.neverempty.Fragments.NewFoodFragment;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Fragments.ViewFoodFragment;

public class ViewFoodActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_food);
        if(getIntent().getStringExtra("barcode")!=null){
            getSupportFragmentManager().beginTransaction().replace(R.id.viewFood_Container,ViewFoodFragment.newInstance()).commit();
        }else if(getIntent().getStringExtra("notbarcode")!=null){
            getSupportFragmentManager().beginTransaction().replace(R.id.viewFood_Container, NewFoodFragment.newInstance()).commit();
        }
    }


}
