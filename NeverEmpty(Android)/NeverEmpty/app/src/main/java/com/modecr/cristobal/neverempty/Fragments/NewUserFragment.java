package com.modecr.cristobal.neverempty.Fragments;

import com.modecr.cristobal.neverempty.R;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.modecr.cristobal.neverempty.Classes.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewUserFragment extends Fragment {

    private TextInputLayout text_input_Email;
    private TextInputLayout text_input_Password;
    private TextInputLayout text_input_fname;
    private TextInputLayout text_input_RePassword;
    private ImageButton blueBttn;
    private ImageButton greenBttn;
    private ImageButton orangeBttn;
    private ImageButton purpleBttn;
    private int photo;
    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void Cancel();
        void Save(User newUser, String _password);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }
    public static NewUserFragment newInstance() {

        Bundle args = new Bundle();

        NewUserFragment fragment = new NewUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_user_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        text_input_Email = getActivity().findViewById(R.id.input_Name);
        text_input_Password = getActivity().findViewById(R.id.input_name);
        text_input_RePassword = getActivity().findViewById(R.id.input_repassword);
        text_input_fname = getActivity().findViewById(R.id.input_fullname);
        blueBttn = getActivity().findViewById(R.id.blueBttn);
        greenBttn = getActivity().findViewById(R.id.greenBttn);
        orangeBttn = getActivity().findViewById(R.id.orangeBttn);
        purpleBttn = getActivity().findViewById(R.id.purpleBttn);
        photo = 0;
        blueBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
        blueBttn.setOnClickListener(blue);
        greenBttn.setOnClickListener(green);
        orangeBttn.setOnClickListener(orange);
        purpleBttn.setOnClickListener(purple);
        Button cancelBttn = getActivity().findViewById(R.id.CancelBttn);
        cancelBttn.setOnClickListener(cancel);
        Button singIn = getActivity().findViewById(R.id.SignInBttn);
        singIn.setOnClickListener(signin);
    }


    private final View.OnClickListener signin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validate()){
                User newUser = new User(text_input_fname.getEditText().getText().toString(),true,text_input_Email.getEditText().getText().toString(),photo,"id");
                mFinishedInterface.Save(newUser,text_input_RePassword.getEditText().getText().toString());
            }
        }
    };

    private final View.OnClickListener blue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 0;
            blueBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
            greenBttn.setBackground(null);
            orangeBttn.setBackground(null);
            purpleBttn.setBackground(null);
            Log.e("Photo","_________ "+photo+" ________");        }
    };

    private final View.OnClickListener green = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 1;
            greenBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
            blueBttn.setBackground(null);
            orangeBttn.setBackground(null);
            purpleBttn.setBackground(null);
            Log.e("Photo","_________ "+photo+" ________");
        }
    };

    private final View.OnClickListener orange = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 2;
            orangeBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
            greenBttn.setBackground(null);
            blueBttn.setBackground(null);
            purpleBttn.setBackground(null);
            Log.e("Photo","_________ "+photo+" ________");
        }
    };

    private final View.OnClickListener purple = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            photo = 3;
            purpleBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
            greenBttn.setBackground(null);
            orangeBttn.setBackground(null);
            blueBttn.setBackground(null);
            Log.e("Photo","_________ "+photo+" ________");

        }
    };

    private final View.OnClickListener cancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mFinishedInterface.Cancel();
        }
    };

    private Boolean validate(){

        if(!isEmailValid(text_input_Email.getEditText().getText().toString().trim())){
           Toast.makeText(getActivity(),"Invalid Email address", Toast.LENGTH_SHORT).show();
            return false;
        }else if(text_input_fname.getEditText().getText().toString().split(" ").length<2){
            Toast.makeText(getActivity(),"Full Name Incomplete", Toast.LENGTH_SHORT).show();
            return false;
        }else if(text_input_RePassword.getEditText().getText().toString().trim().length()<6){

            Toast.makeText(getActivity(),"Short Password, Min length 6 characters", Toast.LENGTH_LONG).show();
            return false;
        }else if(!text_input_Password.getEditText().getText().toString().trim().equals(text_input_RePassword.getEditText().getText().toString().trim())){
            Toast.makeText(getActivity(),"Passwords do not match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    private static boolean isEmailValid(String email) {
        String expression = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }




}
