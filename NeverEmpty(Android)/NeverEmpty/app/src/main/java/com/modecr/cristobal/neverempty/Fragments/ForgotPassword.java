package com.modecr.cristobal.neverempty.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.modecr.cristobal.neverempty.R;

public class ForgotPassword extends Fragment {

    private TextInputLayout text_input_Email;

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void Cancel();
        void Send(String email);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    public static ForgotPassword newInstance() {

        Bundle args = new Bundle();

        ForgotPassword fragment = new ForgotPassword();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        text_input_Email = getActivity().findViewById(R.id.input_Name);
        Button sendBttn = getActivity().findViewById(R.id.SendBttn);
        sendBttn.setOnClickListener(send);
        Button cancelBttn = getActivity().findViewById(R.id.cancelBttn);
        cancelBttn.setOnClickListener(cancel);
    }

    private final View.OnClickListener send = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(text_input_Email.getEditText().getText().toString().trim().length()>0){
                mFinishedInterface.Send(text_input_Email.getEditText().getText().toString());
            }
        }
    };

    private final View.OnClickListener cancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mFinishedInterface.Cancel();
        }
    };
}
