package com.modecr.cristobal.neverempty.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Classes.Food;
import com.modecr.cristobal.neverempty.Fragments.CartFragment;
import com.modecr.cristobal.neverempty.Fragments.PantryFragment;
import com.modecr.cristobal.neverempty.Fragments.ScanFragment;

public class MainActivity extends AppCompatActivity implements ScanFragment.CompletionObj, PantryFragment.CompletionObj{

    private TextView mTitle;
    private String onView = "";
    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, ScanFragment.newInstance()).commit();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mTitle = toolbar.findViewById(R.id.toolbar_title);
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_scan);

    }



    @Override
    public void NotFound(String barcode) {
        Intent i = new Intent(MainActivity.this,ViewFoodActivity.class);
        i.putExtra("notbarcode",barcode);
        startActivity(i);

    }

    @Override
    public void Found(Food foundFood) {
        Intent i = new Intent(MainActivity.this,ViewFoodActivity.class);
        i.putExtra("barcode",foundFood.getBarcode());
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Boolean shouldAllowBack = false;
        if (!shouldAllowBack) {

        } else {
            super.onBackPressed();
        }
    }
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_cart:
                    if(!onView.equals("cart")){
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, CartFragment.newInstance()).commit();
                        onView = "cart";
                    }
                    mTitle.setText("Cart");
                    return true;
                case R.id.navigation_scan:
                    if(!onView.equals("scan")){
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, ScanFragment.newInstance()).commit();
                        onView = "scan";
                    }
                    mTitle.setText("Scan");
                    return true;
                case R.id.navigation_pantry:
                    if(!onView.equals("pantry")){
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, PantryFragment.newInstance()).commit();
                        onView = "pantry";
                    }
                    mTitle.setText("Pantry");
                    return true;
            }
            return false;
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.Profile){
           Intent i = new Intent(MainActivity.this,ProfileActivity.class);
           startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void scann() {
        navigation.setSelectedItemId(R.id.navigation_scan);
    }
}
