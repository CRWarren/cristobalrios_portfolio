package com.modecr.cristobal.neverempty.Classes;

public class User {

//    var email: String
//    var photo: Int
//    var firstTime: Bool
//    var id: String
    private String full_name;
    private Boolean first;
    private String email;
    private int photo;
    private String id;



    public User() {}
    public User(String _fname,Boolean _first, String e_mail, int _photo, String _id ){
        full_name =_fname;
        email=e_mail;
        first = _first;
        photo = _photo;
        id=_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
