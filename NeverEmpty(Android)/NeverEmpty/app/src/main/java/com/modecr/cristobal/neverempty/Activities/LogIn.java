package com.modecr.cristobal.neverempty.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Classes.User;
import com.modecr.cristobal.neverempty.Fragments.ForgotPassword;
import com.modecr.cristobal.neverempty.Fragments.LogInFragment;
import com.modecr.cristobal.neverempty.Fragments.NewUserFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class LogIn extends AppCompatActivity implements LogInFragment.CompletionObj, NewUserFragment.CompletionObj, ForgotPassword.CompletionObj {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, LogInFragment.newInstance()).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        if(mAuth.getCurrentUser()!=null){
            StartIntent();
        }
    }
    @Override
    public void signUp() {
        getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, NewUserFragment.newInstance()).commit();
    }

    @Override
    public void logIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {


                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information

                    if(mAuth.getCurrentUser().isEmailVerified()){
                        //Todo
                        //new activity
                        StartIntent();
                    }else{
                        new AlertDialog.Builder(LogIn.this)
                                .setTitle("Verify Account")
                                .setMessage("Check your email for next steps")
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mAuth.getCurrentUser().sendEmailVerification();
                                        mAuth.signOut();
                                        dialog.cancel();
                                    }
                                }).show();
                    }


                } else {
                    try {
                        throw task.getException();
                    }
                    catch(FirebaseTooManyRequestsException e) {
                        Toast.makeText(LogIn.this, "Too Many failed Attempts", Toast.LENGTH_SHORT).show();
                    } catch(FirebaseNetworkException e) {
                        Toast.makeText(LogIn.this, "Not connected to network",Toast.LENGTH_SHORT).show();
                    } catch(FirebaseAuthInvalidUserException e) {
                        Toast.makeText(LogIn.this,"Wrong Email address" ,Toast.LENGTH_SHORT).show();
                    }catch(FirebaseAuthInvalidCredentialsException e){
                        Toast.makeText(LogIn.this,"Wrong Password" ,Toast.LENGTH_SHORT).show();
                    }
                    catch(Exception e) {
                        Log.e("ERROR!", e.getMessage());
                    }
                }

                // ...
            }
        });
    }

    @Override
    public void forgot() {
        getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, ForgotPassword.newInstance()).commit();
    }

    @Override
    public void Cancel() {
        getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, LogInFragment.newInstance()).commit();
    }

    @Override
    public void Save(final User newUser, String _password) {
        mAuth.createUserWithEmailAndPassword(newUser.getEmail(), _password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            newUser.setId(user.getUid());
                            db.collection("users").document(user.getUid()).set(newUser);
                            getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, LogInFragment.newInstance()).commit();
                            user.sendEmailVerification();
                            new AlertDialog.Builder(LogIn.this)
                                    .setTitle("Verify Account")
                                    .setMessage("Check your email for next steps")
                                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LogIn.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void Send(String email) {
        mAuth.sendPasswordResetEmail(email);
        new AlertDialog.Builder(LogIn.this)
                .setTitle("Password Reset Email Sent")
                .setMessage("Check your email for next steps")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.logInFragment, LogInFragment.newInstance()).commit();
                        dialog.cancel();
                    }
                }).show();
    }
    @Override
    public void onBackPressed() {
        Boolean shouldAllowBack = false;
        if (!shouldAllowBack) {

        } else {
            super.onBackPressed();
        }
    }
    private void StartIntent(){
        Intent i = new Intent(LogIn.this,MainActivity.class);
        finish();
        startActivity(i);
    }
}
