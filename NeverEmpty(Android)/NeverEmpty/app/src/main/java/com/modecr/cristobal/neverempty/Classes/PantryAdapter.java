package com.modecr.cristobal.neverempty.Classes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.R;

import com.modecr.cristobal.neverempty.Activities.ViewFoodActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class PantryAdapter extends BaseAdapter {

    private static final long Base_ID = 0x01001;
    private final FireBaseHelper fh = new FireBaseHelper();
    private final Context _Context;
    private final ArrayList<UserFood > _Foods;
    public PantryAdapter(Context _context, ArrayList<UserFood> _foods){
        _Context = _context;
        _Foods = _foods;
    }

    @Override
    public int getCount() {
        if(_Foods!=null){
            return _Foods.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(_Foods != null && (position >= 0||position<_Foods.size())) {
            return _Foods.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID+position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final PantryAdapter.ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.food_cell_layout, parent, false);
            vh =  new PantryAdapter.ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(PantryAdapter.ViewHolder)convertView.getTag();
        }

        final UserFood food = (UserFood) getItem(position);
        if(food!=null){
            vh.Name.setText(food.getName());
            vh.Extra.setText("EXP: "+ food.getExpiration_date());
            vh.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    fh.deleteFromPantry(_Foods.get(position));
                    fh.deleteFromCart(_Foods.get(position));
                    _Foods.remove(_Foods.get(position));

                }
            });
            vh.Name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(_Context,ViewFoodActivity.class);
                    i.putExtra("barcode",_Foods.get(position).getBarcode());
                    i.putExtra("from","pantry");
                    _Context.startActivity(i);
                }
            });
            if(food.getDate_added().equals("")){
                vh.indicator.setVisibility(View.VISIBLE);
                vh.add.setVisibility(View.GONE);
            }else{
                vh.indicator.setVisibility(View.INVISIBLE);
                vh.add.setVisibility(View.VISIBLE);
            }
            vh.add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_Context);
                    String id = preferences.getString("id", "");
                    if(!id.equals("") && !id.equals(mAuth.getCurrentUser().getUid())) {
                        _Foods.get(position).setAddedby(preferences.getString("name",""));
                    }
                    _Foods.get(position).setDate_added("");
                    fh.addPantry(_Foods.get(position));
                    fh.addCart(_Foods.get(position));
                }
            });

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference islandRef = storageRef.child("food_images/"+food.getBarcode());
            final long ONE_MEGABYTE = 1024 * 1024;
            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    // Data for "images/island.jpg" is returns, use this as needed
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                    vh.iv.setImageBitmap(bmp);
                }
            });

        }
        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{


        final Button Name;
        final TextView Extra;
        final Button add;
        final Button delete;
        final ImageView iv;
        final ImageView indicator;
        ViewHolder(View _layout){
            Name = _layout.findViewById(R.id.NameBttn);
            Extra = _layout.findViewById(R.id.extraName);
            add = _layout.findViewById(R.id.AddCartBttn);
            delete = _layout.findViewById(R.id.DeleteBttn);
            iv = _layout.findViewById(R.id.foodImageView);
            indicator = _layout.findViewById(R.id.cartIndicator);

        }
    }


}
