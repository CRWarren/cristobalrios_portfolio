package com.modecr.cristobal.neverempty.Fragments;

import com.modecr.cristobal.neverempty.R;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class LogInFragment extends Fragment {

    private TextInputLayout text_input_Email;
    private TextInputLayout text_input_Password;

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void signUp();
        void logIn(String email, String password);
        void forgot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }


    public static LogInFragment newInstance() {

        Bundle args = new Bundle();

        LogInFragment fragment = new LogInFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.log_in_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        text_input_Email = getActivity().findViewById(R.id.input_Name);
        text_input_Password = getActivity().findViewById(R.id.input_name);
        Button forgot = getActivity().findViewById(R.id.forgotPasswordBttn);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.forgot();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Button signInBttn = getActivity().findViewById(R.id.newUSerBttn);
        signInBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.signUp();
            }
        });
        final Button logInBttn = getActivity().findViewById(R.id.SignInBttn);
        logInBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFinishedInterface.logIn(text_input_Email.getEditText().getText().toString().trim(),text_input_Password.getEditText().getText().toString().trim());
            }
        });
    }
}
