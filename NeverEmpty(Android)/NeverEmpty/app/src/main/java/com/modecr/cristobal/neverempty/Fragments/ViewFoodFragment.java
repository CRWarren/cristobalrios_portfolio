package com.modecr.cristobal.neverempty.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.modecr.cristobal.neverempty.Classes.FireBaseHelper;
import com.modecr.cristobal.neverempty.Classes.Food;
import com.modecr.cristobal.neverempty.Classes.UserFood;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.modecr.cristobal.neverempty.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ViewFoodFragment extends Fragment {
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private final FireBaseHelper fh = new FireBaseHelper();
    private FirebaseStorage storage;
    private Food food;
    private UserFood food1;
    private TextView name;
    private TextView ServingSize;
    private TextView Calories;
    private TextView Sodium;
    private TextView Protein;
    private TextView Fat;
    private TextView Carbs;
    private TextView Ingredients;
    private ImageView foodIV;
    private ProgressBar pb;
    private Button addBttn;
    private String m_Text = "";

    private interface CompletionObj {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            CompletionObj mFinishedInterface = (CompletionObj) context;
        }
    }

    public static ViewFoodFragment newInstance() {

        Bundle args = new Bundle();

        ViewFoodFragment fragment = new ViewFoodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        pb = getActivity().findViewById(R.id.progressBar);
        pb.setIndeterminate(true);
        addBttn = getActivity().findViewById(R.id.AddBttn);
        if(getActivity().getIntent().getStringExtra("barcode")!=null){
            pb.setVisibility(View.VISIBLE);
            String barcode = getActivity().getIntent().getStringExtra("barcode");
            Log.e("FOOD123456", barcode);
            name = getActivity().findViewById(R.id.NameTxt);
            Fat = getActivity().findViewById(R.id.FatTxt);
            Calories = getActivity().findViewById(R.id.CaloriesTxt);
            ServingSize = getActivity().findViewById(R.id.ServingSizeTxt);
            Carbs = getActivity().findViewById(R.id.CarbsTxt);
            Protein = getActivity().findViewById(R.id.ProteinTxt);
            Ingredients = getActivity().findViewById(R.id.IngredientsTxt);
            Sodium = getActivity().findViewById(R.id.SodiumTxt);
            foodIV = getActivity().findViewById(R.id.foodImageView);
            Button cartBttn = getActivity().findViewById(R.id.add_CartBttn);
            ImageButton reportBttn = getActivity().findViewById(R.id.reportBttn);
            reportBttn.setOnClickListener(Report);
            if(getActivity().getIntent().getStringExtra("from")!=null){
                cartBttn.setVisibility(View.VISIBLE);
                addBttn.setVisibility(View.GONE);
                DownloadFoodForCart(barcode);
                cartBttn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addCart();
                    }
                });
            }else{
                cartBttn.setVisibility(View.GONE);
                addBttn.setVisibility(View.VISIBLE);
                DownloadFood(barcode);
            }
        }
        Button Cancel = getActivity().findViewById(R.id.CancelBttn);
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


        addBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               alertDate();
            }
        });

    }


    private final View.OnClickListener Report = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            reportAlert();
        }
    };
    private void DownloadFood(final String barcode){

        DocumentReference secondDocRef = db.collection("food_items").document(barcode);
        secondDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        food = document.toObject(Food.class);
                        UpdateUI();
                    } else {
                        DownloadFoodForCart(barcode);
                    }
                } else {
                    Log.d("1234567890. E", "get failed with ", task.getException());
                }
            }
        });
    }

    private void UpdateUI(){
        if(food!=null){
            name.setText(food.getName());
            ServingSize.setText(food.getServing_size());
            Fat.setText(food.getFat().toString());
            Calories.setText(""+food.getCalories());
            Protein.setText(food.getProteins().toString());
            Sodium.setText(food.getSodium().toString());
            Ingredients.setText(food.getIngredients());
            Carbs.setText(food.getCarbohydrates().toString());
            DownloadImage(food.getBarcode());
        }else if(food1!=null){
            name.setText(food1.getName());
            ServingSize.setText(food1.getServing_size());
            Fat.setText(food1.getFat().toString());
            Calories.setText(""+food1.getCalories());
            Protein.setText(food1.getProteins().toString());
            Sodium.setText(food1.getSodium().toString());
            Ingredients.setText(food1.getIngredients());
            Carbs.setText(food1.getCarbohydrates().toString());
            DownloadImage(food1.getBarcode());
        }
    }

    private void DownloadImage(String barcode){
        StorageReference storageRef = storage.getReference();
        StorageReference islandRef = storageRef.child("food_images/"+barcode);

        final long ONE_MEGABYTE = 1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                // Data for "images/island.jpg" is returns, use this as needed
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                foodIV.setImageBitmap(bmp);
                pb.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                pb.setVisibility(View.GONE);
            }
        });

    }

    private void alertDate(){

        Calendar cal = Calendar.getInstance();
        int _year = cal.get(Calendar.YEAR);
        int _month = cal.get(Calendar.MONTH);
        final int _day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                String todayString = formatter.format(todayDate);


                UserFood temp = new UserFood(mAuth.getCurrentUser().getUid(),""+(month+1)+"/"+""+dayOfMonth+"/"+year,todayString,
                        food.getName(),food.getBarcode(),food.getCalories(),food.getCarbohydrates(),
                        food.getFat(),food.getIngredients(),food.getProteins(),food.getSodium(),
                        food.get_keywords(),food.getServing_size(),food.getServing_quantity());
                fh.addPantry(temp);
                fh.deleteFromCart(temp);
                getActivity().finish();
            }
        }, _year, _month, _day);
        datePicker.show();

    }
    private void DownloadFoodForCart(final String barcode){

        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("pantry").document(barcode);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        food1 = document.toObject(UserFood.class);
                        UpdateUI();
                    } else {
                        DocumentReference secondDocRef = db.collection("food_items").document(barcode);
                        secondDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();
                                    if (document.exists()) {
                                        food1 = document.toObject(UserFood.class);
                                        UpdateUI();
                                    }
                                } else {
                                    Log.d("1234567890. E", "get failed with ", task.getException());
                                }
                            }
                        });
                    }
                } else {
                    Log.d("1234567890. E", "get failed with ", task.getException());
                }
            }
        });
    }
    private void addCart(){
        UserFood temp = new UserFood(mAuth.getCurrentUser().getUid(),food1.getExpiration_date(),"",
                food1.getName(),food1.getBarcode(),food1.getCalories(),food1.getCarbohydrates(),
                food1.getFat(),food1.getIngredients(),food1.getProteins(),food1.getSodium(),
                food1.get_keywords(),food1.getServing_size(),food1.getServing_quantity());
        fh.addCart(temp);
        getActivity().finish();
    }

    private void reportAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Report");
        builder.setMessage("Please tell us what's wrong with this item...\n");

// Set up the input
        final EditText input = new EditText(getActivity());
// Specify the type of input expected; this, for modecr, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                if(m_Text.trim().isEmpty()){
                    reportAlert();
                }else{
                    fh.Report(food,m_Text);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
