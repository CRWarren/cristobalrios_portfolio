package com.modecr.cristobal.neverempty.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.modecr.cristobal.neverempty.R;
import com.modecr.cristobal.neverempty.Classes.FireBaseHelper;
import com.modecr.cristobal.neverempty.Classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.UUID;

public class NewInternalUserFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private ImageView iv;
    private TextInputLayout username;
    private User usertemp;
    private ImageButton blueBttn;
    private ImageButton greenBttn;
    private ImageButton orangeBttn;
    private ImageButton purpleBttn;
    private int photo = 0;
    private final FireBaseHelper fh = new FireBaseHelper();

    private CompletionObj mFinishedInterface;

    public interface CompletionObj {
        void cancel();
    }


    public static NewInternalUserFragment newInstance() {
        Bundle args = new Bundle();
        NewInternalUserFragment fragment = new NewInternalUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_internal_user,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        iv = getActivity().findViewById(R.id.userIV);
        username = getActivity().findViewById(R.id.input_fullname);
        blueBttn = getActivity().findViewById(R.id.blueBttn);
        greenBttn = getActivity().findViewById(R.id.greenBttn);
        orangeBttn = getActivity().findViewById(R.id.orangeBttn);
        purpleBttn = getActivity().findViewById(R.id.purpleBttn);
        blueBttn.setOnClickListener(selectedImage);
        greenBttn.setOnClickListener(selectedImage);
        orangeBttn.setOnClickListener(selectedImage);
        purpleBttn.setOnClickListener(selectedImage);
        Button saveBttn = getActivity().findViewById(R.id.SaveBttn);
        saveBttn.setOnClickListener(saveClicked);
        Button delete = getActivity().findViewById(R.id.Deletebutton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Todo  delete alert --------------------------------------------------------------
                alertDelete();

            }
        });
        Button cancelBttn = getActivity().findViewById(R.id.CancelBttn);
        cancelBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.cancel();
            }
        });

        delete.setVisibility(View.INVISIBLE);
        if( getArguments().getString("id")!=null){
            DownloadInternal();
            delete.setVisibility(View.VISIBLE);
        }
    }

    private  void DownloadInternal(){
        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.collection("internal").document(getArguments().getString("id")).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    usertemp = document.toObject(User.class);
                    photo = usertemp.getPhoto();
                    updateUI();
                }
            }
        });
    }

    private void updateUI(){
        if(usertemp!=null){username.getEditText().setText(usertemp.getFull_name());}
        switch (photo){
            case 0:
                iv.setImageResource(R.drawable.blue);
                break;
            case 1:
                iv.setImageResource(R.drawable.green);
                break;
            case 2:
                iv.setImageResource(R.drawable.orange);
                break;
            case 3:
                iv.setImageResource(R.drawable.purple);
                break;
        }
    }

    private final View.OnClickListener selectedImage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.blueBttn:
                    photo = 0;
                    blueBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                    greenBttn.setBackground(null);
                    orangeBttn.setBackground(null);
                    purpleBttn.setBackground(null);
                    iv.setImageResource(R.drawable.blue);
                    break;
                case R.id.greenBttn:
                    photo = 1;
                    greenBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                    blueBttn.setBackground(null);
                    orangeBttn.setBackground(null);
                    purpleBttn.setBackground(null);
                    iv.setImageResource(R.drawable.green);
                    break;
                case R.id.orangeBttn:
                    photo = 2;
                    orangeBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                    greenBttn.setBackground(null);
                    blueBttn.setBackground(null);
                    purpleBttn.setBackground(null);
                    iv.setImageResource(R.drawable.orange);
                    break;
                case R.id.purpleBttn:
                    photo = 3;
                    purpleBttn.setBackground(getActivity().getDrawable(R.drawable.button_border));
                    greenBttn.setBackground(null);
                    orangeBttn.setBackground(null);
                    blueBttn.setBackground(null);
                    iv.setImageResource(R.drawable.purple);
                    break;
            }

        }
    };

    private final View.OnClickListener saveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           if(username.getEditText().getText().toString().trim().isEmpty()){
               Toast.makeText(getActivity(),"Username is Empty!!!",Toast.LENGTH_SHORT).show();
           }else{
               if(usertemp==null){
                   UUID uuid = UUID.randomUUID();
                   String ID = uuid.toString();
                   usertemp = new User(username.getEditText().getText().toString(),false,"",photo,ID);
               }
               usertemp.setFull_name(username.getEditText().getText().toString());
               usertemp.setPhoto(photo);
               fh.uploadNewInternalUser(usertemp);
               mFinishedInterface.cancel();
           }
        }
    };

    private void alertDelete(){
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Continue to Delete?");
        alert.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fh.DeleteInternalUser(usertemp);
                mFinishedInterface.cancel();
            }
        });
        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alert.show();
    }

}

