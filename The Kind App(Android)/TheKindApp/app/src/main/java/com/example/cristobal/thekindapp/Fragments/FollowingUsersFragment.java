package com.example.cristobal.thekindapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.cristobal.thekindapp.Classes.FollowingUsersAdapter;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FollowingUsersFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private final ArrayList<String> _Following = new ArrayList<>();
    private final ArrayList<String> _Usernames = new ArrayList<>();
    private GridView _gridView;

    public static FollowingUsersFragment newInstance() {

        Bundle args = new Bundle();

        FollowingUsersFragment fragment = new FollowingUsersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.following_users_fragment,container,false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _gridView = getActivity().findViewById(R.id.followingUsersGrid);
        download();
    }
    private void download(){
        _Following.clear();
        _Usernames.clear();
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("following").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        _Following.add(document.getId());
                        _Usernames.add(document.getString("username"));
                    }
                    setupCustomBaseAdapterView();
                }
            }
        });
    }
    private void setupCustomBaseAdapterView() {

        FollowingUsersAdapter aa = new FollowingUsersAdapter(getActivity(), _Usernames,_Following,db,mAuth);
        _gridView.setAdapter(aa);

    }
}
