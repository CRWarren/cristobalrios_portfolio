package com.example.cristobal.thekindapp.Classes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Activities.profileActivity;
import com.example.cristobal.thekindapp.R;
import com.example.cristobal.thekindapp.Activities.ViewProfileActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QuoteAdapter extends BaseAdapter{

        public final static String FOLDER_NAME = "Quotes_Storage";
        private static final long Base_ID = 0x01001;
        private final Context _Context;
        private final ArrayList<Quote > _Quote;
    private final ArrayList<String > _Docs;
    private final String From;
    private int temp = 0;


    public QuoteAdapter(Context _context, ArrayList<Quote> _quote,ArrayList<String> documents, String _from){
        _Context = _context;
        _Quote = _quote;
        _Docs = documents;
        From = _from;
    }
    @Override
    public int getCount() {
        if(_Quote!=null){
            return _Quote.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(_Quote != null && (position >= 0||position<_Quote.size())) {
            return _Quote.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID+position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final QuoteAdapter.ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.quote_layout, parent, false);
            vh =  new QuoteAdapter.ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(QuoteAdapter.ViewHolder)convertView.getTag();
        }

        final Quote quote = (Quote) getItem(position);
        if(quote!=null){
            final FirebaseFirestore db = FirebaseFirestore.getInstance();
            vh.username.setText(quote.getUsername());
            vh.username.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if(quote.getUserId().equals(mAuth.getCurrentUser().getUid())){
                       Intent i = new Intent(_Context, profileActivity.class);
                       i.putExtra("userId",quote.getUserId());
                       _Context.startActivity(i);
                   }else{
                       Intent i = new Intent(_Context, ViewProfileActivity.class);
                       i.putExtra("userId",quote.getUserId());
                       _Context.startActivity(i);
                   }
                }
            });
            vh.quote.setText(quote.getQuote());
            vh.likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DocumentReference docRef = db.collection("quotes").document(_Docs.get(position));
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            FirebaseAuth mAuth = FirebaseAuth.getInstance();
                            FirebaseUser user = mAuth.getCurrentUser();
                            Map<String, Object> like = new HashMap<>();
                            like.put("user", user.getUid());
                                db.collection("quotes").document(_Docs.get(position)).collection("likes").document(user.getUid()).set(like);
                                db.collection("users").document(quote.getUserId()).collection("quotes")
                                        .document(_Docs.get(position)).collection("likes").document(user.getUid()).set(like);
                                likes(position);

                        }
                    });

                }
            });

            if(From.equals("saved")){
                vh.save.setImageResource(R.drawable.ic_delete_black_24dp);
            }
            vh.save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (From){
                        case "saved": alertDelete(position, "saved");
                                       break;
                        case "quote": saveQuote(position); break;
                        case "profile": saveQuote(position); break;
                    }
                }
            });

            if(From == "profile"){
                vh.delete.setVisibility(View.VISIBLE);
                vh.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDelete(position,"profile");
                    }
                });
            }
            db.collection("quotes").document(_Docs.get(position)).collection("likes")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                int likes = 0;
                                for (DocumentSnapshot document : task.getResult()) {
                                    likes++;
                                }
                                temp = likes;
                                vh.answer.setText(temp+" Likes");
                            } else {
                                Log.d("", "Error getting documents: ", task.getException());
                            }
                        }
                    });
        }
        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{

        final Button username;
        final TextView quote;
        final TextView answer;
        final ImageButton likes;
        final ImageButton save;
        final ImageButton delete;
        ViewHolder(View _layout){
            username = _layout.findViewById(R.id.usernameQTxt);
            quote = _layout.findViewById(R.id.QuoteTxt);
            answer = _layout.findViewById(R.id.likesnumber);
            likes = _layout.findViewById(R.id.likeButton);
            save = _layout.findViewById(R.id.SaveBttn);
            delete = _layout.findViewById(R.id.deleteButton);
        }
    }

    private void likes(int position){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes").document(_Docs.get(position)).collection("likes")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int likes = 0;
                            for (DocumentSnapshot document : task.getResult()) {
                                likes++;
                            }
                            temp = likes;
                            notifyDataSetChanged();
                        }
                    }
                });
    }

    private void deleteFile(int position){
        File protectedStorage = _Context.getExternalFilesDir(FOLDER_NAME);
        File imageFile = new File(protectedStorage,_Docs.get(position));
        if(imageFile.exists()){
            imageFile.delete();
            _Quote.remove(position);
            _Docs.remove(position);
            notifyDataSetChanged();
        }
    }

    private void deletePost(final int position){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("quotes").document(_Docs.get(position)).delete();
        db.collection("users").document(_Quote.get(position).getUserId()).collection("quotes").document(_Docs.get(position)).delete();
        _Quote.remove(position);
        _Docs.remove(position);
        notifyDataSetChanged();
    }

    private void alertDelete(final int position, final String deleteFrom){
        AlertDialog.Builder builder = new AlertDialog.Builder(_Context);
        builder.setTitle("Are you sure you want to Delete");
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               switch (deleteFrom){
                   case "saved": deleteFile(position);break;
                   case "profile": deletePost(position);break;
               }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void saveQuote(int position){

        File protectedStorage = _Context.getExternalFilesDir(FOLDER_NAME);

        File quoteFile = new File(protectedStorage,_Docs.get(position));
        try{
            quoteFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(quoteFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(_Quote.get(position));
            oos.close();
            fos.close();
            Toast.makeText(_Context,"Saved",Toast.LENGTH_SHORT).show();

        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
