package com.example.cristobal.thekindapp.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Activities.LogInActivity;
import com.example.cristobal.thekindapp.Activities.ViewProfileActivity;
import com.example.cristobal.thekindapp.Classes.Quote;
import com.example.cristobal.thekindapp.Classes.QuoteAdapter;
import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewProfile_Fragment extends Fragment {

    private String UID;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private final ArrayList<Quote> _Quotes= new ArrayList<>();
    private final ArrayList<String> _Docs= new ArrayList<>();
    private Boolean following = false;
    private Users tempUser;
    private Users tempCurrentUser;
    private TextView username;
    private TextView followers;
    private ImageButton follow;
    private GridView _gridView;
    int temp= 0;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_profile_fragment,container,false);
    }

    public static ViewProfile_Fragment newInstance() {

        Bundle args = new Bundle();

        ViewProfile_Fragment fragment = new ViewProfile_Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity().getIntent().hasExtra("userId")){
            UID = getActivity().getIntent().getStringExtra("userId");
            mAuth = FirebaseAuth.getInstance();
            db = FirebaseFirestore.getInstance();

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        username = getActivity().findViewById(R.id.usernameVP);
        _gridView = getActivity().findViewById(R.id._VPQuotes);
        followers = getActivity().findViewById(R.id.followersVP);
        follow = getActivity().findViewById(R.id.FollowButton);
        checkForFollow();
        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(following){
                   unfollowUser();
               }else { followUser();}
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        download();
    }

    private void download(){
        if(UID!=null){
            undo_followers();
            DocumentReference docRef = db.collection("users").document(UID);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    tempUser = documentSnapshot.toObject(Users.class);
                    username.setText(tempUser.getUserName());
                    _Quotes.clear();
                    _Docs.clear();
                    if(tempUser!=null){
                        db.collection("users").document(UID).collection("quotes").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        Quote temp =document.toObject(Quote.class);
                                        _Quotes.add(temp);
                                        _Docs.add(document.getId());
                                    }
                                    setupCustomBaseAdapterView();
                                } else {
                                    Log.d("", "Error getting documents: ", task.getException());
                                }
                            }
                        });
                        DocumentReference docRef2 = db.collection("users").document(mAuth.getCurrentUser().getUid());
                        docRef2.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                tempCurrentUser = documentSnapshot.toObject(Users.class);
                            }
                        });

                    }else{

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("User Unavailable");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                follow.setEnabled(false);
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
    }

    private void undo_followers(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(UID).collection("followers").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int likes = 0;
                            for (DocumentSnapshot document : task.getResult()) {
                                likes++;
                            }
                            temp = likes;
                            followers.setText(temp+" Followers");
                        }else{
                            Log.e("ON FOLLOWERS","FAILED TO GET FOLLOWERS");
                        }
                    }
                });
    }

    private void checkForFollow(){
        DocumentReference docRef =  db.collection("users").document(mAuth.getCurrentUser()
                .getUid()).collection("following").document(UID);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
               if(documentSnapshot.getData()!=null){
                   follow.setImageResource(R.drawable.follow);
                   follow.setScaleType(ImageView.ScaleType.FIT_XY);
                   following = true;
               }else{following = false;
                   follow.setImageResource(R.drawable.follow_black_24dp);
                   follow.setScaleType(ImageView.ScaleType.FIT_XY);
               }

            }
        });
    }
    private void unfollowUser(){
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("following").document(UID).delete();
        db.collection("users").document(UID).collection("followers").document(mAuth.getCurrentUser().getUid()).delete();
        checkForFollow();
    }

    private void followUser(){
        Map<String, Object> follower = new HashMap<>();
        follower.put("username", tempUser.getUserName());
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("following").document(UID).set(follower);
        follower.remove("username");
        follower.put("username", tempCurrentUser.getUserName());
        db.collection("users").document(UID).collection("followers").document(mAuth.getCurrentUser().getUid()).set(follower);
        checkForFollow();
    }
    private void setupCustomBaseAdapterView() {

        QuoteAdapter aa = new QuoteAdapter(getActivity(), _Quotes,_Docs,"quote");
        _gridView.setAdapter(aa);

    }
}
