package com.example.cristobal.thekindapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.cristobal.thekindapp.Classes.Quote;
import com.example.cristobal.thekindapp.Classes.QuoteAdapter;
import com.example.cristobal.thekindapp.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static com.example.cristobal.thekindapp.Classes.QuoteAdapter.FOLDER_NAME;

public class SavedFragment extends Fragment {
    private final ArrayList<Quote> _Quotes = new ArrayList<>();
    private GridView _gridView;
    private final ArrayList<String>_Documents = new ArrayList<>();

    public static SavedFragment newInstance() {

        Bundle args = new Bundle();

        SavedFragment fragment = new SavedFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.saved_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _gridView = getActivity().findViewById(R.id.savedQuotes);

    }

    private void setupCustomBaseAdapterView() {

        QuoteAdapter aa = new QuoteAdapter(getActivity(), _Quotes,_Documents,"saved");
        _gridView.setAdapter(aa);
    }


    @Override
    public void onStart() {
        super.onStart();
        readQuote();
    }


    private void readQuote(){
        _Quotes.clear();
        _Documents.clear();
        File protectedStorage = getActivity().getExternalFilesDir(FOLDER_NAME);
        if (protectedStorage != null) {
            for (String f : protectedStorage.list()) {
                try{
                    FileInputStream fis = new FileInputStream(protectedStorage + "/" + f);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    _Documents.add(f);
                    Quote quote = (Quote)ois.readObject();
                    _Quotes.add(quote);
                    ois.close();
                    fis.close();
                }catch (IOException | ClassNotFoundException e){
                    e.printStackTrace();
                }
            }
            setupCustomBaseAdapterView();
        }
    }
}
