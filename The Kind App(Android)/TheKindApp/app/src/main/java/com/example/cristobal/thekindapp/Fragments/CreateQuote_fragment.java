package com.example.cristobal.thekindapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Classes.Quote;
import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.UUID;

public class CreateQuote_fragment extends Fragment {

    private FirebaseFirestore db;
    private FirebaseUser user;
    private EditText quoteText;
    private TextView username;
    private Users tempUser;
    public static CreateQuote_fragment newInstance() {

        Bundle args = new Bundle();

        CreateQuote_fragment fragment = new CreateQuote_fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        username = getActivity().findViewById(R.id.qUsernameTxt);
        download();
        quoteText = getActivity().findViewById(R.id.QquoteTxt);
        Button submit = getActivity().findViewById(R.id.buttonSubmit);
        submit.setOnClickListener(submitListener);
    }
    private final View.OnClickListener submitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(quoteText.getText().toString().trim().length()>0){
                long milis = java.lang.System.currentTimeMillis();
                Quote quote = new Quote(user.getUid(),tempUser.getUserName(),quoteText.getText().toString(),milis);
                String uniqueID = UUID.randomUUID().toString();
                db.collection("quotes").document(milis+"").set(quote);
                db.collection("users").document(user.getUid()).collection("quotes").document(milis+"").set(quote);
//                db.collection("quotes").document(uniqueID+"_"+user.getUid()).set(quote);
//                db.collection("users").document(user.getUid()).collection("quotes").document(uniqueID+"_"+user.getUid()).set(quote);
                getActivity().finish();
            }else{
                Toast.makeText(getActivity(),"Please fill the Text space", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void download(){
        DocumentReference docRef = db.collection("users").document(user.getUid());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                tempUser = documentSnapshot.toObject(Users.class);
                username.setText(tempUser.getUserName());
            }
        });
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_quote_fragment,container,false);
    }


}
