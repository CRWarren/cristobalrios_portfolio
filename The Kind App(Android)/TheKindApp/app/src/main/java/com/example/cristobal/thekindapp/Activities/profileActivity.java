package com.example.cristobal.thekindapp.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cristobal.thekindapp.Classes.SectionsPageAdapter;
import com.example.cristobal.thekindapp.Fragments.FollowingUsersFragment;
import com.example.cristobal.thekindapp.Fragments.SavedFragment;
import com.example.cristobal.thekindapp.Fragments.profileFragment;
import com.example.cristobal.thekindapp.R;

public class profileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        //set up ViewPager with sections adapter
        ViewPager mViewPager = findViewById(R.id.container);
        setUpViewPager(mViewPager);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_profile);
    }

    private void setUpViewPager(ViewPager viewPager){

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new profileFragment(),"Profile");
        adapter.addFragment(new FollowingUsersFragment(),"Following");
        adapter.addFragment(new SavedFragment(),"Saved");
        viewPager.setAdapter(adapter);
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_quote:
                    Intent i = new Intent(profileActivity.this, MainActivity.class);
                    profileActivity.this.finish();
                    startActivity(i);
                    return true;
                case R.id.navigation_post:
                    Intent ii = new Intent(profileActivity.this, shareActivity.class);
                    profileActivity.this.finish();
                    startActivity(ii);
                    return true;
                case R.id.navigation_profile:

                    return true;
            }
            return false;
        }
    };






}
