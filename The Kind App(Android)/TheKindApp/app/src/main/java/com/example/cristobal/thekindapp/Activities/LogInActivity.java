package com.example.cristobal.thekindapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Fragments.LogInFragment;
import com.example.cristobal.thekindapp.R;

import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.FirebaseFirestore;


public class LogInActivity extends AppCompatActivity implements LogInFragment.CompletionObj{

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.initialScreenContainer, LogInFragment.newInstance()).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        if(mAuth.getCurrentUser()!=null){
            startIntent();
        }
    }

    @Override
    public void signUp() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void logIn(String email, String password) {
        try {
            mAuth.signInWithEmailAndPassword(email, password);

        }catch (Exception e){e.printStackTrace();}
        if(mAuth.getCurrentUser()!=null){
          startIntent();
        }
    }

    private void startIntent(){
        if(validConnection()){
            Intent i = new Intent(LogInActivity.this,MainActivity.class);
            finish();
            Toast.makeText(LogInActivity.this, "SignedIn",
                    Toast.LENGTH_SHORT).show();
            startActivity(i);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Internet Connection");
            builder.setMessage("app may not work properly without internet access");

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(LogInActivity.this,MainActivity.class);
                    finish();
                    startActivity(i);
                }
            });
            builder.show();
        }
    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }




}
