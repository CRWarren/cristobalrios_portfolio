package com.example.cristobal.thekindapp.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.thekindapp.Fragments.ViewProfile_Fragment;
import com.example.cristobal.thekindapp.R;

public class ViewProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        getSupportFragmentManager().beginTransaction().replace(R.id.profileContainer, ViewProfile_Fragment.newInstance()).commit();
    }
}
