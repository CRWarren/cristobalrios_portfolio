package com.example.cristobal.thekindapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.cristobal.thekindapp.Classes.Quote;
import com.example.cristobal.thekindapp.Classes.QuoteAdapter;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FollowingQuotes_Fragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GridView _gridView;
    private ArrayList<Quote> _Quote = new ArrayList<>();
    private ArrayList<String> _Documents = new ArrayList<>();
    private final ArrayList<String> _Following = new ArrayList<>();


    public static Quotes_Fragment newInstance() {
        Bundle args = new Bundle();
        Quotes_Fragment fragment = new Quotes_Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.quotes_following_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _gridView = getActivity().findViewById(R.id.quotesFollowingGrid);
    }

    @Override
    public void setUserVisibleHint(boolean visible)
    {
        super.setUserVisibleHint(visible&&isResumed());
        if (visible)
        {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            _Quote.clear();
            _Documents.clear();
            downlodQuotes();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        _Following.clear();
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("following").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot document : task.getResult()){
                        String docId= document.getId();
                        _Following.add(docId);
                    }
                }
            }
        });
    }

    private void setupCustomBaseAdapterView() {

        QuoteAdapter aa = new QuoteAdapter(getActivity(), _Quote,_Documents,"quote");
        _gridView.setAdapter(aa);

    }

    private void downlodQuotes(){
        if(mAuth.getCurrentUser()!=null){
            db.collection("quotes").orderBy("milis", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String[] docId= document.getId().split("_");
                            Quote temp =document.toObject(Quote.class);
                            for (String userId:_Following) {
                                if(temp.getUserId().equals(userId)){
                                    _Quote.add(temp);
                                    _Documents.add(document.getId());
                                }
                            }

                        }
                        setupCustomBaseAdapterView();
                    } else {
                        Log.d("", "Error getting documents: ", task.getException());
                    }
                }
            });

        }

    }

}
