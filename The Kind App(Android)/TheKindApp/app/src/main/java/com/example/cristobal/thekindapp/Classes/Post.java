package com.example.cristobal.thekindapp.Classes;

public class Post {
    private String userID;
    private String userName;
    private String question;
    private String answer;
    private String answerID;
    private String postId;

    public Post(){}
    public Post(String UID, String _question, String _answer, String _userName, String _AnswerId, String _PostId){
        userID = UID;
        question = _question;
        answer= _answer;
        userName = _userName;
        answerID = _AnswerId;
        postId = _PostId;
    }


    public String getUserName() {
        return userName;
    }

    public String getAnswer() {
        return answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getUserID() {
        return userID;
    }

    public String getAnswerID() {
        return answerID;
    }

    public String getPostId() {
        return postId;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setAnswerID(String answerID) {
        this.answerID = answerID;
    }
}
