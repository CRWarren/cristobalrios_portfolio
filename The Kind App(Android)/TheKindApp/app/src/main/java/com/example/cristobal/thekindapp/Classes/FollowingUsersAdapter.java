package com.example.cristobal.thekindapp.Classes;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.cristobal.thekindapp.Activities.ViewProfileActivity;
import com.example.cristobal.thekindapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class FollowingUsersAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<String > _UIDS;
    private final ArrayList<String > _Usernames;
    private final FirebaseFirestore db;
    private final FirebaseAuth mAuth;

    public FollowingUsersAdapter(Context context,ArrayList<String> usernames,
                                 ArrayList<String> UIDS,FirebaseFirestore _db, FirebaseAuth _mAuth){
        _UIDS = UIDS;
        _Usernames = usernames;
        _Context =context;
        db =_db;
        mAuth = _mAuth;
    }

    @Override
    public int getCount() {
        if(_UIDS!=null){
            return _UIDS.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(_UIDS != null && (position >= 0||position<_UIDS.size())) {
            return _UIDS.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID+position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FollowingUsersAdapter.ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.following_users_layout, parent, false);
            vh =  new FollowingUsersAdapter.ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(FollowingUsersAdapter.ViewHolder)convertView.getTag();
        }
        vh.username.setText(_Usernames.get(position));
        vh.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(_Context, ViewProfileActivity.class);
                i.putExtra("userId",_UIDS.get(position));
                _Context.startActivity(i);
            }
        });
        vh.following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(_Context);
                builder.setTitle("Are you sure you want to unfollow this user?");
                builder.setMessage(_Usernames.get(position));
                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        unfollowUser(position);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{

        final Button username;
        final ImageButton following;
        ViewHolder(View _layout){
            username = _layout.findViewById(R.id.usernameButton);
            following = _layout.findViewById(R.id.followingButton);
        }
    }

    private void unfollowUser(int position){
        db.collection("users").document(mAuth.getCurrentUser().getUid()).collection("following").document(_UIDS.get(position)).delete();
        db.collection("users").document(_UIDS.get(position)).collection("followers").document(mAuth.getCurrentUser().getUid()).delete();
        _Usernames.remove(position);
        _UIDS.remove(position);
        notifyDataSetChanged();

    }

}
