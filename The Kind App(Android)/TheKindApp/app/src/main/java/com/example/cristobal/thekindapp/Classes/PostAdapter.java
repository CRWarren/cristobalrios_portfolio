package com.example.cristobal.thekindapp.Classes;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class PostAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<Post > _Post;

    public PostAdapter(Context _context, ArrayList<Post> _post){
        _Context = _context;
        _Post = _post;
    }
    @Override
    public int getCount() {
        if(_Post!=null){
            return _Post.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(_Post != null && (position >= 0||position<_Post.size())) {
            return _Post.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID + position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.posts_layout, parent, false);
            vh =  new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ViewHolder)convertView.getTag();
        }

        Post post = (Post) getItem(position);
        if(post!=null){
            vh.username.setText(post.getUserName());
            vh.question.setText(post.getQuestion());
            vh.answer.setText(post.getAnswer());
            vh.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  alertDelete(position);
                }
            });
            Log.e("Report Button",post.getAnswerID()+"");
            vh.report.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertReport(position);
                }
            });
            if(post.getAnswerID().length()<=0){
                vh.report.setVisibility(View.INVISIBLE);
            }
        }

        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{

        final TextView username;
        final TextView question;
        final TextView answer;
        final ImageButton delete;
        final ImageButton report;

        ViewHolder(View _layout){
            username = _layout.findViewById(R.id.usernameSTXT);
            question = _layout.findViewById(R.id.questionTxt);
            answer = _layout.findViewById(R.id.answerTxt);
            delete = _layout.findViewById(R.id.deleteButton);
            report = _layout.findViewById(R.id.reportButton);
        }
    }

    private void delete(int position){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(_Post.get(position).getUserID())
                .collection("posts").document(_Post.get(position).getPostId()).delete();
        if(_Post.get(position).getAnswer().length()<=0){
            db.collection("posts").document(_Post.get(position).getPostId()).delete();
        }
        _Post.remove(position);
        notifyDataSetChanged();
    }

    private void reportUser(final int position){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("users").document(_Post.get(position).getAnswerID());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Users tempUser = documentSnapshot.toObject(Users.class);
                int rep = tempUser.getReports()+1;
                tempUser.setReports(rep);
                db.collection("users").document(_Post.get(position).getAnswerID()).set(tempUser);
            }
        });
    }

    private void alertReport(final int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(_Context);
        builder.setTitle("Are you sure you want to Delete");
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                reportUser(position);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void alertDelete(final int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(_Context);
        builder.setTitle("Are you sure you want to Delete");
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delete(position);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}