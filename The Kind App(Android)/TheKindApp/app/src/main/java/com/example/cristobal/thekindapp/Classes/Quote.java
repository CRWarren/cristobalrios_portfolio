package com.example.cristobal.thekindapp.Classes;

import java.io.Serializable;

public class Quote implements Serializable{
    private String userId;
    private String username;
    private String quote;
    private long milis;


    public Quote(){}

    public Quote(String uid, String _username, String _quote,long _milis){
     userId=uid;
     username = _username;
     quote =_quote;
     milis = _milis;
    }



    public String getQuote() {
        return quote;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public long getMilis() {
        return milis;
    }
}
