package com.example.cristobal.thekindapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import com.example.cristobal.thekindapp.Classes.SectionsPageAdapter;
import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.Fragments.FollowingQuotes_Fragment;
import com.example.cristobal.thekindapp.Fragments.Quotes_Fragment;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //set up ViewPager with sections adapter
        ViewPager mViewPager = findViewById(R.id.containerQuote);
        setUpViewPager(mViewPager);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mAuth= FirebaseAuth.getInstance();
        db=FirebaseFirestore.getInstance();
        downloadUser();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quote_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.addButton){
            Intent i = new Intent(this, CreateQuoteActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpViewPager(ViewPager viewPager){

        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new Quotes_Fragment(),"For You");
        adapter.addFragment(new FollowingQuotes_Fragment(),"Following");

        viewPager.setAdapter(adapter);
    }


    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_quote:

                    return true;
                case R.id.navigation_post:
                    Intent i = new Intent(MainActivity.this, shareActivity.class);
                    finish();
                    startActivity(i);

                    return true;
                case R.id.navigation_profile:
                    Intent ii = new Intent(MainActivity.this, profileActivity.class);
                    finish();
                    startActivity(ii);
                    return true;
            }
            return false;
        }
    };

    private void downloadUser(){

        DocumentReference docRef = db.collection("users").document(mAuth.getCurrentUser().getUid());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Users tempUser = documentSnapshot.toObject(Users.class);
                if(tempUser!=null){
                    if(tempUser.getReports()>=5){
                        alertUpdate(tempUser.getEmail());
                    }
                }

            }
        });
    }

    private void alertUpdate(final String email){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Security Measure");
        builder.setMessage("Please enter password before continuing");
        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = input.getText().toString();
                deleteUserQuotes(password, email);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    private void deleteUserQuotes(final String _password, final String email){
        db.collection("quotes").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String[] docId= document.getId().split("_");
                        if(docId[0].equals(mAuth.getCurrentUser().getUid())){
                            db.collection("quotes").document(document.getId()).delete();
                            db.collection("users").document(mAuth.getCurrentUser()
                                    .getUid()).collection("quotes").document(document.getId()).delete();
                            db.collection("users").document(mAuth.getCurrentUser()
                                    .getUid()).delete();
                        }
                    }
                    deleteUserPosts(_password, email);
                } else {
                    Log.d("", "Error getting documents: ", task.getException());
                }
            }
        });
    }
    private void deleteUserPosts(final String _password,final String email){
        db.collection("posts").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String[] docId= document.getId().split("_");
                        if(docId[0].equals(mAuth.getCurrentUser().getUid())){
                            db.collection("posts").document(document.getId()).delete();
                        }
                    }
                    deleteUser(_password, email);
                } else {
                    Log.d("", "Error getting documents: ", task.getException());
                }
            }
        });
    }
    private void deleteUser(String _password, String email){

        final FirebaseUser user = mAuth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider
                .getCredential(email, _password);
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        user.delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                            builder.setTitle("You have been deleted");
                                            builder.setMessage("Multiple reports on your account led to your account been deleted.");
                                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent i = new Intent(MainActivity.this,LogInActivity.class);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            });
                                            builder.show();
                                        }else{
                                            Log.e("UPDATED FAILED", "User email address FAILED.");

                                        }
                                    }
                                });
                    }
                });

    }
}
