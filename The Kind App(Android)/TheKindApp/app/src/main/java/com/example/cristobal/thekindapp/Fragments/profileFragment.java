package com.example.cristobal.thekindapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.cristobal.thekindapp.Activities.AccountActivity;
import com.example.cristobal.thekindapp.Classes.Quote;
import com.example.cristobal.thekindapp.Classes.QuoteAdapter;
import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class profileFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser user;
    private Users tempUser;
    private TextView username;
    private TextView followers;
    private GridView _gridview;
    private final ArrayList<Quote> _Quotes = new ArrayList<>();
    private final ArrayList<String> _Docs = new ArrayList<>();
    int temp= 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

    }

    @Override
    public void onStart() {
        super.onStart();
        download();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        username = getActivity().findViewById(R.id.usernameProfile);
        followers = getActivity().findViewById(R.id.followersProfile);
        _gridview = getActivity().findViewById(R.id._profileQuotes);
        user = mAuth.getCurrentUser();
        ImageButton account = getActivity().findViewById(R.id.accountButton);
        account.setOnClickListener(accountListener);
    }

    private final View.OnClickListener accountListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getActivity(), AccountActivity.class);
            startActivity(i);
        }
    };

    private void download(){
        if(user!=null){
            undo_followers();
            DocumentReference docRef = db.collection("users").document(user.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    tempUser = documentSnapshot.toObject(Users.class);
                    username.setText(tempUser.getUserName());
                }
            });
            _Quotes.clear();
            _Docs.clear();
            if(mAuth.getCurrentUser()!=null){
                db.collection("users").document(user.getUid()).collection("quotes").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Quote temp =document.toObject(Quote.class);
                                _Quotes.add(temp);
                                _Docs.add(document.getId());
                            }
                            setupCustomBaseAdapterView();
                        } else {
                            Log.d("", "Error getting documents: ", task.getException());
                        }
                    }
                });

            }
        }
    }
    private void undo_followers(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users").document(user.getUid()).collection("followers").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    int likes = 0;
                    for (DocumentSnapshot document : task.getResult()) {
                        likes++;
                    }
                    temp = likes;
                    followers.setText(temp+" Followers");
                }else{
                    Log.e("ON FOLLOWERS","FAILED TO GET FOLLOWERS");
                }
            }
        });
    }

    private void setupCustomBaseAdapterView() {

        QuoteAdapter aa = new QuoteAdapter(getActivity(), _Quotes,_Docs,"profile");
        _gridview.setAdapter(aa);

    }



}
