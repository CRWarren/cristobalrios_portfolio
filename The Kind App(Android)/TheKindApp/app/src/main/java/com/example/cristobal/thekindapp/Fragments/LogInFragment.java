package com.example.cristobal.thekindapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.cristobal.thekindapp.R;

public class LogInFragment extends Fragment {

    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void signUp();
        void logIn(String email, String password);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    public static LogInFragment newInstance() {
        
        Bundle args = new Bundle();
        
        LogInFragment fragment = new LogInFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_screen, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Button signInBttn = getActivity().findViewById(R.id.signUpBttn2);
        signInBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.signUp();
            }
        });
        final Button logInBttn = getActivity().findViewById(R.id.logInBttn);
        final EditText emailTxt = getActivity().findViewById(R.id.emailInput);
        final EditText passwordTxt = getActivity().findViewById(R.id.passwordInut);
        logInBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishedInterface.logIn(emailTxt.getText().toString(),passwordTxt.getText().toString());
            }
        });
    }
}
