package com.example.cristobal.thekindapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.R;

public class SignUpFragment extends Fragment {

    private  finishedObj signedUp;
    public interface finishedObj{
        void register(Users user, String _password);

    }



    private EditText firstName;
    private EditText email;
    private EditText useName;
    private EditText password;
    private EditText repassword;
    public static SignUpFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.signup_screen, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof finishedObj){
            signedUp = (finishedObj) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Button signup = getActivity().findViewById(R.id.signUpBttn2);
        firstName = getActivity().findViewById(R.id.firstText);
        email = getActivity().findViewById(R.id.EmailText);
        useName = getActivity().findViewById(R.id.userText);
        password = getActivity().findViewById(R.id.passwordText);
        repassword = getActivity().findViewById(R.id.repasswordText);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    Users user = new Users(firstName.getText().toString(),false,
                            email.getText().toString(),useName.getText().toString(),0);
                    signedUp.register(user,password.getText().toString());
                }
            }
        });

        final Button cancelBttn = getActivity().findViewById(R.id.CancelBttn);
        cancelBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    private Boolean validate(){
        String fname = firstName.getText().toString().trim();
        String _email = email.getText().toString().trim();
        String usern = useName.getText().toString().trim();
        String _password = password.getText().toString().trim();
        String _repassword = repassword.getText().toString().trim();

        if(fname.isEmpty()) {
            showtoast("Full Name");
            return false;
        }else  if(_email.isEmpty()){
            showtoast("Email");
            return false;
        }else if(!Patterns.EMAIL_ADDRESS.matcher(_email).matches()){
            showtoast("Email");
            return false;
        }else if(usern.isEmpty()){
            showtoast("username");
            return false;
        }else if(_password.isEmpty()){
            showtoast("password");
            return false;
        }else if(_repassword.isEmpty()){
            showtoast("re-password");
            return false;
        }else if(_password.length()<6 || _repassword.length()<6){
            Toast.makeText(getActivity(),"Password too Short! min. length 6",Toast.LENGTH_SHORT).show();
            return false;
        }else if(!_password.matches(_repassword)){
            Toast.makeText(getActivity(),"Passwords doesn't match",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    private void showtoast(String missing){
        Toast.makeText(getActivity(),"please fill "+missing+" Field Correctly!",Toast.LENGTH_SHORT).show();
    }

}
