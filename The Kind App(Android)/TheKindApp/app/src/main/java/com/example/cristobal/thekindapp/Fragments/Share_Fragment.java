package com.example.cristobal.thekindapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.cristobal.thekindapp.Classes.Post;
import com.example.cristobal.thekindapp.Classes.PostAdapter;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class Share_Fragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private GridView _gridView;
    private final ArrayList<Post> _Posts = new ArrayList<>();

    public static Share_Fragment newInstance() {
        
        Bundle args = new Bundle();
        
        Share_Fragment fragment = new Share_Fragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.share_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _gridView = getActivity().findViewById(R.id.postsGrid);
        downlodPosts();
    }

    private void setupCustomBaseAdapterView() {

        PostAdapter aa = new PostAdapter(getActivity(), _Posts);
        _gridView.setAdapter(aa);

    }

    private void downlodPosts(){
        _Posts.clear();
        if(mAuth.getCurrentUser()!=null){
            FirebaseUser user = mAuth.getCurrentUser();
            db.collection("users").document(user.getUid()).collection("posts").limit(10).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Post temp =document.toObject(Post.class);
                            _Posts.add(temp);
                        }
                        setupCustomBaseAdapterView();
                    } else {
                        Log.d("", "Error getting documents: ", task.getException());
                    }
                }
            });
        }
    }
}
