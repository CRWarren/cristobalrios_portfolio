package com.example.cristobal.thekindapp.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.thekindapp.Fragments.CreateQuote_fragment;
import com.example.cristobal.thekindapp.R;

public class CreateQuoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createquote);
        getSupportFragmentManager().beginTransaction().replace(R.id.create_container, CreateQuote_fragment.newInstance()).commit();
    }

}
