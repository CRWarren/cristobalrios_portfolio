package com.example.cristobal.thekindapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.cristobal.thekindapp.Classes.Post;
import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.Fragments.Share_Fragment;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class shareActivity extends AppCompatActivity {

    private String m_Text;
    private FirebaseFirestore db;
    private FirebaseAuth m_Auth;
    private Users PostUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        getSupportFragmentManager().beginTransaction().replace(R.id.postsContainer, Share_Fragment.newInstance()).commit();
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_post);
        db = FirebaseFirestore.getInstance();
        m_Auth = FirebaseAuth.getInstance();

        FirebaseUser user = m_Auth.getCurrentUser();
        if(user.getUid()!=null){
            DocumentReference docRef = db.collection("users").document(user.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    PostUser = documentSnapshot.toObject(Users.class);
                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().beginTransaction().replace(R.id.postsContainer, Share_Fragment.newInstance()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.add_share){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("How are you feeling today?");
            // Set up the input
            final EditText input = new EditText(this);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    m_Text = input.getText().toString();
                    share();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }else if(item.getItemId() ==R.id.answer){
            Intent i = new Intent(this, AnswerActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().beginTransaction().replace(R.id.postsContainer, Share_Fragment.newInstance()).commit();
    }

    private void share(){
        FirebaseUser user = m_Auth.getCurrentUser();
        String UID = user.getUid();
        long millis = java.lang.System.currentTimeMillis();
        String username = PostUser.getUserName();
        if(PostUser.getVisibility()){
            username = "Anonymous";
        }
        Post post = new Post(UID,m_Text,"",username,"",UID+"_"+millis);
        db.collection("users").document(UID).collection("posts").document(UID+"_"+millis).set(post);
        db.collection("posts").document(UID+"_"+millis).set(post);
        getSupportFragmentManager().beginTransaction().replace(R.id.postsContainer, Share_Fragment.newInstance()).commit();
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_quote:
                    Intent i = new Intent(shareActivity.this, MainActivity.class);
                    finish();
                    startActivity(i);
                    return true;
                case R.id.navigation_post:

                    return true;
                case R.id.navigation_profile:
                    Intent ii = new Intent(shareActivity.this, profileActivity.class);
                    finish();
                    startActivity(ii);
                    return true;
            }
            return false;
        }
    };
}
