package com.example.cristobal.thekindapp.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.thekindapp.Fragments.AnswerPostsFragment;
import com.example.cristobal.thekindapp.R;

public class AnswerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        getSupportFragmentManager().beginTransaction().replace(R.id.answer_container, AnswerPostsFragment.newInstance()).commit();
    }
}
