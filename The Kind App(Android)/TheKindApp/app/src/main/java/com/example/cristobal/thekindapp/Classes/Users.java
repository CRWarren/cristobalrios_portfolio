package com.example.cristobal.thekindapp.Classes;
public class Users {

    private String fName;
    private Boolean visibility;
    private String email;
    private String userName;
    private int reports;


    public Users() {}
    public Users(String first,Boolean vis, String e_mail, String user, int rep ){
        fName=first;
        email=e_mail;
        userName=user;
        visibility = vis;
        reports = rep;
    }

    public String getEmail() {
        return email;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public String getfName() {
        return fName;
    }

    public String getUserName() {
        return userName;
    }

    public int getReports() {
        return reports;
    }

    public void setReports(int reports) {
        this.reports = reports;
    }
}
