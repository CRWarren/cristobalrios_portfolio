package com.example.cristobal.thekindapp.Fragments;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Classes.Post;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class AnswerPostsFragment extends Fragment {

    private TextView username;
    private TextView question;
    private EditText answer;
    private String docId = "";
    private Post post;
    private Button submit;
    private FirebaseUser user;

    public static AnswerPostsFragment newInstance() {
        
        Bundle args = new Bundle();
        
        AnswerPostsFragment fragment = new AnswerPostsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.answer_post,container,false);
    }

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        username = getActivity().findViewById(R.id.usernameSTXT);
        question = getActivity().findViewById(R.id.questionTxt);
        answer = getActivity().findViewById(R.id.answerTxt);
        submit = getActivity().findViewById(R.id.SubmitButton);
        submit.setVisibility(View.INVISIBLE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
        user = mAuth.getCurrentUser();
        download();
    }

    private void submit(){
       if(answer.getText().toString().trim().length()>0){
           post.setAnswer(answer.getText().toString());
           post.setAnswerID(mAuth.getCurrentUser().getUid());
           db.collection("users").document(post.getUserID()).collection("posts").document(docId).set(post);
           db.collection("posts").document(docId).delete();
           getActivity().finish();
       }else{
           Toast.makeText(getActivity(),"please answer question",Toast.LENGTH_SHORT).show();
       }
    }

    private void download(){
        db.collection("posts").limit(10).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        post = document.toObject(Post.class);
                        docId = document.getId();


                       if(post.getUserID().equals(user.getUid())){
                           post = null;
                       }else {
                           break;
                        }
                    }
                }
                if(post!=null){
                    username.setText(post.getUserName());
                    question.setText(post.getQuestion());
                    submit.setVisibility(View.VISIBLE);
                }else{
                    Toast.makeText(getActivity(),"No Posts Available",Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            }
        });

    }
}
