package com.example.cristobal.thekindapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.cristobal.thekindapp.Classes.Users;
import com.example.cristobal.thekindapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class AccountActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private EditText fname;
    private EditText email;
    private EditText username;
    private Switch visibility;
    private ImageButton editBttn;
    private ImageButton SaveButton;
    private Users tempUser;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        editBttn = findViewById(R.id.editButton);
        fname = findViewById(R.id.fNameTxt);
        email = findViewById(R.id.emailTxt);
        username = findViewById(R.id.usernameTxt);
        visibility = findViewById(R.id.visibilitySwitch);
        visibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateUser();
            }
        });
        Button signOut = findViewById(R.id.SignOutButton);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mAuth.signOut();
                    Intent i  = new Intent(AccountActivity.this, LogInActivity.class);
                    finish();
                    startActivity(i);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        FirebaseUser user=mAuth.getCurrentUser();

        if(user!=null){
            DocumentReference docRef = db.collection("users").document(user.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    tempUser = documentSnapshot.toObject(Users.class);
                    fname.setText(tempUser.getfName());
                    email.setText(tempUser.getEmail());
                    username.setText(tempUser.getUserName());
                    visibility.setChecked(tempUser.getVisibility());
                    editBttn.setOnClickListener(editListener);
                }
            });
        }
    }


    private final View.OnClickListener editListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fname.setEnabled(true);
            email.setEnabled(true);
            username.setEnabled(true);
            SaveButton = findViewById(R.id.saveButton);
            SaveButton.setVisibility(View.VISIBLE);
            SaveButton.setOnClickListener(saveListener);
            editBttn.setVisibility(View.GONE);
        }
    };

    private final View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

           if(email.getText().toString().equals(tempUser.getEmail())){
               updateUser();
           }else{
               alertUpdate();
           }
        }
    };

    private void alertUpdate(){
        AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);
        builder.setTitle("Security Measure");
        builder.setMessage("for your security please enter password before updating your email");
        // Set up the input
        final EditText input = new EditText(AccountActivity.this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                password = input.getText().toString();
                updateUser(password);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateUser(){
        FirebaseUser user = mAuth.getCurrentUser();
        Users temp = new Users(fname.getText().toString(),visibility.isChecked(),
                tempUser.getEmail(),username.getText().toString(),tempUser.getReports());
        db.collection("users").document(user.getUid()).set(temp);
        updateUI();
    }

    private void updateUser(String _password){

        final FirebaseUser user = mAuth.getCurrentUser();
        AuthCredential credential = EmailAuthProvider
                .getCredential(tempUser.getEmail(), _password);
        Users temp = new Users(fname.getText().toString(),visibility.isChecked(),
                email.getText().toString(),username.getText().toString(),tempUser.getReports());
        db.collection("users").document(user.getUid()).set(temp);
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        user.updateEmail( email.getText().toString())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            updateUI();
                                        }else{
                                            Log.e("UPDATED FAILED", "User email address FAILED.");

                                        }
                                    }
                                });
                    }
                });

    }

    private void updateUI(){
        SaveButton.setVisibility(View.GONE);
        editBttn.setVisibility(View.VISIBLE);
        fname.setEnabled(false);
        email.setEnabled(false);
        username.setEnabled(false);
    }
}

