# The Kind App

### The Kind App is an app were users can create content to boost or improve your day, when you are feeling low you can share what you are feeling and receive an anonymous answer.

### t contains a section of quotes, where users can view or create inspirational, motivational or just positive content.

### It also contains the section where a user is feeling low, or wants a helpful tip, users can share those feelings in a anonymous way or not! you decide, and another user will anonymously reply to what you shared!

### you have the ability to follow other users to see what they Share, like, and even save those posts on your device to be viewed when there’s no internet access.

### This app is for everyone from happy to grumpy people, from teens to adults and senior citizens, everyone can join in and share kindness to one another

#### Created By Cristobal Rios 
