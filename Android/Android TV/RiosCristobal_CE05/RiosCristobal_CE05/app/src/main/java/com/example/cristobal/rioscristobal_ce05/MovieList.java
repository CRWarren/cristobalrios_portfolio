/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.cristobal.rioscristobal_ce05;

import java.util.ArrayList;
import java.util.List;

public final class MovieList {
    public static final String MOVIE_CATEGORY[] = {
            "Fish",
            "Turtles",
            "Octopus",

    };

    private static List<Movie> list;
    private static long count = 0;

    public static List<Movie> getList() {
        if (list == null) {
            list = setupMovies();
        }
        return list;
    }

    public static List<Movie> setupMovies() {
        list = new ArrayList<>();
        String title[] = {
                "Fish 1",
                "Fish 2",
                "Fish 3",
                "Fish 4",
                "Fish 5",

                "Turtles 1",
                "Turtles 2",
                "Turtles 3",
                "Turtles 4",
                "Turtles 5",

                "Octopus 1",
                "Octopus 2",
                "Octopus 3",
                "Octopus 4",
                "Octopus 5"
        };

        String[] description = {
                "Fish 1 Description",
                "Fish 2 Description",
                "Fish 3 Description",
                "Fish 4 Description",
                "Fish 5 Description",

                "Turtles 1 Description",
                "Turtles 2 Description",
                "Turtles 3 Description",
                "Turtles 4 Description",
                "Turtles 5 Description",

                "Octopus 1 Description",
                "Octopus 2 Description",
                "Octopus 3 Description",
                "Octopus 4 Description",
                "Octopus 5 Description"
        };
        String studio[] = {
                "Fish Studio Zero", "Fish Studio One", "Fish Studio Two", "Fish Studio Three", "Fish Studio Four",
                "Turtles Studio Zero", "Turtles Studio One", "Turtles Studio Two", "Turtles Studio Three", "Turtles Studio Four",
                "Octopus Studio Zero", "Octopus Studio One", "Octopus Studio Two", "Octopus Studio Three", "Octopus Studio Four"
        };
        String videoUrl[] = {
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Zeitgeist/Zeitgeist%202010_%20Year%20in%20Review.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Gmail%20Blue.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Fiber%20to%20the%20Pole.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Nose.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Zeitgeist/Zeitgeist%202010_%20Year%20in%20Review.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Gmail%20Blue.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Fiber%20to%20the%20Pole.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Nose.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Zeitgeist/Zeitgeist%202010_%20Year%20in%20Review.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/Demo%20Slam/Google%20Demo%20Slam_%2020ft%20Search.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Gmail%20Blue.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Fiber%20to%20the%20Pole.mp4",
                "http://commondatastorage.googleapis.com/android-tv/Sample%20videos/April%20Fool's%202013/Introducing%20Google%20Nose.mp4"
        };
        String bgImageUrl[] = {
                "https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Fish/H-P/pufferfish-closeup.ngsversion.1427141760081.adapt.1900.1.jpg",
                "https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Fish/H-P/pufferfish-inflated-closeup.ngsversion.1427141760081.adapt.1900.1.jpg",
                "https://i.pinimg.com/originals/b5/aa/b7/b5aab77ba49cfe1d10d6f0596bb24a93.jpg",
                "http://aquascope.net/wp-content/uploads/2016/08/Betta-Fish-Tank-Mates.jpg",
                "http://dreamatico.com/data_images/fish/fish-8.jpg",

                "https://lonelyplanetwpnews.imgix.net/2017/10/sea-turtle-conservation-success.jpg",
                "http://www.turtleholic.com/wp-content/uploads/2016/03/TurtleHolic.jpg",
                "https://news.nationalgeographic.com/content/dam/news/2016/05/14/sea_turtles/01seaturtles.jpg",
                "https://i.pinimg.com/originals/7f/c4/6d/7fc46d41821fed4f2f0c4091ad86ff08.jpg",
                "http://www.wallpapers13.com/wp-content/uploads/2016/03/Sea-Turtle-swimming-underwater-scene-with-coral-beautiful-desktop-wallpaper-hd-for-mobile-phones-and-laptops.jpg",

                "https://img.thedailybeast.com/image/upload/v1492181040/articles/2015/05/22/does-the-octopus-have-a-soul/150521-oconnor-octopus-tease_zenfje.jpg",
                "https://images.newrepublic.com/159d843d297dc2212296a60fb05abe510c8c75ec.jpeg",
                "http://animalanswers.wpengine.com/wp-content/uploads/2016/11/NPL_01331996.jpg",
                "https://wallpapertag.com/wallpaper/full/4/6/1/222384-best-octopus-wallpaper-3072x2040-for-ipad-2.jpg",
                "https://cdn-media-2.lifehack.org/wp-content/files/2015/11/14174128/Blue-Ringed-Octupus.jpg"
        };
        String cardImageUrl[] = {
                "https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Fish/H-P/pufferfish-closeup.ngsversion.1427141760081.adapt.1900.1.jpg",
                "https://kids.nationalgeographic.com/content/dam/kids/photos/animals/Fish/H-P/pufferfish-inflated-closeup.ngsversion.1427141760081.adapt.1900.1.jpg",
                "https://i.pinimg.com/originals/b5/aa/b7/b5aab77ba49cfe1d10d6f0596bb24a93.jpg",
                "http://aquascope.net/wp-content/uploads/2016/08/Betta-Fish-Tank-Mates.jpg",
                "http://dreamatico.com/data_images/fish/fish-8.jpg",

                "https://lonelyplanetwpnews.imgix.net/2017/10/sea-turtle-conservation-success.jpg",
                "http://www.turtleholic.com/wp-content/uploads/2016/03/TurtleHolic.jpg",
                "https://news.nationalgeographic.com/content/dam/news/2016/05/14/sea_turtles/01seaturtles.jpg",
                "https://i.pinimg.com/originals/7f/c4/6d/7fc46d41821fed4f2f0c4091ad86ff08.jpg",
                "http://www.wallpapers13.com/wp-content/uploads/2016/03/Sea-Turtle-swimming-underwater-scene-with-coral-beautiful-desktop-wallpaper-hd-for-mobile-phones-and-laptops.jpg",

                "https://img.thedailybeast.com/image/upload/v1492181040/articles/2015/05/22/does-the-octopus-have-a-soul/150521-oconnor-octopus-tease_zenfje.jpg",
                "https://images.newrepublic.com/159d843d297dc2212296a60fb05abe510c8c75ec.jpeg",
                "http://animalanswers.wpengine.com/wp-content/uploads/2016/11/NPL_01331996.jpg",
                "https://wallpapertag.com/wallpaper/full/4/6/1/222384-best-octopus-wallpaper-3072x2040-for-ipad-2.jpg",
                "https://cdn-media-2.lifehack.org/wp-content/files/2015/11/14174128/Blue-Ringed-Octupus.jpg"
        };

        for (int index = 0; index < title.length; ++index) {
            list.add(
                    buildMovieInfo(
                            title[index],
                            description[index],
                            studio[index],
                            videoUrl[index],
                            cardImageUrl[index],
                            bgImageUrl[index]));
        }

        return list;
    }

    private static Movie buildMovieInfo(
            String title,
            String description,
            String studio,
            String videoUrl,
            String cardImageUrl,
            String backgroundImageUrl) {
        Movie movie = new Movie();
        movie.setId(count++);
        movie.setTitle(title);
        movie.setDescription(description);
        movie.setStudio(studio);
        movie.setCardImageUrl(cardImageUrl);
        movie.setBackgroundImageUrl(backgroundImageUrl);
        movie.setVideoUrl(videoUrl);
        return movie;
    }


    /*
     * Copyright (C) 2017 The Android Open Source Project
     *
     * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
     * in compliance with the License. You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software distributed under the License
     * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
     * or implied. See the License for the specific language governing permissions and limitations under
     * the License.
     */


}