package com.example.cristobal.rioscristobal_integrativece07;

import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class RenderGl implements GLSurfaceView.Renderer {

    private float mMatrix[] = new float[16];
    private Circle mCircle;
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        mCircle = new Circle();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

    }

    @Override
    public void onDrawFrame(GL10 gl) {
        Matrix.orthoM(mMatrix, 0, -1, 1, -1, 1, -1, 1);
        mCircle.draw(mMatrix);
    }
}
