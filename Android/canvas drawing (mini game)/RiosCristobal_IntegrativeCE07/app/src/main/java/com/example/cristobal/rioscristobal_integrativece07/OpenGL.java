package com.example.cristobal.rioscristobal_integrativece07;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.AttributeSet;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class OpenGL extends GLSurfaceView{


    public OpenGL(Context context) {
        super(context);

    }

    public OpenGL(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
