package com.fullsail.android.mediaplayerdemo;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.lang.reflect.ReflectPermission;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class AudioPlaybackService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener{

    private static final int STATE_IDLE = 0;
    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_PREPARING = 2;
    private static final int STATE_PREPARED = 3;
    private static final int STATE_STARTED = 4;
    private static final int STATE_PAUSED = 5;
    private static final int STATE_STOPPED = 6;
    private static final int STATE_PLAYBACK_COMPLETED = 7;
    private static final int STATE_END = 8;

    private int mState;

    private static final int NOTIFICATION_ID = 0x01001;

    private MediaPlayer mPlayer;
    public AudioPlaybackService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPlayer = new MediaPlayer();
        mState = STATE_IDLE;

        //can be don in any state
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlayer.release();
        mState = STATE_END;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mState = STATE_PREPARED;
        mPlayer.start();
        mState = STATE_STARTED;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mState = STATE_PLAYBACK_COMPLETED;
        //do what ever you need for next song
    }

    public class AudioServiceBinder extends Binder{
        public AudioPlaybackService getService(){
            return AudioPlaybackService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return new AudioServiceBinder();
    }

    public void play(){
        if(mState==STATE_PAUSED){
           mPlayer.start();
           mState = STATE_STARTED;
        }else if(mState != STATE_STARTED&&mState!=STATE_PREPARING) {
            mPlayer.reset();
            mState = STATE_IDLE;
            try {
                Uri songUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.something_elated);
                mPlayer.setDataSource(this, songUri);
                mState = STATE_INITIALIZED;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (mState == STATE_INITIALIZED) {
                mPlayer.prepareAsync();
                mState = STATE_PREPARING;
            }
        }
        Notification ongoing = buildNotification();
        //above api 26 needs channel
        startForeground(NOTIFICATION_ID,ongoing);
    }

    public void pause(){
        if(mState == STATE_STARTED){
            mPlayer.pause();
            mState = STATE_PAUSED;
        }
    }
    public void stop(){
        if(mState==STATE_STARTED||mState==STATE_PAUSED||mState==STATE_PLAYBACK_COMPLETED){
            mPlayer.stop();
            mState = STATE_STOPPED;

            stopForeground(true);
        }
    }

    private Notification buildNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle("music Playing");
        builder.setContentText("click to reopen App");
        builder.setOngoing(true);

        Intent activityIntent = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this,0,activityIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);
        return builder.build();
    }
}
