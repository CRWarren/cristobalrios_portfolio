package com.example.cristobal.rioscristobal_ce04;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.rioscristobal_ce04.Fragments.DetailsFragment;
import com.example.cristobal.rioscristobal_ce04.Fragments.List_fragment;

public class MainActivity extends AppCompatActivity implements List_fragment.clickObj {

    private static final int PERMISSION_REQUEST = 0x01001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(getResources().getConfiguration().orientation == 2){
            if(checkPermission()){
                getSupportFragmentManager().beginTransaction().replace(R.id.list_container, List_fragment.newInstance()).commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getResources().getConfiguration().orientation == 2){
            if(checkPermission()){
                getSupportFragmentManager().beginTransaction().replace(R.id.list_container, List_fragment.newInstance()).commit();
            }
        }
    }

    private boolean checkPermission() {
        int result =  ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);

        if(result != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[] {Manifest.permission.READ_CONTACTS},PERMISSION_REQUEST);
            }
            return false;
        }
        return true;
    }

    @Override
    public void click(String s) {
        getSupportFragmentManager().beginTransaction().replace(R.id.details_container, DetailsFragment.newInstance(s)).commit();
    }
}
