package com.example.cristobal.rioscristobal_ce04.Fragments;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.cristobal.rioscristobal_ce04.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class DetailsFragment extends Fragment {
    private static final String ID_ARGS = "USER_ID";
    public static DetailsFragment newInstance(String s) {

        Bundle args = new Bundle();
        args.putString(ID_ARGS,s);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getImage();


        assert getArguments() != null;
        String id = getArguments().getString(ID_ARGS);

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Data.CONTENT_URI,
                new String[] {ContactsContract.Data.CONTACT_ID, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                        ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
                        ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME},
                ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                        + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                new String[] {id}, null);
        if(cur!=null&&cur.getCount()>0){
            cur.moveToFirst();
            String fname = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
            //Log.e("Names: ", fname);
            String mname = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
            //Log.e("Names: ", mname);
            String lname = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
            String fullName="";
            //Log.e("Names: ", lname);
            if(fname!=null){
                fullName+=" "+fname;
            }
            if(mname!=null){
                fullName+=" "+mname;
            }
            if(lname!=null){
                fullName+=" "+lname;
            }
            TextView name = getView().findViewById(R.id.textViewfull);
            name.setText(fullName);

        }
        Cursor outcur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                ContactsContract.Contacts._ID +" = ?", new String[]{id}, null);
        if(outcur!=null&&outcur.getCount()>0){
            outcur.moveToFirst();
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
            ArrayList<String> numbers = new ArrayList<>();
            while (phones.moveToNext()) {
                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if(number!=null){
                    numbers.add(number);
                }

            }
            ListView lv = getView().findViewById(R.id.phones_list);
            ArrayAdapter<String> aa =
                    new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, numbers);
            lv.setAdapter(aa);
            phones.close();
        }

    }



    private void getImage(){
        String id = getArguments().getString(ID_ARGS);
        long _id = Integer.parseInt(id);
        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, _id));

            if (inputStream != null) {
                Bitmap photo = BitmapFactory.decodeStream(inputStream);
                ImageView imageView = getView().findViewById(R.id.imageViewPhoto);
                imageView.setImageBitmap(photo);
                inputStream.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
