package com.example.cristobal.rioscristobal_ce04.Fragments;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import com.example.cristobal.rioscristobal_ce04.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class List_fragment extends ListFragment {

    private clickObj finishedObj;

    public interface clickObj{
        void click(String s);
    }
    public List_fragment(){
        
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof clickObj){
            finishedObj = (clickObj) context;
        }
    }

    public static List_fragment newInstance() {
        
        Bundle args = new Bundle();
        
        List_fragment fragment = new List_fragment();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment,container,false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ListView list =getListView();
        Cursor cursor = (Cursor) list.getItemAtPosition(position);
        String selectedItem = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        finishedObj.click(selectedItem);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            //create content Resolver
            ContentResolver cr = Objects.requireNonNull(getActivity()).getContentResolver();
            //firstQuery
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if(cur!=null&&cur.getCount()>0){
                setListAdapter(new ContactAdapter(cur));
            }

    }

    private class ContactAdapter extends ResourceCursorAdapter {

        private ContactAdapter(Cursor cursor){
            super(getActivity(),R.layout.contact_cell,cursor,0);
        }

        @Override
        public void bindView(View view, Context context, Cursor _cursor) {

            //content resolver for inner querys
            ContentResolver cr = Objects.requireNonNull(getActivity()).getContentResolver();

            //name and Id
            TextView tvTitle = view.findViewById(R.id.textViewFN);
            String id = _cursor.getString(_cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor cur = cr.query(ContactsContract.Data.CONTENT_URI,
                    new String[] {ContactsContract.Data.CONTACT_ID, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME},
                    ContactsContract.Data.CONTACT_ID + "=?" + " AND "
                            + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                    new String[] {id}, null);
            if(cur!=null&&cur.getCount()>0){
                cur.moveToFirst();
                String fname = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                String lname = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                String fullName="";

                if(fname!=null){
                    fullName+=" "+fname;
                }
                if(lname!=null){
                    fullName+=" "+lname;
                }
                tvTitle.setText(fullName);
                cur.close();
            }

            int _id = Integer.parseInt(id);
            try {
                InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(cr,
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, _id));

                if (inputStream != null) {
                    Bitmap photo = BitmapFactory.decodeStream(inputStream);
                    ImageView imageView = view.findViewById(R.id.imageViewThumb);
                    imageView.setImageBitmap(photo);
                    inputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if(Integer.parseInt(_cursor.getString(_cursor.getColumnIndex(ContactsContract.Contacts.
                    HAS_PHONE_NUMBER))) > 0){
                Cursor phone_Cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                +" = ?", new String[]{id}, null);
              if(phone_Cursor!=null){
                  phone_Cursor.moveToFirst();
                  String phoneNumber = phone_Cursor.getString(phone_Cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                  TextView phoneNum = view.findViewById(R.id.textViewPhone);
                  phoneNum.setText(phoneNumber);
                  phone_Cursor.close();
              }

            }

        }
    }



}
