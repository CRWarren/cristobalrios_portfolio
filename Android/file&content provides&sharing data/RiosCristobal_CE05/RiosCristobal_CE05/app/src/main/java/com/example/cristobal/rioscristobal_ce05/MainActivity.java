package com.example.cristobal.rioscristobal_ce05;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.example.cristobal.rioscristobal_ce05.Fragment.List_fragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AsyncTaskReddit.CompletionObj {
    ArrayList<Posts> posts = new ArrayList<>();
    DBHelper DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DB = DBHelper.getmInstance(MainActivity.this);
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, List_fragment.newInstance()).commit();
        redditConnection();
    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }

    private void redditConnection() {

        if (!validConnection()){
            Toast noInternet = Toast.makeText(this,"No Internet Connection",Toast.LENGTH_SHORT);
            noInternet.show();
            return;
        }
        final String webAddress = "https://www.reddit.com/r/iphonex/hot.json";

        AsyncTaskReddit downloadTask = new AsyncTaskReddit(this);

        downloadTask.execute(webAddress);

    }

    @Override
    public void updateInfo(String downloaded) {
        posts.clear();
        Log.i("printing Results-------",downloaded);
        try {

            JSONObject outerObj = new JSONObject(downloaded);
            JSONObject obj = outerObj.getJSONObject("data");
            JSONArray arrayOBJ = obj.getJSONArray("children");

            for (int i = 0; i < arrayOBJ.length() ; i++) {
                JSONObject postObj = arrayOBJ.getJSONObject(i);
                JSONObject dataObject = postObj.getJSONObject("data");
                String title = dataObject.getString("title");
                String body = dataObject.getString("selftext");
                String image = dataObject.getString("thumbnail");
                Posts post = new Posts(title,body,image);
                posts.add(post);
                Log.i("Debug--------",""+posts.size());

            }
        } catch (JSONException e){

            posts.clear();
            e.printStackTrace();

        }
        for (Posts p:posts) {

            if(p.get_image()=="self"){
                DB.insert(p.get_title(),null,p.get_body());
            }else{
                DB.insert(p.get_title(),p.get_image(),p.get_body());
            }
            Log.e("Debug---", "inserted");

        }
    }
}
