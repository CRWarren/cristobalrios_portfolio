package com.example.cristobal.rioscristobal_ce05;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_FILE = "database.db";
    private static final int DATABASE_VERSION = 1;
    public static final String STUDENT_DATA = "student_data";
    public static final String ID = "_id";
    public static final String BODY = "body";
    public static final String TITLE = "title";
    public static final String THUMBNAIL = "thumbnail";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + STUDENT_DATA + " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TITLE + " TEXT, " + THUMBNAIL + " TEXT, " +
            BODY + " TEXT)";

    private final SQLiteDatabase mDB;
    private DBHelper(Context context){
        super(context, DATABASE_FILE,null,DATABASE_VERSION);
        mDB = getWritableDatabase();
    }

    private static DBHelper mInstance = null;

    public static DBHelper getmInstance(Context context) {
        if(mInstance==null){
            mInstance = new DBHelper(context);
        }
        return mInstance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public void insert(String title, String thumbnail, String body){

        ContentValues cv = new ContentValues();
        cv.put(TITLE,title);
        cv.put(THUMBNAIL,thumbnail);
        cv.put(BODY,body);
        mDB.insert(STUDENT_DATA,null,cv);

    }


}
