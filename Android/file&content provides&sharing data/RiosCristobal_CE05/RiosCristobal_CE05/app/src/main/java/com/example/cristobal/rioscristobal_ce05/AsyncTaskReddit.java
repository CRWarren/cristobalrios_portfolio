package com.example.cristobal.rioscristobal_ce05;

import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsyncTaskReddit extends AsyncTask<String,String, String> {

    final private CompletionObj mFinishedInterface;

    interface CompletionObj {
        void updateInfo(String downloaded);
    }

    AsyncTaskReddit(CompletionObj _finished) {

        mFinishedInterface = _finished;
    }

    @Override
    protected String doInBackground(String... strings) {
        if(strings[0]==null){
            return "";
        }
        String result = "";


        URL url;
        HttpURLConnection connection = null;
        InputStream inStream = null;

        try {
            url = new URL(strings[0]);
            connection = (HttpURLConnection)url.openConnection();
            connection.connect();
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            inStream = connection.getInputStream();
            result = IOUtils.toString(inStream, "UTF-8");
        } catch(Exception e) {
            e.printStackTrace();
        }

        finally {

            if (connection != null) {
                if (inStream != null) {
                    try {
                        // close stream if open
                        inStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                // always disconnect if there is a connection
                connection.disconnect();
            }
        }


        // Display result
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        mFinishedInterface.updateInfo(s);
    }
}
