package com.example.cristobal.rioscristobal_ce05;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Posts implements Parcelable,Serializable{
    private final String _title;
    private final String _body;
    private final String _image;
    public Posts(String title, String body, String imageLink){
        _title = title;
        _body = body;
        _image = imageLink;
    }

    private Posts(Parcel in) {
        _title = in.readString();
        _body = in.readString();
        _image = in.readString();
    }

    public String get_title() {
        return _title;
    }

    public String get_image() {
        return _image;
    }

    public String get_body() {
        return _body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_title);
        dest.writeString(_body);
        dest.writeString(_image);
    }

    @Override
    public String toString() {
        return _title+"\n\n"+"Author: "+_body;
    }

    public static final Creator<Posts> CREATOR = new Creator<Posts>() {
        @Override
        public Posts createFromParcel(Parcel in) {
            return new Posts(in);
        }

        @Override
        public Posts[] newArray(int size) {
            return new Posts[size];
        }
    };
}
