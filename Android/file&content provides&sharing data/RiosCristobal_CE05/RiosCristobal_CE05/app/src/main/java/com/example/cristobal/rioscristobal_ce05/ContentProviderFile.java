package com.example.cristobal.rioscristobal_ce05;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ContentProviderFile extends ContentProvider {



    public static final String DATA = "articles";
    public static final String AUTHORITY = "com.fullsail.ce6.student.provider";

    private static final int TABLE_MATCH = 01;
    private DBHelper mDataBase;
    private UriMatcher mMatcher = null;

    @Override
    public boolean onCreate() {

         mDataBase= DBHelper.getmInstance(getContext());
        mMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mMatcher.addURI(AUTHORITY, DATA, TABLE_MATCH);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        int resultCode = mMatcher.match(uri);

        if (resultCode == TABLE_MATCH) {
            return mDataBase.getWritableDatabase()
                    .query(DBHelper.STUDENT_DATA,projection, selection, selectionArgs,null, null
                            , sortOrder);
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int resultCode = mMatcher.match(uri);

        if (resultCode == TABLE_MATCH) {
            return "vnd.android.cursor.dir/vnd."+ AUTHORITY + "." + DBHelper.STUDENT_DATA;
        }

        throw new UnsupportedOperationException("Uri did not match... Check Your authority or table name.");
    }

    //do nothing they just help to complete requirements
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
