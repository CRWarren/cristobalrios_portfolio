package com.example.cristobal.rioscristobal_ce05.Fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.example.cristobal.rioscristobal_ce05.Books;
import com.example.cristobal.rioscristobal_ce05.R;
import com.squareup.picasso.Picasso;

public class List_fragment extends ListFragment {

    public static final String BOOK_DATA = "provider_data";
    public static final String TABLENAME = "books";
    public static final String BOOK_URI_AUTHORITY = "com.fullsail.ce6.provider";
    private static final String CONTENT_URI_STRING = "content://" + BOOK_URI_AUTHORITY + "/" + TABLENAME;
    private static final Uri CONTENT_URI = Uri.parse(CONTENT_URI_STRING + BOOK_DATA);

    public static List_fragment newInstance() {

        Bundle args = new Bundle();

        List_fragment fragment = new List_fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(CONTENT_URI,null,null,null,null);
        setListAdapter(new BooksAdapter(cur,getContext()));
    }
}

 class BooksAdapter extends ResourceCursorAdapter {

    private static final String TITLE = "title";
    private static final String THUMBNAIL = "thumbnail";
    private static final String DESCRIPTION = "Description";

    public BooksAdapter(Cursor _cursor, Context _context) {

        super(_context, R.layout.list_cell, _cursor, 0);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        String title = cursor.getString(cursor.getColumnIndex(TITLE));
        String desc = cursor.getString(cursor.getColumnIndex(DESCRIPTION));
        String imageString = cursor.getString(cursor.getColumnIndex(THUMBNAIL));
                Books book = new Books(title,imageString,desc);

        TextView titleTV = view.findViewById(R.id.textViewTitle);
        titleTV.setText(book.getTitle());

        TextView descriptionTV=  view.findViewById(R.id.textViewBody);
        descriptionTV.setText(book.getDescription());

        ImageView img = view.findViewById(R.id.imageViewBook);
        Picasso.with(context).load(book.getImage()).into(img);
         }
}
