package com.example.cristobal.rioscristobal_ce05;

public class Books {

    private final String title;
    private final String image;
    private final String description;

    public Books(String _title,String _image, String _desc) {
        this.title = _title;
        this.image = _image;
        this.description = _desc;
    }

    public String getTitle() {

        return title;
    }

    public String getImage() {

        return image;
    }

    public String getDescription() {

        return description;
    }
}
