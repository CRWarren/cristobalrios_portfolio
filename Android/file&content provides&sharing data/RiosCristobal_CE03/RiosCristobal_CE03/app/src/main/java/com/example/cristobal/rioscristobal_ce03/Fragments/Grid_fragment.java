package com.example.cristobal.rioscristobal_ce03.Fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import com.example.cristobal.rioscristobal_ce03.CustomBaseAdapter;
import com.example.cristobal.rioscristobal_ce03.R;
import java.util.ArrayList;
import java.util.List;

public class Grid_fragment extends Fragment {

    private static ArrayList<Uri> _images;
    private static Intent _intent;
    private static final String INTENT_ACTION_SEND = "com.example.cristobal.rioscristobal_ce03.ACTION_SEND";
    private static final String IMAGE_EXTRA = "IMAGE_EXTRA";

    public static Grid_fragment newInstance(ArrayList<Uri> images, Intent intent) {

        Bundle args = new Bundle();
        _intent = intent;
        _images = images;
        Log.e("TEST______",""+_images.size());
        Grid_fragment fragment = new Grid_fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.grid_layout, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getView()!=null){
            setupCustomBaseAdapterView();
        }
    }

    private void setupCustomBaseAdapterView() {

        CustomBaseAdapter aa = new CustomBaseAdapter(getActivity(), _images);
        GridView _gridView = getView().findViewById(R.id.images_grid);
        _gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Debug-----","img"+_images.size()+" pos"+position);



                        if (_intent.getAction().equals("com.example.cristobal.rioscristobal_ce03_part2.ACTION_SEND")) {
                            Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.cristobal.rioscristobal_ce03_part2");
                            intent.setAction(INTENT_ACTION_SEND);
                            try{
                                Uri send_uri = _images.get(position);
                                intent.putExtra(IMAGE_EXTRA,send_uri.toString());
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                                for (ResolveInfo resolveInfo : resInfoList) {
                                    String packageName = resolveInfo.activityInfo.packageName;
                                    getActivity().grantUriPermission(packageName,send_uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                }
                                startActivity(intent);
                                getActivity().finish();
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            } else {
                            if (_images.get(position) != null) {
                                Intent intentShow = new Intent(Intent.ACTION_VIEW);
                                intentShow.setDataAndType(_images.get(position), "image/*");
                                intentShow.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivity(intentShow);
                            }
                }
            }
        });
        _gridView.setAdapter(aa);

    }


}
