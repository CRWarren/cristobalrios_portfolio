package com.example.cristobal.rioscristobal_ce03;

import android.content.Context;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.util.ArrayList;

public class CustomBaseAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<Uri> images;

    public CustomBaseAdapter(Context _context, ArrayList<Uri> _images){
        _Context = _context;
        images = _images;
    }

    @Override
    public int getCount() {
        if(images != null) {
            return images.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(images != null) {
            return images.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.grid_custom_cell, parent, false);
            vh =  new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ViewHolder)convertView.getTag();
        }


            vh.imageIV.setImageURI(images.get(position));


        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{
        final ImageView imageIV;

        ViewHolder(View _layout){
            imageIV = _layout.findViewById(R.id.imageCell);
        }
    }
}
