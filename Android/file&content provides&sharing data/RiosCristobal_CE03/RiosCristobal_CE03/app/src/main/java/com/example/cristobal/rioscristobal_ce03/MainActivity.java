package com.example.cristobal.rioscristobal_ce03;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cristobal.rioscristobal_ce03.Fragments.Grid_fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "com.example.cristobal.rioscristobal_ce03";
    private static final String IMAGE_FOLDER = "images";
    private static final String IMAGE_FILENAME = "img_";
    private static final int REQUEST_TAKE_PIC = 10;
    private final ArrayList<Uri> images = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getStoredImages();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                Grid_fragment.newInstance(images, getIntent())).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.camera_option,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //verify menu item
        if(item.getItemId()==findViewById(R.id.open_camera).getId()){
           //open camera
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,getOutputUri());
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent,REQUEST_TAKE_PIC);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_TAKE_PIC){
          getStoredImages();
          getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                  Grid_fragment.newInstance(images, getIntent() )).commit();
        }
    }

    private Uri getOutputUri() {
        return FileProvider.getUriForFile(this,AUTHORITY,getImageFile());
    }

    private File getImageFile(){
        File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
        File imageFile = new File(protectedStorage,IMAGE_FILENAME+System.currentTimeMillis());
        try{
            imageFile.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
        return imageFile;
    }

    private void getStoredImages(){
        images.clear();
        File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
        for (File f:protectedStorage.listFiles()) {
            images.add(FileProvider.getUriForFile(this,AUTHORITY,f));
        }
    }
}
