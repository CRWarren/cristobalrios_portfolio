package com.example.cristobal.rioscristobal_ce03_part2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String INTENT_ACTION_SEND = "com.example.cristobal.rioscristobal_ce03_part2.ACTION_SEND";
    private static final String IMAGE_EXTRA = "IMAGE_EXTRA";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();

        if(intent.getAction()=="com.example.cristobal.rioscristobal_ce03.ACTION_SEND"){

            ImageView img = findViewById(R.id.showImageView);
            String uri_sent = intent.getExtras().getString(IMAGE_EXTRA);
            Uri tempUri = Uri.parse(uri_sent);
            try{

                Bitmap bmp= MediaStore.Images.Media.getBitmap(getContentResolver(), tempUri);
                img.setImageBitmap(bmp);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_image,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.addImage){
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.cristobal.rioscristobal_ce03");
            launchIntent.setAction(INTENT_ACTION_SEND);
                startActivity(launchIntent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
