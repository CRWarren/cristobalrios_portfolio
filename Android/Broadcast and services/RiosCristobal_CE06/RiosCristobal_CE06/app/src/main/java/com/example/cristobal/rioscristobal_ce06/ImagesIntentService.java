package com.example.cristobal.rioscristobal_ce06;

import android.app.IntentService;
import android.content.Intent;

import android.support.annotation.Nullable;

import android.util.Log;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;

public class ImagesIntentService extends IntentService {


    public static final String ACTION_SEND_LIST= "com.example.cristobal.rioscristobal_ce06.ACTION_SEND_LIST";
    public ImagesIntentService(){super("ImagesIntentService");}
    public static final String FOLDER_NAME = "images";


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        for (String s:Url2ndParts.IMAGES) {
            if (!checkForImg(s)) {
                Log.e("Saving Images", "part 1");
                byte[] bytes = ImageHelper.getImage(s);
                saveImage(bytes,s);

            }
            Intent i = new Intent(ACTION_SEND_LIST);
            sendBroadcast(i);

        }


    }
    private boolean checkForImg(String name){
        File protectedStorage = getApplicationContext().getExternalFilesDir(FOLDER_NAME);
        File imageFile = new File(protectedStorage,name);
        if(imageFile.exists()){
            Log.e("Image Exists", "------------------");
            return true;
        }
        return false;
    }

    private void saveImage(byte[] img, String name){
        Log.e("Saving", "------------------");
        File protectedStorage = getExternalFilesDir(FOLDER_NAME);

        File imageFile = new File(protectedStorage,name);
        try{
            imageFile.createNewFile();
            Log.e("CreatingFile", "------------------");
            FileOutputStream fos = new FileOutputStream(imageFile);
            Log.e("Writing", "------------------");
            fos.write(img);
            Log.e("Write", "------------------");
            fos.close();

        }catch (IOException e){
            e.printStackTrace();
        }

    }

}
