package com.example.cristobal.rioscristobal_ce06;

import android.util.Log;
import org.apache.commons.io.IOUtils;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageHelper {

    private static final String ARG_PART1_URL = "http://i.imgur.com/";

    public static byte[] getImage(String part2){
        HttpURLConnection connection = null;
        byte[] result = null;
        try {
            URL url = new URL(ARG_PART1_URL+part2);
            connection = (HttpURLConnection)url.openConnection();
            connection.connect();
        } catch(Exception e) {
            e.printStackTrace();
        }
        try {
            result = IOUtils.toByteArray(new URL(ARG_PART1_URL+part2));
            Log.e("Test Images Downloaded",": "+result);

        } catch(Exception e) {
            e.printStackTrace();
        }
        finally {

            if (connection != null) {

                // always disconnect if there is a connection
                connection.disconnect();
            }
        }

        return result;
    }



}
