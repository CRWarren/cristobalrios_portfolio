package com.example.cristobal.rioscristobal_ce06;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<String> images;

    public CustomAdapter(Context _context, ArrayList<String> _images){
        _Context = _context;
        images = _images;
    }

    @Override
    public int getCount() {
        if(images != null) {
            return images.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(images != null) {
            return images.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.grid_cell, parent, false);
            vh =  new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ViewHolder)convertView.getTag();
        }

        Uri uri = Uri.parse(images.get(position));
        vh.imageIV.setImageURI(uri);


        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{
        final ImageView imageIV;
        ViewHolder(View _layout){
            imageIV = _layout.findViewById(R.id.imagePlaceHolder);
        }
    }
}
