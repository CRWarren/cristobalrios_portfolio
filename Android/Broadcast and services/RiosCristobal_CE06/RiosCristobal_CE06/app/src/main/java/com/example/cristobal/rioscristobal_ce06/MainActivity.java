package com.example.cristobal.rioscristobal_ce06;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cristobal.rioscristobal_ce06.Fragment.ImagesFragment;

public class MainActivity extends AppCompatActivity  {
    DialogReceiver mReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter filter = new IntentFilter(ImagesIntentService.ACTION_SEND_LIST);
        mReceiver = new DialogReceiver();
        registerReceiver(mReceiver,filter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_items,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.downloadMenuItem){

            if(validConnection()){
                Intent i = new Intent(this, ImagesIntentService.class);
                startService(i);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }


    private class DialogReceiver extends BroadcastReceiver { // TODO: Extend
        // TODO: onReceive

        @Override
        public void onReceive(Context context, Intent intent) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,ImagesFragment.newInstance()).commit();
        }
    }
}

