package com.example.cristobal.rioscristobal_ce06.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import com.example.cristobal.rioscristobal_ce06.CustomAdapter;
import com.example.cristobal.rioscristobal_ce06.ImagesIntentService;
import com.example.cristobal.rioscristobal_ce06.R;
import com.example.cristobal.rioscristobal_ce06.Url2ndParts;
import java.io.File;
import java.util.ArrayList;

public class ImagesFragment extends Fragment {
    private static final String AUTHORITY = "com.example.cristobal.rioscristobal_ce06";
    ArrayList<String> images = new ArrayList<>();

    public static ImagesFragment newInstance() {

        Bundle args = new Bundle();
        ImagesFragment fragment = new ImagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.images_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            setupCustomAdapter();
        }
    }

    private void setupCustomAdapter() {
        for (String s : Url2ndParts.IMAGES) {
            Uri uri = getOutputUri(s);
            if(uri!=null){
                images.add(uri.toString());
            }
        }
        CustomAdapter aa = new CustomAdapter(getActivity(), images);
        GridView gridView = getView().findViewById(android.R.id.list);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentShow = new Intent(Intent.ACTION_VIEW);
                intentShow.setDataAndType(getOutputUri(Url2ndParts.IMAGES[position]), "image/*");
                intentShow.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intentShow);
            }
        });
        gridView.setAdapter(aa);
    }

    private Uri getOutputUri(String name) {

        if(getImageFile(name)!=null){
            return FileProvider.getUriForFile(getActivity(),AUTHORITY,getImageFile(name));
        }
        return null;
    }



    private File getImageFile(String name){
        File protectedStorage = getActivity().getExternalFilesDir(ImagesIntentService.FOLDER_NAME);
        File imageFile = new File(protectedStorage,name);
        if(imageFile.exists()){
            return imageFile;
        }
        return null;
    }
}
