package com.example.cristobal.rioscristobal_ce08;

import java.io.Serializable;

public class Person implements Serializable{
    private String firstName;
    private String lastName;
    private String jobName;
    
    public Person(String name, String last, String job){
        firstName = name;
        lastName = last;
        jobName = job;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getJobName() {
        return jobName;
    }

    public String getLastName() {
        return lastName;
    }
}
