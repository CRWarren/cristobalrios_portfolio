package com.example.cristobal.rioscristobal_ce08;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce08.Fragments.AddPersonFragmen;
import com.example.cristobal.rioscristobal_ce08.WebFiles.WebInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class AddPersonActivity extends AppCompatActivity implements WebInterface.AddPerson {

    public static final String FOLDER_NAME = "PEOPLE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        getSupportFragmentManager().beginTransaction().replace(R.id.webView2_Container, AddPersonFragmen.newInstance()).commit();
    }

    private void save(Person person){
        File protectedStorage = getExternalFilesDir(FOLDER_NAME);

        File personFile = new File(protectedStorage,person.getFirstName()+person.getLastName());
        try{
            personFile.createNewFile();
            Log.e("CreatingFile", "------------------");
            FileOutputStream fos = new FileOutputStream(personFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Log.e("Writing", "------------------");
            oos.writeObject(person);
            Log.e("Write", "------------------");
            oos.close();
            fos.close();

        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public void savePerson(Person p) {
       if(p != null){
           save(p);
       }
       finish();
    }
}
