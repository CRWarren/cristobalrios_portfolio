package com.example.cristobal.rioscristobal_ce08.WebFiles;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.cristobal.rioscristobal_ce08.DetailsPersonActivity;

public class WebClient extends WebViewClient {
    private Context _context = null;
    public static final String EXTRA_FOLDER = "EXTRA_FOLDER";
    public static final String  EXTRA_FILENAME = "EXTRA_FILENAME";


    public WebClient(Context _context) {

        this._context = _context;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return urlHandler(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return urlHandler(view, request.getUrl().toString());
    }

    private boolean urlHandler(WebView _view, String _url) {
        if( _url.startsWith("file:///android_asset/rioscristobal_ce08#")){
            String[] dataPoints = _url.split("#");
                Intent i = new Intent(_context, DetailsPersonActivity.class);
                String[] fileData = dataPoints[1].split("/");
                for (int j = 0; j < fileData.length; j++) {
                    Log.e("fd-----------",fileData[j]);
                }
                i.putExtra(EXTRA_FILENAME,fileData[2]);
                _context.startActivity(i);
            return true;
        }else if (_url.startsWith("file:///")){
            return true;
        }
        // Return TRUE so the system knows you handled it
        // Return FALSE so the system knows it was not handled, resulting in an error being displayed.
        return false;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.i("Error On Web Client","onReceivedError() errorCode: " + errorCode + " description: \"" + description + "\"");

    }
}
