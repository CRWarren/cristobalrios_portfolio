package com.example.cristobal.rioscristobal_ce08;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce08.Fragments.DetailsFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import static com.example.cristobal.rioscristobal_ce08.AddPersonActivity.FOLDER_NAME;
import static com.example.cristobal.rioscristobal_ce08.WebFiles.WebClient.EXTRA_FILENAME;

public class DetailsPersonActivity extends AppCompatActivity  {

    Person _person;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_person);
        getFile();
        getSupportFragmentManager().beginTransaction().replace(R.id.details_container, DetailsFragment.newInstance(this,_person)).commit();
    }

    public void getFile(){
        Intent i =this.getIntent();
        if(i.hasExtra(EXTRA_FILENAME)){
            String fn = i.getStringExtra(EXTRA_FILENAME);
            File protectedStorage = getExternalFilesDir(FOLDER_NAME);
            if (protectedStorage != null) {
                try{
                    FileInputStream fis = new FileInputStream(protectedStorage + "/" + fn);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    _person = (Person) ois.readObject();
                    Log.e("Get Person/ added ",fn);
                    ois.close();
                    fis.close();
                }catch (IOException |ClassNotFoundException e){
                    e.printStackTrace();
                }

            }
        }

    }

}
