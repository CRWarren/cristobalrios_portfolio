package com.example.cristobal.rioscristobal_ce08;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cristobal.rioscristobal_ce08.Fragments.ListFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static com.example.cristobal.rioscristobal_ce08.AddPersonActivity.FOLDER_NAME;

public class MainActivity extends AppCompatActivity {

    public ArrayList<Person> _people;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.addPerson){
            Intent i = new Intent(this, AddPersonActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        _people = getPeople();
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, ListFragment.newInstance(this,_people)).commit();

    }

    private  ArrayList<Person> getPeople(){

        ArrayList<Person> temp = new ArrayList<>();
        File protectedStorage = getExternalFilesDir(FOLDER_NAME);
        if (protectedStorage != null) {
            Log.e("Get People: ",""+protectedStorage.list().length);
            for (String f : protectedStorage.list()) {
                try{
                    FileInputStream fis = new FileInputStream(protectedStorage + "/" + f);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    temp.add( (Person) ois.readObject() );
                    Log.e("Get Post/ added ",f);
                    ois.close();
                    fis.close();
                }catch (IOException |ClassNotFoundException e){
                    e.printStackTrace();
                }
            }
        }
        return temp;
    }
}
