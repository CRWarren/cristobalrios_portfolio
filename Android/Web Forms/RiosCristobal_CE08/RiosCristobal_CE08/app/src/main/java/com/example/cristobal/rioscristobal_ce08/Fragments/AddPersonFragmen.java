package com.example.cristobal.rioscristobal_ce08.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.cristobal.rioscristobal_ce08.R;
import com.example.cristobal.rioscristobal_ce08.WebFiles.WebClient;
import com.example.cristobal.rioscristobal_ce08.WebFiles.WebInterface;

import java.util.ArrayList;

public class AddPersonFragmen extends Fragment {

    WebView wb = null;
    private final static String _Schema  = "file:///";


    public static AddPersonFragmen newInstance() {

        Bundle args = new Bundle();

        AddPersonFragmen fragment = new AddPersonFragmen();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("great we are here", "------------------");
        setupWebView();
        webViewLoad("file:///android_asset/add_person.html");
    }

    @Override
    public void onResume() {
        super.onResume();
        setupWebView();
        webViewLoad("file:///android_asset/add_person.html");
    }

    private void setupWebView() {
        wb = getActivity().findViewById(R.id.webViewAdd);
        if(wb == null) {
            Log.e("Returning null", "------------------");
            return;
        }
        Log.e("Add Fragent", "We Are setting up the view");

        // TODO: Enable JS
        WebSettings ws = wb.getSettings();
        ws.setJavaScriptEnabled(true);

        // TODO: Set Web Client
        wb.setWebViewClient(new WebClient(getActivity()));
        // TODO: Add JS interface
        wb.addJavascriptInterface(new WebInterface(getActivity()), "Android");

        // Add debugging relay message handling
        // Read more here: https://developer.android.com/guide/webapps/debugging.html
        wb.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("ON Add Activity", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId() );
                return true;
            }
        });
    }

    private void webViewLoad(String _uri) {
        if(wb == null || _uri==null || _uri.trim().isEmpty()) {
            return;
        }
        Log.e("Loading", "Web View");

            if (_uri.startsWith(_Schema)) {
                // TODO
                wb.loadUrl(_uri);
                return;
            }


       // String errorMsg = getText(R.string.toast_invalid_uri) + mSchemas.toString();
       // Toast.makeText(MainActivity.this,errorMsg, Toast.LENGTH_SHORT).show();
    }
}
