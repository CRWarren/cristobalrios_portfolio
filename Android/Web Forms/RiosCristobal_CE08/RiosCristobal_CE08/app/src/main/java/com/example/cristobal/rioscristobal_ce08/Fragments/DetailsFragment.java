package com.example.cristobal.rioscristobal_ce08.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.cristobal.rioscristobal_ce08.Person;
import com.example.cristobal.rioscristobal_ce08.R;
import com.example.cristobal.rioscristobal_ce08.WebFiles.WebClient;
import com.example.cristobal.rioscristobal_ce08.WebFiles.WebInterface;



public class DetailsFragment extends Fragment {
    static Context _context;
    WebView wb = null;
    static Person person;
    private final static String _Schema  = "file:/";

    public static DetailsFragment newInstance(Context context, Person p) {

        Bundle args = new Bundle();
        _context = context;
        person = p;
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.details_fragment,container,false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupWebView();
        webViewLoad("file:///android_asset/details_person_view.html");
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebView() {
        wb = getActivity().findViewById(R.id.webViewDetails);
        if(wb == null) {

            return;
        }

        // TODO: Enable JS
        WebSettings ws = wb.getSettings();
        ws.setJavaScriptEnabled(true);

        // TODO: Set Web Client
        wb.setWebViewClient(new WebClient(getActivity()));
        // TODO: Add JS interface
        wb.addJavascriptInterface(new WebInterface(getActivity(),person), "Android");

        // Add debugging relay message handling
        // Read more here: https://developer.android.com/guide/webapps/debugging.html
        wb.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.d("ON Add Activity", cm.message() + " -- From line "
                        + cm.lineNumber() + " of "
                        + cm.sourceId() );
                return true;
            }
        });
    }

    private void webViewLoad(String _uri) {
        if(wb == null || _uri==null || _uri.trim().isEmpty()) {
            return;
        }


        if (_uri.startsWith(_Schema)) {
            // TODO
            wb.loadUrl(_uri);
            Log.e("Loading", "Web View");
            return;
        }
    }
}
