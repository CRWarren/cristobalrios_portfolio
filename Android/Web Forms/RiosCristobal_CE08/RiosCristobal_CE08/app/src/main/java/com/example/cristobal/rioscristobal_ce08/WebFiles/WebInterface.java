package com.example.cristobal.rioscristobal_ce08.WebFiles;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.example.cristobal.rioscristobal_ce08.DetailsPersonActivity;
import com.example.cristobal.rioscristobal_ce08.MainActivity;
import com.example.cristobal.rioscristobal_ce08.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.example.cristobal.rioscristobal_ce08.AddPersonActivity.FOLDER_NAME;
import static com.example.cristobal.rioscristobal_ce08.WebFiles.WebClient.EXTRA_FILENAME;


public class WebInterface {
    private Context _context;
    private AddPerson addPersonInterface;
    private ArrayList<Person> _people;
    private Person _person;
    public WebInterface(Context classContext){
        _context = classContext;
        if(_context instanceof AddPerson){
            addPersonInterface = (AddPerson)_context;
        }



    }

    public  WebInterface(Context context, ArrayList<Person> people){
        _context = context;
        _people = people;
    }

    public  WebInterface(Context context, Person person){
        _context = context;
        _person = person;
    }

    public interface AddPerson{
        void savePerson(Person p);
    }


    @JavascriptInterface
    public void addPerson(String newObject){


            try {

                JSONObject obj = new JSONObject(newObject);
                String first = obj.getString("firstname");
                String last = obj.getString("lastname");
                String job = obj.getString("job");
                Person newPerson = new Person(first, last,job);

                Log.e("New Person", newPerson.getFirstName());
                addPersonInterface.savePerson(newPerson);

            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    @JavascriptInterface
    public String getList(){
        String links = "<ul>";
        if(_people!= null){
            for (Person p:_people) {
                links += "<li> <a href=\"" +"rioscristobal_ce08#"+"/"+FOLDER_NAME +"/"+ p.getFirstName() +
                        p.getLastName() + "\"> " + p.getFirstName() + " "+ p.getLastName() + " </a> </li>";

            }
        }
        links += "</ul>";
        return links;
    }

    @JavascriptInterface
    public String getOBJName(){

        return _person.getFirstName();
    }
    @JavascriptInterface
    public String getOBJLast(){

        return _person.getLastName();
    }
    @JavascriptInterface
    public String getOBJjob(){

        return _person.getJobName();
    }

    @JavascriptInterface
    public void deletePerson(String name){
        Log.e("Deleting Person","-------------");
        File protectedStorage = _context.getApplicationContext().getExternalFilesDir(FOLDER_NAME);
        File personFile = new File(protectedStorage,name);
        if(personFile.exists()){
            Log.e("person Exists", "------------------");
            personFile.delete();
            ((DetailsPersonActivity)_context).finish();

        }

    }







}
