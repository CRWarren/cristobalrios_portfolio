
package com.fullsail.android.ce06_starter.Espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.fullsail.android.ce06_starter.DetailsActivity;
import com.fullsail.android.ce06_starter.MainActivity;
import com.fullsail.android.ce06_starter.R;
import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule =
            new IntentsTestRule<>(MainActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;

    @Before
    public void launchMain(){
        //do set up
        Context context = InstrumentationRegistry.getTargetContext();
        person = new Person("John", "Doe", 30);
        //save person so we can check opening form and details
        PersonStorageUtil.savePerson(context, person);
        intentsTestRule.launchActivity(null);
    }

    @Test
    public void formTest(){
        //fake click the add button
        onView(ViewMatchers.withId(R.id.action_add)).perform(click());
        //fill text fields
        onView(withId(R.id.edit_first_name)).perform(typeText(person.getFirstName()));
        onView(withId(R.id.edit_last_name)).perform(typeText(person.getLastName()));
        onView(withId(R.id.edit_age)).perform(typeText(String.valueOf(person.getAge())));

        //save
        onView(withId(R.id.action_save)).perform(click());
        Context context = InstrumentationRegistry.getTargetContext();

        //validate
        ArrayList<Person> people = PersonStorageUtil.loadPeople(context);
        assertTrue(people.contains(person));
    }

    @Test
    public void detailTest(){
        //perform click on the person to open the details
       onView(withText(person.toString())).perform(click());

       //check our intent has necessary extras
        intending(
                allOf(
                        hasExtra(DetailsActivity.EXTRA_FIRST_NAME,
                                person.getFirstName()),
                        hasExtra(DetailsActivity.EXTRA_LAST_NAME,
                                person.getLastName()),
                        hasExtra(DetailsActivity.EXTRA_AGE,
                                person.getAge())
        ));
    }

    @After
    public void clean(){
        //delete person so we don't modify our app data.
        Context context = InstrumentationRegistry.getTargetContext();
        PersonStorageUtil.deletePerson(context, person);
    }

}
