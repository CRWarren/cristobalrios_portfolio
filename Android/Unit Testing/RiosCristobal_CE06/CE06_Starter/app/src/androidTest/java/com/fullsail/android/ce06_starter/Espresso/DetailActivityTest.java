
package com.fullsail.android.ce06_starter.Espresso;

import android.content.Intent;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.fullsail.android.ce06_starter.DetailsActivity;
import com.fullsail.android.ce06_starter.R;
import com.fullsail.android.ce06_starter.object.Person;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.ArgumentMatchers.startsWith;

@RunWith(AndroidJUnit4.class)
public class DetailActivityTest {

    @Rule
    public IntentsTestRule<DetailsActivity> intentsTestRule =
            new IntentsTestRule<>( DetailsActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;

    @Before
    public void startDetailsActivity() {
        //set up our person
        person = new Person("John", "Doe", 30);
        //launch activity with intent to fake a real launch
        intentsTestRule.launchActivity(null);
        //create intent
        Intent i = new Intent();
        //add necessary extras to our intent
        i.putExtra(DetailsActivity.EXTRA_FIRST_NAME, person.getFirstName());
        i.putExtra(DetailsActivity.EXTRA_LAST_NAME, person.getLastName());
        i.putExtra(DetailsActivity.EXTRA_AGE, person.getAge());
    }

    @Test
    public void detailTest(){
        //check textViews have text
        onView(withId(R.id.text_first_name)).check(matches(withText(startsWith(person.getFirstName()))));
        onView(withId(R.id.text_last_name)).check(matches(withText(startsWith(person.getLastName()))));
        onView(withText(person.getAge()));
        //check our delete button is displayed
        onView(ViewMatchers.withId(R.id.action_delete)).check(matches(isDisplayed()));
        //manually perform click action to delete peron
        onView(withId(R.id.action_delete)).perform(click());
    }




}
