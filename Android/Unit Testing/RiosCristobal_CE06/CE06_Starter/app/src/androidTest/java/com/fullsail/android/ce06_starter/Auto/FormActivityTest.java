
package com.fullsail.android.ce06_starter.Auto;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;

import com.fullsail.android.ce06_starter.FormActivity;
import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FormActivityTest {

    @Rule
    public IntentsTestRule<FormActivity> intentsTestRule =
            new IntentsTestRule<>( FormActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;

    @Before
    public void launchForm()  {
        intentsTestRule.launchActivity(null);
        person =  new Person("John", "Doe", 30);
    }


    @Test
    public void FormTest() {

        UiDevice device;
        Context context = InstrumentationRegistry.getTargetContext();
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        device.findObject(By.res(context.getPackageName(), "fn_textField")).setText(person.getFirstName());
        device.findObject(By.res(context.getPackageName(), "ln_textField")).setText(person.getLastName());
        device.findObject(By.res(context.getPackageName(), "age_textField")).setText(String.valueOf(person.getAge()));
        String saveBttn = "saveBttn";
        device.findObject(By.res(context.getPackageName(), saveBttn)).click();
        PersonStorageUtil.savePerson(context, person);
        ArrayList<Person> people = PersonStorageUtil.loadPeople(context);
        assertTrue(people.contains(person));
    }


}
