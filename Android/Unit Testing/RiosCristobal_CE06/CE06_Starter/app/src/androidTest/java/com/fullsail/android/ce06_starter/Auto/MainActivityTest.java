
package com.fullsail.android.ce06_starter.Auto;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import com.fullsail.android.ce06_starter.DetailsActivity;
import com.fullsail.android.ce06_starter.FormActivity;
import com.fullsail.android.ce06_starter.MainActivity;
import com.fullsail.android.ce06_starter.object.Person;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static org.hamcrest.Matchers.allOf;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule =
            new IntentsTestRule<>( MainActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default
    @Rule
    public IntentsTestRule<DetailsActivity> detailintentsTestRule =
            new IntentsTestRule<>( DetailsActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default
    @Rule
    public IntentsTestRule<FormActivity> addintentsTestRule =
            new IntentsTestRule<>( FormActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default


    private Person person;

    @Before
    public void startMainActivity() {
        person = new Person("John", "Doe", 30);
        intentsTestRule.launchActivity(null);
    }

    @Test
    public void detail()  {
        //get Device
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        //get Object
        UiObject object = device.findObject(new UiSelector().text(person.toString()));

        try {
            object.clickAndWaitForNewWindow();
            detailintentsTestRule.launchActivity(null);
        }catch (UiObjectNotFoundException e){
            e.printStackTrace();
        }
        intending(allOf
                (
                        IntentMatchers.hasExtra(DetailsActivity.EXTRA_FIRST_NAME,
                                person.getFirstName()),
                        hasExtra(DetailsActivity.EXTRA_LAST_NAME,
                                person.getLastName()),
                        hasExtra(DetailsActivity.EXTRA_AGE,
                                person.getAge())
                )
        );
    }

    @Test
    public void add(){
        //get Device
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        //get Object
        UiObject addBttn = device.findObject(new UiSelector().description("New"));
        try {
            addBttn.clickAndWaitForNewWindow();
            addintentsTestRule.launchActivity(null);
        }catch (UiObjectNotFoundException e){
            e.printStackTrace();
        }

        //get Context
        Context context = InstrumentationRegistry.getTargetContext();
        device.findObject(By.res(context.getPackageName(), "action_add"));
    }



}
