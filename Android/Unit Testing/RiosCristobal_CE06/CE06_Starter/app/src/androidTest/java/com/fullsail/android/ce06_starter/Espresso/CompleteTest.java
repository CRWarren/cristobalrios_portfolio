
package com.fullsail.android.ce06_starter.Espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.fullsail.android.ce06_starter.DetailsActivity;
import com.fullsail.android.ce06_starter.MainActivity;
import com.fullsail.android.ce06_starter.R;
import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class CompleteTest {

    @Rule
    public IntentsTestRule<MainActivity> intentsTestRule =
            new IntentsTestRule<>(MainActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;

    @Before
    public void launchMain(){
        person = new Person("John", "Doe", 30);
        intentsTestRule.launchActivity(null);
    }

    @Test
    public void appTest(){
        Context context = InstrumentationRegistry.getTargetContext();
        //first check, do the form

        //open add.
        onView(ViewMatchers.withId(R.id.action_add)).perform(click());

        //fill text fields
        onView(withId(R.id.edit_first_name)).perform(typeText(person.getFirstName()));
        onView(withId(R.id.edit_last_name)) .perform(typeText(person.getLastName()));
        onView(withId(R.id.edit_age)).perform(typeText(String.valueOf(person.getAge())));

        //save
        onView(withId(R.id.action_save)).perform(click());

        ArrayList<Person> people = PersonStorageUtil.loadPeople(context);

        assertTrue(people.contains(person));

        //second check, do the activity
        //open the activity
        onView(withText(person.toString())).perform(click());

        //check necessary intents
        intended(allOf(
                hasExtra(DetailsActivity.EXTRA_FIRST_NAME,
                        person.getFirstName()),
                hasExtra(DetailsActivity.EXTRA_LAST_NAME,
                        person.getLastName()),
                hasExtra(DetailsActivity.EXTRA_AGE,
                        person.getAge())
        ));

        //check textViews have text
        onView(withId(R.id.text_first_name)).check(matches(withText(person.getFirstName())));
        onView(withId(R.id.text_last_name)).check(matches(withText(person.getLastName())));
        onView(withText(person.getAge()));
        //check our delete button is displayed
        onView(ViewMatchers.withId(R.id.action_delete)).check(matches(isDisplayed()));
        onView(withId(R.id.action_delete)).perform(click());
        //check it was correctly deleted after performed click
        ArrayList<Person> peopleDetails = PersonStorageUtil.loadPeople(context);
        Assert.assertFalse(peopleDetails.contains(person));

    }

    @After
    public void clean(){

        //delete saved person so our test didn't modify our data
        Context context = InstrumentationRegistry.getTargetContext();
        PersonStorageUtil.deletePerson(context, person);
    }

}
