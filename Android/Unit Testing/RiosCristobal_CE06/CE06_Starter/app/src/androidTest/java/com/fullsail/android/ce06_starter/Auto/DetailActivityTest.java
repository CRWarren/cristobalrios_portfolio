
package com.fullsail.android.ce06_starter.Auto;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.fullsail.android.ce06_starter.DetailsActivity;
import com.fullsail.android.ce06_starter.MainActivity;
import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junit.framework.Assert.assertFalse;


@RunWith(AndroidJUnit4.class)
public class DetailActivityTest {
    @Rule
    public IntentsTestRule<MainActivity> mIntentsTestRule =
            new IntentsTestRule<>(MainActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default
    @Rule
    public IntentsTestRule<DetailsActivity> dIntentsTestRule =
            new IntentsTestRule<>(DetailsActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;
    private UiDevice device;

    @Before
    public void launchMain() {
        Context context = InstrumentationRegistry.getTargetContext();
        person = new Person("John", "Doe", 30);
        PersonStorageUtil.savePerson(context, person);
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mIntentsTestRule.launchActivity(null);
    }

    @Test
    public void detailTest() throws UiObjectNotFoundException {
        Context context = InstrumentationRegistry.getTargetContext();

        //find all proper objects contained in the detail layout
        device.findObject(By.text(person.getFirstName()));
        device.findObject(By.text(person.getLastName()));
        String age = Integer.toString(person.getAge());
        device.findObject(By.text(age));
        device.findObject(By.res(context.getPackageName(), "delete_action"));

        //get the "Button"(row)
        UiObject object = device.findObject(new UiSelector().text(person.toString()));

        //perform action on button, open details
        object.clickAndWaitForNewWindow();

        //get Button
        UiObject deleteButton = device.findObject(new UiSelector().description("Delete"));
        //perform action on button, open activity
        deleteButton.clickAndWaitForNewWindow();
        //validate person was deleted
        ArrayList<Person> people = PersonStorageUtil.loadPeople(context);
        assertFalse(people.contains(person));
    }

    @After
    public void clean() {
        //delete saved data, that way we don't modify our apps data
        Context context = InstrumentationRegistry.getTargetContext();
        PersonStorageUtil.deletePerson(context, person);
    }
}
