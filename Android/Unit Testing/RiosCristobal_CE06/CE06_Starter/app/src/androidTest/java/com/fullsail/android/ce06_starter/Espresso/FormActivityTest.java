
package com.fullsail.android.ce06_starter.Espresso;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;

import com.fullsail.android.ce06_starter.FormActivity;
import com.fullsail.android.ce06_starter.R;
import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class FormActivityTest {

    @Rule
    public IntentsTestRule<FormActivity> intentsTestRule =
            new IntentsTestRule<>(FormActivity.class,//class to launch
                    true,//interaction enabled
                    false);//launch by default

    private Person person;

    @Before
    public void launchForm(){
        intentsTestRule.launchActivity(null);
        person = new Person("John", "Doe", 30);
    }

    @Test
    public void form(){
        //once the activity launched fill text to our textFields
        onView(ViewMatchers.withId(R.id.edit_first_name)).perform(typeText(person.getFirstName()));
        onView(withId(R.id.edit_last_name)).perform(typeText(person.getLastName()));
        onView(withId(R.id.edit_age)).perform(typeText(String.valueOf(person.getAge())));

        //fake a click on the save button
        onView(withId(R.id.action_save)).perform(click());

        //validate that person was saved.
        Context context = InstrumentationRegistry.getTargetContext();
        ArrayList<Person> people = PersonStorageUtil.loadPeople(context);
        assertTrue(people.contains(person));
    }

    @After
    public void clean(){

        //delete saved person so our test didn't modify our data
        Context context = InstrumentationRegistry.getTargetContext();
        PersonStorageUtil.deletePerson(context, person);
    }

}
