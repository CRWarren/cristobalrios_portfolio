package com.fullsail.android.ce06_starter;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.fullsail.android.ce06_starter.object.Person;
import com.fullsail.android.ce06_starter.util.PersonStorageUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;


public class StorageTest {
    private static final String MOCK_FILE_LOCATION = "mockData.dat";
    private static Person person;

    @Test
    public void StoragePersonSaving(){
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        // When using our mock context to open a file, use the real context
        // to open a different file.
        try{
            Mockito.when(mockContext.openFileInput(Mockito.anyString()))
                    .thenReturn(context.openFileInput(MOCK_FILE_LOCATION));
            Mockito.when(mockContext.openFileOutput(Mockito.anyString(),Mockito.anyInt()))
                    .thenReturn(context.openFileOutput(MOCK_FILE_LOCATION,Context.MODE_PRIVATE));
        }catch (IOException e){
            e.printStackTrace();
        }
        // Save the person to a file. using mock
        PersonStorageUtil.savePerson(mockContext, person);
        // Load the data from the file.
        ArrayList<Person> people = PersonStorageUtil.loadPeople(mockContext);
        // Verify the saved person exists in the loaded list.
        assertThat(people,hasItems(person));

    }

    @Test
    public void deletePersonTest(){
        deletePerson();
        // Load the data from the file.
        Context mockContext = Mockito.mock(Context.class);
        ArrayList<Person> people = PersonStorageUtil.loadPeople(mockContext);
        assertFalse(people.contains(person));
    }

    private void deletePerson(){
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        // Change our mock implementation of deleteFile to delete our test file.
        Mockito.when(mockContext.deleteFile(Mockito.anyString()))
                .thenReturn(context.deleteFile(MOCK_FILE_LOCATION));
        // Delete our person using our mock context.
        PersonStorageUtil.deletePerson(mockContext,person);
    }

    @Before
    public void setup(){
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a file handle to the mock storage location.
        File mockStorage =new File(context.getFilesDir(),MOCK_FILE_LOCATION);
        // Create a blank mock storage file.
        try {
            mockStorage.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }

        //setup our person
        person = new Person("John","Doe",30);

    }

    @After
    public void clean(){
        deletePerson();
    }
}
