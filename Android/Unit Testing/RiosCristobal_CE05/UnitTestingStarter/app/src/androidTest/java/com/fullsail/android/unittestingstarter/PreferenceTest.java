package com.fullsail.android.unittestingstarter;

import android.content.Context;
import android.content.SharedPreferences;
import com.fullsail.android.unittestingstarter.util.PersonFormatUtil;
import com.fullsail.android.unittestingstarter.util.PreferenceUtil;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

public class PreferenceTest {

    //example followed from https://stackoverflow.com/questions/35105545/how-to-mock-a-sharedpreferences-using-mockito
    @Test
    public void NameFormatTest(){
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);

        //Create a mock preferences
        SharedPreferences mockPreferences = Mockito.mock(SharedPreferences.class);

        //use our mock context and preferences
        Mockito.when(mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(mockPreferences);

        //for preference value 0
        Mockito.when(mockPreferences.getInt(anyString(), anyInt())).thenReturn(0);
        //FIRST_LAST
        assertEquals(PersonFormatUtil.FORMAT_FIRST_LAST, PreferenceUtil.getNameFormat(mockContext));

        //for preference value 1
        Mockito.when(mockPreferences.getInt(anyString(), anyInt())).thenReturn(1);

        //LAST_FIRST
        assertEquals(PersonFormatUtil.FORMAT_LAST_FIRST, PreferenceUtil.getNameFormat(mockContext));

    }

    @Test
    public void PhoneFormatTest(){

        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        //Create a mock preferences
        SharedPreferences mockPreferences = Mockito.mock(SharedPreferences.class);

        //use our mock context and preferences
        Mockito.when(mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(mockPreferences);

        //for preference value 0
        Mockito.when(mockPreferences.getInt(anyString(), anyInt())).thenReturn(0);
        //FORMAT_ALL_DASHES
        assertEquals(PersonFormatUtil.FORMAT_ALL_DASHES, PreferenceUtil.getPhoneFormat(mockContext));

        //for preference value 1
        Mockito.when(mockPreferences.getInt(anyString(), anyInt())).thenReturn(1);
        //FORMAT_WITH_PARENS
        assertEquals(PersonFormatUtil.FORMAT_WITH_PARENS, PreferenceUtil.getPhoneFormat(mockContext));

        //for preference value 2
        Mockito.when(mockPreferences.getInt(anyString(), anyInt())).thenReturn(2);

        //FORMAT_WITH_SPACES
        assertEquals(PersonFormatUtil.FORMAT_WITH_SPACES, PreferenceUtil.getPhoneFormat(mockContext));
    }
}
