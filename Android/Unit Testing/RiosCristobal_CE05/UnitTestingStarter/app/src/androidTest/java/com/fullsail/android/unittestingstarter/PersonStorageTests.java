package com.fullsail.android.unittestingstarter;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.fullsail.android.unittestingstarter.object.Person;
import com.fullsail.android.unittestingstarter.util.PersonStorageUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class PersonStorageTests {
    private static final String FILE_NAME = "people.json";
    private static final String FILE_FOLDER = "json";

    @Test
    public void testPersonSaving() {
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        // use our mock context
        Mockito.when(mockContext.getExternalFilesDir(Mockito.anyString())).
                thenReturn(context.getExternalFilesDir(FILE_NAME));
        // Create a new person.
        Person person = new Person("John","Doe","4071234567",30);
        // Save the person to a file. using mock
        PersonStorageUtil.savePerson(mockContext, person);
        // Load the data from the file.
        ArrayList<Person> people = PersonStorageUtil.loadPeople(mockContext);
        // Verify the saved person exists in the loaded list.
        assertThat(people,hasItems(person));
    }

    @Test
    public void testPersonDeleting() {
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        // use our mock context
        Mockito.when(mockContext.getExternalFilesDir(Mockito.anyString())).
                thenReturn(context.getExternalFilesDir(FILE_NAME));
        // Create a new person.
        Person person = new Person("John","Doe","4071234567",30);
        // Save the person to a file. using mock
        PersonStorageUtil.savePerson(mockContext, person);

        //delete person
        PersonStorageUtil.deletePerson(mockContext, person);
        // Load the data from the file.
        ArrayList<Person> people = PersonStorageUtil.loadPeople(mockContext);
        // Verify the saved person exists in the loaded list.
        assertTrue((people.isEmpty()));
    }

    @Test
    public void deletePersonFile() {
        // Get our real context.
        Context context = InstrumentationRegistry.getTargetContext();
        // Create a mock context.
        Context mockContext = Mockito.mock(Context.class);
        // use our mock context
        Mockito.when(mockContext.getExternalFilesDir(Mockito.anyString())).
                thenReturn(context.getExternalFilesDir(FILE_NAME));
        // Create a new person.
        Person person = new Person("John","Doe","4071234567",30);
        // Save the person to a file. using mock
        PersonStorageUtil.savePerson(mockContext, person);
        // Change our mock implementation of deleteFile to delete our test file.
        Mockito.when(mockContext.deleteFile(Mockito.anyString()))
                .thenReturn(context.deleteFile(FILE_FOLDER));
        // Delete our mock file using our mock context.
        PersonStorageUtil.deletePersonFile(mockContext);
    }


}
