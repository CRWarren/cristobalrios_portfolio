package com.fullsail.android.unittestingstarter;

import android.util.Log;

import com.fullsail.android.unittestingstarter.object.Person;
import com.fullsail.android.unittestingstarter.util.PersonConversionUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

//had to add dependency to be able to use JSON assert
//http://jsonassert.skyscreamer.org/cookbook.html
import org.skyscreamer.jsonassert.JSONAssert;
import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class PersonConversionTest {


    private ArrayList<Person> OnePersonList = new ArrayList<>();
    private JSONArray OneJson = new JSONArray();

    private ArrayList<Person> ThreePeopleList = new ArrayList<>();
    private JSONArray ThreeJson = new JSONArray();

    //prepare our lists of 3 Persons and another one with one
    @Before
    public void prepareData()  {

        OnePersonList.add(new Person("Jhon", "Doe", "4777294603", 40));

        ThreePeopleList.add(new Person("Derek", "Shepard", "4777879337", 40));
        ThreePeopleList.add(new Person("Meredith", "Grey", "4001234567", 29));
        ThreePeopleList.add(new Person("Christina", "Yang", "(412)3456789", 29));
        try{
            OneJson.put(OnePersonList.get(0).getPersonAsJSON());
            //Log.e("ONEJSON",OneJson.toString());
            ThreeJson.put(ThreePeopleList.get(0).getPersonAsJSON());
            ThreeJson.put(ThreePeopleList.get(1).getPersonAsJSON());
            ThreeJson.put(ThreePeopleList.get(2).getPersonAsJSON());
            //Log.e("ThreeJSON",ThreeJson.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Test
    public void getPeopleJSONFromList() throws JSONException  {
       try{
           //One Person object in an ArrayList converted to the appropriate JSON.
           JSONAssert.assertEquals(PersonConversionUtil.getPeopleJSONFromList(OnePersonList), OneJson, true);
           //Three Person objects in an ArrayList converted to the appropriate JSON.
           JSONAssert.assertEquals(PersonConversionUtil.getPeopleJSONFromList(ThreePeopleList), ThreeJson, true);
       }catch (JSONException e){
           e.printStackTrace();
       }
    }

    @Test
    public void getPeopleListFromJson() throws JSONException {
       try{
           //JSON of one person converted to an ArrayList of one Person objects.
           assertEquals(PersonConversionUtil.getPeopleListFromJSON(OneJson), OnePersonList);
           //JSON of three people converted to an ArrayList of three Person objects.
           assertEquals(PersonConversionUtil.getPeopleListFromJSON(ThreeJson), ThreePeopleList);
       }catch (JSONException e){
           e.printStackTrace();
       }
    }

    //clear our lists
    @After
    public void finishTest() {
        OnePersonList.clear();
        ThreePeopleList.clear();
    }
}