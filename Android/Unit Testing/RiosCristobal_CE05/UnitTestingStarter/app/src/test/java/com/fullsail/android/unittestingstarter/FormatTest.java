package com.fullsail.android.unittestingstarter;

import com.fullsail.android.unittestingstarter.util.PersonConversionUtil;
import com.fullsail.android.unittestingstarter.util.PersonFormatUtil;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FormatTest {

    @Test
    public void testNameFormats() {
        //FORMAT_FIRST_LAST
        assertTrue(PersonFormatUtil.formatName(PersonFormatUtil.FORMAT_FIRST_LAST,"John","Doe").equals("John Doe"));
        assertTrue(PersonFormatUtil.formatName(PersonFormatUtil.FORMAT_FIRST_LAST,"David","Smith").equals("David Smith"));

        //FORMAT_LAST_FIRST
        assertTrue(PersonFormatUtil.formatName(PersonFormatUtil.FORMAT_LAST_FIRST,"John","Doe").equals("Doe, John"));
        assertTrue(PersonFormatUtil.formatName(PersonFormatUtil.FORMAT_LAST_FIRST,"David","Smith").equals("Smith, David"));
    }

    @Test
    public void testingPhoneNumberFormats() {

        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_ALL_DASHES,"4075550123").equals("407-555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_ALL_DASHES,"407 555 0123").equals("407-555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_ALL_DASHES,"(407)555-0123").equals("407-555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_ALL_DASHES,"407-555-0123").equals("407-555-0123"));

        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_PARENS,"4075550123").equals("(407)555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_PARENS,"407 555 0123").equals("(407)555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_PARENS,"(407)555-0123").equals("(407)555-0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_PARENS,"407-555-0123").equals("(407)555-0123"));

        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_SPACES,"4075550123").equals("407 555 0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_SPACES,"407 555 0123").equals("407 555 0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_SPACES,"(407)555-0123").equals("407 555 0123"));
        assertTrue(PersonFormatUtil.formatPhoneNumber(PersonFormatUtil.FORMAT_WITH_SPACES,"407-555-0123").equals("407 555 0123"));

        assertTrue(PersonFormatUtil.unformatPhoneNumber("4075550123").equals("4075550123"));
        assertTrue(PersonFormatUtil.unformatPhoneNumber("407 555 0123").equals("4075550123"));
        assertTrue(PersonFormatUtil.unformatPhoneNumber("(407)555-0123").equals("4075550123"));
        assertTrue(PersonFormatUtil.unformatPhoneNumber("407-555-0123").equals("4075550123"));

        assertTrue(PersonFormatUtil.isPhoneNumberValid("4075550123"));
        assertTrue(PersonFormatUtil.isPhoneNumberValid("407 555 0123"));
        assertTrue(PersonFormatUtil.isPhoneNumberValid("(407)555-0123"));
        assertTrue(PersonFormatUtil.isPhoneNumberValid("407-555-0123"));

        assertFalse(PersonFormatUtil.isPhoneNumberValid("1-407-555-0123"));
        assertFalse(PersonFormatUtil.isPhoneNumberValid("40755501234"));
        assertFalse(PersonFormatUtil.isPhoneNumberValid("407555012"));
    }

}
