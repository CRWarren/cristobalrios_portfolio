package com.example.cristobal.rioscristobal_ce03.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce03.Service.UpdateService;
import com.example.cristobal.rioscristobal_ce03.WidgetUtil;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper;

public class WeatherWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.e("ON UPDATE","UPDATE RECIEVED");
        Intent i = new Intent(context, UpdateService.class);
        i.putExtra(UpdateService.EXTRA_WIDGET_IDS, appWidgetIds);
        context.startService(i);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("ON UPDATE","UPDATE RECIEVED");
    }
}
