package com.example.cristobal.rioscristobal_ce03;

import android.os.Bundle;
import android.preference.PreferenceFragment;
public class ConfigFragment extends PreferenceFragment {
    public static ConfigFragment newInstance() {

        Bundle args = new Bundle();

        ConfigFragment fragment = new ConfigFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preference_fragment);
    }


}
