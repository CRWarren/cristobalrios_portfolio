package com.example.cristobal.rioscristobal_ce03.widget.Weather;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper.DIRECTORY_NAME;

public class WeatherForecastHelper extends AsyncTask<String,String,String> {
    private static final String FILE_NAME = "forecast";
    private Context _context;
    private finishedObj mInterface;
    public WeatherForecastHelper(Context context){
        if(context instanceof finishedObj){
            mInterface = (finishedObj) context;
        }
        _context = context;
    }
    public interface finishedObj{
        void update();
    }

    @Override
    protected String doInBackground(String... strings) {
        if(validConnection()){
            if(strings[0]==null){
                return "";
            }
            String result = "";


            URL url;
            HttpURLConnection connection = null;
            InputStream inStream = null;

            try {
                Log.e("ON HELPER","TRY");
                url = new URL(strings[0]);
                Log.e("ON HELPER",url.toString());
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
            } catch(Exception e) {
                e.printStackTrace();
            }

            try {
                inStream = connection.getInputStream();
                result = IOUtils.toString(inStream, "UTF-8");
            } catch(Exception e) {
                e.printStackTrace();
            }

            finally {

                if (connection != null) {
                    if (inStream != null) {
                        try {
                            // close stream if open
                            inStream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // always disconnect if there is a connection
                    connection.disconnect();
                }
            }


            // Display result
            return result;
        }
        Log.e("ON HELPER","NO INternet");
        return null;
    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s!=null){
            parseJson(s);
        }
        if (mInterface!=null){
            mInterface.update();
        }

    }

    private void parseJson(String result){
        ArrayList<WeatherData> WD= new ArrayList<>();
        Log.e("ON FORECAST", result);
        try{
            JSONObject outer = new JSONObject(result);
            JSONObject inner = outer.getJSONObject("forecast");
            JSONObject in_inner = inner.getJSONObject("simpleforecast");
            JSONArray  details = in_inner.getJSONArray("forecastday");
            for (int i = 0; i < details.length(); i++) {
                JSONObject detailsObj = details.getJSONObject(i);
                JSONObject dayObj = detailsObj.getJSONObject("date");
                String day = dayObj.getString("weekday");
                JSONObject hiObj = detailsObj.getJSONObject("high");
                JSONObject lowObj = detailsObj.getJSONObject("low");
                String Temps = hiObj.getString("celsius")+" C - "+lowObj.getString("celsius")+" C";
                String icon = detailsObj.getString("icon");
                String humidity = Double.toString(detailsObj.getDouble("avehumidity"));

                WD.add(new WeatherData("",icon,humidity,Temps,"",day));
            }




        }catch (JSONException e){
            e.printStackTrace();
        }
        if(WD!=null){
            Save(WD);
        }
    }

    private void Save(ArrayList<WeatherData> wd){
        File protectedStorage = _context.getExternalFilesDir(DIRECTORY_NAME);
        for (int i = 0; i < wd.size(); i++) {
            File file = new File(protectedStorage,FILE_NAME+i);
            try{
                file.createNewFile();
                Log.e("CreatingFile WD", "------------------");
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                Log.e("Writing WD", "------------------");
                oos.writeObject(wd.get(i));
                Log.e("Write WD", "------------------");
                oos.close();
                fos.close();
            }catch (IOException e){
                e.printStackTrace();
            }

        }
    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }
}
