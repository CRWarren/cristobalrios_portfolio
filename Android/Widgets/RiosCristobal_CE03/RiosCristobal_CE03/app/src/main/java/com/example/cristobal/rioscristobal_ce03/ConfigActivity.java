package com.example.cristobal.rioscristobal_ce03;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.cristobal.rioscristobal_ce03.Service.UpdateService;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper;

public class ConfigActivity extends AppCompatActivity {

    private int mWidgetID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        //get the starting intent
        Intent i = getIntent();
        mWidgetID = i.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        //get the ID of the widget from extras

        //if the ID is invalid, close activity to prevent possible crashes with updating an invalid widget
        if(mWidgetID == AppWidgetManager.INVALID_APPWIDGET_ID){
            finish();
        }
        if(savedInstanceState==null){
            ConfigFragment fragment = ConfigFragment.newInstance();
            getFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.config_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_save){

            //update the widget
            Intent service = new Intent(this, UpdateService.class);
            service.putExtra(UpdateService.EXTRA_WIDGET_IDS, new int[]{mWidgetID});
            startService(service);
            //send activity result with id
            Intent i = new Intent();
            i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,mWidgetID);
            setResult(RESULT_OK,i);
            //close the activity
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
