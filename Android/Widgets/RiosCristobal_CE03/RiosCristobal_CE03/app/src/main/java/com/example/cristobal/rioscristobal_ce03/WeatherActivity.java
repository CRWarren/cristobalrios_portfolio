package com.example.cristobal.rioscristobal_ce03;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.cristobal.rioscristobal_ce03.Fragments.ForecastFragment;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherForecastHelper;

public class WeatherActivity extends AppCompatActivity implements WeatherForecastHelper.finishedObj{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
    }

    @Override
    protected void onResume() {
        super.onResume();

       if(validConnection()){
           SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
           WeatherForecastHelper downloadTask2 = new WeatherForecastHelper(this);
           final String webAddress2 = "http://api.wunderground.com/api/8084335d58efcbe7/forecast/q/"+sp.getString("list_preference_1","WA/Seattle")+".json";
           downloadTask2.execute(webAddress2);
       }else {
           Toast.makeText(this,"No Internet Connection",Toast.LENGTH_SHORT).show();
           getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, ForecastFragment.newInstance()).commit();
       }


    }
    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }
    @Override
    public void update() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, ForecastFragment.newInstance()).commit();
    }
}
