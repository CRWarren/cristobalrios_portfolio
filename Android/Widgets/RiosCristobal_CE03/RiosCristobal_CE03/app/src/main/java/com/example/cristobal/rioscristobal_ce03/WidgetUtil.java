package com.example.cristobal.rioscristobal_ce03;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherData;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import static com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper.DIRECTORY_NAME;
import static com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper.FILE_NAME;

public class WidgetUtil {

    private static void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetID){
        RemoteViews widgetViews = new RemoteViews(context.getPackageName(),R.layout.weather_widget_layout);
        WeatherData WD = getData(context);
        widgetViews.setTextViewText(R.id.CityNameTV,WD.getCiti());
        widgetViews.setTextViewText(R.id.cloudState,WD.getStatus());
        widgetViews.setTextViewText(R.id.changeOfRain,"Humidity "+WD.getHumidity());
        widgetViews.setTextViewText(R.id.time,WD.getTimeStamp());
        widgetViews.setTextViewText(R.id.Temperature,WD.getTemperature());
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        if(sp.getBoolean("switch_preference_1",false)){
            widgetViews.setInt(R.id.Linerarlayout, "setBackgroundColor", R.color.DarkBlue);
        }
        switch (WD.getStatus()){
            case"chanceofflurries":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.chanceflurries);
                break;
            case"chanceofrain":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.chancerain);
                break;
            case"chancefreezingrain":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.chancesnow);
                break;
            case"chanceofsnow":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.chancesnow);
                break;
            case"chanceofthunderstorms":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.chancetstorms);
                break;
            case"clear":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.clear);
                break;
            case"cloudy":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.cloudy);
                break;
            case"flurries":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.flurries);
                break;
            case"fog":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.fog);
                break;
            case"haze":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.hazy);
                break;
            case"mostlycloudy":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.mostlycloudy);
                break;
            case"mostlysunny":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.mostlysunny);
                break;
            case"partlycloudy":
                Log.e("On SWITCH", "PARTLY CLOUDY");
                widgetViews.setImageViewResource(R.id.photo,R.drawable.partlycloudy);
                break;
            case"partlysunny":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.partlysunny);
                break;
            case"rain":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.rain);
                break;
            case"freezingrain":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.rain);
                break;
            case"snow":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.snow);
                break;
            case"sunny":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.sunny);
                break;
            case"thunderstorms":
                widgetViews.setImageViewResource(R.id.photo,R.drawable.tstorms);
                break;


        }
        //create a pending intent template to open our DetailsActivity;
        Intent details =  new Intent(context, WeatherActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context,0,details,PendingIntent.FLAG_UPDATE_CURRENT);

        //set our pending intent to be our template and attach it to the list.
        widgetViews.setOnClickPendingIntent(R.id.photo,pi);
        //create pending intent for config activity
        Intent ii = new Intent(context, ConfigActivity.class);
        ii.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetID);
        //unique pending intent for config activity
        PendingIntent configPendingIntent = PendingIntent.getActivity(context,widgetID,ii, PendingIntent.FLAG_UPDATE_CURRENT);
        widgetViews.setOnClickPendingIntent(R.id.configButton,configPendingIntent);

        //update the widget with the newly configured remote view
        appWidgetManager.updateAppWidget(widgetID,widgetViews);
    }

    private static WeatherData getData(Context context){
        WeatherData WD=null;
        File protectedStorage = context.getExternalFilesDir(DIRECTORY_NAME);
        try {
            FileInputStream fis = new FileInputStream(protectedStorage+"/"+FILE_NAME);
            Log.e("On TRY", "On OBJECT");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Log.e("On TRY", "On Reading");
            WD = (WeatherData) ois.readObject();
            Log.e("On TRY", WD.getCiti());
            ois.close();
            fis.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return WD;
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager, int[] widgetIDS){

        for (int widgetID : widgetIDS) {
            updateWidget(context, appWidgetManager, widgetID);
        }
    }


}
