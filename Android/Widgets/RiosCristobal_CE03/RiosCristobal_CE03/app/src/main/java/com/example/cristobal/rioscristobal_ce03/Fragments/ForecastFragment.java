package com.example.cristobal.rioscristobal_ce03.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.cristobal.rioscristobal_ce03.ForecastAdapter;
import com.example.cristobal.rioscristobal_ce03.R;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherData;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import static com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper.DIRECTORY_NAME;

public class ForecastFragment extends ListFragment {
    private ArrayList<WeatherData> WD;
    public static ForecastFragment newInstance() {
        Bundle args = new Bundle();
        ForecastFragment fragment = new ForecastFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forecast_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WD = getWeather();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(WD!=null){
            ForecastAdapter fa = new ForecastAdapter(getActivity(),WD);
            setListAdapter(fa);
        }
    }

    private ArrayList<WeatherData> getWeather(){
        ArrayList<WeatherData> WD= new ArrayList<>();
        File protectedStorage = getActivity().getExternalFilesDir(DIRECTORY_NAME);
        for (File f:protectedStorage.listFiles()) {
            if(!f.getName().equals(WeatherHelper.FILE_NAME)){
                Log.e("FILE NAME",f.getName());
                try {
                    FileInputStream fis = new FileInputStream(f);
                    Log.e("On TRY", "On OBJECT");
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    Log.e("On TRY", "On Reading");
                    WD.add((WeatherData) ois.readObject());
                    Log.e("On TRY", WD.size() + "");
                    ois.close();
                    fis.close();

                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return WD;

    }
}
