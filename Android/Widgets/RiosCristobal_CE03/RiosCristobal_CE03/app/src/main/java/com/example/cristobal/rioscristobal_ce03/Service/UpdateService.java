package com.example.cristobal.rioscristobal_ce03.Service;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherForecastHelper;
import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherHelper;

public class UpdateService extends IntentService {
    public static final String EXTRA_WIDGET_IDS = "com.example.cristobal.rioscristobal_ce03.EXTRA_WIDGET_IDS";
    public UpdateService() {
        super("UpdateIntentService");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
       if(intent.hasExtra(EXTRA_WIDGET_IDS)){
           int[] IDs = intent.getIntArrayExtra(EXTRA_WIDGET_IDS);
           SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
           if(IDs!=null){
               AppWidgetManager mgr = AppWidgetManager.getInstance(this);
               WeatherHelper downloadTask = new WeatherHelper(this,mgr,IDs);
               final String webAddress = "http://api.wunderground.com/api/8084335d58efcbe7/conditions/q/"+sp.getString("list_preference_1","WA/Seattle")+".json";
               downloadTask.execute(webAddress);
           }
       }
    }
}
