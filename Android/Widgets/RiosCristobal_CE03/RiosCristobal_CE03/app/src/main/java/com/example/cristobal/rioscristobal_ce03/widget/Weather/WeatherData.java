package com.example.cristobal.rioscristobal_ce03.widget.Weather;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.io.Serializable;

public class WeatherData implements Serializable {
    private String Citi;
    private String Status;
    private String humidity;
    private String temperature;
    private String timeStamp;
    private String Day;

    public WeatherData(String _Citi, String _Status,String _humidity, String _temp,String time, String _Day){
        Citi = _Citi;
        Status = _Status;
        humidity = _humidity;
        temperature = _temp;
        timeStamp = time;
        Day =_Day;
    }

    public String getCiti() {
        return Citi;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getStatus() {
        return Status;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getDay() {
        return Day;
    }
}
