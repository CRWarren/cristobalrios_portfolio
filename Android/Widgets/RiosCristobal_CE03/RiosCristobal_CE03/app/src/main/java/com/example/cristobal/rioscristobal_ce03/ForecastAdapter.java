package com.example.cristobal.rioscristobal_ce03;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cristobal.rioscristobal_ce03.widget.Weather.WeatherData;

import java.util.ArrayList;

public class ForecastAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private Context _Context;
    private ArrayList<WeatherData> WD;
    public ForecastAdapter(Context _context, ArrayList<WeatherData> data){
        _Context = _context;
        WD = data;
    }
    @Override
    public int getCount() {
        if(WD != null) {
            return WD.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(WD != null) {
            return WD.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.weather_cell, parent, false);
            vh =  new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ViewHolder)convertView.getTag();
        }

        switch (WD.get(position).getStatus()){

            case"chanceflurries":
                vh.imageIV.setImageResource(R.drawable.chanceflurries);
                break;
            case"chancerain":
                vh.imageIV.setImageResource(R.drawable.chancerain);
                break;
            case"chancesleet":
                vh.imageIV.setImageResource(R.drawable.chancesleet);
                break;
            case"chancesnow":
                vh.imageIV.setImageResource(R.drawable.chancesnow);
                break;
            case"chancetstorms":
                vh.imageIV.setImageResource(R.drawable.chancetstorms);
                break;
            case"clear":
                vh.imageIV.setImageResource(R.drawable.clear);
                break;
            case"cloudy":
                vh.imageIV.setImageResource(R.drawable.cloudy);
                break;
            case"flurries":
                vh.imageIV.setImageResource(R.drawable.flurries);
                break;
            case"fog":
                vh.imageIV.setImageResource(R.drawable.fog);
                break;
            case"hazy":
                vh.imageIV.setImageResource(R.drawable.hazy);
                break;
            case"mostlycloudy":
                vh.imageIV.setImageResource(R.drawable.mostlycloudy);
                break;
            case"mostlysunny":
                vh.imageIV.setImageResource(R.drawable.mostlysunny);
                break;
            case"partlycloudy":
                Log.e("On SWITCH", "PARTLY CLOUDY");
                vh.imageIV.setImageResource(R.drawable.partlycloudy);
                break;
            case"partlysunny":
                vh.imageIV.setImageResource(R.drawable.partlysunny);
                break;
            case"rain":
                vh.imageIV.setImageResource(R.drawable.rain);
                break;
            case"sleet":
                vh.imageIV.setImageResource(R.drawable.sleet);
                break;
            case"snow":
                vh.imageIV.setImageResource(R.drawable.snow);
                break;
            case"sunny":
                vh.imageIV.setImageResource(R.drawable.sunny);
                break;
            case"tstorms":
                vh.imageIV.setImageResource(R.drawable.tstorms);
                break;
        }
        vh.temp.setText(WD.get(position).getTemperature());
        String hum = "Humidity: "+ WD.get(position).getHumidity()+"%";
        vh.humidity.setText(hum);
        vh.title.setText(WD.get(position).getDay());


        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{
        final ImageView imageIV;
        final TextView temp;
        final TextView title;
        final TextView humidity;

        ViewHolder(View _layout){
            imageIV = _layout.findViewById(R.id.iconHolder);
            title =_layout.findViewById(R.id.dayLbl);
            temp =_layout.findViewById(R.id.TempsLbl);
            humidity =_layout.findViewById(R.id.humidityLbl);

        }
    }
}
