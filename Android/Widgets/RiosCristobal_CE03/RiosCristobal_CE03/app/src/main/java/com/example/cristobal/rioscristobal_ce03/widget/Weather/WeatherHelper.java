package com.example.cristobal.rioscristobal_ce03.widget.Weather;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce03.WidgetUtil;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherHelper extends AsyncTask<String,String,String> {
    public static final String DIRECTORY_NAME = "Weather";
    public static final String FILE_NAME = "current";

    private Context _context;
    private AppWidgetManager _mgr;
    private int[] appWidgetIds;
    public WeatherHelper(Context context, AppWidgetManager mgr, int[] IDs){
        _mgr = mgr;
        appWidgetIds = IDs;
        _context = context;
    }
    @Override
    protected String doInBackground(String... strings) {
        if(validConnection()){
            if(strings[0]==null){
                return "";
            }
            String result = "";


            URL url;
            HttpURLConnection connection = null;
            InputStream inStream = null;

            try {
                Log.e("ON HELPER","TRY");
                url = new URL(strings[0]);
                Log.e("ON HELPER",url.toString());
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
            } catch(Exception e) {
                e.printStackTrace();
            }

            try {
                inStream = connection.getInputStream();
                result = IOUtils.toString(inStream, "UTF-8");
            } catch(Exception e) {
                e.printStackTrace();
            }

            finally {

                if (connection != null) {
                    if (inStream != null) {
                        try {
                            // close stream if open
                            inStream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // always disconnect if there is a connection
                    connection.disconnect();
                }
            }


            // Display result
            return result;
        }
        Log.e("ON HELPER","NO INternet");
        return null;
    }


    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s!=null){
            parseJson(s);
        }
        WidgetUtil.updateWidget(_context,_mgr,appWidgetIds);
    }

    private void parseJson(String result){
        WeatherData WD= null;
        try{
            JSONObject outer = new JSONObject(result);
            JSONObject obj = outer.getJSONObject("current_observation");
            String time = obj.getString("observation_time");
            JSONObject CityObj = obj.getJSONObject("display_location");
            String city = CityObj.getString("full");
            String temp = Double.toString(obj.getDouble("temp_c"))+" C";
            String humidity = obj.getString("relative_humidity");
            String status = obj.getString("icon");
            WD = new WeatherData(city,status,humidity,temp,time,"");

        }catch (JSONException e){
            e.printStackTrace();
        }
        if(WD!=null){
            Save(WD);
        }
    }

    private void Save(WeatherData wd){
        File protectedStorage = _context.getExternalFilesDir(DIRECTORY_NAME);
        File file = new File(protectedStorage,FILE_NAME);
        try{
            file.createNewFile();
            Log.e("CreatingFile", "------------------");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Log.e("Writing", "------------------");
            oos.writeObject(wd);
            Log.e("Write", "------------------");
            oos.close();
            fos.close();
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }

}
