package com.example.cristobal.rioscristobal_ce01.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;


import com.example.cristobal.rioscristobal_ce01.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static com.example.cristobal.rioscristobal_ce01.Main2Activity.AUTHORITY;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.IMAGE_FOLDER;

public class Form_Fragment extends Fragment {
    static String image = null;
    takePic mInterface;
    public interface takePic{
       void getImage();
       void saveFile(String title, String city, String image);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof takePic){
            mInterface = (takePic) context;
        }
    }

    public static Form_Fragment newInstance(String imageFIleName) {

        Bundle args = new Bundle();
        image = imageFIleName;
        Form_Fragment fragment = new Form_Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.add_form_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.camera){
            mInterface.getImage();
        }else if(item.getItemId() == R.id.save){
            saveFile();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.form_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(image,"on Fragment------------");
        if(image.length()>0){
            getimage();
        }
    }

    private void saveFile(){
        if(image.length()>0){
            if(checkFIelds()){
                EditText titleET = (EditText)getActivity().findViewById(R.id.editText);
                EditText cityET = (EditText)getActivity().findViewById(R.id.editText2);
                mInterface.saveFile(titleET.getText().toString(),cityET.getText().toString(),image);
            }else{
                new AlertDialog.Builder(getActivity()).setTitle("Please Fill all fields").setMessage("all spaces invalid, please check your input")
                        .setIcon(R.mipmap.ic_launcher).setPositiveButton("ok", null).show();
            }
        }
    }

    private boolean checkFIelds(){
        EditText titleET = (EditText)getActivity().findViewById(R.id.editText);
        EditText cityET = (EditText)getActivity().findViewById(R.id.editText2);
        if(titleET.getText().toString().trim().length()>0&&cityET.getText().toString().trim().length()>0){
            return true;
        }
        return false;
    }
    private void getimage(){
        ImageView iv = (ImageView) getActivity().findViewById(R.id.imageView);
        File protectedStorage = getActivity().getExternalFilesDir(IMAGE_FOLDER);
        File imageFile = new File(protectedStorage,image);
               // images.add(FileProvider.getUriForFile(this,AUTHORITY,f));
            if(imageFile.exists()){
                Log.e("IT EXISTS!!!!","on Fragment------------");
                iv.setImageURI(getOutputUri(imageFile));
            }
    }
    private Uri getOutputUri(File f) {
        return FileProvider.getUriForFile(getActivity(),AUTHORITY,f);
    }
}
