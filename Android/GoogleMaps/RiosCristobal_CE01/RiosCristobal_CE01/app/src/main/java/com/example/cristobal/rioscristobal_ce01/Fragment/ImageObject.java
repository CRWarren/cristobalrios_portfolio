package com.example.cristobal.rioscristobal_ce01.Fragment;

import java.io.Serializable;

public class ImageObject implements Serializable {
    String title;
    String city;
    String fileName;
    String latitude;
    String longitude;

    public ImageObject(String _title,String _city,String _filename,String _latitude,String _longitude){
        title=_title;
        city=_city;
        fileName=_filename;
        latitude=_latitude;
        longitude=_longitude;
    }

    public String getCity() {
        return city;
    }

    public String getFileName() {
        return fileName;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getTitle() {
        return title;
    }
}
