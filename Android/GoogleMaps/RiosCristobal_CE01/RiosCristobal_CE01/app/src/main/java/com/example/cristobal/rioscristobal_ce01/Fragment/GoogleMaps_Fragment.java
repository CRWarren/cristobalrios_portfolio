package com.example.cristobal.rioscristobal_ce01.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cristobal.rioscristobal_ce01.DetailsActivity;
import com.example.cristobal.rioscristobal_ce01.Main2Activity;
import com.example.cristobal.rioscristobal_ce01.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;

import static android.content.Context.LOCATION_SERVICE;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.AUTHORITY;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.FILES_FOLDERNAME;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.IMAGE_FOLDER;

public class GoogleMaps_Fragment extends MapFragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter,
        GoogleMap.OnInfoWindowClickListener, LocationListener, GoogleMap.OnMapLongClickListener {

    private HashMap<String,String> files = new HashMap<>();
    private GoogleMap _Map;
    public static final String EXTRA_LATITUDE = "EXTRA_LATITUDE";
    public static final String EXTRA_LONGITUDE = "EXTRA_LONGITUDE";
    private static final int REQUEST_LOCATION_PERMISSIONS = 0x01001;
    public static final String EXTRA_FILENAME = "EXTRA_FILENAME";

    LocationManager mLocationManager;

    public static GoogleMaps_Fragment newInstance() {

        Bundle args = new Bundle();

        GoogleMaps_Fragment fragment = new GoogleMaps_Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {

        super.onActivityCreated(bundle);
        Log.e("On ActivityCreated", "--------------");
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Rquest permission if we dont have them
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSIONS);
        }
        getMapAsync(this);

    }


    private void getFiles(){
        files.clear();
        File protectedStorage = getActivity().getExternalFilesDir(FILES_FOLDERNAME);
        ImageObject io=null;
        for (File f:protectedStorage.listFiles()) {
            try{
                Log.e("On TRY", "--------------"+f.getName());
                FileInputStream fis = new FileInputStream(f);
                Log.e("On TRY", "On OBJECT");
                ObjectInputStream ois = new ObjectInputStream(fis);
                Log.e("On TRY", "On Reading");
                io = (ImageObject) ois.readObject();
                files.put(io.getTitle(),io.getFileName());
                Log.e("On TRY", io.getFileName());
                ois.close();
                fis.close();
            }catch (IOException |ClassNotFoundException e){
                e.printStackTrace();
            }
            if(_Map==null&&io==null){
                return;
            }else{
                LatLng imageLocation = new LatLng(Double.parseDouble(io.latitude), Double.parseDouble(io.longitude));
                MarkerOptions options = new MarkerOptions();
                options.position(imageLocation);
                options.title(io.title);
                options.snippet(io.city);
                _Map.addMarker(options);
            }

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.add_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add){
            if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                Location lastKnown = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if(lastKnown==null){
                    new AlertDialog.Builder(getActivity()).setTitle("Function Unavailable").setMessage("Location Unavailable")
                            .setIcon(R.mipmap.ic_launcher).setPositiveButton("ok", null).show();
                }else{
                    Intent i = new Intent(getActivity(), Main2Activity.class);
                    i.putExtra(EXTRA_LATITUDE, lastKnown.getLatitude());
                    i.putExtra(EXTRA_LONGITUDE, lastKnown.getLongitude());
                    startActivity(i);
                }

            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        View contents = LayoutInflater.from(getActivity()).inflate(R.layout.info_window,null);
        ((TextView)contents.findViewById(R.id.title)).setText(marker.getTitle());
        ((TextView)contents.findViewById(R.id.snippet)).setText(marker.getSnippet());
        File protectedStorage = getActivity().getExternalFilesDir(IMAGE_FOLDER);
        File imageFile = new File(protectedStorage,files.get(marker.getTitle()));
        if(imageFile.exists()){
            Log.e("IT EXISTS!!!!","on MAP__________");
            ((ImageView)contents.findViewById(R.id.mImageVIew)).setImageURI(getOutputUri(imageFile));

        }
        return contents;
    }
    private Uri getOutputUri(File f) {
        return FileProvider.getUriForFile(getActivity(),AUTHORITY,f);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent i = new Intent(getActivity(),DetailsActivity.class);
        i.putExtra(EXTRA_FILENAME,files.get(marker.getTitle()));
        startActivity(i);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("On Map Ready", "--------------");
        _Map = googleMap;
        mLocationManager = (LocationManager)getActivity().getSystemService(LOCATION_SERVICE);
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            //request location updates using 'this' as our locationListener
            Location loc = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(loc==null){
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,100,5.0f,this);
            }else {
                zoomInCamera(loc);
            }
            Log.e("On Map Ready", "PermissionGranted");
            _Map.setMyLocationEnabled(true);
            getFiles();
        }
        _Map.setOnMapLongClickListener(this);
        _Map.setInfoWindowAdapter(this);
        _Map.setOnInfoWindowClickListener(this);

    }

    @Override
    public void onLocationChanged(Location location) {
        zoomInCamera(location);
    }
    private void zoomInCamera(Location loc){
        if(_Map==null){
            return;
        }
        LatLng lastKnown = new LatLng(loc.getLatitude(),loc.getLongitude());
        CameraUpdate cmu = CameraUpdateFactory.newLatLngZoom(lastKnown,16);
        _Map.animateCamera(cmu);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Intent i = new Intent(getActivity(), Main2Activity.class);
        i.putExtra(EXTRA_LATITUDE, latLng.latitude);
        i.putExtra(EXTRA_LONGITUDE, latLng.longitude);
        startActivity(i);
    }
}
