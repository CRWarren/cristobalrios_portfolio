package com.example.cristobal.rioscristobal_ce01;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cristobal.rioscristobal_ce01.Fragment.Form_Fragment;
import com.example.cristobal.rioscristobal_ce01.Fragment.ImageObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static com.example.cristobal.rioscristobal_ce01.Fragment.GoogleMaps_Fragment.EXTRA_LATITUDE;
import static com.example.cristobal.rioscristobal_ce01.Fragment.GoogleMaps_Fragment.EXTRA_LONGITUDE;

public class Main2Activity extends AppCompatActivity implements Form_Fragment.takePic {

    public static final String AUTHORITY = "com.example.cristobal.rioscristobal_ce01";
    public static final String FILES_FOLDERNAME = "files";
    public static final String IMAGE_FOLDER = "images";
    String IMAGE_FILENAME = "";
    private static final int REQUEST_TAKE_PIC = 10;
     double latitude;
     double  longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent i = getIntent();
        if(i.hasExtra(EXTRA_LATITUDE)&&i.hasExtra(EXTRA_LONGITUDE)){
             latitude = i.getExtras().getDouble(EXTRA_LATITUDE);
             longitude = i.getExtras().getDouble(EXTRA_LONGITUDE);
            getSupportFragmentManager().beginTransaction().replace(R.id.form_Container,Form_Fragment.newInstance("")).commit();
        }
    }

    private Uri getOutputUri() {
        return FileProvider.getUriForFile(this,AUTHORITY,getImageFile());
    }

    private File getImageFile(){
        checkForPreviousImage();
        IMAGE_FILENAME=latitude+"_"+longitude+"_"+System.currentTimeMillis();
        Log.i(IMAGE_FILENAME,"on GIF------------");
        File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
        File imageFile = new File(protectedStorage,IMAGE_FILENAME);
        try{
            imageFile.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
        return imageFile;
    }
    private void checkForPreviousImage(){
        File protectedStorage = getExternalFilesDir(IMAGE_FOLDER);
        File imageFile = new File(protectedStorage,IMAGE_FILENAME);
        if(imageFile.exists()){
            Log.i(IMAGE_FILENAME,"DELETED--------DELETED");
            imageFile.delete();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_TAKE_PIC){
            Log.i(IMAGE_FILENAME,"on REQUEST--------");
            getSupportFragmentManager().beginTransaction().replace(R.id.form_Container,Form_Fragment.newInstance(IMAGE_FILENAME)).commit();
        }
    }

    @Override
    public void getImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,getOutputUri());
        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent,REQUEST_TAKE_PIC);
    }

    @Override
    public void saveFile(String title, String city, String image) {
        File protectedStorage = getExternalFilesDir(FILES_FOLDERNAME);
        File file = new File(protectedStorage,image);
        ImageObject io = new ImageObject(title,city,image,Double.toString(latitude),Double.toString(longitude));
        try{

            file.createNewFile();
            Log.e("CreatingFile", "------------------");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Log.e("Writing", "------------------");
            oos.writeObject(io);
            Log.e("Write", "------------------");
            oos.close();
            fos.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        finish();
        Toast.makeText(this,"Saved",Toast.LENGTH_SHORT).show();
    }


}
