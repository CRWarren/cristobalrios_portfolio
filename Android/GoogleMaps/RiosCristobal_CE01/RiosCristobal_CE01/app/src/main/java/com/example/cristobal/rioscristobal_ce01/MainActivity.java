package com.example.cristobal.rioscristobal_ce01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.rioscristobal_ce01.Fragment.GoogleMaps_Fragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GoogleMaps_Fragment frag = GoogleMaps_Fragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, frag).commit();
    }
}
