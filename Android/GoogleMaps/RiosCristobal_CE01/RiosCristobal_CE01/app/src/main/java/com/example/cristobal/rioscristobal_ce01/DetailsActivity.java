package com.example.cristobal.rioscristobal_ce01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cristobal.rioscristobal_ce01.Fragment.DetailsFragment;

import static com.example.cristobal.rioscristobal_ce01.Fragment.GoogleMaps_Fragment.EXTRA_FILENAME;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent i = getIntent();
        if(i.hasExtra(EXTRA_FILENAME)){
            String fn = i.getStringExtra(EXTRA_FILENAME);
            getSupportFragmentManager().beginTransaction().replace(R.id.details_container, DetailsFragment.newInstance(fn)).commit();
        }

    }
}
