package com.example.cristobal.rioscristobal_ce01.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cristobal.rioscristobal_ce01.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import static com.example.cristobal.rioscristobal_ce01.Main2Activity.AUTHORITY;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.FILES_FOLDERNAME;
import static com.example.cristobal.rioscristobal_ce01.Main2Activity.IMAGE_FOLDER;

public class DetailsFragment extends Fragment {

    public static String _fileName;
    public static DetailsFragment newInstance(String fileName) {

        Bundle args = new Bundle();
        _fileName = fileName;
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.details_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.details_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.delete){
            new AlertDialog.Builder(getActivity()).setTitle("Are you sure you want to delete?")
                    .setIcon(R.mipmap.ic_launcher).setNegativeButton("no",null)
                    .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    File protectedStorage = getActivity().getExternalFilesDir(IMAGE_FOLDER);
                    File imageFile = new File(protectedStorage,_fileName);
                    if(imageFile.exists()){
                        Log.e("On DELETE", "Image Deleted");
                        imageFile.delete();
                    }
                    protectedStorage = getActivity().getExternalFilesDir(FILES_FOLDERNAME);
                    File file = new File(protectedStorage,_fileName);
                    if(file.exists()){
                        Log.e("On DELETE", "File Deleted");
                        file.delete();
                        getActivity().finish();
                    }
                }
            }).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
      if(_fileName.length()>0){
          ImageObject io = getSavedIO();
          ((TextView)getActivity().findViewById(R.id.TitleTV)).setText(io.getTitle());
          ((TextView)getActivity().findViewById(R.id.cityTV)).setText(io.getCity());

          File protectedStorage = getActivity().getExternalFilesDir(IMAGE_FOLDER);
          File imageFile = new File(protectedStorage,_fileName);
          // images.add(FileProvider.getUriForFile(this,AUTHORITY,f));
          if(imageFile.exists()){
              Log.e("IT EXISTS!!!!","on DETAILS");
              ((ImageView)getActivity().findViewById(R.id.detailsIV)).setImageURI(getOutputUri(imageFile));

          }
      }
    }

    private Uri getOutputUri(File f) {
        return FileProvider.getUriForFile(getActivity(),AUTHORITY,f);
    }

    private ImageObject getSavedIO() {
        File protectedStorage = getActivity().getExternalFilesDir(FILES_FOLDERNAME);
        ImageObject io = null;
            try {
                FileInputStream fis = new FileInputStream(protectedStorage+"/"+_fileName);
                Log.e("On TRY", "On OBJECT");
                ObjectInputStream ois = new ObjectInputStream(fis);
                Log.e("On TRY", "On Reading");
                io = (ImageObject) ois.readObject();
                Log.e("On TRY", io.getFileName());
                ois.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        return io;
    }
}
