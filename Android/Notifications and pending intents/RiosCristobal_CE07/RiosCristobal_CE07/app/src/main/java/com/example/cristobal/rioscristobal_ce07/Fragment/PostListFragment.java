package com.example.cristobal.rioscristobal_ce07.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.cristobal.rioscristobal_ce07.CustomAdapter;
import com.example.cristobal.rioscristobal_ce07.CustomReditObject;
import com.example.cristobal.rioscristobal_ce07.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static com.example.cristobal.rioscristobal_ce07.StringsFile.FOLDER_NAME;

public class PostListFragment extends ListFragment {
    private final ArrayList<CustomReditObject> croList = new ArrayList<>();

    public static PostListFragment newInstance() {

        Bundle args = new Bundle();

        PostListFragment fragment = new PostListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.posts_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPosts();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            setupCustomAdapter();
        }
    }
    private void setupCustomAdapter() {
        CustomAdapter aa = new CustomAdapter(getActivity(), croList);
        ListView listView = getView().findViewById(android.R.id.list);
        listView.setAdapter(aa);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.reddit.com"+croList.get(position).getLinkString()));
        startActivity(i);
    }

    private void getPosts(){

        File protectedStorage = getActivity().getExternalFilesDir(FOLDER_NAME);
        if (protectedStorage != null) {
            Log.e("Get Post: ",""+protectedStorage.list().length);
            for (String f : protectedStorage.list()) {
                try{
                    FileInputStream fis = new FileInputStream(protectedStorage + "/" + f);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    croList.add( (CustomReditObject) ois.readObject() );
                    Log.e("Get Post/ added ",f);
                    ois.close();
                    fis.close();
                }catch (IOException|ClassNotFoundException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
