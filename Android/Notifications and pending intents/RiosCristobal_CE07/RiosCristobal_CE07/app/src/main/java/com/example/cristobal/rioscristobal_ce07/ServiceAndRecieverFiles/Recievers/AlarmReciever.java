package com.example.cristobal.rioscristobal_ce07.ServiceAndRecieverFiles.Recievers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce07.ServiceAndRecieverFiles.RedditService;

public class AlarmReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //this is our reciever for the on Boot, the manifest must have the uses permission for boot,
        // and our service, once it starts this will recieve the on boot and start sending alerts
        //create our manager
        Log.e("Alarm!!! Recieved","Alarm Reciever");
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (mgr != null) {

            Intent serviceIntent = new Intent(context, RedditService.class);
            PendingIntent pi = PendingIntent.getService(context, 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 60000, 60000, pi);

        }

    }
}
