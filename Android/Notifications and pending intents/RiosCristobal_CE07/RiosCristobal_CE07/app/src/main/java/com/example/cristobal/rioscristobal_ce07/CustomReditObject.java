package com.example.cristobal.rioscristobal_ce07;

import java.io.Serializable;

public class CustomReditObject implements Serializable {
    private final String title;
    private final String text;
    private final String thumbnailString;
    private final String link;


    public CustomReditObject(String _title, String _text, String _thumbnail, String _link) {
        this.title = _title;
        this.text = _text;
        this.thumbnailString = _thumbnail;
        this.link = _link;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;

    }

    public String getThumbnail() {
        return thumbnailString;
    }

    public String getLinkString() {
        return link;
    }

}
