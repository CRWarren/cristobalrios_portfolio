package com.example.cristobal.rioscristobal_ce07.ServiceAndRecieverFiles;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce07.CustomReditObject;
import com.example.cristobal.rioscristobal_ce07.StringsFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import static com.example.cristobal.rioscristobal_ce07.StringsFile.FOLDER_NAME;

public class SavePost extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @ param name Used to name the worker thread, important only for debugging.
     */
    public SavePost() {
        super(StringsFile.SAVE_REDIT_SERVICE);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        CustomReditObject cro = (CustomReditObject) intent.getExtras().getSerializable(StringsFile.SAVE_REDIT);
        savePost(cro);
    }

    private void savePost(CustomReditObject cor){
        File protectedStorage = getExternalFilesDir(FOLDER_NAME);

        File postFile = new File(protectedStorage,cor.getTitle());
        try{
            postFile.createNewFile();
            Log.e("CreatingFile", "------------------");
            FileOutputStream fos = new FileOutputStream(postFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Log.e("Writing", "------------------");
            oos.writeObject(cor);
            Log.e("Write", "------------------");
            oos.close();
            fos.close();
            Intent i = new Intent(StringsFile.SAVE_BROADCAST);
            sendBroadcast(i);
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
