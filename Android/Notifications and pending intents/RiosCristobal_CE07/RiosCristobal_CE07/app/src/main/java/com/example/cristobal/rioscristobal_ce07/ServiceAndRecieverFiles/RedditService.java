package com.example.cristobal.rioscristobal_ce07.ServiceAndRecieverFiles;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce07.CustomReditObject;
import com.example.cristobal.rioscristobal_ce07.R;
import com.example.cristobal.rioscristobal_ce07.StringsFile;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class RedditService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @ param name Used to name the worker thread, important only for debugging.
     */
    public RedditService() {
        super(StringsFile.GET_POST);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if(validConnection()){
            ArrayList<CustomReditObject> croList = parseResult(downloadReddit());
            if(croList.size()>0){
                Random rand = new Random();
                int randomPost = rand.nextInt(croList.size()-1);
                Log.e("onHandleIntentReddit",croList.get(randomPost).getTitle());
                onExpandedNotification(croList.get(randomPost));
            }
        }
    }

    private void onExpandedNotification(CustomReditObject randomPost) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, StringsFile.CHANNEL_ID);
        //required
        builder.setContentTitle(randomPost.getTitle());
        builder.setContentText(randomPost.getText());
        builder.setSmallIcon(R.drawable.notification_icon);
        try {
            InputStream is = (InputStream) new URL(randomPost.getThumbnail()).getContent();
            builder.setLargeIcon(BitmapFactory.decodeStream(is));

        } catch (Exception e) {
           Log.e("Getting Image","Failed");
        }
        //optional
        builder.setOngoing(false);
        //BIGSTYLE...
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.setBigContentTitle(randomPost.getTitle());
        style.setSummaryText(randomPost.getText());

        //style.bigText("this is a really really long\"Big Text\"value...");
        builder.setStyle(style);
        Log.e(randomPost.getLinkString(),"this is the link");
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.reddit.com"+randomPost.getLinkString()));

        PendingIntent openPostPI = PendingIntent.getActivity(this, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(openPostPI);

        Intent saveI = new Intent(this, SavePost.class);
        saveI.putExtra(StringsFile.SAVE_REDIT, randomPost);
        PendingIntent savePI = PendingIntent.getService(this, 2, saveI, PendingIntent.FLAG_UPDATE_CURRENT);


        builder.addAction(android.R.drawable.ic_menu_save, "Save", savePI);

        NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(mgr!=null){
            mgr.notify(StringsFile.EXPANDED_NOTIFICATION,builder.build());
        }
    }

    private String downloadReddit(){
        String result = "";

            URL url;
            HttpURLConnection connection = null;
            InputStream inStream = null;

            try {
                url = new URL(StringsFile.REDDIT_URL);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
            } catch(Exception e) {
                e.printStackTrace();
            }

            try {
                inStream = connection.getInputStream();
                result = IOUtils.toString(inStream, "UTF-8");
            } catch(Exception e) {
                e.printStackTrace();
            }

            finally {

                if (connection != null) {
                    if (inStream != null) {
                        try {
                            // close stream if open
                            inStream.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // always disconnect if there is a connection
                    connection.disconnect();
                }
            }
            return result;
    }

    private ArrayList<CustomReditObject> parseResult(String result){
        ArrayList<CustomReditObject> croList = new ArrayList<>();
        try {

            JSONObject outerObj = new JSONObject(result);
            JSONObject obj = outerObj.getJSONObject("data");
            JSONArray arrayOBJ = obj.getJSONArray("children");

            for (int i = 0; i < arrayOBJ.length() ; i++) {
                JSONObject postObj = arrayOBJ.getJSONObject(i);
                JSONObject dataObject = postObj.getJSONObject("data");
                String title = dataObject.getString("title");
                String text = dataObject.getString("selftext");
                String image = dataObject.getString("thumbnail");
                String link = dataObject.getString("permalink");
                CustomReditObject cro = new CustomReditObject(title,text, image,link);
                croList.add(cro);

            }
        } catch (JSONException e){
            e.printStackTrace();

        }
       return croList;
    }

    private boolean validConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }

}
