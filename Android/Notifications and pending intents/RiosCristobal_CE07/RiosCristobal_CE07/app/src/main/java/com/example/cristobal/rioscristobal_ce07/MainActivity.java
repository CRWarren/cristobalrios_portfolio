package com.example.cristobal.rioscristobal_ce07;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.cristobal.rioscristobal_ce07.Fragment.PostListFragment;
import com.example.cristobal.rioscristobal_ce07.ServiceAndRecieverFiles.RedditService;

public class MainActivity extends AppCompatActivity {

    private UpdateReciever mReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //since we had to create an update for the fragment, reuse it
        updateFragment();
        startAlarm();
        IntentFilter filter = new IntentFilter(StringsFile.SAVE_BROADCAST);
        mReceiver = new UpdateReciever();
        registerReceiver(mReceiver,filter);
    }

    private void startAlarm(){
        //works just like notification Manager, just change notification for alarm
        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if(mgr!=null){
            Intent i = new Intent(this, RedditService.class);
            PendingIntent pI = PendingIntent.getService(this, 0, i , PendingIntent.FLAG_UPDATE_CURRENT);
            mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+6000,60000,pI);
            Log.e("Alarm!!!","MainActivity");
        }
    }

    private void updateFragment(){
        getSupportFragmentManager().beginTransaction().replace(R.id.posts_container, PostListFragment.newInstance()).commit();
    }

    private class UpdateReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
           if(intent.getAction() == StringsFile.SAVE_BROADCAST){
               //dont use the support fragment manager here cause it will crash!!!!!
               updateFragment();

           }
        }
    }
}

