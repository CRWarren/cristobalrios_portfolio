package com.example.cristobal.rioscristobal_ce07;

public class StringsFile {
    public static final String REDDIT_URL = "https://www.reddit.com/r/iphonex/hot.json";
    public static final String GET_POST = "com.example.cristobal.rioscristobal_ce07.ServiceFiles";
    public static final String CHANNEL_ID = "com.example.cristobal.rioscristobal_ce07_CHANNEL_ID";
    public static final int EXPANDED_NOTIFICATION = 0x01002;
    public static final String SAVE_REDIT = "com.example.cristobal.rioscristobal_ce07.SAVE_REDDIT";
    public static final String SAVE_REDIT_SERVICE = "com.example.cristobal.rioscristobal_ce07.SAVEPOST";
    public static final String FOLDER_NAME = "posts";
    public static final String SAVE_BROADCAST = "com.example.cristobal.rioscristobal_ce07.save";
}
