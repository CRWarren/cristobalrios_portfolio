package com.example.cristobal.rioscristobal_ce07;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.sephiroth.android.library.picasso.Picasso;

public class CustomAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<CustomReditObject> posts;

    public CustomAdapter(Context _context, ArrayList<CustomReditObject> _posts){
        _Context = _context;
        posts = _posts;
    }

    @Override
    public int getCount() {
        if(posts != null) {
            return posts.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(posts != null) {
            return posts.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.cell_layout, parent, false);
            vh =  new ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ViewHolder)convertView.getTag();
        }

        if(!posts.get(position).getThumbnail().equals("self")){
            Log.e("Image: ",posts.get(position).getThumbnail());
            Picasso.with(_Context).load(posts.get(position).getThumbnail()).into(vh.imageIV);
        }

        vh.title.setText(posts.get(position).getTitle());
        vh.text.setText(posts.get(position).getText());
        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{
        final ImageView imageIV;
        final TextView title;
        final TextView text;
        ViewHolder(View _layout){
            imageIV = _layout.findViewById(R.id.placeholder_img);
            title = _layout.findViewById(R.id.title);
            text = _layout.findViewById(R.id.text);
        }

    }
}
