package com.example.codingchallenge_geniusplaza.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.example.codingchallenge_geniusplaza.Classes.User;
import com.example.codingchallenge_geniusplaza.R;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {
    private static final long Base_ID = 0x01001;
    private final Context _Context;
    private final ArrayList<User> users;

    public ListAdapter(Context _context, ArrayList<User> _users){
        _Context = _context;
        users = _users;
    }

    @Override
    public int getCount() {
        if(users !=null){
            return users.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(users != null && (position >= 0||position<users.size())) {
            return users.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return Base_ID+position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ListAdapter.ViewHolder vh;
        if(convertView == null){
            convertView = LayoutInflater.from(_Context).inflate(R.layout.user_cell_layout, parent, false);
            vh =  new ListAdapter.ViewHolder(convertView);
            convertView.setTag(vh);
        }else{
            vh =(ListAdapter.ViewHolder)convertView.getTag();
        }

        vh.Name.setText(users.get(position).full_name());
        Picasso.with(_Context).load(users.get(position).getImage_url()).into(vh.iv);

        return convertView;
    }

    // Optimize with view holder!
    static class ViewHolder{


        final TextView Name;
        final ImageView iv;

        ViewHolder(View _layout){
            Name = _layout.findViewById(R.id.nameTextView);
            iv = _layout.findViewById(R.id.userImage);

        }
    }
}
