package com.example.codingchallenge_geniusplaza.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.codingchallenge_geniusplaza.Adapters.ListAdapter;
import com.example.codingchallenge_geniusplaza.AsyncTasks.DownloadAsyncTask;
import com.example.codingchallenge_geniusplaza.Classes.User;
import com.example.codingchallenge_geniusplaza.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListViewFragment extends Fragment implements DownloadAsyncTask.CompletionObj {

    private final ArrayList<User> _Users = new ArrayList<>();
    private GridView _gridView;


    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void create(int id);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }

    public static ListViewFragment newInstance() {

        Bundle args = new Bundle();
        ListViewFragment fragment = new ListViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _gridView = getActivity().findViewById(R.id.users_grid);
        setHasOptionsMenu(true);
        webConnection();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.addItem){
           mFinishedInterface.create(_Users.size());

        }

        return super.onOptionsItemSelected(item);
    }
    private void webConnection() {

        if (!validConnection()){
            Toast nointernet = Toast.makeText(getActivity(),"No Internet Connection",Toast.LENGTH_SHORT);
            nointernet.show();
            return;
        }
        final String webAddress = "https://reqres.in/api/users";

        DownloadAsyncTask downloadTask = new DownloadAsyncTask( this);

        downloadTask.execute(webAddress);


    }

    private boolean validConnection(){
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            if(networkInfo.isAvailable()){
                return true;
            }else {
                Toast noInternet = Toast.makeText(getActivity(),"No internet Access", Toast.LENGTH_SHORT);
                noInternet.show();
                return false;
            }
        }
        return false;
    }
    private void setupCustomBaseAdapterView() {
        ListAdapter aa = new ListAdapter(getActivity(), _Users);
        _gridView.setAdapter(aa);
    }

    private void parseData(String data){

        try {
            JSONObject outerObj = new JSONObject(data);
            JSONArray obj = outerObj.getJSONArray("data");

            for (int i = 0; i < obj.length(); i++) {
               JSONObject temp = obj.getJSONObject(i);
               int id = temp.getInt("id");
               String first = temp.getString("first_name");
               String last = temp.getString("last_name");
               String avatar = temp.getString("avatar");
               _Users.add(new User(first,last,avatar,id));
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        setupCustomBaseAdapterView();

    }


    @Override
    public void updateInfo(String downloaded) {
        parseData(downloaded);
    }
}
