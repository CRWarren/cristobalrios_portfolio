package com.example.codingchallenge_geniusplaza.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.codingchallenge_geniusplaza.Classes.User;
import com.example.codingchallenge_geniusplaza.R;

public class NewUserFragment extends Fragment {

    TextInputLayout firstName;
    TextInputLayout lastName;
    TextInputLayout urlString;
    Button save;
    static int _id = 0;


    private CompletionObj mFinishedInterface;
    public interface CompletionObj {
        void save(User user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CompletionObj){
            mFinishedInterface = (CompletionObj) context;
        }
    }
    public static NewUserFragment newInstance(int id) {

        Bundle args = new Bundle();
        _id = id;
        NewUserFragment fragment = new NewUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_user_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        save = getActivity().findViewById(R.id.SaveBttn);
        save.setOnClickListener(saveClick);
        firstName = getActivity().findViewById(R.id.input_first);
        lastName = getActivity().findViewById(R.id.input_last);
        urlString = getActivity().findViewById(R.id.input_url);
    }

    View.OnClickListener saveClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(validate()){
                mFinishedInterface.save(new User(firstName.getEditText().getText().toString(),lastName.getEditText().getText().toString(),urlString.getEditText().getText().toString(),_id+1));
            }
        }
    };

    private Boolean validate(){

        if(firstName.getEditText().getText().toString().trim().isEmpty()){
            alert("First Name");
            return false;
        }else if(lastName.getEditText().getText().toString().trim().isEmpty()){
            alert("Last Name");
            return false;
        }else if(urlString.getEditText().getText().toString().trim().isEmpty()){
            alert("Image Url");
            return false;
        }

        return true;
    }
    private void alert(String errorTxt){
        new AlertDialog.Builder(getActivity()).setTitle("Error, Missing Information.")
                .setMessage("Please don't leave "+errorTxt+ "Empty")


                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        dialog.dismiss();
                    }
                });
    }




}
