package com.example.codingchallenge_geniusplaza;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.codingchallenge_geniusplaza.AsyncTasks.DownloadAsyncTask;
import com.example.codingchallenge_geniusplaza.AsyncTasks.PostAsyncTask;
import com.example.codingchallenge_geniusplaza.Classes.User;
import com.example.codingchallenge_geniusplaza.Fragments.ListViewFragment;
import com.example.codingchallenge_geniusplaza.Fragments.NewUserFragment;

public class MainActivity extends AppCompatActivity implements NewUserFragment.CompletionObj, PostAsyncTask.CompletionObj, ListViewFragment.CompletionObj {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, ListViewFragment.newInstance()).commit();
    }

    @Override
    public void Completition(String downloaded) {
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, ListViewFragment.newInstance()).commit();

    }

    @Override
    public void save(User user) {
        webConnection(user);
    }

    private void webConnection(User _user) {

        if (!validConnection()){
            Toast nointernet = Toast.makeText(this,"No Internet Connection",Toast.LENGTH_SHORT);
            nointernet.show();
            return;
        }
        final String webAddress = "https://reqres.in/api/users";

        PostAsyncTask postAsyncTask = new PostAsyncTask( this);

        postAsyncTask.execute(String.valueOf(_user.returnJSON()),webAddress);


    }

    private boolean validConnection(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            if(networkInfo.isAvailable()){
                return true;
            }else {
                Toast noInternet = Toast.makeText(this,"No internet Access", Toast.LENGTH_SHORT);
                noInternet.show();
                return false;
            }
        }
        return false;
    }

    @Override
    public void create(int id) {
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, NewUserFragment.newInstance(id)).commit();
    }
}
