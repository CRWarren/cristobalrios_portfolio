package com.example.codingchallenge_geniusplaza.Classes;

import org.json.JSONException;
import org.json.JSONObject;

public class User {

    private String first_name;
    private String last_name;
    private String image_url;
    private int id;

    public User(String first_name, String last_name, String image_url, int id) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.image_url = image_url;
    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public  String full_name(){
        return first_name+ " " + last_name;
    }

    public JSONObject returnJSON(){
        JSONObject post_dict = new JSONObject();

        try {
            post_dict.put("id" , id);
            post_dict.put("first_name" , first_name);
            post_dict.put("last_name", last_name);
            post_dict.put("avatar", image_url);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return post_dict;
    }
}
