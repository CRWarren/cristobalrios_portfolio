package com.example.codingchallenge_geniusplaza.AsyncTasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PostAsyncTask extends AsyncTask<String,String, String> {

    String TAG = "com.example.codingchallenge_geniusplaza";

    final private CompletionObj mFinishedInterface;

    public interface CompletionObj {
        void Completition(String downloaded);
        //void startProgressBar();
    }

    public PostAsyncTask(CompletionObj _finished) {

        mFinishedInterface = _finished;

    }

    @Override
    protected void onPreExecute() {


    }


    @Override
    protected String doInBackground(String... strings) {

        try {

            URL url = new URL(strings[1]); // here is your URL path


            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(60000 /* milliseconds */);
            conn.setConnectTimeout(60000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(strings[0]);

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_CREATED) {

                BufferedReader in=new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                StringBuffer sb = new StringBuffer("");
                String line="";

                while((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();

            }
            else {
                return new String("false : "+responseCode);
            }
        }
        catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }

    }

    @Override
    protected void onPostExecute(String s) {
        if(mFinishedInterface != null){
            super.onPostExecute(s);
            Log.e(TAG, s);
            mFinishedInterface.Completition(s);
        }
    }
}
