//
//  User.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import Foundation

class User{
    let id: Int
    let name: String
    let email: String
    let username: String

    init(id: Int, name: String, email: String, username: String) {
        self.id = id
        self.name = name
        self.email = email
        self.username = username
    }

}
