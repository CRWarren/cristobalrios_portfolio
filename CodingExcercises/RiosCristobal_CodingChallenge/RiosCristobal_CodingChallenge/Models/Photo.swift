//
//  Photo.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import Foundation


class Photo{
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String

    init(albumId: Int, id: Int, title: String, url: String, thumbnailUrl: String) {
        self.albumId = albumId
        self.id = id
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
}
