//
//  ImageViewExtension.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import Foundation
import Kingfisher
extension UIImageView
{
    func setImage(urlString:String?,placeHolder:UIImage?,cornerRadius:CGFloat)
    {
        guard let urlString = urlString
        else
        {
            print("Image url is nil")
            return;
        }

        let url = URL(string: urlString)
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: placeHolder,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success( _):
             //   print("Task done for: \(value.source.url?.absoluteString ?? "")")
                break;
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}
