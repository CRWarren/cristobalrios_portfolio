//
//  NotificationNames.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import Foundation
extension Notification.Name {

    static let AlbumsNotification = Notification.Name("AlbumsNotification")
    static let usersNotification = Notification.Name("usersNotification")
    static let PhotosNotification = Notification.Name("PhotosNotification")


}
