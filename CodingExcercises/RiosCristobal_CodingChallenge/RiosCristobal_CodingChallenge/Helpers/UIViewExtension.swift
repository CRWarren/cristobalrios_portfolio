//
//  UIViewExtension.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/30/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    func showIndicator(message:String?) {
        DispatchQueue.main.async {
            self.startAnimating(ActivityIndicatorService.shared.size, message: message, type: ActivityIndicatorService.shared.indicatorType, color: .white, backgroundColor: .clear)
        }
    }

    func showDarkIndicator(message:String?) {
        DispatchQueue.main.async {
            self.startAnimating(ActivityIndicatorService.shared.size, message: message, type: ActivityIndicatorService.shared.indicatorType, color: .darkGray)
        }
    }

    @objc func hideIndicator() {
        DispatchQueue.main.async {
            self.stopAnimating(nil)
        }
    }
}
