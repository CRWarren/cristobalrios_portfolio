//
//  DataStore.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class DataStore {

    // MARK: - Properties
    let albumsAPI = "https://jsonplaceholder.typicode.com/albums"
    let usersAPI = "https://jsonplaceholder.typicode.com/users"

    static let shared = DataStore()
    var users: [User] = []
    var albums: [Album] = []
    var currentAlbum: Album?
    var photos: [Photo] = []
    var selectedPhoto: Photo?

    func retrieveAlbums(){
        guard let albumsURl = URL(string: albumsAPI) else {
            print("Error - Failed to retireve albums")
            return
        }
        AF.request(albumsURl).responseJSON { response in
            guard let data = response.data else {
                print("Error - failed to get data(albums)")
                return
            }
            self.albums = []
            do{
                let json = try JSON(data: data).arrayValue
                for object in json{
                    let userId = object["userId"].intValue
                    let id = object["id"].intValue
                    let title = object["title"].stringValue
                    self.albums.append(Album(userId: userId, id: id, title: title))
                }
                self.albums.sort(by: {$0.id < $1.id})
                NotificationCenter.default.post(name: .AlbumsNotification, object: nil)

                print(self.albums.count)
            }catch{
                print(error.localizedDescription)
            }


        }
    }
     func retrieveCurrentAlbum(){
        guard let albumsURl = URL(string: "\(albumsAPI)/\(currentAlbum?.id ?? 0)") else {
            print("Error - Failed to retireve albums")
            return
        }
        AF.request(albumsURl).responseJSON { response in
            guard let data = response.data else {
                print("Error - failed to get data(albums)")
                return
            }
            do{
                let json = try JSON(data: data)
                let userId = json["userId"].intValue
                let id = json["id"].intValue
                let title = json["title"].stringValue
                self.albums.removeAll(where: {$0.id == self.currentAlbum?.id})
                self.currentAlbum = Album(userId: userId, id: id, title: title)
                self.albums.append(Album(userId: userId, id: id, title: title))
                self.albums.sort(by: {$0.id < $1.id})
                NotificationCenter.default.post(name: .AlbumsNotification, object: nil)

                print(self.albums.count)
            }catch{
                print(error.localizedDescription)
            }


        }
    }

    func retrieveUsers(){
        guard let usersURl = URL(string: usersAPI) else {
            print("Error - Failed to retireve users")
            return
        }
        AF.request(usersURl).responseJSON { response in
            guard let data = response.data else {
                print("Error - failed to get data(users)")
                return
            }
            do{
                let json = try JSON(data: data).arrayValue
                self.users = []
                for object in json{
                    let id = object["id"].intValue
                    let name = object["name"].stringValue
                    let username = object["username"].stringValue
                    let email = object["email"].stringValue
                    self.users.append(User(id: id, name: name, email: email, username: username))
                }
                print(self.users.count)
            }catch{
                print(error.localizedDescription)
            }
        }
    }

    func retrievePhotos(albumId: Int?){
        if albumId == nil {
            self.photos = []
            return}
        guard let photosURl = URL(string: "\(albumsAPI)/\(albumId)/photos") else {
                   print("Error - Failed to retireve photos")
                   return
               }
        AF.request(photosURl).responseJSON { response in
            guard let data = response.data else {
                print("Error - failed to get data(photos)")
                return
            }
            do{
                self.photos = []
                let json = try JSON(data: data).arrayValue
                for object in json{
                    let _albumId = object["albumId"].intValue
                    let id = object["id"].intValue
                    let title = object["title"].stringValue
                    let url = object["url"].stringValue
                    let thumbnailUrl = object["thumbnailUrl"].stringValue
                    print(200002, "\(albumId)  vs \(_albumId)")
                    if albumId == _albumId {
                        self.photos.append(Photo(albumId: _albumId, id: id, title: title, url: url, thumbnailUrl: thumbnailUrl))
                    }
                }
                NotificationCenter.default.post(name: .PhotosNotification, object: nil)
            }catch{
                print(error.localizedDescription)
            }
        }
    }
}


