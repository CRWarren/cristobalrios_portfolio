//
//  AlbumCell.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit

class AlbumCell: UITableViewCell{

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var userLabel: UILabel!

}
