//
//  PhotosViewController.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
class PhotosViewController: UIViewController{
    //MARK: IBOutlets
    @IBOutlet var headerView: UIView!
    @IBOutlet var photosCollectionView: UICollectionView!
    @IBOutlet var albumLabel: UILabel!
    @IBOutlet var usernameLable: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var headerHeightConstraint: NSLayoutConstraint!
    var refreshControl: UIRefreshControl!
    
    
    //MARK: Variables
    var minHeaderHeight: CGFloat = 0
    var maxHeaderHeight: CGFloat = 90
    var previousScrollOffset: CGFloat = 0
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        self.showIndicator(message: nil)
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .PhotosNotification, object: nil)
        self.photosCollectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.photosCollectionView.addSubview(refreshControl)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photosCollectionView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCell")
        DataStore.shared.photos = []
        
    }
    
    // MARK: Functions
    
    @objc func reloadData(){

        albumLabel.text = DataStore.shared.currentAlbum?.title
        usernameLable.text = DataStore.shared.users.first(where: {$0.id == DataStore.shared.currentAlbum?.userId})?.username
        fullNameLabel.text = DataStore.shared.users.first(where: {$0.id == DataStore.shared.currentAlbum?.userId})?.name
        emailLabel.text = DataStore.shared.users.first(where: {$0.id == DataStore.shared.currentAlbum?.userId})?.email
        self.hideIndicator()
        photosCollectionView.reloadData()
    }
    @objc func didPullToRefresh() {
        self.showIndicator(message: nil)
        DataStore.shared.retrieveCurrentAlbum()
        DataStore.shared.retrievePhotos(albumId: DataStore.shared.currentAlbum?.id ?? 0)
        
        // For End refrshing
        refreshControl?.endRefreshing()
        
    }
    
}

// MARK: CollectoinView Delegate, DataSource & FlowLayout

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataStore.shared.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  4
        let collectionViewSize = UIScreen.main.bounds.width - padding
        return CGSize(width: collectionViewSize/3, height: collectionViewSize/3)
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath)
            as! PhotoCell
        cell.photoIV.setImage(urlString: DataStore.shared.photos[indexPath.row].thumbnailUrl, placeHolder: UIImage() , cornerRadius: 0)
        
        return cell
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DataStore.shared.selectedPhoto = DataStore.shared.photos[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "photoDetails")
        controller.modalPresentationStyle = .popover
        self.present(controller, animated: true, completion: nil)
    }
}


// MARK: ScrollViewDelegate
extension PhotosViewController{

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = scrollView.contentOffset.y - self.previousScrollOffset
        let absoluteTop: CGFloat = 0
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
        if canAnimateHeader(scrollView) {
            // Calculate new header height
            var newHeight = self.headerHeightConstraint.constant
            if isScrollingDown {
                newHeight = max(self.minHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
            } else if isScrollingUp {
                if photosCollectionView.contentOffset.y <= 0 {
                    newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
                }
            } else {
                if photosCollectionView.contentOffset.y <= 0 {
                    newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
                }
            }
            // Header needs to animate
            if newHeight != self.headerHeightConstraint.constant {
                self.headerHeightConstraint.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(self.previousScrollOffset)
            }
            self.previousScrollOffset = scrollView.contentOffset.y
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewDidStopScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.scrollViewDidStopScrolling()
        }
    }
    
    func scrollViewDidStopScrolling() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)
        
        if self.headerHeightConstraint.constant > midPoint {
            updateHeader()
        } else {
            updateHeader()
        }
    }
    
    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate the size of the scrollView when header is collapsed
        let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight - minHeaderHeight
    }
    
    func setScrollPosition(_ position: CGFloat) {
        self.photosCollectionView.contentOffset = CGPoint(x: self.photosCollectionView.contentOffset.x, y: position)
    }
    
    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        let percentage = (openAmount / range)
        let inversePercentage = abs(percentage - 1.0)
        var scaleBy = inversePercentage
    }
}


