//
//  ViewController.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//
import UIKit
import Foundation

class AlbumsDashboardVC: UIViewController {
    //MARK: IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    //MARK: Variables
    var refreshControl: UIRefreshControl!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showIndicator(message: nil)
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.prefersLargeTitles = true
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .AlbumsNotification, object: nil)
        DataStore.shared.retrieveUsers()
        DataStore.shared.retrieveAlbums()
        navigationController?.view.backgroundColor = UIColor(red: 31/255, green: 33/255, blue: 36/255, alpha: 1)
        navigationController?.navigationBar.barStyle = .black
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AlbumCell", bundle: nil), forCellReuseIdentifier: "AlbumCell")
    }

    //MARK: Functions
    @objc func reloadData(){
        tableView.reloadData()
        self.hideIndicator()
    }
    @objc func didPullToRefresh() {
        self.showIndicator(message: nil)
        DataStore.shared.retrieveAlbums()
        // For End refrshing
        refreshControl?.endRefreshing()
        
    }
}


// MARK: Tableview Delegate and DataSource

extension AlbumsDashboardVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataStore.shared.albums.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell", for: indexPath)
            as! AlbumCell
        cell.titleLabel.text = DataStore.shared.albums[indexPath.row].title
        cell.userLabel.text = DataStore.shared.users.first(where:  { $0.id ==  DataStore.shared.albums[indexPath.row].userId })?.username ?? ""
        if indexPath.row % 2 == 0 {
            cell.contentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationFactory.makeMoveUpWithFadeandScaleUp(rowHeight: cell.frame.height, duration: 0.3, delayFactor: 0.0)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataStore.shared.currentAlbum = DataStore.shared.albums[indexPath.row]
        
        DataStore.shared.retrievePhotos(albumId: DataStore.shared.currentAlbum?.id)
        performSegue(withIdentifier: "toPhotos", sender: nil)
    }
    
}
