//
//  PhotoDetailsVC.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/29/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
class PhotoDetailsVC: UIViewController{
    //MARK: IBOutlets
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var photoNameLabel: UILabel!
    @IBOutlet var photoIV: UIImageView!
    @IBOutlet var albumLabel: UILabel!
    @IBOutlet var albumText: UILabel!
    @IBOutlet var closeBttn: UIButton!
    @IBOutlet var saveBttn: UIButton!


    //MARK: Variables
    var photo: Photo?
    var album: Album?
    var user: User?


    //MARK: LifeCycle
    override func viewDidLoad() {
        album = DataStore.shared.currentAlbum
        photo = DataStore.shared.selectedPhoto
        user = DataStore.shared.users.first(where: {$0.id == album?.userId ?? 0})
    }
    override func viewDidAppear(_ animated: Bool) {
        guard let photo = photo, let album = album, let user = user else{
            return
        }
        userLabel.text = user.username
        albumText.text = "Album:"
        photoNameLabel.text = photo.title
        albumLabel.text = album.title
        photoIV.setImage(urlString: photo.url, placeHolder: UIImage(), cornerRadius: 0)
        animateLabelInwithSpring(label: self.userLabel)
        animateLabelInwithSpring(label: self.albumText)
        animateLabelInwithSpring(label: self.albumLabel)
        animateLabelInwithSpring(label: self.photoNameLabel)
        animateImageViewWithSpring(IV: photoIV)
        animateButtonWithSpring(button: self.closeBttn)
        animateButtonWithSpring(button: self.saveBttn)

    }

    //MARK: Actions

    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveImage(_ sender: Any) {
        guard let image = photoIV.image else {return}
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        self.showToast(message : "Image Saved") 
    }

    //MARK: Animations
    func animateLabelInwithSpring(label: UILabel){
        label.alpha = 0
        UIView.animate(withDuration: 2, delay: 0.25, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            label.alpha = 1
            label.frame.origin.y += 150
        }, completion: nil)
    }
    func animateImageViewWithSpring(IV: UIImageView){
        IV.alpha = 0
        UIView.animate(withDuration: 2, delay: 0.25, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            IV.alpha = 1
            IV.frame.origin.y += 150
        }, completion: nil)
    }

    func animateButtonWithSpring(button: UIButton){
        button.alpha = 0
        UIView.animate(withDuration: 2, delay: 0.25, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            button.alpha = 1
            button.frame.origin.y += 150
        }, completion: nil)
    }



}
