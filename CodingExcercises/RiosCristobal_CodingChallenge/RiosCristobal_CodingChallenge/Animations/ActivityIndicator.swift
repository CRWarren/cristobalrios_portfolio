//
//  ActivityIndicator.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/30/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

// MARK: - Main UI Loader

class ActivityIndicatorService
{

    // MARK: - Properties
    static let shared = ActivityIndicatorService()
    let size = CGSize(width: 40, height: 40)
    let indicatorType:NVActivityIndicatorType = .pacman

    lazy var activityIndicatorView:NVActivityIndicatorView = {
        let screenBound = UIScreen.main.bounds
        let frame = CGRect(x: (screenBound.width - size.width)/2, y: (screenBound.height - size.height)/2, width: size.width, height: size.height)

        return NVActivityIndicatorView(frame: frame,type: indicatorType,color: .white)
    }()

    // MARK: - Show Loader
    func show(view:UIView)
    {
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

    }

    // MARK: - Hide Loader
    func hide(view:UIView)
    {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }

}
