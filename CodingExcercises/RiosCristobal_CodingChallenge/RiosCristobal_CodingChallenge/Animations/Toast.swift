//
//  Toast.swift
//  RiosCristobal_CodingChallenge
//
//  Created by cristobal rios on 1/30/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
extension UIViewController {

func showToast(message : String) {


    //create label
    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2-75, y: self.view.frame.size.height-130, width: 150, height: 30))

    //add background to label
    toastLabel.backgroundColor = UIColor.darkGray

    //set text color
    toastLabel.textColor = UIColor.white

    //align text
    toastLabel.textAlignment = .center;

    //set text font
    toastLabel.font = UIFont(name: "System", size: 12)

    //set message
    toastLabel.text = message

    //give corners to alpha
    toastLabel.layer.cornerRadius = 15;

    toastLabel.clipsToBounds  =  true


    //add label to current view
    self.view.addSubview(toastLabel)

    //animate lable to disssapear and once is completed remove
    UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }
