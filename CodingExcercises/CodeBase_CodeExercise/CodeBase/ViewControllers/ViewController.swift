//
//  ViewController.swift
//  simpsonsviewer
//
//  Created by cristobal rios on 2/12/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
   
    
    @IBOutlet weak var toggle: UISegmentedControl!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var Desctiption: UITextView!
    
    private let viewModel = APIDownload()
    private var urlString = ""
    private var selectedChar: Charater?
    
    let bundleIdentifier =  Bundle.main.bundleIdentifier
    var characters = [Charater]()
    var filteredArray = [Charater]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //nonno
        SearchBar.delegate = self
        if(bundleIdentifier == "com.CristobalRios.simpsonsviewer"){
            self.title = "Simpsons Character Viewer"
             urlString = "https://api.duckduckgo.com/?q=simpsons+characters&format=json"
        }else if(bundleIdentifier == "com.CristobalRios.wireviewer"){
            self.title = "The Wire Character Viewer"
            urlString = "https://api.duckduckgo.com/?q=the+wire+characters&format=json"
        }
        fetchData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.toggle.isHidden = true
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            self.toggle.isEnabled = true
            self.toggle.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cell_1", for: indexPath)
        
        cell.textLabel!.text = filteredArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        characterSelected(number: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_2", for: indexPath) as! CollectionViewCell
        cell.image.image = filteredArray[indexPath.row].image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        characterSelected(number: indexPath.row)
    }
    
    func fetchData(){

        //download the data and update UI
        
        viewModel.DownloadData(urlString: urlString) { [unowned self] (collections) in
            
            self.characters = collections
            self.filteredArray = self.characters
            self.tableview.reloadData()
    
            if(UIDevice.current.userInterfaceIdiom == .phone){
                self.SetUpCOllectionView()
            }
        }
    }
    
    func SetUpCOllectionView(){
        //get width and height for auto layout cell sizing
        let collectionWidth = view.frame.size.width * 0.43
        
        //change the collection views layout to flow and set the new witdths and heights
        let collectionLayout = CollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionLayout.itemSize = CGSize(width: collectionWidth, height: collectionWidth)
        CollectionView.reloadData()
    }

    func characterSelected(number: Int){
        self.SearchBar.resignFirstResponder()
         selectedChar = filteredArray[number]
        if(UIDevice.current.userInterfaceIdiom == .phone){
          
            performSegue(withIdentifier: "toDetail", sender: nil)
        }else{
       
          updateView()
        }
    }
    
    @IBAction func viewToggle(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            tableview.isHidden = false
            CollectionView.isHidden = true
        case 1:
            tableview.isHidden = true
            CollectionView.isHidden = false
        default:
            print("Ooops")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailViewController{
            detailVC.selectedChar = selectedChar
        }
    }
    
    func updateView(){
        viewContainer.isHidden = false
        imageView.image = selectedChar!.image
        Label.text = selectedChar!.name
        Desctiption.text = selectedChar!.text
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SearchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredArray = characters
        
        if searchText != "" {
            filteredArray = filteredArray.filter{ $0.name.lowercased().starts(with: searchText.lowercased())}
        }else{
            filteredArray = characters
        }
        self.tableview.reloadData()
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            CollectionView.reloadData()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.SearchBar.resignFirstResponder()
        self.SearchBar.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
         self.SearchBar.resignFirstResponder()
    }
}

