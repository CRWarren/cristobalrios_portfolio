//
//  DetailViewController.swift
//  simpsonsviewer
//
//  Created by cristobal rios on 2/12/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController{
    
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var Description: UITextView!
    
    @IBOutlet weak var name: UILabel!
    public var selectedChar: Charater?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //nonno
        if(selectedChar != nil){
            self.title = selectedChar!.name
            name.text = selectedChar!.name
            Description.text = selectedChar!.Description
            imageView.image = selectedChar?.image
        }
    }
}
