//
//  ParseJson.swift
//  simpsonsviewer
//
//  Created by cristobal rios on 2/12/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class ParseJSON{
    //parse our JSON data to usable format
        func GetData(from data: Data) -> [Charater] {
            //Array to store our data so we can return it.
            var collections = [Charater]()
            
            do{
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]{
                    guard let charactersArray = json["RelatedTopics"] as? [[String: Any]]
                        else{return collections}
                    
                    for jsonCharacter in charactersArray {
                        guard let text = jsonCharacter["Text"] as? String
                            else {return collections}
                        guard let Icon = jsonCharacter["Icon"] as? [String: Any]
                            else {return collections}
                       guard let stringUrl = Icon["URL"] as? String
                         else {return collections}
                        let _charcacter = Charater(text: text, imageString: stringUrl)
                        collections.append(_charcacter)
                    }
                }
            }catch{
                print(error.localizedDescription)
            }
            return collections
        }
}
