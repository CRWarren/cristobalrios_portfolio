//
//  CollectionViewCell.swift
//  simpsonsviewer
//
//  Created by cristobal rios on 2/12/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class CollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var image: UIImageView!
    
}
