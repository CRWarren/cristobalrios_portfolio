//
//  Character.swift
//  simpsonsviewer
//
//  Created by cristobal rios on 2/12/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class Charater{
    var text: String?
    var imageString: String?
    init(text: String?, imageString: String?) {
        self.text = text
        self.imageString = imageString
    }
   // var image: UIImage?
    
    
    
    
    var name: String {
        let name = text?.split(separator: "-")[0]
        return "\(String(describing: name!))"
    }
   
    
    var Description: String {
        let DescriptionArray = text?.split(separator: "-")[1]
        var Description = ""
        for text in DescriptionArray!{
            Description += "\(text)"
        }
        return Description
    }
    var image: UIImage{
        var image = UIImage(named: "Green")
        if(imageString!.count>0){
            do {
                let _url = URL(string: imageString!)
                let data = try Data(contentsOf: _url!)
                image = UIImage(data: data)
            }catch{
                return image!
            }
        }
        return image!
    }
}
