//
//  OneVC.swift
//  PaceControllerWithColorChange
//
//  Created by cristobal rios on 1/23/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit
class OneViewController: UIViewController {
    let _view = UIView()
    weak var containerVC: ContainerViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        _view.frame = CGRect(x: UIScreen.main.bounds.width/2 - 20, y: UIScreen.main.bounds.height/2 - 40, width: 40, height: 40)
        _view.backgroundColor = .fgMango
        self.view.addSubview(_view)
    }
}
