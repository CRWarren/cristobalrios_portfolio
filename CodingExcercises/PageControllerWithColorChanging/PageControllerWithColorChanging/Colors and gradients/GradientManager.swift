//
//  GradientManager.swift
//  PageControllerWithColorChanging
//
//  Created by cristobal rios on 1/23/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit

struct GradientConfig {
    
    let gameGradients: (UIColor, UIColor) = (UIColor.fgBlue, UIColor.fgDarkBlue)
    let leaderboardGradients: (UIColor, UIColor) = (UIColor.fgYellowOrange, UIColor.fgMango)
    let winnersGradients: (UIColor, UIColor) = (UIColor.fgWinnersLightGreen, UIColor.fgWinnersDarkGreen)
    let oneToTwoStartingReds: (CGFloat, CGFloat)
    let oneToTwoStartingGreens: (CGFloat, CGFloat)
    let oneToTwoStartingBlues: (CGFloat, CGFloat)
    let oneToTwoEndingReds: (CGFloat, CGFloat)
    let oneToTwoEndingGreens: (CGFloat, CGFloat)
    let oneToTwoEndingBlues: (CGFloat, CGFloat)
    let twoToThreeStartingReds: (CGFloat, CGFloat)
    let twoToThreeStartingGreens: (CGFloat, CGFloat)
    let twoToThreeStartingBlues: (CGFloat, CGFloat)
    let twoToThreeEndingReds: (CGFloat, CGFloat)
    let twoToThreeEndingGreens: (CGFloat, CGFloat)
    let twoToThreeEndingBlues: (CGFloat, CGFloat)
    
    init() {
        oneToTwoStartingReds = (gameGradients.0.redValue, leaderboardGradients.0.redValue)
        oneToTwoStartingGreens = (gameGradients.0.greenValue, leaderboardGradients.0.greenValue)
        oneToTwoStartingBlues = (gameGradients.0.blueValue, leaderboardGradients.0.blueValue)
        oneToTwoEndingReds = (gameGradients.1.redValue, leaderboardGradients.1.redValue)
        oneToTwoEndingGreens = (gameGradients.1.greenValue, leaderboardGradients.1.greenValue)
        oneToTwoEndingBlues = (gameGradients.1.blueValue, leaderboardGradients.1.blueValue)
        twoToThreeStartingReds = (leaderboardGradients.0.redValue, winnersGradients.0.redValue)
        twoToThreeStartingGreens = (leaderboardGradients.0.greenValue, winnersGradients.0.greenValue)
        twoToThreeStartingBlues = (leaderboardGradients.0.blueValue, winnersGradients.0.blueValue)
        twoToThreeEndingReds = (leaderboardGradients.1.redValue, winnersGradients.1.redValue)
        twoToThreeEndingGreens = (leaderboardGradients.1.greenValue, winnersGradients.1.greenValue)
        twoToThreeEndingBlues = (leaderboardGradients.1.blueValue, winnersGradients.1.blueValue)
    }
    
}

struct GradientManager {
    
    static func applyGradient(to view: UIView, startingColor: UIColor, endingColor: UIColor, verticle: Bool) {
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = frame
        gradient.bounds = frame
        gradient.colors = [startingColor.cgColor, endingColor.cgColor]
        if verticle {
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 0, y: 1)
        } else {
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 1)
        }
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    
    static func applyGradient(to view: UIView, startColor: UIColor, endColor: UIColor, verticle: Bool) -> CAGradientLayer {
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = frame
        gradient.bounds = frame
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        if verticle {
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 0, y: 1)
        } else {
            gradient.startPoint = CGPoint(x: 0, y: 1)
            gradient.endPoint = CGPoint(x: 1, y: 1)
        }
        view.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    
}
