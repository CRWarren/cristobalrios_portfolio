//
//  Colors.swift
//  PaceControllerWithColorChange
//
//  Created by cristobal rios on 1/23/20.
//  Copyright © 2020 Cristobal Rios. All rights reserved.
//

import UIKit

extension UIColor {
    
    var redValue: CGFloat{ return CIColor(color: self).red }
    var greenValue: CGFloat{ return CIColor(color: self).green }
    var blueValue: CGFloat{ return CIColor(color: self).blue }
    var alphaValue: CGFloat{ return CIColor(color: self).alpha }
    
    
    static var fgGreen: UIColor {
        return UIColor(red: 35/255, green: 202/255, blue: 91/255, alpha: 1)
    }
    
    static var fgPearGreen: UIColor {
        return UIColor(red:0.73, green:0.97, blue:0.34, alpha:1.0)
    }
    
    static var fgWinnersLightGreen: UIColor {
        return UIColor(red: 8/255, green: 204/255, blue: 50/255, alpha: 1)
    }
    
    static var fgWinnersDarkGreen: UIColor {
        return UIColor(red: 0/255, green: 169/255, blue: 62/255, alpha: 1)
    }
    
    static var fgDarkGreen: UIColor {
        return UIColor(red: 0/255, green: 163/255, blue: 66/255, alpha: 1)
    }
    
    static var fgLimeGreen: UIColor {
        return UIColor(red: 66/255, green: 238/255, blue: 68/255, alpha: 1)
    }
    
    static var fgDarkBlue: UIColor {
        return UIColor(red: 21/255, green: 115/255, blue: 251/255, alpha: 1)
    }
    
    static var fgBlue: UIColor {
        return UIColor(red: 30/255, green: 174/255, blue: 253/255, alpha: 1)
    }
    
    static var fgPaleBlue: UIColor {
        return UIColor(red: 83/255, green: 171/255, blue: 253/255, alpha: 1)
    }
    
    static var fgOrange: UIColor {
//        return UIColor(red: 255/255, green: 163/255, blue: 50/255, alpha: 1)
        return UIColor(red:1.00, green:0.58, blue:0.02, alpha:1.0)
    }
    
    static var fgLightOrange: UIColor {
        return UIColor(red: 254/255, green: 186/255, blue: 42/255, alpha: 1)
    }
    
    static var fgYellowOrange: UIColor {
        return UIColor(red:1.00, green:0.73, blue:0.00, alpha:1.0)
    }
    
    static var fgMango: UIColor {
        return UIColor(red:1.00, green:0.59, blue:0.13, alpha:1.0)
    }
    
    static var fgGold: UIColor {
        return UIColor(red: 253/255, green: 214/255, blue: 50/255, alpha: 1)
    }
    
    static var fgCardDarkBlue: UIColor {
        return UIColor(red: 68/255, green: 147/255, blue: 246/255, alpha: 1)
    }
    
    static var fgCardBlue: UIColor {
        return UIColor(red: 127/255, green: 179/255, blue: 249/255, alpha: 1)
    }
    
    static var fgTransparentBlack: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    
    static var nextButtonGray: UIColor {
        return UIColor(red: 94/255, green: 92/255, blue: 116/255, alpha: 1)
    }
    
    static var fgRed: UIColor {
        return UIColor(red: 251/255, green: 0/255, blue: 86/255, alpha: 1)
    }
    
    static var fgPendingGray: UIColor {
        return UIColor(red: 119/255, green: 144/255, blue: 164/255, alpha: 1)
    }
    
    static var fgLostBlue: UIColor {
        return UIColor(red: 0/255, green: 43/255, blue: 72/255, alpha: 1)
    }
    
    static var fgPendingLighGray: UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1)
    }
    
    static var fgWonGold: UIColor {
        return UIColor(red: 254/255, green: 195/255, blue: 0/255, alpha: 1)
    }
    
    static var fgAlertGold: UIColor {
        return UIColor(red: 255/255, green: 196/255, blue: 2/255, alpha: 1)
    }
    
    static var fgAliasGray: UIColor {
        return UIColor(red: 205/255, green: 202/255, blue: 225/255, alpha: 1)
    }
    
    static var fgDriveEndPendingGold: UIColor {
        return UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1)
    }
    
    static var fgDriveEndPendingTextGold: UIColor {
        return UIColor(red: 255/255, green: 226/255, blue: 178/255, alpha: 1)
    }
    
    static var fgDriveEndDarkGray: UIColor {
        return UIColor(red: 54/255, green: 54/255, blue: 54/255, alpha: 1)
    }
    
    static var fgNewGameLightOrange: UIColor {
        return UIColor(red: 250/255, green: 217/255, blue: 97/255, alpha: 1)
    }
    
    static var fgNewGameDarkOrange: UIColor {
        return UIColor(red: 247/255, green: 107/255, blue: 28/255, alpha: 1)
    }
    
    static var fgLeaderboardBoarder: UIColor {
        return UIColor(red: 29/255, green: 167/255, blue: 253/255, alpha: 1)
    }
    
    static var fgNavy: UIColor {
        return UIColor(red:0.00, green:0.17, blue:0.28, alpha:1.0)
    }
    
    static var blueberry: UIColor {
        return UIColor(red: 73.0 / 255.0, green: 65.0 / 255.0, blue: 158.0 / 255.0, alpha: 1.0)
    }
    
    static var twillightBlue: UIColor {
        return UIColor(red: 14.0 / 255.0, green: 33.0 / 255.0, blue: 112.0 / 255.0, alpha: 1.0)
    }
    
    static var lightMustard: UIColor {
        return UIColor(red: 250.0 / 255.0, green: 217.0 / 255.0, blue: 97.0 / 255.0, alpha: 1.0)
    }
    
    static var orange: UIColor {
        return UIColor(red: 247.0 / 255.0, green: 107.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }

}

