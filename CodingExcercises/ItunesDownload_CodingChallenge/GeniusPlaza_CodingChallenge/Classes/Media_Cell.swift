//
//  Media_Cell.swift
//  GeniusPlaza_CodingChallenge
//
//  Created by cristobal rios on 3/7/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit
class Media_Cell: UITableViewCell {
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var ArtistLbl: UILabel!
    @IBOutlet weak var MediaLbl: UILabel!
    @IBOutlet weak var MediaIMage: UIImageView!
    
    
}
