//
//  ParseJson.swift
//  GeniusPlaza_CodingChallenge
//
//  Created by cristobal rios on 3/7/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class ParseJson{
    //parse our JSON data to usable format
    func GetData(from data: Data) -> [MediaObject] {
        //Array to store our data so we can return it.
        var collections = [MediaObject]()
        
        do{
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]{

                if let feed = json["feed"] as? [String: Any]{
                    if let mediaArray = feed["results"] as? [[String: Any]]{
                        for media in mediaArray{
                            
                            guard let artist = media["artistName"] as? String
                                else {return collections}
                            guard let name = media["name"] as? String
                                else {return collections}
                            guard let type = media["kind"] as? String
                                else {return collections}
                            guard let imageString = media["artworkUrl100"] as? String
                                else {return collections}
                            let tempMedia = MediaObject(name: name, artist: artist, imageString: imageString, MediaType: type)
                            collections.append(tempMedia)
                
                        }
                    }
                }
                
                
            }
        }catch{
            print(error.localizedDescription)
        }
        return collections
    }
}
