//
//  MediaObject.swift
//  GeniusPlaza_CodingChallenge
//
//  Created by cristobal rios on 3/7/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class MediaObject{
    var name: String?
    var artist: String?
    var imageString: String?
    var MediaType: String?
    
    init(name: String?, artist: String?, imageString: String?, MediaType: String?) {
        self.name = name
        self.artist = artist
        self.imageString = imageString
        self.MediaType = MediaType
    }
    
    
    var image: UIImage{
        var image = UIImage(named: "Green")
        if(imageString!.count>0){
            do {
                let _url = URL(string: imageString!)
                let data = try Data(contentsOf: _url!)
                image = UIImage(data: data)
            }catch{
                return image!
            }
        }
        return image!
    }
}
