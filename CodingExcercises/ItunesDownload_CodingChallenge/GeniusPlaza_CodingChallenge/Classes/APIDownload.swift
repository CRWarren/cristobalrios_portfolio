//
//  APIDownload.swift
//  GeniusPlaza_CodingChallenge
//
//  Created by cristobal rios on 3/7/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class APIDownload {
    
    let PJ = ParseJson()
    // Type Alias that our completition will return.
    typealias DownloadDataCompletion = ([MediaObject]) -> Void
    
    // Download Data pending on Completition.
    func DownloadData(urlString: String, completion: @escaping DownloadDataCompletion) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        //this time we dont need to create a request url, so just pass the url.
        if let validURL = URL(string: urlString) {
            
            
            //create task
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                //check for error
                if let error = error {
                    print("data task failed \nerror: \(error.localizedDescription)")
                    return
                }
                
                //check the response status
                guard  let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let validData = data
                    else{return}
                
                //Do UI Stuff
                DispatchQueue.main.async {
                    
                    //this will get our data and send it to our View after completition.
                    completion(self.PJ.GetData(from: validData))
                }
                
            })
            task.resume()
        }
        
    }
    
    
    
}
