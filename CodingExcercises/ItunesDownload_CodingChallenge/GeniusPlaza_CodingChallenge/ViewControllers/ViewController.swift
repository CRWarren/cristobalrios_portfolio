//
//  ViewController.swift
//  GeniusPlaza_CodingChallenge
//
//  Created by cristobal rios on 3/7/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    private let DownloadApi = APIDownload()
    private var mediaCollection = [MediaObject]()
    private var urlString = "https://rss.itunes.apple.com/api/v1/us/apple-music/top-songs/all/25/explicit.json"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        fetchData()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaCollection.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_1", for: indexPath) as! Media_Cell
        
        cell.NameLbl.text = mediaCollection[indexPath.row].name
        cell.ArtistLbl.text = mediaCollection[indexPath.row].artist
        cell.MediaLbl.text = mediaCollection[indexPath.row].MediaType
        cell.MediaIMage.image = mediaCollection[indexPath.row].image
        
        return cell
    }
    
    func fetchData(){
        
        //download the data and update UI
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false

        DownloadApi.DownloadData(urlString: urlString) { [unowned self] (collections) in
            self.mediaCollection = collections
            if(self.mediaCollection.count != 0){
                self.title = self.mediaCollection[0].MediaType
                self.activityIndicator.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
    
    
    @IBAction func view(_ sender: Any) {
        let mediaTypeMenu = UIAlertController(title: "Media Type", message: "Select Media Type to display", preferredStyle: .actionSheet)
        
        let appAction = UIAlertAction(title: "Apps", style: .default) { (UIAlertAction) in
            self.urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/new-apps-we-love/all/25/explicit.json"
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.fetchData()
        }
        let mediaAction = UIAlertAction(title: "Songs", style: .default){ (UIAlertAction) in
            self.urlString = "https://rss.itunes.apple.com/api/v1/us/apple-music/top-songs/all/25/explicit.json"
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.fetchData()
        }
        let bookAction = UIAlertAction(title: "Books", style: .default){ (UIAlertAction) in
            self.urlString = "https://rss.itunes.apple.com/api/v1/us/books/top-free/all/25/explicit.json"
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.fetchData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        mediaTypeMenu.addAction(appAction)
        mediaTypeMenu.addAction(mediaAction)
        mediaTypeMenu.addAction(bookAction)
        mediaTypeMenu.addAction(cancelAction)
        
        self.present(mediaTypeMenu, animated: true)
    }
    
}

