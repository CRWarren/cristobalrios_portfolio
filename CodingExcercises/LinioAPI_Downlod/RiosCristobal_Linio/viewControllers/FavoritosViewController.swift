//
//  FavoritosViewController.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import UIKit

class FavoritosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //outlets
    @IBOutlet weak var todosFavsLbl: UILabel!
    @IBOutlet weak var CollectionView_collections: UICollectionView!
    @IBOutlet weak var CollectionView_Favoritos: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //variables
    private let viewModel = DataViewModel()
    private var _collections = [Collection]()
    private var _items = [ItemClass]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //add delegates for collection views
        CollectionView_collections.delegate = self
        CollectionView_collections.dataSource = self
        CollectionView_Favoritos.delegate = self
        CollectionView_Favoritos.dataSource = self
        
        //get width and height for auto layout cell sizing
        let collectionWidth = view.frame.size.width * 0.45
        let collectionHeight = view.frame.size.height * 0.23
        
        //change the collection views layout to flow and set the new witdths and heights
        let collectionLayout = CollectionView_collections.collectionViewLayout as! UICollectionViewFlowLayout
        collectionLayout.itemSize = CGSize(width: collectionWidth, height: collectionWidth)
        let collection_layout_Favoritos = CollectionView_Favoritos.collectionViewLayout as! UICollectionViewFlowLayout
        collection_layout_Favoritos.itemSize = CGSize(width: collectionWidth * 0.95, height: collectionHeight)
        
        //remove navigation bar under line
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        // Do any additional setup after loading the view, typically from a nib.
        fetchData()
    }
    
    //set the number of items for our collection views
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.CollectionView_collections {
            
            return _collections.count
        }
       
        return _items.count
    }
    
    
    //set up method for Cells in view collections
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.CollectionView_collections {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_1", for: indexPath) as! SectionViewCell
            if(_collections[indexPath.section].products!.count>=1){
                 cellA.mainImage.image = _collections[indexPath.row].products![0].image_!
                cellA.mainImage.layer.cornerRadius = 10
                cellA.mainImage.layer.masksToBounds = true
                if(_collections[indexPath.section].products!.count>=2){
                    cellA.support1.image = _collections[indexPath.row].products![1].image_!
                    cellA.support1.layer.cornerRadius = 10
                    cellA.support1.layer.masksToBounds = true
                    if(_collections[indexPath.section].products!.count>=3){
                        cellA.support2.image = _collections[indexPath.row].products![2].image_!
                         cellA.support2.layer.cornerRadius = 10
                        cellA.support2.layer.masksToBounds = true
                    }
                }
            }
            cellA.Title.text = _collections[indexPath.row].name
            cellA.Count.text = "\(_collections[indexPath.row].products!.count)"
            return cellA
        }
            
        else {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_2", for: indexPath) as! FavoritosViewCell
            cellB.MainImage.image = _items[indexPath.row].image_!
            cellB.MainImage.layer.cornerRadius = 10
            cellB.MainImage.layer.masksToBounds = true
            switch _items[indexPath.row].linioPlusLevel{
            case 1:
                cellB.Bage1.isHidden = false
            case 2:
                cellB.Bage1.image = UIImage(named: "ndIc30Plus48Square")
                cellB.Bage1.isHidden = false
            default:
                cellB.Bage1.isHidden = true
            }
            switch _items[indexPath.row].condition{
            case "new":
                cellB.Badge2.image = UIImage(named: "ndIc30NewSquare")
                cellB.Badge2.isHidden = false
            case "refurbished":
                cellB.Badge2.image = UIImage(named: "ndIc30RefurbishedSquare")
                cellB.Badge2.isHidden = false
            default:
                cellB.Badge2.isHidden = true
            }
            if(_items[indexPath.row].freeShipping!){
                cellB.Badge4.isHidden = false
            }
            if(_items[indexPath.row].imported!){
                cellB.Badge3.isHidden = false
            }
            cellB.layer.cornerRadius = 10
            cellB.layer.masksToBounds = true
            return cellB
        }
    }
    
    func fetchData(){
        //use our view model
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        //download the data and update UI
        viewModel.DownloadData { [unowned self] (collections) in
            self._collections.removeAll()
            self._items.removeAll()
            self._collections = collections
            for collection in self._collections{
                self._items.append(contentsOf: collection.products!)
            }
            self.todosFavsLbl.text = "Todos mis favoritos (\(self._items.count))"
            self.CollectionView_Favoritos.reloadData()
            self.CollectionView_collections.reloadData()
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
    
    
}

