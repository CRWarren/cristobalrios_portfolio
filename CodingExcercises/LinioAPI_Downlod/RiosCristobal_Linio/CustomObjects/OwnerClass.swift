//
//  OwnerClass.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class Owner {
    var name: String?
    var email: String?
    var linioId: String?
    
    init(name: String?, email: String?, linioId: String?) {
        self.name = name
        self.email = email
        self.linioId = linioId
    }
}
