//
//  CollectionClass.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation

class Collection {
    var id: Int?
    var name: String?
    var description: String?
    var _default: Bool?
    var owner: Owner?
    var createdAt: String?
    var visibility: String?
    var products: [ItemClass]?
    
    init(id: Int?, name: String?, description: String?, _default: Bool?, owner: Owner?, createdAt: String?, visibility: String?, products: [ItemClass]?) {
        self.id = id
        self.name = name
        self.description = description
        self._default = _default
        self.owner = owner
        self.createdAt = createdAt
        self.visibility = visibility
        self.products = products
    }
    
    
}


