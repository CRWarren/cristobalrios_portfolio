//
//  ItemClass.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class ItemClass {
    var name: String?
    var id: Int?
    var item_url: String?
    var imageString: String?
    var linioPlusLevel: Int?
    var condition: String?
    var freeShipping: Bool?
    var imported: Bool?
    var active: Bool?
    var wishListPrice: Int?
    var slug: String?
    var image_: UIImage?
    
    init(name: String?, id: Int?, item_url: String?, imageString: String?, linioPlusLevel: Int?, condition: String?, freeShipping: Bool?, imported: Bool?, active: Bool?, wishListPrice: Int?, slug: String?, image_: UIImage?) {
        self.name = name
        self.id = id
        self.item_url = item_url
        self.imageString = imageString
        self.linioPlusLevel = linioPlusLevel
        self.condition = condition
        self.freeShipping = freeShipping
        self.imported = imported
        self.active = active
        self.wishListPrice = wishListPrice
        self.slug = slug
        self.image_ = image_
    }
    
    
    
    
}
