//
//  DownloadHelper.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class DataViewModel{
    
    let urlApi = "https://gist.githubusercontent.com/egteja/98ad43f47d40b0868d8a954385b5f83a/raw/5c00958f81f81d6ba0bb1b1469c905270e8cdfed/wishlist.json"
    
    // Type Alias that our completition will return.
    typealias DownloadDataCompletion = ([Collection]) -> Void
    
    // Download Data pending on Completition.
    func DownloadData(completion: @escaping DownloadDataCompletion) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        //this time we dont need to create a request url, so just pass the url.
        if let validURL = URL(string: urlApi) {
            
                    
                    //create task
                    let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                        
                        //check for error
                        if let error = error {
                            print("data task failed \nerror: \(error.localizedDescription)")
                            return
                        }
                        
                        //check the response status
                        guard  let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let validData = data
                            else{return}
               
                //Do UI Stuff
                DispatchQueue.main.async {
                    print("here ")
                    
                    //this will get our data and send it to our View after completition.
                    completion(self.GetData(from: validData))
                }
                
            })
            task.resume()
        }
    
    }
    
    //parse our JSON data to usable format
    func GetData(from data: Data) -> [Collection] {
        //Array to store our data so we can return it.
        var collections = [Collection]()
        do{
           //change it to a Outer Object
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: Any]]{
                for jsonObj in json{
                    
                    //Temporary variable to hold our products, so we can add the to the collection
                    var temp = [ItemClass]()
                    guard let id = jsonObj["id"] as? Int else { return collections}
                    guard let name = jsonObj["name"] as? String else { return collections}
                    guard let description = jsonObj["description"] as? String else { return collections}
                    guard let _default = jsonObj["default"] as? Bool else { return collections}
                    guard let createdAt = jsonObj["createdAt"] as? String else { return collections}
                    guard let visibility = jsonObj["visibility"] as? String else { return collections}
                    guard let owner = jsonObj["owner"] as? [String: Any] else { return collections}
                    guard let o_name = owner["name"] as? String else { return collections}
                    guard let o_email = owner["email"] as? String else { return collections}
                    guard let o_linioId = owner["linioId"] as? String else { return collections}
                    
                    //get the producs
                    if let products = jsonObj["products"] as? [String: Any]{
                        //since we "Don't Know" the keys we need to get each object key
                        for key in products.keys{
                            
                            //now get each product.
                            if let product = products[key] as? [String: Any]{
                                guard let id = product["id"] as? Int else { return collections}
                                guard let name = product["name"] as? String else { return collections}
                                guard let wishListPrice = product["wishListPrice"] as? Int else { return collections}
                                guard let slug = product["slug"] as? String else { return collections}
                                guard let url = product["url"] as? String else { return collections}
                                guard let image = product["image"] as? String else { return collections}
                                guard let linioPlusLevel = product["linioPlusLevel"] as? Int else { return collections}
                                guard let conditionType = product["conditionType"] as? String else { return collections}
                                guard let freeShipping = product["freeShipping"] as? Bool else { return collections}
                                guard let imported = product["imported"] as? Bool else { return collections}
                                guard let active = product["active"] as? Bool else { return collections}
                                
                                //download the image and store it in the object
                                do {
                                    let _url = URL(string: image)
                                    let data = try Data(contentsOf: _url!)
                                    temp.append(ItemClass(name: name, id: id, item_url: url, imageString: image, linioPlusLevel: linioPlusLevel, condition: conditionType, freeShipping: freeShipping, imported: imported, active: active, wishListPrice: wishListPrice, slug: slug, image_:UIImage(data: data)))
                                }
                                catch{
                                    print(error)
                                }
                                
                            }
                        }
                        
                    }else{
                        return collections
                    }
                    //create the "new" collection and add it to the list.
                    collections.append(Collection(id: id, name: name, description: description, _default: _default, owner: Owner(name: o_name, email: o_email, linioId: o_linioId), createdAt: createdAt, visibility: visibility, products: temp))
                    
                }
            }else{
                
                return collections
            }
        }
         catch{print(error.localizedDescription)}
        
        return collections
    }
    
    
    
}
