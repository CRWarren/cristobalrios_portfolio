//
//  SectionViewCell.swift
//  RiosCristobal_Linio
//
//  Created by cristobal rios on 1/31/19.
//  Copyright © 2019 cristobal rios. All rights reserved.
//

import Foundation
import UIKit

class SectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var support1: UIImageView!
    @IBOutlet weak var support2: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var Count: UILabel!
    
}
